#include "BufferOperations.h"
#include <stack>

#include "Segmentation/ScanLineFill.h"

namespace lssplib
{
namespace polygonization
{

//! @brief 穴埋め処理時に、オブジェクト(肝臓など)の外側のボクセルに割り当てられる値
const unsigned char kMaskValueOutside = 1;

//! @brief 穴埋め処理時に、穴の部分のボクセルに割り当てられる値
const unsigned char kMaskValueInHole = 2;

//!< 書き込まれているマスクのそれぞれのボクセルが所持する値
const unsigned int kDefaultValueOfBuffer = 255;

//! @brief 値の異なるボクセル数。二つのマスクの中に、値の異なるボクセルが幾つあるのかを数え、この数を超えた場合には、一つのマスクが他方に含まれないと判断する。
const unsigned int kThresholdOfNumVoxels = 20;

BufferOperations::BufferOperations()
{

}

BufferOperations::~BufferOperations()
{

}

//! @brief 外側に曝されているマスクと、そうでないマスク(囲まれているマスク)を分類する。
//! 
//! @param[in] baseBuffer 判断材料となるマスク。このマスクに含まれるかそうでないかにより分類される。
//! @param[in] bufferDim バッファの大きさ(幅、高さ、深さ)
//! @param[in] testedBuffers 分類されるマスク
//! @param[out] exposedBuffer 外側に曝されているマスク
//! @param[out] nonExposedBuffer 囲まれているマスク
void BufferOperations::ClassifyMassesIntoExposedAndNonExposed(
	const unsigned char* baseBuffer,
	const Size3i& bufferDim,
	const std::vector<unsigned char*>& testedBuffers,
	std::vector<unsigned char*>& exposedBuffer,
	std::vector<unsigned char*>& nonExposedBuffer)
{
	for (size_t n=0; n<testedBuffers.size(); ++n)
	{
		bool isThereExclusiveElement = IsOneBufferIncludedInOtherBuffer(testedBuffers[n], baseBuffer, bufferDim);
		if (isThereExclusiveElement == true)
		{
			nonExposedBuffer.push_back(testedBuffers[n]);
		}
		else
		{
			exposedBuffer.push_back(testedBuffers[n]);
		}
	}
}

//! @brief 一つのマスクが他方のマスクに含まれているかどうかを判定する。
//! 
//! @param[in] bufOne 一つのマスク
//! @param[in] bufOther 他方のマスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
//! return 含まれているかどうかを表すフラグ
bool BufferOperations::IsOneBufferIncludedInOtherBuffer(const unsigned char* bufOne, const unsigned char* bufOther, const Size3i& bufferDim)
{
	int width = bufferDim.width();
	int height = bufferDim.height();
	int depth = bufferDim.depth();
	int bufSize = width * height * depth;

	unsigned int numVoxelsWithDifferentContent = 0;
	for (int n=0; n<bufSize; ++n)
	{
		unsigned int bufferContent = bufOne[n];
		if (bufferContent == kDefaultValueOfBuffer)
		{
			if (bufOther[n] != kDefaultValueOfBuffer)
			{
				++numVoxelsWithDifferentContent;
				if (numVoxelsWithDifferentContent > kThresholdOfNumVoxels)
				{
					return false;
				}
			}
		}
	}
	return true;
}

//! @brief 一つのマスクに離れ離れな領域があるときに、それぞれを一つずつ配列に収納する。
//!
//! @param[in] inputBuffer マスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param[in, out] massOfBuffers 連結マスクを保持する配列
void BufferOperations::DivideBufferIntoMasses(const unsigned char* inputBuffer, const Size3i& bufferDim, std::vector<unsigned char*>& massOfBuffers)
{
	int width = bufferDim.width();
	int height = bufferDim.height();
	int depth = bufferDim.depth();
	int bufSize = width * height * depth;

	for (int n=0; n<bufSize; ++n)
	{
		unsigned int bufferContent = inputBuffer[n];
		if (bufferContent == kDefaultValueOfBuffer)
		{
			if(IsThereAnyBufferThatTakesTrueAtTheIndex(n, massOfBuffers)==false)
			{
				CreateAMassOfBuffer(n, inputBuffer, bufferDim, massOfBuffers);
			}
		}
	}
}

//! @brief マスクの境界を取得する。
//!
//! @param[in] buffer マスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param[out] objectBoundMin 境界の角の位置
//! @param[out] objectBoundMax 境界の角の位置(対角位置)
//! @return 境界の取得に成功したかどうかを表すフラグ
bool BufferOperations::ObtainBoundingBoxOfObject(const unsigned char* buffer, const Size3i& bufferDim, Point3i& objectBoundMin, Point3i& objectBoundMax)
{
	objectBoundMin = Point3i(INT_MAX, INT_MAX, INT_MAX);
	objectBoundMax = Point3i(INT_MIN, INT_MIN, INT_MIN);

	int width = bufferDim.width();
	int height = bufferDim.height();
	int depth = bufferDim.depth();

	for (int i=0; i<width; ++i)
	{
		for (int j=0; j<height; ++j)
		{
			for (int k=0; k<depth; ++k)
			{
				int voxelIndex = k * width * height + j * width + i;
				if (buffer[voxelIndex] > 0)
				{
					if(objectBoundMin.x() > i) // min X
					{
						objectBoundMin.x() = i;
					}
					if(objectBoundMax.x() < i) // max X
					{
						objectBoundMax.x() = i;
					}

					if(objectBoundMin.y() > j) // min Y
					{
						objectBoundMin.y() = j;
					}
					if(objectBoundMax.y() < j) // max Y
					{
						objectBoundMax.y() = j;
					}

					if(objectBoundMin.z() > k) // min Z
					{
						objectBoundMin.z() = k;
					}
					if(objectBoundMax.z() < k) // max Z
					{
						objectBoundMax.z() = k;
					}
				}
			}
		}
	}

	if(objectBoundMin.x() == INT_MAX) return false;  // if bounding box remains in the initial value, return false 

	if(objectBoundMin.x()>=2)
	{
		objectBoundMin.x() = objectBoundMin.x()-2;
	}

	if(objectBoundMax.x()<width-2)
	{
		objectBoundMax.x() = objectBoundMax.x()+2;
	}

	if(objectBoundMin.y()>=2)
	{
		objectBoundMin.y() = objectBoundMin.y()-2;
	}

	if(objectBoundMax.y()<height-2)
	{
		objectBoundMax.y() = objectBoundMax.y()+2;
	}

	if(objectBoundMin.z()>=2)
	{
		objectBoundMin.z() = objectBoundMin.z()-2;
	}

	if(objectBoundMax.z()<depth-2)
	{
		objectBoundMax.z() = objectBoundMax.z()+2;
	}

	return true;
}

//! @brief マスクを3方向から穴埋めする。
//!
//! @param[in, out] buffer マスク
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::FillBufferHoles(unsigned char* buffer, const Size3i& bufferDim)
{
	Point3i objectBoundMin, objectBoundMax;
	if(!ObtainBoundingBoxOfObject(buffer, bufferDim, objectBoundMin, objectBoundMax)) return;

	// create sub buffer
	Size3i subBufferDim = objectBoundMax - objectBoundMin + Point3i::Ones();
	unsigned char* subBuffer = new unsigned char[subBufferDim.prod()];
	for (int k = 0; k < bufferDim.depth(); ++k)
	{
		for (int j = 0; j < bufferDim.height(); ++j)
		{
			for (int i = 0; i < bufferDim.width(); ++i)
			{
				if ((i >= objectBoundMin[0] && i <= objectBoundMax[0]) &&
					(j >= objectBoundMin[1] && j <= objectBoundMax[1]) &&
					(k >= objectBoundMin[2] && k <= objectBoundMax[2]) )
				{
					int idx = i + (j + k * bufferDim[1]) * bufferDim[0];
					int subIdx = (i - objectBoundMin[0]) + ( (j - objectBoundMin[1]) + (k - objectBoundMin[2]) * subBufferDim[1] ) * subBufferDim[0];
					subBuffer[subIdx] = buffer[idx];
				}
			}
		}
	}

	// fill holes
	lssplib::segmentation::ScanLineFill filler;
	for (int idx = 0; idx < subBufferDim.width(); ++idx)
	{
		if (buffer[idx] == 0)
		{
			// fill exterior region of the mask
			filler.Fill(Point3i(idx, 0, 0), 0, kMaskValueOutside, subBufferDim, subBuffer);
		}
	}

	// invert exterior and interior region
	for (int i = 0; i < subBufferDim.prod(); ++i) {
		if (subBuffer[i] == 1) subBuffer[i] = 0;
		else subBuffer[i] = kDefaultValueOfBuffer;
	}

	// put subbuffer back into buffer
	for (int k = 0; k < subBufferDim.depth(); ++k)
	{
		for (int j = 0; j < subBufferDim.height(); ++j)
		{
			for (int i = 0; i < subBufferDim.width(); ++i)
			{
				int subIdx = i + (j + k * subBufferDim[1]) * subBufferDim[0];
				int idx = (i + objectBoundMin[0]) + ( (j + objectBoundMin[1]) + (k + objectBoundMin[2]) * bufferDim[1] ) * bufferDim[0];
				buffer[idx] = subBuffer[subIdx];
			}
		}
	}

	delete [] subBuffer;

	return;
}

//! @brief 二つのマスクがあるとき、一つのマスクを他方のマスクから差し引く。
//! 
//! @param[in] bufferOne 一つのマスク
//! @param[in, out] bufferOther 他方のマスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::SubtractOneBufferFromOtherBuffer(const unsigned char* bufferOne, unsigned char* bufferOther, const Size3i& bufferDim)
{
	int width = bufferDim.width();
	int height = bufferDim.height();
	int depth = bufferDim.depth();

	int bufferSize = width * height * depth;

	unsigned char* copyOfOther = new unsigned char[bufferSize];

	for (int i=0; i<bufferSize; ++i)
	{
		copyOfOther[i] = bufferOther[i];
	}

	for (int i=0; i<bufferSize; ++i)
	{
		bufferOther[i] = 0;
	}

	for (int n=0; n<bufferSize; ++n)
	{
		unsigned char bufferContentOfOne = bufferOne[n];
		unsigned char bufferContentOfOther = copyOfOther[n];
		if ( (bufferContentOfOne != kDefaultValueOfBuffer) && (bufferContentOfOther == kDefaultValueOfBuffer) )
		{
			bufferOther[n] = kDefaultValueOfBuffer;
		}
	}
	delete [] copyOfOther;
}

//! @brief マスクをリセットする。
//!
//! @param[in,out] buffer マスク
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::ResetToInputBuffer(unsigned char*& buffer, const Size3i& bufferDim)
{
	int width = bufferDim.width();
	int height = bufferDim.height();
	int depth = bufferDim.depth();

	int numVoxels = width * height * depth;

	for (int n=0; n<numVoxels; ++n)
	{
		if(buffer[n]==kMaskValueOutside || buffer[n]==kMaskValueInHole)
		{
			buffer[n] = 0;
		}
	}
}

//! @brief 26-近傍を取得する。
//!
//! @param[in] voxelIndex ボクセルのインデックス
//! @param[in, out] twentySixNeighbours 26-近傍
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::ComputeIndicesOf26Neighbours(const int voxelIndex, std::vector<int>& twentySixNeighbours, const Size3i& bufferDim)
{
	twentySixNeighbours.reserve(26);

	int width = bufferDim.width();
	int height = bufferDim.height();
	int Count = bufferDim.depth();

	int cellNumberOnPlane = width * height;
	int indexInSlice = (voxelIndex%(cellNumberOnPlane));

	int i_x = (indexInSlice%width);
	int i_y = static_cast<int> ( (indexInSlice/width) );
	int i_z = static_cast<int> (voxelIndex/(cellNumberOnPlane));

	int index = voxelIndex+1;
	if(i_x!=width-1) twentySixNeighbours.push_back(index);

	index = voxelIndex - 1;
	if(i_x!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex + height;
	if(i_y!=height-1) twentySixNeighbours.push_back(index);

	index = voxelIndex - height;
	if(i_y!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex + cellNumberOnPlane;
	if(i_z!=Count-1) twentySixNeighbours.push_back(index);

	index = voxelIndex - cellNumberOnPlane;
	if(i_z!=0) twentySixNeighbours.push_back(index);   // 6-neighbors


	index = voxelIndex + cellNumberOnPlane +1;
	if(i_x!=width-1 && i_z!=Count-1) twentySixNeighbours.push_back(index);

	index = voxelIndex + cellNumberOnPlane -1;
	if(i_x!=0 && i_z!=Count-1) twentySixNeighbours.push_back(index);

	index = voxelIndex + cellNumberOnPlane + height;
	if(i_y!=height-1 && i_z!=Count-1) twentySixNeighbours.push_back(index);

	index = voxelIndex + cellNumberOnPlane - height;
	if(i_y!=0 && i_z!=Count-1) twentySixNeighbours.push_back(index);

	index = voxelIndex + height + 1;
	if(i_x!=width-1 && i_y!=height-1) twentySixNeighbours.push_back(index);

	index = voxelIndex + height - 1;
	if(i_x!=0 && i_y!=height-1) twentySixNeighbours.push_back(index);

	index = voxelIndex - height + 1;
	if(i_x!=width-1 && i_y!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex - height - 1;
	if(i_x!=0 && i_y!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex - cellNumberOnPlane +1;
	if(i_x!=width-1 && i_z!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex - cellNumberOnPlane -1;
	if(i_x!=0 && i_z!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex - cellNumberOnPlane + height;
	if(i_y!=height-1 && i_z!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex - cellNumberOnPlane - height;
	if(i_y!=0 && i_z!=0) twentySixNeighbours.push_back(index); // 18-neighbours


	index = voxelIndex + cellNumberOnPlane + height +1;
	if(i_x!=width-1 && i_y!=height-1 && i_z!=Count-1) twentySixNeighbours.push_back(index);

	index = voxelIndex + cellNumberOnPlane + height -1;
	if(i_x!=0 && i_y!=height-1 && i_z!=Count-1) twentySixNeighbours.push_back(index);

	index = voxelIndex + cellNumberOnPlane - height +1;
	if(i_x!=width-1 && i_y!=0 && i_z!=Count-1)  twentySixNeighbours.push_back(index);

	index = voxelIndex + cellNumberOnPlane - height -1;
	if(i_x!=0 && i_y!=0 && i_z!=Count-1) twentySixNeighbours.push_back(index);

	index = voxelIndex - cellNumberOnPlane + height +1;
	if(i_x!=width-1 && i_y!=height-1 && i_z!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex - cellNumberOnPlane + height -1;
	if(i_x!=0 && i_y!=height-1 && i_z!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex - cellNumberOnPlane - height +1;
	if(i_x!=width-1 && i_y!=0 && i_z!=0) twentySixNeighbours.push_back(index);

	index = voxelIndex - cellNumberOnPlane - height -1;
	if(i_x!=0 && i_y!=0 && i_z!=0) twentySixNeighbours.push_back(index); // 26-neighbours
}

//! @brief 与えられたボクセルを含むXY平面上で、4-近傍を取得する。
//!
//! @param[in] voxelIndex ボクセルのインデックス
//! @param[in, out] fourNeighbours 4-近傍
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::ComputeIndicesOf4NeighboursInXY(const int voxelIndex, std::vector<int>& fourNeighbours, const Size3i& bufferDim)
{
	int width = bufferDim.width();
	int height = bufferDim.height();

	int numPixelsInSlice = width * height;

	int indexInSlice = (voxelIndex%(numPixelsInSlice));
	int i_x = (indexInSlice%width);
	int i_y = static_cast<int> ( (indexInSlice/width) );

	if (i_x == 0) // 最左列
	{
		fourNeighbours.push_back(voxelIndex + 1);

		if (i_y == 0) // 左上 i_y==0
		{
			fourNeighbours.push_back(voxelIndex + width);
			return;
		}
		if (i_y == height-1) // 左下
		{
			fourNeighbours.push_back(voxelIndex - width);
			return;
		}

		fourNeighbours.push_back(voxelIndex + width);
		fourNeighbours.push_back(voxelIndex - width);
		return;
	}

	if (i_x == width-1) // 最右列
	{
		fourNeighbours.push_back(voxelIndex - 1);

		if (i_y == 0) // 右上
		{
			fourNeighbours.push_back(voxelIndex + width);
			return;
		}
		if (i_y == height-1) // 右下
		{
			fourNeighbours.push_back(voxelIndex - width);
			return;
		}

		fourNeighbours.push_back(voxelIndex + width);
		fourNeighbours.push_back(voxelIndex - width);
		return;
	}

	if (i_y == 0) //最上列
	{
		fourNeighbours.push_back(voxelIndex +1);
		fourNeighbours.push_back(voxelIndex -1);
		fourNeighbours.push_back(voxelIndex + width);
		return;
	}

	if (i_y == height-1) //最下列
	{
		fourNeighbours.push_back(voxelIndex +1);
		fourNeighbours.push_back(voxelIndex -1);
		fourNeighbours.push_back(voxelIndex - width);
		return;
	}

	fourNeighbours.push_back(voxelIndex + 1);
	fourNeighbours.push_back(voxelIndex - 1);
	fourNeighbours.push_back(voxelIndex + width);
	fourNeighbours.push_back(voxelIndex - width);
}

//! @brief 与えられたボクセルを含むYZ平面上で、4-近傍を取得する。
//!
//! @param[in] voxelIndex ボクセルのインデックス
//! @param[in, out] fourNeighbours 4-近傍
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::ComputeIndicesOf4NeighboursInYZ(const int voxelIndex, std::vector<int>& fourNeighbours, const Size3i& bufferDim)
{
	int width = bufferDim.width();
	int height = bufferDim.height();
	int depth = bufferDim.depth();

	int numPixelsInSlice = width * height;

	int indexInSlice = (voxelIndex%(numPixelsInSlice));
	int i_z = static_cast<int> (voxelIndex/(numPixelsInSlice));
	int i_y = static_cast<int> ( (indexInSlice/width) );

	if (i_y == 0) // 最左列
	{
		fourNeighbours.push_back(voxelIndex + height);

		if (i_z == 0) // 左上
		{
			fourNeighbours.push_back(voxelIndex + numPixelsInSlice);
			return;
		}
		if (i_z == depth-1) // 左下
		{
			fourNeighbours.push_back(voxelIndex - numPixelsInSlice);
			return;
		}

		fourNeighbours.push_back(voxelIndex + numPixelsInSlice);
		fourNeighbours.push_back(voxelIndex - numPixelsInSlice);
		return;
	}

	if (i_y == height-1) // 最右列
	{
		fourNeighbours.push_back(voxelIndex - height);

		if (i_z == 0) // 右上
		{
			fourNeighbours.push_back(voxelIndex + numPixelsInSlice);
			return;
		}
		if (i_z == (depth-1)) // 右下
		{
			fourNeighbours.push_back(voxelIndex - numPixelsInSlice);
			return;
		}

		fourNeighbours.push_back(voxelIndex + numPixelsInSlice);
		fourNeighbours.push_back(voxelIndex - numPixelsInSlice);
		return;
	}

	if (i_z==0) //最上列
	{
		fourNeighbours.push_back(voxelIndex + height);
		fourNeighbours.push_back(voxelIndex - height);
		fourNeighbours.push_back(voxelIndex + numPixelsInSlice);
		return;
	}

	if (i_z==depth-1) //最下列
	{
		fourNeighbours.push_back(voxelIndex + height);
		fourNeighbours.push_back(voxelIndex - height);
		fourNeighbours.push_back(voxelIndex - numPixelsInSlice);
		return;
	}

	fourNeighbours.push_back(voxelIndex + height);
	fourNeighbours.push_back(voxelIndex - height);
	fourNeighbours.push_back(voxelIndex + numPixelsInSlice);
	fourNeighbours.push_back(voxelIndex - numPixelsInSlice);
}

//! @brief 与えられたボクセルを含むZX平面上で、4-近傍を取得する。
//!
//! @param[in] voxelIndex ボクセルのインデックス
//! @param[in, out] fourNeighbours 4-近傍
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::ComputeIndicesOf4NeighboursInZX(const int voxelIndex, std::vector<int>& fourNeighbours, const Size3i& bufferDim)
{
	int width = bufferDim.width();
	int height = bufferDim.height();
	int depth = bufferDim.depth();

	int numPixelsInSlice = width * height;

	int indexInSlice = (voxelIndex%(numPixelsInSlice));
	int i_z = static_cast<int> (voxelIndex/(numPixelsInSlice));
	int i_x = (indexInSlice%width);

	if(i_x!=width-1)
	{
		fourNeighbours.push_back(voxelIndex + 1);
	}

	if(i_x!=0)
	{
		fourNeighbours.push_back(voxelIndex - 1);
	}

	if(i_z!=depth-1)
	{
		fourNeighbours.push_back(voxelIndex + numPixelsInSlice);
	}

	if(i_z!=0)
	{
		fourNeighbours.push_back(voxelIndex - numPixelsInSlice);
	}
}

//! @brief マスクを平面分割したときに現れるマスク内部の穴を埋める。YZ 平面で分割した場合。
//!
//! @param[in] sliceIndex スライスのインデックス
//! @param[in,out] buffer マスク
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param [in] objectBoundMin 矩形の角の位置(座標値が最小)
//! @param [in] objectBoundMax 矩形の角の位置(座標値が最大。objectBoundMinの対角位置)
void BufferOperations::FillHoleInXYPlane(const int sliceIndex, unsigned char* buffer, const Size3i& bufferDim, const Point3i& objectBoundMin, const Point3i& objectBoundMax)
{
	FillOutsideBufferInXYPlane(sliceIndex, buffer, bufferDim);

	int width = bufferDim.width();
	int height = bufferDim.height();

	std::vector<int> indicesInHole;
	std::vector<int> fourNeighbours;
	for (int j=objectBoundMin.y(); j<objectBoundMax.y(); ++j)
	{
		for (int i=objectBoundMin.x(); i<objectBoundMax.x(); ++i)
		{
			int index = sliceIndex*width*height + j*width + i;

			if (buffer[index] == 0)
			{
				std::stack<int> stack;
				buffer[index] = kMaskValueInHole;
				stack.push(index);

				while (stack.empty()==false)
				{
					int currentIndex = stack.top();
					indicesInHole.push_back(currentIndex);
					stack.pop();

					ComputeIndicesOf4NeighboursInXY(currentIndex, fourNeighbours, bufferDim);
					for (size_t i=0; i<fourNeighbours.size(); ++i)
					{
						int neighbor = fourNeighbours[i];
						if (buffer[neighbor] == 0)
						{
							stack.push(neighbor);
							buffer[neighbor] = kMaskValueInHole;
						}
					}
					fourNeighbours.clear();
				}

				for (size_t i=0; i<indicesInHole.size(); ++i)
				{
					int indexInHole = indicesInHole[i];
					buffer[indexInHole] = kDefaultValueOfBuffer;
				}
				indicesInHole.clear();
			}
		}
	}
}

//! @brief マスクを平面分割したときに現れるマスク内部の穴を埋める。YZ 平面で分割した場合。
//!
//! @param[in] sliceIndex_X スライスのインデックス
//! @param[in,out] buffer マスク
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param [in] objectBoundMin  矩形の角の位置(座標値が最小)
//! @param [in] objectBoundMax 矩形の角の位置(座標値が最大。objectBoundMinの対角位置)
void BufferOperations::FillHoleInYZPlane(const int sliceIndex_X, unsigned char* buffer, const Size3i& bufferDim, const Point3i& objectBoundMin, const Point3i& objectBoundMax)
{
	int width = bufferDim.width();
	int height = bufferDim.height();

	FillOutsideBufferInYZPlane(sliceIndex_X, buffer, bufferDim);

	std::vector<int> indicesInHole;
	std::vector<int> fourNeighbours;
	for (int k = objectBoundMin.z(); k<objectBoundMax.z(); ++k)
	{
		for (int j = objectBoundMin.y(); j< objectBoundMax.y(); ++j)
		{
			int index = k*width*height + j*width + sliceIndex_X;

			if (buffer[index] == 0)
			{
				std::stack<int> stack;
				buffer[index] = kMaskValueInHole;
				stack.push(index);

				while (stack.empty()==false)
				{
					int currentIndex = stack.top();
					indicesInHole.push_back(currentIndex);
					stack.pop();

					ComputeIndicesOf4NeighboursInYZ(currentIndex, fourNeighbours, bufferDim);
					for (size_t i=0; i<fourNeighbours.size(); ++i)
					{
						int neighbor = fourNeighbours[i];
						if (buffer[neighbor] == 0)
						{
							stack.push(neighbor);
							buffer[neighbor] = kMaskValueInHole;
						}
					}
					fourNeighbours.clear();
				}

				for (size_t i=0; i<indicesInHole.size(); ++i)
				{
					int indexInHole = indicesInHole[i];
					buffer[indexInHole] = kDefaultValueOfBuffer;
				}
				indicesInHole.clear();
			}
		}
	}
}

//! @brief マスクを平面分割したときに現れるマスク内部の穴を埋める。ZX 平面で分割した場合。
//!
//! @param[in] sliceIndex_Y スライスのインデックス
//! @param[in,out] buffer マスク
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param [in] objectBoundMin 矩形の角の位置(座標値が最小)
//! @param [in] objectBoundMax 矩形の角の位置(座標値が最大。objectBoundMinの対角位置)
void BufferOperations::FillHoleInZXPlane(const int sliceIndex_Y, unsigned char* buffer, const Size3i& bufferDim, const Point3i& objectBoundMin, const Point3i& objectBoundMax)
{
	int width = bufferDim.width();
	int height = bufferDim.height();

	FillOutsideBufferInZXPlane(sliceIndex_Y, buffer, bufferDim);

	for (int k=objectBoundMin.z(); k<objectBoundMax.z(); ++k)
	{
		for (int i=objectBoundMin.x(); i<objectBoundMax.x(); ++i)
		{
			int index = k * width * height + sliceIndex_Y * width + i;
			if (buffer[index] == 0)
			{
				std::stack<int> stack;
				buffer[index] = kMaskValueInHole;
				stack.push(index);

				std::vector<int> indicesInHole;
				while (stack.empty()==false)
				{
					int currentIndex = stack.top();
					indicesInHole.push_back(currentIndex);
					stack.pop();

					std::vector<int> fourNeighbours;
					ComputeIndicesOf4NeighboursInZX(currentIndex, fourNeighbours, bufferDim);
					for (size_t i=0; i<fourNeighbours.size(); ++i)
					{
						int neighbor = fourNeighbours[i];
						if (buffer[neighbor] == 0)
						{
							stack.push(neighbor);
							buffer[neighbor] = kMaskValueInHole;
						}
					}
				}

				for (size_t i=0; i<indicesInHole.size(); ++i)
				{
					int indexInHole = indicesInHole[i];
					buffer[indexInHole] = kDefaultValueOfBuffer;
				}
			}
		}
	}
}

//! @brief 与えられた二次元スライス (XY 平面と平行) 上にあり、なおかつマスクの外側にある全てのボクセルに値を付与する。
//!
//! @param[in] sliceIndex_Z スライスのインデックス
//! @param[in, out] buffer マスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::FillOutsideBufferInXYPlane(const int sliceIndex_Z, unsigned char* buffer, const Size3i& bufferDim)
{
	std::stack<int> stack;

	int startIndex = sliceIndex_Z;
	const bool isStartIndexFound = FindStartIndex_XY(sliceIndex_Z, buffer, bufferDim, startIndex);
	if (isStartIndexFound==false)
	{
		return;
	}
	stack.push(startIndex);

	std::vector<int> fourNeighbors;
	while (stack.empty()==false)
	{
		int currentIndex = stack.top();
		buffer[currentIndex] = kMaskValueOutside;
		stack.pop();

		ComputeIndicesOf4NeighboursInXY(currentIndex, fourNeighbors, bufferDim);
		for (size_t i=0; i<fourNeighbors.size(); ++i)
		{
			int neighbor = fourNeighbors[i];
			if (buffer[neighbor] == 0)
			{
				buffer[neighbor] = kMaskValueOutside;
				stack.push(neighbor);
			}
		}
		fourNeighbors.clear();
	}
}

//! @brief 与えられた二次元スライス (YZ 平面と平行) 上にあり、なおかつマスクの外側にある全てのボクセルに値を付与する。
//!
//! @param[in] sliceIndex_X スライスのインデックス
//! @param[in, out] buffer マスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::FillOutsideBufferInYZPlane(const int sliceIndex_X, unsigned char* buffer, const Size3i& bufferDim)
{
	int startIndex = sliceIndex_X;
	const bool isStartIndexFound = FindStartIndex_YZ(sliceIndex_X, buffer, bufferDim, startIndex);
	if(isStartIndexFound==false)
	{
		return;
	}

	std::stack<int> stack;
	stack.push(startIndex);

	std::vector<int> fourNeighbors;
	while (stack.empty()==false)
	{
		int currentIndex = stack.top();
		stack.pop();

		ComputeIndicesOf4NeighboursInYZ(currentIndex, fourNeighbors, bufferDim);
		for (size_t i=0; i<fourNeighbors.size(); ++i)
		{
			int neighbor = fourNeighbors[i];
			if (buffer[neighbor] == 0)
			{
				buffer[neighbor] = kMaskValueOutside;
				stack.push(neighbor);
			}
		}
		fourNeighbors.clear();
	}
}

//! @brief 与えられた二次元スライス (ZX 平面と平行) 上にあり、なおかつマスクの外側にある全てのボクセルに値を付与する。
//!
//! @param[in] sliceIndex_Y スライスのインデックス
//! @param[in,out] buffer マスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
void BufferOperations::FillOutsideBufferInZXPlane(const int sliceIndex_Y, unsigned char* buffer, const Size3i& bufferDim)
{
	int startIndex = sliceIndex_Y;
	const bool isStartIndexFound = FindStartIndex_ZX(sliceIndex_Y, buffer, bufferDim, startIndex);
	if (isStartIndexFound == false)
	{
		return;
	}

	std::stack<int> stack;
	stack.push(startIndex);


	std::vector<int> fourNeighbors;
	while (stack.empty()==false)
	{
		int currentIndex = stack.top();
		stack.pop();

		ComputeIndicesOf4NeighboursInZX(currentIndex, fourNeighbors, bufferDim);
		for (size_t i=0; i<fourNeighbors.size(); ++i)
		{
			int neighbor = fourNeighbors[i];
			if (buffer[neighbor] == 0)
			{
				buffer[neighbor] = kMaskValueOutside;
				stack.push(neighbor);
			}
		}
		fourNeighbors.clear();
	}
}

//! @brief 与えられた二次元スライス (XY 平面と平行) の端から、0 の値を持つマスクセルを見つけ、そのインデックスを返す。\n
//! 返されるインデックスは、外側塗りつぶし探索の開始地点を与える。
//!
//! @param[in] sliceIndex_Z スライスのインデックス
//! @param[in] buffer マスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param[out] startIndex 探索開始インデックス
//! @return 0 の値が見つかった場合 true。見つからない場合 false。
const bool BufferOperations::FindStartIndex_XY(const int sliceIndex_Z, unsigned char* buffer, const Size3i& bufferDim, int& startIndex)
{
	int width = bufferDim.width();
	int height = bufferDim.height();

	int index = 0;
	for (int i=0; i<width; ++i)
	{
		index =  sliceIndex_Z*height*width + 0*width + i;
		if (buffer[index]==0)
		{
			startIndex = index;
			return true;
		}
	}
	return false;
}

//! @brief 与えられた二次元スライス (YZ 平面と平行) の端から、0 の値を持つマスクセルを見つけ、そのインデックスを返す。\n
//! 返されるインデックスは、外側塗りつぶし探索の開始地点を与える。
//!
//! @param[in] sliceIndex_X スライスのインデックス
//! @param[in] buffer マスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param[out] startIndex 探索開始インデックス
//! @return 0 の値が見つかった場合 true。見つからない場合 false。
const bool BufferOperations::FindStartIndex_YZ(const int sliceIndex_X, unsigned char* buffer, const Size3i& bufferDim, int& startIndex)
{
	int width = bufferDim.width();
	int height = bufferDim.height();

	int index = 0;
	for (int j=0; j<height; ++j)
	{
		index = 0*height*width + j*width + sliceIndex_X;
		if (buffer[index]==0)
		{
			startIndex = index;
			return true;
		}
	}
	return false;
}

//! @brief 与えられた二次元スライス (ZX 平面と平行) の端から、0 の値を持つマスクセルを見つけ、そのインデックスを返す。\n
//! 返されるインデックスは、外側塗りつぶし探索の開始地点を与える。
//!
//! @param[in] sliceIndex_Y スライスのインデックス
//! @param[in] buffer マスク
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param[out] startIndex 探索開始インデックス
//! @return 0 の値が見つかった場合 true。見つからない場合 false。
const bool BufferOperations::FindStartIndex_ZX(const int sliceIndex_Y, unsigned char* buffer, const Size3i& bufferDim, int& startIndex)
{
	int width = bufferDim.width();
	int height = bufferDim.height();

	int index = 0;
	for (int i=0; i<width; ++i)
	{
		index = 0*height*width + sliceIndex_Y*width + i;
		if (buffer[index]==0)
		{
			startIndex = index;
			return true;
		}
	}
	return false;
}

//! @brief マスクの連結部分を全て取り出し、配列に格納する。
//!
//! @param[in] voxelIndex ボクセルのインデックス
//! @param[in] inputBuffer マスク
//! @param [in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param[in, out] massOfBuffers マスクの配列
void BufferOperations::CreateAMassOfBuffer(const int voxelIndex, const unsigned char* inputBuffer, const Size3i& bufferDim, std::vector<unsigned char*>& massOfBuffers)
{
	int width = bufferDim.width();
	int height = bufferDim.height();
	int depth = bufferDim.depth();
	int bufSize = width*height*depth;

	unsigned char* newBuffer = new unsigned char[width*height*depth];
	for (int i=0; i<bufSize; ++i)
	{
		newBuffer[i] = 0;
	}

	std::stack<int> indexToBeSearched;
	indexToBeSearched.push(voxelIndex);

	while(indexToBeSearched.empty()==false)
	{
		int bufferIndexAtTop = indexToBeSearched.top();

		newBuffer[bufferIndexAtTop] = kDefaultValueOfBuffer;
		indexToBeSearched.pop();

		std::vector<int> twentySixNeighbours;
		ComputeIndicesOf26Neighbours(bufferIndexAtTop, twentySixNeighbours, bufferDim);

		for (size_t j=0; j<twentySixNeighbours.size(); ++j)
		{
			int indexArrayOfTwentySixNeighbours = twentySixNeighbours[j];
			unsigned char bufferContentOfInputAtNeighbor = inputBuffer[indexArrayOfTwentySixNeighbours];
			unsigned char bufferContentOfNewMaskAtNeighbor = newBuffer[indexArrayOfTwentySixNeighbours];
			if (bufferContentOfInputAtNeighbor == kDefaultValueOfBuffer && bufferContentOfNewMaskAtNeighbor != kDefaultValueOfBuffer) 
			{
				indexToBeSearched.push(indexArrayOfTwentySixNeighbours);
			}
		}
	}
	massOfBuffers.push_back(newBuffer);
}

//! @brief シードボクセルと繋がっている領域を全て取り出し、その部分をコピーしたボリュームデータを生成する。
//!
//! @param[in] seed シードインデックス。
//! @param[in] input 入力ボリュームデータ
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param[out] copy 繋がっている領域を表すボリュームデータ
//! @return count ボクセル数
//
int BufferOperations::CopyConnectingVol(
	const int seed, 
	const unsigned char* input, 
	const Size3i& bufferDim, 
	unsigned char*& copy)
{
	int bufSize = bufferDim.width()*bufferDim.height()*bufferDim.depth();

	copy = new unsigned char[bufSize];
	for (int i=0; i<bufSize; ++i) copy[i] = 0; // 初期化

	std::stack<int> stack;
	stack.push(seed);

	int count=0;
	while(stack.empty()==false)
	{
		int top = stack.top();

		copy[top] = kDefaultValueOfBuffer;
		++count;
		stack.pop();

		std::vector<int> neighbours;
		ComputeIndicesOf26Neighbours(top, neighbours, bufferDim);

		for (size_t j=0; j<neighbours.size(); ++j)
		{
			int neighbor = neighbours[j];
			unsigned char inputValue = input[neighbor];
			unsigned char copyValue = copy[neighbor];

			if (inputValue == kDefaultValueOfBuffer && copyValue != kDefaultValueOfBuffer) 
			{
				stack.push(neighbor);
			}
		}
	}
	return count;
}

//! @brief 与えられたインデックスにあるボクセルが 255 という値を持つマスクがあるかどうかを確認する。
//!
//! @param[in] targetIndex インデックス
//! @param[in] buffersToBeTested マスクの配列
//! @return フラグ
bool BufferOperations::IsThereAnyBufferThatTakesTrueAtTheIndex(const int targetIndex, const std::vector<unsigned char*>& buffersToBeTested)
{
	for (size_t i=0; i<buffersToBeTested.size(); ++i)
	{
		if(buffersToBeTested[i]==NULL) continue;

		if (buffersToBeTested[i][targetIndex] == kDefaultValueOfBuffer)
		{
			return true;
		}
	}
	return false;
}

//! @brief 一方のボリュームから他方のボリュームを引く。
//!
//! @param[in] bufferDim バッファサイズ(幅、高さ、深さ)
//! @param[in] one ボリュームデータ
//! @param[in,out] another ボリュームデータ
//
void BufferOperations::SubtractOneFromAnother(const Size3i& bufferDim, const unsigned char* one, unsigned char* another)
{
	int bufSize = bufferDim.width()*bufferDim.height()*bufferDim.depth();

	for (size_t n=0; n<bufSize; ++n)
	{
		if(one[n] == kDefaultValueOfBuffer) // 値が255の領域を0にする。
		{
			another[n] = 0;
		}
	}
}

//! @brief ボリュームを拡大する。
//!
//! @param[in] input 入力ボリューム
//! @param[in] bufferDim 入力ボリュームのバッファサイズ(幅、高さ、深さ)
//! @param[in] expansionN 拡大量。この数の分だけ上下・前後・左右に、空のボクセルが追加されることで、ボリュームが拡大する。
//! @param[in,out] expanded 拡大後のボリューム
//
void BufferOperations::ExpandBuffer(
	const unsigned char* input, 
	const Size3i& bufferDim, 
	int expansionN,
	unsigned char*& expanded)
{
	int width = bufferDim.width();
	int height = bufferDim.height();
	int depth = bufferDim.depth();

	int exN = expansionN + expansionN;

	int exWidth = width + exN;
	int exHeight = height + exN;
	int exDepth = depth + exN;

	int exSize = exWidth * exHeight * exDepth;
	expanded = new unsigned char[exSize];

	for (int i=0; i<exSize; ++i) expanded[i] = 0; // 初期化

	for (int i=0; i<width; ++i)
	{
		for (int j=0; j<height; ++j)
		{
			for (int k=0; k<depth; ++k)
			{
				int id = k*width*height + j*width + i;
				if (input[id]!=0)
				{
					int exId = (k+expansionN)*exWidth*exHeight + (j+expansionN)*exWidth + (i+expansionN);
					expanded[exId] = input[id];
				}
			}
		}
	}
}

} // namespace polygonization
} // namespace lssplib
