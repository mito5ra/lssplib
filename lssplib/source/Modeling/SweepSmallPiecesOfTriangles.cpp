#include "Modeling/SweepSmallPiecesOfTriangles.h"

#include <algorithm>
#include <stack>

namespace lssplib
{
namespace polygonization
{

//! @brief コンストラクタ
//! 
//! @param[in] triangles 三角形の頂点インデックスの集合
//! @param[in] vertexPositions 頂点位置の集合
SweepSmallPiecesOfPolygons::SweepSmallPiecesOfPolygons(std::vector<TriangleVertexIndex>& triangles, 
	std::vector<VertexPosition>& vertexPositions)
:m_trianglesWithConnectivity()
{
	m_triangles = &triangles;
	m_vertices = &vertexPositions;
}

//! @brief デストラクタ
SweepSmallPiecesOfPolygons::~SweepSmallPiecesOfPolygons()
{
}

//! @brief ポリゴンから小片を取り除く。
void SweepSmallPiecesOfPolygons::SweepSmallPieces()
{
	if(m_triangles->empty()==true) return;

	m_trianglesWithConnectivity.MakeTrianglesWithLocalConnectivity(*m_triangles, *m_vertices);

	std::vector<std::vector<size_t>> connectingGroups;
	DivideTrianglesIntoConnectingGroups(connectingGroups);

	size_t indexOfBiggestGroup = FindBiggestGroup(connectingGroups);
	std::vector<size_t> arrangedIndicesOfVertex;
	MakeArrayOfVertexIndexToBeRemoved_ArrangedInAscendingOrder(connectingGroups,indexOfBiggestGroup, arrangedIndicesOfVertex);
	std::vector<size_t> arrangedIndicesOfTriangle;
	MakeArrayOfTriangleIndexToBeRemoved_ArrangedInAscendingOrder(connectingGroups,indexOfBiggestGroup, arrangedIndicesOfTriangle);

	UpdateTriangles(arrangedIndicesOfVertex, arrangedIndicesOfTriangle);

//	output();
}

//! @brief 頂点と三角形の頂点インデックスを除去し、データを更新する。
//! 
//! @param[in] vertexIndexToBeRemoved 取り除かれる頂点のインデックス全体
//! @param[in] triangleIndexToBeRemoved 取り除かれる三角形頂点インデックス全体
void SweepSmallPiecesOfPolygons::UpdateTriangles(const std::vector<size_t>& vertexIndexToBeRemoved,
	const std::vector<size_t>& triangleIndexToBeRemoved)
{
	int numTrianglesToBeRemoved = static_cast<int>(triangleIndexToBeRemoved.size());
	for (int i=numTrianglesToBeRemoved-1; i>=0; --i)
	{
		size_t indexOfTriangleToBeRemoved = triangleIndexToBeRemoved[i];
		m_triangles->erase(m_triangles->begin()+indexOfTriangleToBeRemoved);
	}

	int numVertciesToBeRemoved = static_cast<int>(vertexIndexToBeRemoved.size());
	for (int i=numVertciesToBeRemoved-1; i>=0; --i)
	{
		size_t indexOfVertexToBeRemoved = vertexIndexToBeRemoved[i];
		m_vertices->erase(m_vertices->begin()+indexOfVertexToBeRemoved);
	}

	size_t numTriangles = m_triangles->size();
	for (int i=numVertciesToBeRemoved-1; i>=0; --i)
	{
		size_t vertexToBeRemoved = vertexIndexToBeRemoved[i];
		for (size_t j=0; j<numTriangles; ++j)
		{
			for (size_t k=0; k<3; ++k)
			{
				size_t vertexIndex = (*m_triangles)[j].vertex[k];
				if (vertexIndex > vertexToBeRemoved)
				{
					(*m_triangles)[j].vertex[k] = vertexIndex-1;
				}
			}
		}
	}
}

//! @brief 除去される三角形のインデックスを小さい順に整理して出力する。
//! 
//! @param[in] connectingGroups 連結グループの配列。一般に複数のグループ。
//! @param[in] indexOfBiggestGroup 一番大きな連結グループのインデックス
//! @param[out] arrangedIndices 三角形インデックスの配列。小さい順に整理されている。
void SweepSmallPiecesOfPolygons::MakeArrayOfTriangleIndexToBeRemoved_ArrangedInAscendingOrder(const std::vector<std::vector<size_t>>& connectingGroups,
	const size_t indexOfBiggestGroup, 
	std::vector<size_t> & arrangedIndices) const
{
	for (size_t i=0; i<connectingGroups.size(); ++i)
	{
		if(i!=indexOfBiggestGroup)
		{
			for (size_t j=0; j<connectingGroups[i].size(); ++j)
			{
				size_t triangleIndex = connectingGroups[i][j];
				arrangedIndices.push_back(triangleIndex);
			}
		}
	}

	std::sort(arrangedIndices.begin(), arrangedIndices.end());
}

//! @brief 除去される頂点のインデックスを小さい順に整理して出力する。
//! 
//! @param[in] connectingGroups 結グループの配列。一般に複数のグループ。
//! @param[in] indexOfBiggestGroup 一番大きな連結グループのインデックス
//! @param[out] arrangedIndices 頂点インデックスの配列。小さい順に整理されている。
void SweepSmallPiecesOfPolygons::MakeArrayOfVertexIndexToBeRemoved_ArrangedInAscendingOrder(const std::vector<std::vector<size_t>>& connectingGroups,
	const size_t indexOfBiggestGroup, 
	std::vector<size_t> & arrangedIndices) const
{
	const std::vector<TriangleData>& triangles = m_trianglesWithConnectivity.GetTriangleData();

	for (size_t i=0; i<connectingGroups.size(); ++i)
	{
		if(i!=indexOfBiggestGroup)
		{
			for (size_t j=0; j<connectingGroups[i].size(); ++j)
			{
				size_t triangleIndex = connectingGroups[i][j];
				for (size_t k=0; k<3; ++k)
				{
					size_t vertexIndex = triangles[triangleIndex].GetVertexIndex(k);
					arrangedIndices.push_back(vertexIndex);
				}
			}
		}
	}

	std::sort(arrangedIndices.begin(), arrangedIndices.end());
	std::vector<size_t>::iterator it;
	it = std::unique (arrangedIndices.begin(), arrangedIndices.end());
	arrangedIndices.resize( std::distance(arrangedIndices.begin(),it) );
}

//! @brief 一番大きな連結グループのインデックスを返す。
//! 
//! @param[in] connectingGroups 連結グループの全体
const size_t SweepSmallPiecesOfPolygons::FindBiggestGroup(const std::vector<std::vector<size_t>>& connectingGroups) const
{

	size_t maximumNumberOfTriangles = 0;
	size_t indexOfBiggestGroup = 0;
	for (size_t i=0; i<connectingGroups.size(); ++i)
	{
		size_t numTrinanglesInGroup = connectingGroups[i].size();
		if (maximumNumberOfTriangles < numTrinanglesInGroup)
		{
			maximumNumberOfTriangles = numTrinanglesInGroup;
			indexOfBiggestGroup = i;
		}
	}
	return indexOfBiggestGroup;
}

//! @brief 一つのポリゴンデータを、複数の連結グループに分割する。一つのグループは繋がっている。
//! 
//! @param[out] connectingGroups 連結グループの全体
void SweepSmallPiecesOfPolygons::DivideTrianglesIntoConnectingGroups(std::vector<std::vector<size_t>>& connectingGroups)
{
	size_t startIndex = 0;
	for(;;)
	{
		if(FindStartIndex(startIndex)==false)
		{
			return;
		}
		else
		{
			std::vector<size_t> singleGroup;
			CollectConnectingTriangle(startIndex, singleGroup);
			connectingGroups.push_back(singleGroup);
		}
	}
}

//! @brief 連結された全てのポリゴンをかき集める。
//! 
//! @param[in] startIndex 探索に使われる最初の三角形のインデックス
//! @param[out] connectingTriangle 連結ポリゴンの全体。各ポリゴンのインデックスが保管される。
void SweepSmallPiecesOfPolygons::CollectConnectingTriangle(const size_t startIndex, std::vector<size_t>& connectingTriangle)
{
	std::vector<TriangleData>& triangleData = m_trianglesWithConnectivity.GetTriangleData();
	const std::vector<EdgeData>& edgeData = m_trianglesWithConnectivity.GetEdgeData();

	std::stack<size_t> stack;
	stack.push(startIndex);

	while(stack.empty()==false)
	{
		size_t triangleIndex = stack.top();
		if (triangleData[triangleIndex].HasTheTriangleBeenUsed()==false)
		{
			connectingTriangle.push_back(triangleIndex);
			triangleData[triangleIndex].WriteUsedOrNot(true);
			stack.pop();
			for (size_t i=0; i<3; ++i)
			{
				size_t edgeIndex = triangleData[triangleIndex].GetEdgeIndex(i);
				const std::vector<size_t>& trianglesAroundEdge = edgeData[edgeIndex].GetTriangleAroundEdge();
				for (size_t i=0; i<trianglesAroundEdge.size(); ++i)
				{
					size_t indexOfNeighborTriangle = trianglesAroundEdge[i];
					if (triangleData[indexOfNeighborTriangle].HasTheTriangleBeenUsed()==false)
					{
						stack.push(indexOfNeighborTriangle);
					}
				}
			}
		}
		else
		{
			stack.pop();
		}
	}
}

//! @brief 探索を開始する三角形を見つける。
//! 
//! @param[out] startIndex 探索を開始する三角形のインデックス
const bool SweepSmallPiecesOfPolygons::FindStartIndex(size_t& startIndex) const
{
	const std::vector<TriangleData>& triangles = m_trianglesWithConnectivity.GetTriangleData();
	for (size_t i=0; i<triangles.size(); ++i)
	{
		if(triangles[i].HasTheTriangleBeenUsed()==false)
		{
			startIndex = i;
			return true;
		}
	}
	return false;
}

} // namespace polygonization
} // namespace lssplib

