#include "Modeling/ScalarValueEvaluator.h"
#include <math.h>
#include <float.h>

namespace lssplib
{
namespace polygonization
{

//! @brief セルがバウンディングボックスの外側にある場合に付与されるインデックス。
const int signRepresentingOutOfBoundingBox = -1;

//! @brief ガウス分布の幅を定義する数。isoValue の逆数の対数関数。log32。\n 
//! 線分が無限に長い直線の場合に、線分に与えた半径を持つ円筒状で、スカラー値がゼロになるように調整する数。
const float gaussianWidth = static_cast<float>(5.0*log(2.0));

//! @brief スカラー分布がゼロになる領域を定義する数。
const float isoValue = 0.03125f; //1/32

//! @brief 線分の一端を指定するインデックス
const unsigned int oneEndOfLineSegment = 0;

//! @brief 線分の一端を指定するインデックス
const unsigned int otherEndOfLineSegment = 1;

const Point3i kInvalidCellIndex(-1, -1, -1);

//! @brief コンストラクタ
ScalarValueEvaluator::ScalarValueEvaluator()
: m_lineSegments(NULL)
, m_cellScale(0)
, m_integralLookUpTable()
, m_sourceIndexCells(NULL)
, m_boundingBox(Point3f::Zero(), Size3f::Zero())
{
}

//! @brief デストラクタ
ScalarValueEvaluator::~ScalarValueEvaluator()
{
}

//! @brief セルデータを持つポインタをコピーする。
//! 
//! @param[in] sourceIndexCells セルデータ
void ScalarValueEvaluator::SetCells(SourceIndexHolder**** sourceIndexCells)
{
	m_sourceIndexCells = sourceIndexCells;
}

//! @brief 与えられた座標値上のスカラー値を計算する。\n
//! もとになる線分データは、本来は全ての線分データであるべきだが、計算量を削減するため、与えられた点の近傍の線分データのみが選択されている。
//!　
//! @param[in] contributingLineSegments 線分データ
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @return スカラー値
float ScalarValueEvaluator::ComputeScalarValueFromAllContributingLineSegments(
	const std::vector<unsigned int>& contributingLineSegments, 
	const Point3f& srcPoint) const
{
	float scalarValue = 0.0f;
	for(unsigned int i = 0; i < contributingLineSegments.size(); ++i)
	{
		unsigned int lineSegmentIndex = contributingLineSegments[i];
		const SourceLineSegment& lineSegment = (*m_lineSegments)[lineSegmentIndex];
		scalarValue += ComputeScalarValueFromSingleLineSegment(lineSegment, srcPoint);
	}

	return scalarValue;
}

//! @brief スカラー値を計算において、一つの線分からの寄与を求める。
//! 
//! @param[in] lineSegmentIndex 線分データのインデックス
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @return 一つの線分が付与するスカラー値
float ScalarValueEvaluator::ComputeScalarValueFromSingleLineSegment(
	const SourceLineSegment& lineSegment,
	const Point3f& srcPoint) const
{
	float distanceFilter = ComputeDistanceFilter(lineSegment, srcPoint);

	float scalarValue = distanceFilter * ComputeIntegralFilter(lineSegment, srcPoint);

	return scalarValue;
}

//! @brief Distance Filter に対する一つの線分からの寄与を計算する。\n
//! スカラー値の計算は、Distance Filter と Integral Filter の積によって与えられる。ここでは、Distance Filter 部分の計算を行う。
//! 
//! @param[in] lineSegmentIndex 線分データのインデックス
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @return Distance Filter の値
float ScalarValueEvaluator::ComputeDistanceFilter(
	const SourceLineSegment& lineSegment, 
	const Point3f& srcPoint) const
{
	float lineSegmentRadius = EstimateLineSegmentThicknessAtPoint(lineSegment, srcPoint);

	float distanceFromLineSegment = lineSegment.ComputeDistanceFromPoint(srcPoint);

	const float distanceFilter = static_cast<float> (exp( - gaussianWidth*( (distanceFromLineSegment*distanceFromLineSegment)/(lineSegmentRadius*lineSegmentRadius) ) ) );

	return distanceFilter;
}

//! @brief 与えられた点から、指定された線分を含む直線上への射影を求め、その射影と線分の両端の間の距離を計算し、それを返す。
//! 
//! @param[in] lineSegmentIndex 線分データのインデックス
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @param[in, out] distanceFromProjectionToOneEnd 線分の一端から射影までの距離
//! @param[in, out] distanceFromProjectionToOtherEnd 線分の他端から射影までの距離
void ScalarValueEvaluator::ComputeDistancesFromProjectionToLineSegmentEnd(const SourceLineSegment& lineSegment, 
	const Point3f& srcPoint,
	float& distanceFromProjectionToOneEnd,
	float& distanceFromProjectionToOtherEnd) const
{

	Point3f pointOfProjection = lineSegment.ComputePointOfProjection(srcPoint);

	const Point3f& p0 = lineSegment.GetEndPosition(0);
	const Point3f& p1 = lineSegment.GetEndPosition(1);

	distanceFromProjectionToOneEnd = (pointOfProjection - p0).norm();

	distanceFromProjectionToOtherEnd = (pointOfProjection - p1).norm();
}

//! @brief 与えられた点における線分の太さ(血管の半径に相当)を返す。\n
//! 与えられた点 R から線分を含む直線上への射影 P を求め、射影 P から線分の両端 L1, L2 までの距離 D1, D2 を計算する。\n
//! 線分の両端は太さ T1, T2 を持つので、それらをもとに線形補間を実行し、射影上の太さ PT1 を推定する。その太さ PT1 を点上 R の太さとして、それを返す。
//! 
//! @param[in] lineSegmentIndex 線分データのインデックス
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @return 与えられた点における線分の太さ
float ScalarValueEvaluator::EstimateLineSegmentThicknessAtPoint(
	const SourceLineSegment& lineSegment, 
	const Point3f& srcPoint) const
{
	const float& thicknessAtOneEnd = lineSegment.GetThickness()[0];
	const float& thicknessAtOtherEnd = lineSegment.GetThickness()[1];

	float distanceFromProjectionToOneEnd = 0.0f;
	float distanceFromProjectionToOtherEnd = 0.0f;

	ComputeDistancesFromProjectionToLineSegmentEnd(lineSegment, srcPoint, distanceFromProjectionToOneEnd, distanceFromProjectionToOtherEnd);

	const float& edgeLengh = lineSegment.GetLineSegmentLength();

	float radiusOnPoint = 0.0f;
	if ( (edgeLengh>=distanceFromProjectionToOneEnd) && (edgeLengh>=distanceFromProjectionToOtherEnd) ) // 射影が線分の中
	{
		radiusOnPoint = (thicknessAtOtherEnd * distanceFromProjectionToOneEnd + thicknessAtOneEnd * distanceFromProjectionToOtherEnd)/ edgeLengh;
	}
	else if ( (edgeLengh<=distanceFromProjectionToOneEnd) && (distanceFromProjectionToOtherEnd <= distanceFromProjectionToOneEnd) ) //射影が線分の外(other寄りに)
	{
		radiusOnPoint = thicknessAtOtherEnd;
	}
	else  // 射影が線分の外(one寄りに)
	{
		radiusOnPoint = thicknessAtOneEnd;
	}

	if (radiusOnPoint<1e-5f) // 万が一、半径 0 を返そうとした場合に、0 にしないための処理。
	{
		radiusOnPoint = 1e-5f;
	}

	return radiusOnPoint;
}

//! @brief 点の直線への射影を返す。
//! 
//! @param[in] pointOnLine 直線上の点。直線を定義するための要素。
//! @param[in] lineVector 直線の方向ベクトル。直線を定義するための要素。
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @param[out] projectionOfTargetPointOnLine 直線への射影
void ScalarValueEvaluator::ComputePointProjectionOnLine(
	const Point3f& pointOnLine,
	const Vector3f& lineVector, 
	const Point3f& srcPoint, 
	Point3f& projectionOfTargetPointOnLine) const
{
	float innerProduct = (srcPoint - pointOnLine).dot(lineVector);
	projectionOfTargetPointOnLine = pointOnLine + lineVector * innerProduct;
}

//! @brief Integral Filter に対する一つの線分からの寄与を計算する。\n
//! スカラー値の計算は、Distance Filter と Integral Filter の積によって与えられる。ここでは、Integral Filter 部分の計算を行う。\n
//! ガウス積分は、有限の積分範囲で解く方法が知られていないので、数値的に評価した積分テーブルを使って、積分値を与える。
//! 
//! @param[in] lineSegmentIndex 線分データのインデックス
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @return Integral Filter の値
float ScalarValueEvaluator::ComputeIntegralFilter(const SourceLineSegment& lineSegment, 
	const Point3f& srcPoint) const
{
	float lowerBoundOfIntegralRange = 0.0f;
	float upperBoundOfIntegralRange = 0.0f;
	ComputeIntegralRange(lineSegment, srcPoint, lowerBoundOfIntegralRange, upperBoundOfIntegralRange); // a/r, b/r, を取得する。

	const float thicknessAtThisPoint = EstimateLineSegmentThicknessAtPoint(lineSegment, srcPoint);

	lowerBoundOfIntegralRange /= thicknessAtThisPoint;
	upperBoundOfIntegralRange /= thicknessAtThisPoint;

	const float gaussianIntegralForLowerBound = GetGaussianIntegral(lowerBoundOfIntegralRange);

	const float gaussiangIntegralForUpperBound = GetGaussianIntegral(upperBoundOfIntegralRange);

	float integralFilter = (gaussiangIntegralForUpperBound - gaussianIntegralForLowerBound);

	return integralFilter;
}

//! @brief 積分範囲を与える。
//! 
//! @param[in] lineSegmentIndex 線分データのインデックス
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @param[out] lowerBoundOfIntegralRange 積分の下限
//! @param[out] upperBoundOfIntegralRange 積分の上限
void ScalarValueEvaluator::ComputeIntegralRange(const SourceLineSegment& lineSegment, 
	const Point3f& srcPoint, 
	float& lowerBoundOfIntegralRange, float& upperBoundOfIntegralRange) const
{
	float distanceFromProjectionToOneEnd = 0.0f;
	float distanceFromProjectionToOtherEnd = 0.0f;
	ComputeDistancesFromProjectionToLineSegmentEnd(lineSegment, srcPoint, distanceFromProjectionToOneEnd, distanceFromProjectionToOtherEnd);
	const float& edgeLength = lineSegment.GetLineSegmentLength();

	if ( (edgeLength>=distanceFromProjectionToOneEnd) && (edgeLength>=distanceFromProjectionToOtherEnd) ) // 射影がエッジ上
	{
		lowerBoundOfIntegralRange = - distanceFromProjectionToOneEnd;
		upperBoundOfIntegralRange = distanceFromProjectionToOtherEnd;
	}
	else if ( (edgeLength<=distanceFromProjectionToOneEnd) && (distanceFromProjectionToOtherEnd<=distanceFromProjectionToOneEnd) ) //射影がエッジの外(other寄りに)
	{
		lowerBoundOfIntegralRange = - distanceFromProjectionToOneEnd;
		upperBoundOfIntegralRange = - distanceFromProjectionToOtherEnd;
	}
	else if ( (edgeLength<=distanceFromProjectionToOtherEnd) && (distanceFromProjectionToOneEnd<=distanceFromProjectionToOtherEnd)) // 射影がエッジの外(one寄りに)
	{
		lowerBoundOfIntegralRange = distanceFromProjectionToOneEnd;
		upperBoundOfIntegralRange = distanceFromProjectionToOtherEnd;
	}
}

// {number of interval from -10 to 10 = 20000 starting -10 with index 0 means that the index correspoinding to 0.3 = 10300, and that to -2.3 = 7700}
//! @brief 積分値を返す。積分テーブルから適切な値を取得し、それを返す。
//! 
//! @param[in] integralEndPoint 積分範囲の上限
//! @return 積分値
float ScalarValueEvaluator::GetGaussianIntegral(float integralEndPoint) const
{
	unsigned int indexOfIntegralTable_UINT = 0;

	if (integralEndPoint>0) // positive value
	{
		float numberOfIntervalsFromOrigin = 1000 * integralEndPoint;

		float indexOfIntegralTable_FLT = numberOfIntervalsFromOrigin + 9999; // if num is 0.3, index must be 9999, since index 9999 corresponds to integral from -10 to origin 
		indexOfIntegralTable_UINT = static_cast<unsigned int>(indexOfIntegralTable_FLT);
	}
	else // negative value
	{
		float numberOfIntervalsFromOrigin = 1000 * integralEndPoint;

		int indexOfIntegralTable_INT = static_cast<int>(numberOfIntervalsFromOrigin)-1;
		indexOfIntegralTable_INT = - indexOfIntegralTable_INT; // convert to positve value
		indexOfIntegralTable_INT = 9999 - indexOfIntegralTable_INT;
		indexOfIntegralTable_UINT = static_cast<unsigned int>(indexOfIntegralTable_INT); 
	}

	float integralValue =0.0f;
	if ( indexOfIntegralTable_UINT<20000 ) // 20000 = number of elements in the table
	{
		integralValue = static_cast<float>((*m_integralLookUpTable)[indexOfIntegralTable_UINT]);
	}
	else
	{
		integralValue = 0.0f;
	}

	return integralValue;
}

//! @brief 与えられた座標値におけるスカラー値を返す。このクラスの主処理。
//! 
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @return スカラー値
float ScalarValueEvaluator::value(float x, float y, float z)
{
	Point3f srcPoint(x, y, z);
	Point3i cellIndex;

	GetCellContainingGivenPoint(srcPoint, cellIndex);

	std::vector<unsigned int> contributingLineSegments;
	GetIndicesOfContributingLineSegment(cellIndex, contributingLineSegments);

	float scalarValue = ComputeScalarValueFromAllContributingLineSegments(contributingLineSegments, srcPoint) - isoValue;

	return scalarValue;
}

//! @brief 積分テーブルの参照をメンバーにセットする。
//! 
//! @param[in] table 積分テーブルへの参照
void ScalarValueEvaluator::SetGaussianIntegralTable(const std::vector<double>& table)
{
	m_integralLookUpTable = &table;
}

//! @brief 線分データへの参照をメンバーにセットする。
//! 
//! @param[in] lineSegments 線分データへの参照
void ScalarValueEvaluator::SetLineSegments(const std::vector<SourceLineSegment>& lineSegments)
{
	m_lineSegments = &lineSegments;
}

//! @brief 与えられた点がバウンディングボックスのに含まれるかどうかを検証する。
//! 
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @return 与えられた点がバウンディングボックスのに含まれるかどうかを表すフラグ
bool ScalarValueEvaluator::IsPointInsideOfBoundingBox(const Point3f& srcPoint) const
{
	const Point3f& bboxOrigin = m_boundingBox.origin();
	Point3f bboxDiagonalPoint = m_boundingBox.origin() + m_boundingBox.size();

	// 下限範囲チェック
	if ( (srcPoint.array() <= bboxOrigin.array()).any() ) return false;

	// 上限範囲チェック
	if ( (srcPoint.array() >= bboxDiagonalPoint.array()).any() ) return false; 

	return true;
}

//! @brief 与えられた点を含むセルのインデックスを取得する。
//! 
//! @param[in] x x 座標値
//! @param[in] y y 座標値
//! @param[in] z z 座標値
//! @param[out] cellIndex セルのインデックス
void ScalarValueEvaluator::GetCellContainingGivenPoint(const Point3f& srcPoint, Point3i& cellIndex) const
{
	if (!IsPointInsideOfBoundingBox(srcPoint))
	{
		cellIndex.setConstant(signRepresentingOutOfBoundingBox);
		return;
	}

	Point3f diffFromOrigin = srcPoint - m_boundingBox.origin();

	cellIndex = (diffFromOrigin/m_cellScale).cast<int>();
}

//! @brief セルが格納する線分データのインデックス集合を取得する。\n
//! 各セルは近傍にある線分のインデックスを保有している。
//! 
//! @param[in] cell セルのインデックス
//! @param[out] lineSegmentIndices 線分データのインデックス集合
void ScalarValueEvaluator::GetIndicesOfContributingLineSegment(const Point3i& cell,
	std::vector<unsigned int>& lineSegmentIndices) const
{
	if ((cell.array() == signRepresentingOutOfBoundingBox).any())
	{
		lineSegmentIndices.clear(); // 空データを返す。
		return;
	}

	const int& xIndex = cell.x();
	const int& yIndex = cell.y();
	const int& zIndex = cell.z();

	if (!m_sourceIndexCells[xIndex][yIndex][zIndex]) // セルが万が一オブジェクトを所有しない場合
	{
		lineSegmentIndices.clear();
		return;
	}

	lineSegmentIndices = m_sourceIndexCells[xIndex][yIndex][zIndex]->GetSourceIndices();

}

//! @brief 線分データのバウンディングボックスをメンバにセットする。
//! 
//! @param[out] boundingBox バウンディングボックスへの参照
void ScalarValueEvaluator::SetBoundingBoxEnclosingAllLineSegments(const Boxf& boundingBox)
{
	m_boundingBox = boundingBox;
}

} // namespace polygonization
} // namespace lssplib

