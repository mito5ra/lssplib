#include "Modeling/SmoothedVesselNodeGenerator.h"

#include "Segmentation/VesselNode.h"
#include "Segmentation/VesselTree.h"
#include "Modeling/SmoothedVesselNode.h"

using lssplib::segmentation::VesselNode;

namespace lssplib
{
namespace polygonization
{

//! @brief 初期状態の半径
const float kInitialRadius = 0.6f;

//! @brief 短い長さ
const double kSmallPossibleLength0 = 1e-5;
const float kToleranceFloat0 = 1e-5f;

//! @brief 短い長さ
const double kSmallPossibleLength1 = 1e-6;
const float kToleranceFloat1 = 1e-6f;

//------------------------------------------------------------------------------
SmoothedVesselNodeGenerator::SmoothedVesselNodeGenerator()
: m_numSmoothing(1)
, m_voxelSize(1.0f, 1.0f, 1.0f)
, m_smoothedVesselNode(NULL)
{
}

//------------------------------------------------------------------------------
SmoothedVesselNodeGenerator::SmoothedVesselNodeGenerator(size_t nIter)
: m_numSmoothing(nIter)
, m_voxelSize(1.0f, 1.0f, 1.0f)
, m_smoothedVesselNode(NULL)
{
}

//------------------------------------------------------------------------------
SmoothedVesselNodeGenerator::~SmoothedVesselNodeGenerator()
{
}

//------------------------------------------------------------------------------
void SmoothedVesselNodeGenerator::SetParams(
	size_t maxNodesFromBranchToEnd, //!< 分岐から末端の最大ノードの数
	float maxRadiusAtEnd, //!< 末端の最大半径
	float maxRadiusAtBranchToEnd) //!< 分岐から末端の最大半径
{
	m_maxNodesFromBranchToEnd = maxNodesFromBranchToEnd; 
	m_maxRadiusAtEnd = maxRadiusAtEnd;
	m_maxRadiusAtBranchToEnd = maxRadiusAtBranchToEnd;
}

//------------------------------------------------------------------------------
SmoothedVesselNode* SmoothedVesselNodeGenerator::Convert(
	const VesselNode* vesselNode,
	const Point3f& vesselOrigin,
	const Size3f& voxelSize)
{
	m_voxelSize = voxelSize;

	ConvertPixelCoordToRealCoord(vesselNode, vesselOrigin, voxelSize);

	UpdateBranchLevels();

	ReduceRadiiOfFirstLevelJunctions();

	SetRadiiOfBranches();

	ReduceRadiiOfTerminalNodes();

	SetRadiusByMonotomicallyDecreasingManner();

	for (size_t i = 0; i < m_numSmoothing; ++i)
	{
		SmoothPositions();
	}

	SmoothedVesselNode* node = m_smoothedVesselNode;
	m_smoothedVesselNode = NULL;

	return node;
}

//------------------------------------------------------------------------------
// private methods
//------------------------------------------------------------------------------
//! @brief tree 構造の血管芯線データをCT座標系での値に変換し、格納する
void SmoothedVesselNodeGenerator::ConvertPixelCoordToRealCoord(
	const VesselNode* vesselNode, // root node of a vessel tree
	const Point3f& vesselOrigin, // real valued position
	const Size3f& voxelSpacing) // w,h,d of a voxel
{
	// create root node and a stack of a smoothed vessel tree
	std::stack<SmoothedVesselNode*> stack_smooth;
	m_smoothedVesselNode = new SmoothedVesselNode;
	stack_smooth.push(m_smoothedVesselNode);

	// prepare stack of given vessel tree starting from vesselNode
	std::stack<const VesselNode*> nodeStack;
	nodeStack.push(vesselNode);

	while(!nodeStack.empty())
	{
		const VesselNode* currentNode = nodeStack.top();
		nodeStack.pop();

		SmoothedVesselNode* currentSmoothedNode = stack_smooth.top();
		stack_smooth.pop();

		// node position is given in pixel coord of the vessel tree
		Point3f tempPos = currentNode->GetPosition().cast<float>().cwiseProduct(voxelSpacing) + vesselOrigin;

		currentSmoothedNode->SetPosition(tempPos);

		if(currentNode->GetRadius() < kInitialRadius)
		{
			currentSmoothedNode->SetRadius(kInitialRadius);
		}
		else
		{
			currentSmoothedNode->SetRadius(static_cast<float>(currentNode->GetRadius()));
		}

		// create child nodes for smoothed vessel tree
		const std::vector<VesselNode*>& children = currentNode->GetOutgoings();
		for (size_t i = 0; i < children.size(); ++i)
		{
			nodeStack.push(children[i]);

			SmoothedVesselNode* childSmoothedNode = new SmoothedVesselNode;
			currentSmoothedNode->AddChild(childSmoothedNode); // 親に子を記憶させる。
			childSmoothedNode->SetParent(currentSmoothedNode); // 子に親を記憶される。
			stack_smooth.push(childSmoothedNode);
		}
	}
}

//------------------------------------------------------------------------------
//! @brief 分岐レベルを記録する。
void SmoothedVesselNodeGenerator::UpdateBranchLevels()
{
	std::vector<SmoothedVesselNode*> terminalNodes;
	CollectTerminalNodes(terminalNodes);

	for (size_t i = 0; i < terminalNodes.size(); ++i)
	{
		SetBranchLevelFromTerminalNode(terminalNodes[i]);
	}
}

//------------------------------------------------------------------------------
//! @brief 末端ノードから、親ノードを探索し分岐点に分岐レベルを与える。
//! @param[in] terminalNode 末端ノード
void SmoothedVesselNodeGenerator::SetBranchLevelFromTerminalNode(SmoothedVesselNode* terminalNode)
{
	std::stack<SmoothedVesselNode*> nodeStack;
	nodeStack.push(terminalNode);

	unsigned short levelCount = 0;
	SmoothedVesselNode* currentNode = terminalNode;
	while(currentNode)
	{
		if (currentNode->GetChildren().size() > 1)
		{
			++levelCount;

			// 分岐レベルが大きい方を優先
			if (currentNode->GetLevel() < levelCount)
			{
				currentNode->SetBranchLevel(levelCount);
			}
		}

		currentNode = currentNode->GetParent();
	}
}

//------------------------------------------------------------------------------
//! @brief 末端にあるノードを取得する。
//! @param[out] terminalNodes 末端のノードへのポインタ配列
void SmoothedVesselNodeGenerator::CollectTerminalNodes(std::vector<SmoothedVesselNode*>& terminalNodes) const
{
	SmoothedVesselNode::Iterator itr = m_smoothedVesselNode->Begin();
	while(itr.Next())
	{
		SmoothedVesselNode* currentNode = itr.Get();
		if (currentNode->GetChildren().empty())
		{
			terminalNodes.push_back(currentNode);
		}
	}
}

//------------------------------------------------------------------------------
//! @brief 条件を満たす分岐レベルが 1 のノードの半径を小さくする。
void SmoothedVesselNodeGenerator::ReduceRadiiOfFirstLevelJunctions()
{
	std::vector<SmoothedVesselNode*> firstLevelJunctions;
	CollectFirstLevelJunctions(firstLevelJunctions);

	for (size_t i=0; i < firstLevelJunctions.size(); ++i)
	{
		SmoothedVesselNode* currentJunction = firstLevelJunctions[i];

		const std::vector<SmoothedVesselNode*>& children = currentJunction->GetChildren();

		size_t maximumNumberOfNodesFromBranchPointToEnd = 0;

		for (size_t j = 0; j < children.size(); ++j)
		{
			size_t numberOfNodesInTerminalBranch = 1;

			const SmoothedVesselNode* node = children[j];
			while(node)
			{
				++numberOfNodesInTerminalBranch;
				node = (node->GetChildren().empty()) ? NULL : node->GetChildren()[0];
			}

			if (numberOfNodesInTerminalBranch > maximumNumberOfNodesFromBranchPointToEnd)
			{
				maximumNumberOfNodesFromBranchPointToEnd = numberOfNodesInTerminalBranch;
			}
		}

		if (maximumNumberOfNodesFromBranchPointToEnd < m_maxNodesFromBranchToEnd) // radius of short branches with end is reduced 
		{
			if (currentJunction->GetRadius() > m_maxRadiusAtBranchToEnd)
			{
				currentJunction->SetRadius(m_maxRadiusAtBranchToEnd);
			}
		}
	}
}

//------------------------------------------------------------------------------
//! @brief 分岐レベルが 1 の分岐ノードを取得する。
//! @param[out] singleLevels 分岐レベルが 1 の分岐ノードの配列
void SmoothedVesselNodeGenerator::CollectFirstLevelJunctions(std::vector<SmoothedVesselNode*>& firstLevelJunctions) const
{
	SmoothedVesselNode::Iterator itr = m_smoothedVesselNode->Begin();
	while (itr.Next())
	{
		SmoothedVesselNode* currentNode = itr.Get();
		if (currentNode->GetLevel() == 1)
		{
			firstLevelJunctions.push_back(currentNode);
		}
	}
}

//------------------------------------------------------------------------------
//! @brief 分岐点 node の太さを再設定する。分岐点 node だけに着目し、枝に近づくにつれて細くなるようにする。
void SmoothedVesselNodeGenerator::SetRadiiOfBranches()
{
	std::vector<SmoothedVesselNode*> junctionNodes;
	CollectJunctionNodes(junctionNodes); // 始点も含まれる

	size_t numJunctions = junctionNodes.size();

	bool flag = true;
	while (flag)
	{
		flag = false;
		for (size_t i = 0; i < numJunctions; ++i)
		{
			if ( ExchangeRadiusAtJunctions(junctionNodes[i]) )
			{
				flag = true;
			}
		}
	}
}

//------------------------------------------------------------------------------
//! @brief 分岐点と始点ノードを集める。
//! @param[out] arrayOfbranchOrStart 分岐点と始点を含む配列
void SmoothedVesselNodeGenerator::CollectJunctionNodes(std::vector<SmoothedVesselNode*>& junctionNodes)
{
	SmoothedVesselNode::Iterator itr = m_smoothedVesselNode->Begin();
	while(itr.Next())
	{
		SmoothedVesselNode* node = itr.Get();
		if (node->GetChildren().size() > 1 || !node->GetParent()) // 分岐点又は始点
		{
			junctionNodes.push_back(node);
		}
	}
}

//------------------------------------------------------------------------------
//! @brief 分岐点の太さを比べて、末端に近い方が太い場合に入れ替える。
//! junctionNodeのみ更新する。
//! @param[in, out] junctionNode 血管分岐点 node または始点 node へのポインタ
//! @return 入れ替えが行われたかどうかを表すフラグ
bool SmoothedVesselNodeGenerator::ExchangeRadiusAtJunctions(SmoothedVesselNode* junctionNode) const
{
	const std::vector<SmoothedVesselNode*>& children = junctionNode->GetChildren();
	if (children.empty()) return false;

	bool isExchanged = false;
	for (size_t i = 0; i < children.size(); ++i)
	{
		SmoothedVesselNode* nextJunctionNode = children[i]->NextJunction();
		float r0 = junctionNode->GetRadius();
		float r1 = nextJunctionNode->GetRadius();
		if (r0 < r1)
		{
			junctionNode->SetRadius(r1);
			isExchanged = true;
		}
	}

	return isExchanged;
}

//------------------------------------------------------------------------------
//! @brierf 末端ノードの半径を設定する。
void SmoothedVesselNodeGenerator::ReduceRadiiOfTerminalNodes()
{
	SmoothedVesselNode::Iterator itr = m_smoothedVesselNode->Begin();
	while(itr.Next())
	{
		SmoothedVesselNode* currentNode = itr.Get();
		if (currentNode->GetChildren().empty()) // 末端ノード
		{
			if ( currentNode->GetRadius() > m_maxRadiusAtEnd )
			{
				currentNode->SetRadius(m_maxRadiusAtEnd); // 0.4 mm = maximum radius at end
			}
		}
	}
}

//------------------------------------------------------------------------------
//! @brief 末端に向けて細くなるように血管の太さをセットする。
void SmoothedVesselNodeGenerator::SetRadiusByMonotomicallyDecreasingManner()
{
	std::vector<SmoothedVesselNode*> junctionNodes;
	CollectJunctionNodes(junctionNodes); // 分岐点と始点
	
	size_t numJunctionsOrStart = junctionNodes.size();

	for (size_t i = 0; i < numJunctionsOrStart; ++i)
	{
		DecreaseRadiusGradually(junctionNodes[i]);
	}
}

//------------------------------------------------------------------------------
//! @brief 各分岐点(or 始点) node から次の分岐点に向けて、血管の太さを徐々に細くなるようにセットする。
//! @param[in, out] nodesInBranch 一つの枝の含まれる node の配列
void SmoothedVesselNodeGenerator::DecreaseRadiusGradually( SmoothedVesselNode* junctionNode )
{
	const std::vector<SmoothedVesselNode*>& children = junctionNode->GetChildren();

	for (size_t i = 0; i < children.size(); ++i)
	{
		std::vector<SmoothedVesselNode*> nodesInBranch;
		nodesInBranch.push_back(junctionNode);
		CollectBranchNodes(children[i], nodesInBranch);

		DecreaseRadiusTowardNextJunction(nodesInBranch);
	}
}

//------------------------------------------------------------------------------
//! @brief 二つの血管分岐点に含まれる全ての node を取得する。
//! @param[in] branchStartNode 枝の最初(根側)のノード
//! @param[out] nodesInBranch 分岐点間のnode の配列
void SmoothedVesselNodeGenerator::CollectBranchNodes(
	SmoothedVesselNode* branchStartNode,
	std::vector<SmoothedVesselNode*>& nodesInBranch)
{
	SmoothedVesselNode* currentNode = branchStartNode;
	while(currentNode)
	{
		nodesInBranch.push_back(currentNode);
		if (currentNode->GetChildren().empty() || currentNode->GetChildren().size() > 1)
		{
			currentNode = NULL;
		}
		else
		{
			currentNode = currentNode->GetChildren()[0];
		}
	}
}

//------------------------------------------------------------------------------
//! @brief  各分岐点(or 始点) node から末端に向けて、血管の太さを徐々に細くなるようにセットする。
//! @param[in, out] nodesInSegment 一つの枝に含まれる node の集合
void SmoothedVesselNodeGenerator::DecreaseRadiusTowardNextJunction(
	const std::vector<SmoothedVesselNode*>& nodesInBranch )
{
	if (nodesInBranch.size() < 2) return;

	const float& startRadius = nodesInBranch[0]->GetRadius();
	const float& endRadius = nodesInBranch.back()->GetRadius();

	float deltaRadius = (endRadius - startRadius)/(nodesInBranch.size() - 1);

	for (size_t i = 0; i < nodesInBranch.size(); ++i)
	{
		if (nodesInBranch[i])
		{
			nodesInBranch[i]->SetRadius( (startRadius + deltaRadius * i) );
		}
	}
}

//------------------------------------------------------------------------------
//! @brief node の位置を平滑化する。
void SmoothedVesselNodeGenerator::SmoothPositions()
{
	if(!m_smoothedVesselNode) return;

	SmoothedVesselNode::Iterator itr = m_smoothedVesselNode->Begin();
	while(itr.Next())
	{
		SmoothedVesselNode* currentNode = itr.Get();

		const std::vector<SmoothedVesselNode*>& children = currentNode->GetChildren();
		const SmoothedVesselNode* parent = currentNode->GetParent();
		if (children.size() == 1 && parent)
		{
			const Point3f& childPos = children[0]->GetPosition();
			const Point3f& currentPos = currentNode->GetPosition();
			const Point3f& parentPos = parent->GetPosition();

			Point3f smoothedPos = (childPos + currentPos + parentPos) * 0.33333333f;
			currentNode->SetPosition(smoothedPos);
		}
	}
}

//------------------------------------------------------------------------------
//! @brief tree 構造データを消去する。
void SmoothedVesselNodeGenerator::ClearNodeTree(SmoothedVesselNode*& startNode)
{
	SmoothedVesselNode::Iterator itr = startNode->Begin();
	while (itr.Next())
	{
		SmoothedVesselNode* node = itr.Get();
		delete node;
	}
	startNode = NULL;
}
} // namespace polygonization
} // namespace lssplib
