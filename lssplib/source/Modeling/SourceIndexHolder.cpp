#include "Modeling/SourceIndexHolder.h"

namespace lssplib
{
namespace polygonization
{

//! @brief コンストラクタ
SourceIndexHolder::SourceIndexHolder()
{
}

//! @brief デストラクタ
SourceIndexHolder::~SourceIndexHolder()
{
}

//! @brief 線分データのインデックス全体を保有する配列への参照を返す。
const std::vector<unsigned int>& SourceIndexHolder::GetSourceIndices() const
{
	return m_sourceIndices;
}

//! @brief 線分データのインデックスを追加する。
void SourceIndexHolder::AddSourceIndex(const unsigned int indexToBeAdded)
{
	m_sourceIndices.push_back(indexToBeAdded);
}

} // namespace polygonization
} // namespace lssplib

