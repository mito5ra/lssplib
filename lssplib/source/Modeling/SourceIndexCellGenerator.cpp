#include "Modeling/SourceIndexCellGenerator.h"
#include "Modeling/SourceIndexHolder.h"
#include <float.h>
#include <math.h>

namespace lssplib
{
namespace polygonization
{

//! @brief コンストラクタ
SourceIndexCellGenerator::SourceIndexCellGenerator()
: m_lineSegments(NULL)
, m_sourceIndexCells(NULL)
, m_boudningBox(Point3f::Zero(), Size3f::Zero())
, m_cellArraySize(0, 0, 0)
, m_cellScale(1.0f)
{
}

//! @brief デストラクタ
SourceIndexCellGenerator::~SourceIndexCellGenerator()
{
}

//! @brief セルを生成する。
void SourceIndexCellGenerator::GenerateCells()
{
	ComputeBoundingBoxEnclosingAllLineSegments();

	ExtendBoundingBox();

	SetCellArraySize();

	AllocateCells();

	RegisterAllLineSegmentsToCells();
}

//! @brief セルに線分のインデックスを記録させる。\n
//! 各セルは線分データのインデックスを保有し、ある一点のスカラー値を計算するときに利用される。
void SourceIndexCellGenerator::RegisterAllLineSegmentsToCells()
{
	if ((*m_lineSegments).empty()) return;

	float invCellScale = 1.0f/m_cellScale;
	const Point3f& bboxOrigin = m_boudningBox.origin();
	size_t numLineSegments = m_lineSegments->size();
	for (unsigned int i=0; i<numLineSegments; ++i)
	{
		const SourceLineSegment& lineSegment = (*m_lineSegments)[i];
		const Point3f& p0 = lineSegment.GetEndPosition(0);
		const Point3f& p1 = lineSegment.GetEndPosition(1);

		// local bbox
		Point3f localMin = p0.cwiseMin(p1);
		Point3f localMax = p0.cwiseMax(p1);

		// enlarge local bbox
		float cylinderRadius = lineSegment.GetBoundingCylinderRadius() * 0.8f; // decrease the extension to reduce the computational time.
		localMin -= Vector3f::Constant(cylinderRadius);
		localMax += Vector3f::Constant(cylinderRadius);

		// convert position to cell index representation
		Point3i minIdx = ((localMin - bboxOrigin) * invCellScale).cast<int>();
		Point3i maxIdx = (Point3f::Ones() + (localMax - bboxOrigin) * invCellScale).cast<int>();

		RegisterSingleLineSegmentToCells(i, minIdx, maxIdx);
	}
}

//! @brief 各セルに線分データのインデックスを付与する。
//! 
//! @param[in] lineSegmentIndex 線分データのインデックス
//! @param[in] boudaryCellIndices 一つの線分に対するバウンディングボックス
void SourceIndexCellGenerator::RegisterSingleLineSegmentToCells(
	const unsigned int lineSegmentIndex, const Point3i& minIdx, const Point3i& maxIdx) const
{
	for (int i = minIdx.x(); i < maxIdx.x(); ++i)
	{
		for (int j = minIdx.y(); j < maxIdx.y(); ++j)
		{
			for (int k = minIdx.z(); k < maxIdx.z(); ++k)
			{
				if (IsCellInsideCylinder(lineSegmentIndex, i, j, k))
				{
					if (!m_sourceIndexCells[i][j][k]) // if no object, give the new object to the cell
					{
						SourceIndexHolder* newVoxelData = new SourceIndexHolder;
						newVoxelData->AddSourceIndex(lineSegmentIndex);
						m_sourceIndexCells[i][j][k] = newVoxelData;
					}
					else // if the cell already has an object, add the line-segment index to the cell
					{
						m_sourceIndexCells[i][j][k]->AddSourceIndex(lineSegmentIndex);
					}
				}
			}
		}
	}
}

//! @brief セルが円筒の中に含まれるかどうかを検証する。セルの中心を使う。正確には円筒ではなく、線分からある距離以内にあるセルについて検証を行う。
//! 
//! @param[in] lineSegmentIndex 線分データのインデックス
//! @param[in] cellIndexX セルのインデックス x 成分
//! @param[in] cellIndexY セルのインデックス y 成分
//! @param[in] cellIndexZ セルのインデックス z 成分
//! @return セルが円筒の中に含まれるかどうかを表すフラグ
const bool SourceIndexCellGenerator::IsCellInsideCylinder(
	const unsigned int lineSegmentIndex,
	const unsigned int cellIndexX, 
	const unsigned int cellIndexY, 
	const unsigned int cellIndexZ) const
{
	Point3f cellPos(cellIndexX + 0.5f, cellIndexY + 0.5f, cellIndexZ + 0.5f);
	Point3f cellCenter = m_boudningBox.origin() + cellPos * m_cellScale;

	float distanceFromLineSegment = (*m_lineSegments)[lineSegmentIndex].ComputeDistanceFromPoint(cellCenter);

	float cylinderRadius = (*m_lineSegments)[lineSegmentIndex].GetBoundingCylinderRadius();

	if (cylinderRadius < distanceFromLineSegment)
	{
		return false;
	}
	else
	{
		return true;
	}
}

//! @brief セルデータをメモリ上に確保する。
void SourceIndexCellGenerator::AllocateCells()
{
	m_sourceIndexCells = new SourceIndexHolder ***[m_cellArraySize[0]];
	for (int i=0; i<m_cellArraySize[0]; ++i)
	{
		m_sourceIndexCells[i] = new SourceIndexHolder** [m_cellArraySize[1]];
	}
	for (int i=0; i<m_cellArraySize[0]; ++i)
	{
		for (int j=0; j<m_cellArraySize[1]; ++j)
		{
			m_sourceIndexCells[i][j] = new SourceIndexHolder* [m_cellArraySize[2]];
		}
	}

	InitializeCells();
}

//! @brief セルデータをメモリから消去する。
void SourceIndexCellGenerator::DeleteCells()
{
	if (m_sourceIndexCells == NULL) return;

	for (int i=0; i<m_cellArraySize[0]; ++i)
	{
		for (int j=0; j<m_cellArraySize[1]; ++j)
		{
			for (int k=0; k<m_cellArraySize[2]; ++k)
			{
				delete m_sourceIndexCells[i][j][k];
			}
		}
	}

	for (int i=0; i<m_cellArraySize[0]; ++i)
	{
		for (int j=0; j<m_cellArraySize[1]; ++j)
		{
			delete [] m_sourceIndexCells[i][j];
		}
	}

	for (int i=0; i<m_cellArraySize[0]; ++i)
	{
		delete[] m_sourceIndexCells[i];;
	}

	delete [] m_sourceIndexCells;

	m_sourceIndexCells = NULL;
}

//! @brief セルデータを初期化する。
void SourceIndexCellGenerator::InitializeCells()
{
	for (int i=0; i<m_cellArraySize[0]; ++i)
	{
		for (int j=0; j<m_cellArraySize[1]; ++j)
		{
			for (int k=0; k<m_cellArraySize[2]; ++k)
			{
				m_sourceIndexCells[i][j][k] = NULL;
			}
		}
	}
}

//! @brief 各方向のセル数を表すメンバ変数に値をセットする。
void SourceIndexCellGenerator::SetCellArraySize()
{
	m_cellArraySize = (m_boudningBox.size() / m_cellScale + Vector3f::Ones()).cast<int>();
}

//! @brief 線分データ全体のバウンディングボックスを拡張する。
void SourceIndexCellGenerator::ExtendBoundingBox()
{
	float maxCylinderRadius = GetMaximumCylinderRadius();

	m_boudningBox.origin() -= Size3f::Constant(maxCylinderRadius + m_cellScale);
	m_boudningBox.size() += Size3f::Constant(2 * (maxCylinderRadius + m_cellScale));
}

//! @brief 線分に付与された円筒半径の最大値を取得する。
const float SourceIndexCellGenerator::GetMaximumCylinderRadius() const
{
	float maxRadius = 0;
	for (std::vector<SourceLineSegment>::const_iterator itr = (*m_lineSegments).begin();
		itr != (*m_lineSegments).end(); ++itr)
	{
		float cylinderRadiusOfSingleLineSegment = itr->GetBoundingCylinderRadius();
		if (cylinderRadiusOfSingleLineSegment>maxRadius)
		{
			maxRadius = cylinderRadiusOfSingleLineSegment;
		}
	}

	return maxRadius;
}

//! @brief 全ての線分データを覆うバウンディングボックスを計算する。
void SourceIndexCellGenerator::ComputeBoundingBoxEnclosingAllLineSegments()
{
	if (m_lineSegments->empty()) return;

	Point3f minPt = (*m_lineSegments)[0].GetEndPosition(0);
	Point3f maxPt = minPt;

	for (std::vector<SourceLineSegment>::const_iterator itr = m_lineSegments->begin();
		itr != m_lineSegments->end(); ++itr)
	{
		const Point3f& p0 = itr->GetEndPosition(0);
		const Point3f& p1 = itr->GetEndPosition(1);

		minPt = minPt.cwiseMin(p0);
		minPt = minPt.cwiseMin(p1);

		maxPt = maxPt.cwiseMax(p0);
		maxPt = maxPt.cwiseMax(p1);
	}

	m_boudningBox.origin() = minPt;
	m_boudningBox.size() = maxPt - minPt;
}

//! @brief セル生成に使われる各メモリをリセットする。
void SourceIndexCellGenerator::Reset()
{
	m_boudningBox.origin().setZero();
	m_boudningBox.size().setZero();

	DeleteCells();

	m_cellArraySize.setZero();
}

} // namespace polygonization
} // namespace lssplib

