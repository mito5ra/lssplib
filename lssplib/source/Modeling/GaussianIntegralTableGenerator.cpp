#include "Modeling/GaussianIntegralTableGenerator.h"
#include <math.h>

namespace lssplib
{
namespace polygonization
{

//! @brief 5Log[2]：ガウス分布の分散を定義する。
const double w = 5.0*log(2.0);

//! @brief 円周率の 1/2 乗
const double rootPi = sqrt(3.141592653589793238462643383279);

//! @brief w の 1/2 乗
const double rootW = sqrt(w);

//! @brief w/π の 1/2 乗：積分計算の係数
const double rootWOverRootPi = rootW/rootPi;

//! @brief 積分を数値評価するときの分割数
const unsigned int numSummation = 20000;

//! @brief 積分範囲の下限。本来は、-∞まで積分されるべきだが、-10 でカットオフしている。
const double lowerCutOff = -10.0;

//! @brief 積分範囲の上限。本来は、+∞まで積分されるべきだが、+10 でカットオフしている。
const double upperCutOff = +10.0;

//! @brief 積分範囲の長さ
const double intergralDomain = (upperCutOff - lowerCutOff);

//! @brief 積分範囲を分割したときの一区分の間隔。
const double pitch = intergralDomain/numSummation;

//! @brief コンストラクタ
GaussianIntegralTableGenerator::GaussianIntegralTableGenerator()
: m_table(),
m_hasBeenGenerated(false)
{

}

//! @brief デストラクタ
GaussianIntegralTableGenerator::~GaussianIntegralTableGenerator()
{

}

//! @brief 積分テーブルを発生し、メンバ変数に格納する。
void GaussianIntegralTableGenerator::GenerateIntegralTable()
{
	if(m_hasBeenGenerated==true)
	{
		return;
	}

	double x = lowerCutOff;
	double xa = lowerCutOff + pitch;
	double area = 0;
	double sum = 0;

	unsigned int counter = 0;
	while(counter < numSummation)
	{
		area = rootWOverRootPi * 0.50 *( exp(-w * x * x) + exp(-w * xa * xa) ) * pitch;   // trapezoidal approximation of integral

		sum = sum + area;

		m_table.push_back(sum);

		x = x+pitch;
		xa = xa +pitch;

		counter++;
	}

	m_hasBeenGenerated = true;

	//	outputGaussianDistribution(); // for ouput test
}

//! @brief 格納されている積分テーブルの参照を渡す。
//!
//! @return 積分テーブル
const std::vector<double>& GaussianIntegralTableGenerator::GetIntegralTable() const
{
	return m_table;
}

} // namespace polygonization
} // namespace lssplib

