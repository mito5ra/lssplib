#include "Modeling/PolygonFromVesselTree.h"
#include "Modeling/TriangleData.h"
#include "Modeling/LineSegmentDataMaker.h"
#include "Segmentation/VesselTree.h"

using lssplib::segmentation::VesselTree;

namespace lssplib
{
namespace polygonization
{

PolygonFromVesselTree::PolygonFromVesselTree(const Size3d& voxelSize, const Params& params)
: m_voxelSize(voxelSize)
, m_params(params)
{
}

PolygonFromVesselTree::~PolygonFromVesselTree()
{
}

//! @brief ポリゴンを生成する
//!
//! @param[in] vesselTrees 生成させるための血管オブジェクト
//! @param[out] triangles 三角形の頂点インデックス
//! @param[out] vertices 頂点の位置
LSSPLIB_API PolygonFromVesselTree::ErrorStatus PolygonFromVesselTree::Generate(
	const std::vector<const VesselTree*>& vesselTrees,
	std::vector<TriangleVertexIndex>& triangles,
	std::vector<VertexPosition>& vertices)
{
	ErrorStatus errorStatus = kErrorStatus_NoError;

	triangles.clear();
	vertices.clear();

//	std::vector<segmentation::VesselTree>::size_type numTrees = vesselTrees.size();
	for (std::vector<const VesselTree*>::const_iterator vesselTree = vesselTrees.begin();
		vesselTree != vesselTrees.end(); ++vesselTree)
	{
		if (!(*vesselTree)->GetStartNode())
		{
			continue;
		}

		LineSegmentDataMaker::Params params(m_params.timesToSmooth, m_params.maxRadiusAtEnd, m_params.maxNodesFromBranchToEnd, m_params.maxRadiusAtBranchToEnd);
		LineSegmentDataMaker* lineSegmentMaker = new LineSegmentDataMaker(*vesselTree, m_voxelSize, params);
		lineSegmentMaker->MakeLineSegmentData(m_sourceLineSegments);
		delete lineSegmentMaker;

		GenerateCells();

		m_integralTableGenerator.GenerateIntegralTable();

		if(Polygonize(vertices, triangles) == false)
		{
			errorStatus = kErrorStatus_SomePortionFailed;
		}

		m_sourceLineSegments.clear();
		m_cellGenerator.Reset();
	}

	RemoveOutlyingFaces(vertices, triangles);

	if(vertices.empty() == true || triangles.empty() == true)
	{
		errorStatus = kErrorStatus_Failed;
	}

	return errorStatus;
}

//! @brief 血管ポリゴンを一つのデータにまとめる。\n 
//! 血管は、一般に一つに繋がっていることはなく、複数ある。\n
//!
//! このプログラムでは、各グループごとにポリゴン化を実行し、その結果を一つのデータに集約するように実装されている。
//! @param[in] polygonizer BloomenthalPolygonizerオブジェクトへのポインタ
//! @param[out] vertexPositions 頂点の位置
//! @param[out] triangles 三角形の頂点のインデックス
void PolygonFromVesselTree::MergeIntoSingleGroupOfPolygons(const BloomenthalPolygonizer* polygonizer, std::vector<VertexPosition>& vertexPositions, std::vector<TriangleVertexIndex>& triangles)
{
	size_t numTrianglesToBeAdded = polygonizer->no_triangles();
	size_t numCurrentTriangles = triangles.size();
	size_t totalNumOfTriangles = numCurrentTriangles + numTrianglesToBeAdded;
	triangles.resize(totalNumOfTriangles);
	size_t numCurrentVertices = vertexPositions.size();

	for (size_t i=0; i<numTrianglesToBeAdded; ++i)
	{
		const lssplib::polygonization::TRIANGLE& triangle = polygonizer->get_triangle(static_cast<int>(i));

		triangles[i+numCurrentTriangles].vertex[0] = triangle.v0 + numCurrentVertices;
		triangles[i+numCurrentTriangles].vertex[1] = triangle.v1 + numCurrentVertices;
		triangles[i+numCurrentTriangles].vertex[2] = triangle.v2 + numCurrentVertices;
	}

	size_t numVerticesToBeAdded = polygonizer->no_vertices();
	size_t totalNumOfVertices = numCurrentVertices + numVerticesToBeAdded;
	vertexPositions.resize(totalNumOfVertices);

	for (size_t i=0; i<numVerticesToBeAdded; ++i)
	{
		const lssplib::polygonization::VERTEX& vertex = polygonizer->get_vertex(static_cast<int>(i));
		vertexPositions[i+numCurrentVertices].position[0] = vertex.x;
		vertexPositions[i+numCurrentVertices].position[1] = vertex.y;
		vertexPositions[i+numCurrentVertices].position[2] = vertex.z;
	}
}

//! @brief 異常なインデックス値を持つ三角形を取り除く
//!
//! @param[in] vertexPositions 頂点位置の集合
//! @param[in, out] triangles 三角形の集合
void PolygonFromVesselTree::RemoveOutlyingFaces(const std::vector<VertexPosition>& vertexPositions, std::vector<TriangleVertexIndex>& triangles) const
{
	size_t maxIndex = vertexPositions.size()-1;
	std::vector<size_t> outliers;

	for(size_t i=0; i<triangles.size(); ++i)
	{
		for (size_t j=0; j<3; ++j)
		{
			size_t vertex = triangles[i].vertex[j];
			if(vertex>maxIndex)
			{
				outliers.push_back(i);
				break;
			}
		}
	}

	if (outliers.size()>0)
	{
		for(std::vector<size_t>::reverse_iterator itr = outliers.rbegin();
			itr != outliers.rend(); ++itr)
		{
			triangles.erase(triangles.begin()+(*itr));
		}
	}
}

//! @brief セルデータを生成する。
void PolygonFromVesselTree::GenerateCells()
{
	m_cellGenerator.SetLineSegments(m_sourceLineSegments);
	m_cellGenerator.SetCellScale(static_cast<float>(m_voxelSize.minCoeff()));
	m_cellGenerator.GenerateCells();
}

//! @brief スカラー分布をもとにポリゴン化する。
//!
//! @return ポリゴン化に成功したかどうかを表すフラグ
const bool PolygonFromVesselTree::Polygonize(std::vector<VertexPosition>& vertexPositions, std::vector<TriangleVertexIndex>& triangles)
{
	const Boxf& boundingBox = m_cellGenerator.GetBoundingBox();
	Point3f bboxCenter = boundingBox.origin() + boundingBox.size() * 0.5f;
	float size = boundingBox.size().maxCoeff() * 0.5f * 1.2f;

	ScalarValueEvaluator valueEvaluator;
	SetParametersToScalarValueEvaluator(valueEvaluator);

	BloomenthalPolygonizer polygonizer(&valueEvaluator, static_cast<float>(m_params.cubeSize), static_cast<int>(m_params.boundsCoeff * size));

	bool isSuccess = polygonizer.march(lssplib::polygonization::TET, bboxCenter[0], bboxCenter[1], bboxCenter[2]);

	MergeIntoSingleGroupOfPolygons(&polygonizer, vertexPositions, triangles);

	return isSuccess;
}

//! @brief スカラー値を計算するクラスに必要な要素を入力する。
//! 
//! @param[out] valueEvaluator スカラー値を計算するクラス
void PolygonFromVesselTree::SetParametersToScalarValueEvaluator(ScalarValueEvaluator& valueEvaluator) const
{
	valueEvaluator.SetGaussianIntegralTable(m_integralTableGenerator.GetIntegralTable());
	valueEvaluator.SetCells(m_cellGenerator.GetCells());
	valueEvaluator.SetCellScale(m_cellGenerator.GetCellScale());
	valueEvaluator.SetLineSegments(m_sourceLineSegments);
	valueEvaluator.SetBoundingBoxEnclosingAllLineSegments(m_cellGenerator.GetBoundingBox());
}

} // namespace polygonization
} // namespace lssplib

