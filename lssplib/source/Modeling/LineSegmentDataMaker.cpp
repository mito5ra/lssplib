#include "Modeling/LineSegmentDataMaker.h"

#include <stack>

#include "Segmentation/VesselNode.h"
#include "Segmentation/VesselTree.h"
#include "Modeling/SmoothedVesselNode.h"
#include "Modeling/SmoothedVesselNodeGenerator.h"

namespace lssplib
{
namespace polygonization
{

//! @brief コンストラクタ
//!
//! @param[in] vesselTree 血管抽出機能側から与えられる tree 化にされた血管芯線データへのポインタ
//! @param[in] voxelSize voxel scale。
//! @param[in] params 処理のためのパラメータ
LineSegmentDataMaker::LineSegmentDataMaker(const segmentation::VesselTree* vesselTree, const Size3d& voxelSize, const Params& params)
: m_vesselTree(vesselTree)
, m_voxelSize(voxelSize)
, m_startNode(NULL)
, m_params(params)
{
}

LineSegmentDataMaker::LineSegmentDataMaker(const Params& params, const Size3d& voxelSize)
: m_vesselTree(NULL)
, m_voxelSize(voxelSize)
, m_startNode(NULL)
, m_params(params)
{
}

//! @brief デストラクタ
LineSegmentDataMaker::~LineSegmentDataMaker()
{
}

//! @brief 線分データを作成する。
//!
//! @param[in, out] lineSegments 線分データ
void LineSegmentDataMaker::MakeLineSegmentData(
	std::vector<SourceLineSegment>& lineSegments)
{
	if (!m_vesselTree->GetStartNode()) return;

	Size3f voxelSpacing = m_vesselTree->GetSpacing().cast<float>();
	Point3f treeOrigin = m_vesselTree->GetOrigin().cast<float>();
	treeOrigin = (treeOrigin + Point3f::Constant(0.5f)).cwiseProduct(m_voxelSize.cast<float>());

	SmoothedVesselNodeGenerator gen(m_params.timesToSmooth);
	gen.SetParams(
		m_params.maxNodesFromBranchToEnd,
		static_cast<float>(m_params.maxRadiusAtEnd),
		static_cast<float>(m_params.maxRadiusAtBranchToEnd));
	m_startNode = gen.Convert(m_vesselTree->GetStartNode(), treeOrigin, voxelSpacing);

	SetEndsAndRadiusFromSmoothedNodes(lineSegments);

	ClearSmoothedTree();
}

//! @brief 線分データに線分の位置と太さを入力する。
//!
//! @param[in, out] lineSegments 線分の集合
void LineSegmentDataMaker::SetEndsAndRadiusFromSmoothedNodes(std::vector<lssplib::polygonization::SourceLineSegment>& lineSegments) const
{
	SmoothedVesselNode::Iterator itr = m_startNode->Begin();
	while (itr.Next())
	{
		SmoothedVesselNode* parent = itr.Get();

		const std::vector<SmoothedVesselNode*>& children = parent->GetChildren();
		size_t numChildren = children.size();
		for (size_t i = 0; i<numChildren; ++i)
		{
			lssplib::polygonization::SourceLineSegment lineSegment(
				parent->GetPosition(),
				children[i]->GetPosition(),
				parent->GetRadius(),
				children[i]->GetRadius());

			lineSegments.push_back(lineSegment);
		}
	}
}

//! @brief tree 構造データを消去する。
void LineSegmentDataMaker::ClearSmoothedTree()
{
	SmoothedVesselNode::Iterator itr = m_startNode->Begin();
	while (itr.Next())
	{
		SmoothedVesselNode* node = itr.Get();
		delete node;
	}
	m_startNode = NULL;
}

} // namespace polygonization
} // namespace lssplib

