#include "Modeling/SourceLineSegment.h"

namespace lssplib
{
namespace polygonization
{

//! @brief コンストラクタ
SourceLineSegment::SourceLineSegment()
: m_p0(0.0f, 0.0f, 0.0f)
, m_p1(0.0f, 0.0f, 0.0f)
, m_lineSegmentVector(0.0f, 0.0f, 0.0f)
, m_normalizedLineSegmentVector(1.0f, 0.0f, 0.0f)
, m_boundingCylinderRadius(0.0f)
, m_length(0.0f)
, m_thickness(0.0f, 0.0f)
{
}

SourceLineSegment::SourceLineSegment(
	const Point3f& p0,
	const Point3f& p1,
	float thickness0,
	float thickness1)
: m_p0(p0)
, m_p1(p1)
, m_lineSegmentVector(p1-p0)
, m_normalizedLineSegmentVector()
, m_length()
, m_thickness(thickness0, thickness1)
{
	MakeNormalizedVector();
	m_boundingCylinderRadius = 2 * std::max(thickness0, thickness1);
}

//! @brief デストラクタ
SourceLineSegment::~SourceLineSegment()
{
}

const Vector3f& SourceLineSegment::GetLineSegmentVector() const
{
	return m_lineSegmentVector;
};

//! @brief 規格化された線分の方向ベクトルへのポインタを返す。
//! 
//! @return 規格化された線分の方向ベクトルへのポインタ
const Vector3f& SourceLineSegment::GetNormalizedLineSegmentVector() const
{
	return m_normalizedLineSegmentVector;
};

//! @brief 円筒の半径を返す。
//! 
//! @return 円筒の半径。
const float& SourceLineSegment::GetBoundingCylinderRadius() const
{
	return m_boundingCylinderRadius;
}

//! @brief  線分のそれぞれの端に与えられた太さを取得する。
//! 
//! @param[in] oneOrTheOther どちらの端かを指定する数(0 又は 1)
//! @return 太さ
const Size2f& SourceLineSegment::GetThickness() const
{
	return m_thickness;
}

const Point3f& SourceLineSegment::GetEndPosition( unsigned int idx ) const
{
	return (idx == 0) ? m_p0 : m_p1;
}

//! @brief 線分の長さを取得する。
//! 
//! @return 線分の長さ
const float& SourceLineSegment::GetLineSegmentLength() const
{
	return m_length;
}

//! @brief 線分の両端の位置をセットする。
//! 
//! @param[in] edgePositions 両端の位置
void SourceLineSegment::SetEndPositions(const Point3f& p0, const Point3f& p1)
{
	m_p0 = p0;
	m_p1 = p1;
	m_lineSegmentVector = p1 - p0;
	MakeNormalizedVector();
}

//! @brief 線分の一端が保持する太さをセットする。
//! 
//! @param[in] oneOrTheOther どちらの端かを指定する数
//! @param[in] thickness 太さ
void SourceLineSegment::SetThickness(const float& thickness0, const float& thickness1)
{
	m_thickness[0] = thickness0;
	m_thickness[1] = thickness1;
	m_boundingCylinderRadius = 2 * std::max(thickness0, thickness1);
}

float SourceLineSegment::ComputeDistanceFromPoint(const Point3f& p) const
{
	Vector3f endToTarget = p - m_p0;
	float innerProduct = m_normalizedLineSegmentVector.dot(endToTarget);
	float distance = (endToTarget - m_normalizedLineSegmentVector * innerProduct).norm();
	return distance;
}

Point3f SourceLineSegment::ComputePointOfProjection(const Point3f& p) const
{
	Vector3f endToTarget = p - m_p0;
	float innerProduct = m_normalizedLineSegmentVector.dot(endToTarget);
	return m_p0 + m_normalizedLineSegmentVector * innerProduct;
}

void SourceLineSegment::MakeNormalizedVector()
{
	m_length = m_lineSegmentVector.norm();
	if (m_length >= kTolerance)
	{
		m_normalizedLineSegmentVector = m_lineSegmentVector.normalized();
	}
	else
	{
		m_normalizedLineSegmentVector.setZero();
		m_length = 0.0f;
	}
}

} // namespace polygonization
} // namespace lssplib

