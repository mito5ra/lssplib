#include "Modeling/PointCloudGenerator/ThickPointBuilder.h"
#include "Modeling/PointCloudGenerator/PointNormalGenerator.h"

namespace lssplib
{
namespace polygonization
{

//! Constructor of CThickPointBuilder class
CThickPointBuilder::CThickPointBuilder(void)
: m_objCThreeDDataProcessor(new CThreeDDataProcessor())
, m_GeneratedThickPoints()
, m_GeneratedNormals()
, m_dataManager(NULL)
{
}

CThickPointBuilder::CThickPointBuilder(CDataManager* dataManager)
: m_objCThreeDDataProcessor(new CThreeDDataProcessor(dataManager))
, m_GeneratedThickPoints()
, m_GeneratedNormals()
, m_dataManager(dataManager)
{
}

//! Destructor of CThickPointBuilder class
CThickPointBuilder::~CThickPointBuilder(void)
{
	delete m_objCThreeDDataProcessor;
}

//!Implementaion of bool CThickPointBuilder::AddPoints(void)
/*!
Generates and populates the point and normal lists in case of the thick structures
*/
bool CThickPointBuilder::AddPoints(void)
{
	cout<<"Generating points from thick structures.."<<endl;

	if(m_objCThreeDDataProcessor != NULL)
	{
		int bufSize = m_dataManager->GetVolumeSize();

		unsigned char* thickData = NULL;
		m_dataManager->GetDataForThickBuilder(&thickData);
		unsigned char* thinData = NULL;
		m_dataManager->GetThinData(&thinData);
		unsigned char* thinOutBoundData = NULL;
		m_dataManager->GetThinOutBounData(&thinOutBoundData);

		m_objCThreeDDataProcessor->ResetIsOutBoundSubvox();
		m_objCThreeDDataProcessor->SetDivParams(VOX_DIV_FACTOR, VOX_NEIGHBOR_FACTOR); 
		m_objCThreeDDataProcessor->SetVoxelDivisionFactor();
		std::vector<Point> PointsFromAlgm;
		std::vector<Point> normalsOfPoints;
		std::vector<Point>::iterator it;
		std::vector<Point>::iterator iter;

		const vector<int>& bufferX = m_dataManager->GetBufferX();
		const vector<int>& bufferY = m_dataManager->GetBufferY();
		const vector<int>& bufferZ = m_dataManager->GetBufferZ();
				
		if(thickData != NULL)			
		{
			for(int Id = 0; Id < bufSize ; Id++)
			{
					//checks if it is outer boudary voxel (not belonging to thick and thin data) and Implements 6 step algorithm
					if(!thickData[Id] && !thinData[Id] && !thinOutBoundData[Id])
					{
						if(m_objCThreeDDataProcessor->GeneratePointsUsingAlgm((float)bufferX[Id], (float)bufferY[Id],(float) bufferZ[Id]))
						{
							m_objCThreeDDataProcessor->GetPointsFromAlgm(PointsFromAlgm);
							m_objCThreeDDataProcessor->GetNormalsOfPoints(normalsOfPoints);
							if(!PointsFromAlgm.empty())
							{						
								for(it = PointsFromAlgm.begin(); it != PointsFromAlgm.end(); ++it)
									m_GeneratedThickPoints.push_back(*it);
								PointsFromAlgm.clear();
						
							}
							if(!normalsOfPoints.empty())
							{
								for(iter = normalsOfPoints.begin(); iter != normalsOfPoints.end(); ++iter)
								{
									m_GeneratedNormals.push_back(*iter);
								}
								normalsOfPoints.clear();
							}
						}
						
					}
			}
				
		}
		else
		{
			m_objCThreeDDataProcessor->ClearBuffers();
			return false;
		}
				
	}	
	
	m_objCThreeDDataProcessor->ClearBuffers();
	return true;
}

//!Implementaion of void CThickPointBuilder::GetPoints(vector<Point> &thickData)
/*!
GetPoints: Returns the points generated for thick structures
*/
void CThickPointBuilder::GetPoints(vector<Point> &thickData)
{
	thickData = m_GeneratedThickPoints;
}

//!Implementaion of void CThickPointBuilder::GetNormals(vector<Point> &normals)
/*!
Returns the normals of the points generated for thick structures
*/
void CThickPointBuilder::GetNormals(vector<Point> &normals)
{
	normals = m_GeneratedNormals;
}

//!Implementaion of void CThickPointBuilder::ClearBuffers()
/*!
Clears the point and normal lists
*/
void CThickPointBuilder::ClearBuffers()
{	
	m_GeneratedThickPoints.clear();	
	m_GeneratedNormals.clear();
}

} // namespace polygonization
} // namespace lssplib

