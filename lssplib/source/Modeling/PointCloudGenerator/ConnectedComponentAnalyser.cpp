#include "Modeling/PointCloudGenerator/ConnectedComponentAnalyser.h"

namespace lssplib
{
namespace polygonization
{

//! Constructor of ConnectedComponentAnalyser class
ConnectedComponentAnalyser::ConnectedComponentAnalyser(void)
: connComp(NULL)
, m_width(0)
, m_height(0)
, m_depth(0)
, m_buffer(NULL)
{
}

ConnectedComponentAnalyser::ConnectedComponentAnalyser(CDataManager* dataManager)
: connComp(NULL)
, m_width(dataManager->GetVolumeWidth())
, m_height(dataManager->GetVolumeHeight())
, m_depth(dataManager->GetVolumeDepth())
, m_buffer(dataManager->GetBinarydata())
{
}

//! Constructor of ConnectedComponentAnalyser class
ConnectedComponentAnalyser::~ConnectedComponentAnalyser(void)
{
}

//!Implementaion of bool ConnectedComponentAnalyser::CheckFloodfillConnectivity(int direction, Point &outBoundVoxel)
/*!
checks if the considering voxels in the opposite directions are connected or not
*/
bool ConnectedComponentAnalyser::CheckFloodfillConnectivity(int direction, Point &outBoundVoxel)
{
	connComp  = new unsigned char[26];
	memset (connComp,0,26);
//	int labelVal = 1;
	unsigned char labelVal = 1;
	bool disconnected = false;
	float NeighborSelectionFactor = 1.0f;
	std::vector<Point> Q;
	Q.push_back(outBoundVoxel);

	while (!(Q.empty())) 
	{
		size_t size = Q.size();
		float i = Q[size - 1].x;
		float j = Q[size - 1].y;
		float k = Q[size - 1].z;

		// remove this element from the queue
		Q.pop_back();
		float nbrI[26], nbrJ[26], nbrK[26];

		//////////////////////////////// 6 - Neighbourhood
		// neighbour on the right
		nbrI[0] = i + NeighborSelectionFactor; nbrJ[0] = j; nbrK[0] = k;
		// neighbour on the left
		nbrI[1] = i - NeighborSelectionFactor; nbrJ[1] = j; nbrK[1] = k;
		// neighbour on the front
		nbrI[2] = i; nbrJ[2] = j; nbrK[2] = k - NeighborSelectionFactor;
		// neighbour on the back
		nbrI[3] = i; nbrJ[3] = j; nbrK[3] = k + NeighborSelectionFactor;
		// neighbour on the top
		nbrI[4] = i; nbrJ[4] = j - NeighborSelectionFactor; nbrK[4] = k;
		// neighbour on the bottom
		nbrI[5] = i; nbrJ[5] = j + NeighborSelectionFactor; nbrK[5] = k;

		//////////////////////////////////////12 neighbours more

		// right front
		nbrI[6] = i + NeighborSelectionFactor; nbrJ[6] = j; nbrK[6] = k - NeighborSelectionFactor;
		// left front
		nbrI[7] = i - NeighborSelectionFactor; nbrJ[7] = j; nbrK[7] = k - NeighborSelectionFactor;
		// up front
		nbrI[8] = i; nbrJ[8] = j - NeighborSelectionFactor; nbrK[8] = k - NeighborSelectionFactor;
		// down front
		nbrI[9] = i; nbrJ[9] = j + NeighborSelectionFactor; nbrK[9] = k - NeighborSelectionFactor;
		// right back
		nbrI[10] = i + NeighborSelectionFactor; nbrJ[10] = j; nbrK[10] = k + NeighborSelectionFactor;	
		// left back
		nbrI[11] = i - NeighborSelectionFactor; nbrJ[11] = j; nbrK[11] = k + NeighborSelectionFactor;
		// up back
		nbrI[12] = i; nbrJ[12] = j - NeighborSelectionFactor; nbrK[12] = k + NeighborSelectionFactor;
		//down back 
		nbrI[13] = i; nbrJ[13] = j + NeighborSelectionFactor; nbrK[13] = k + NeighborSelectionFactor;
		// upper left
		nbrI[14] = i - NeighborSelectionFactor; nbrJ[14] = j - NeighborSelectionFactor; nbrK[14] = k;
		// upper right
		nbrI[15] = i + NeighborSelectionFactor; nbrJ[15] = j - NeighborSelectionFactor; nbrK[15] = k ;
		// lower left
		nbrI[16] = i - NeighborSelectionFactor; nbrJ[16] = j + NeighborSelectionFactor; nbrK[16] = k ;
		// lower right
		nbrI[17] = i + NeighborSelectionFactor; nbrJ[17] = j + NeighborSelectionFactor; nbrK[17] = k ;

		///////////////////////////////////////8 neighbours more
		//
		nbrI[18] = i + NeighborSelectionFactor; nbrJ[18] = j + NeighborSelectionFactor; nbrK[18] = k - NeighborSelectionFactor;
		//
		nbrI[19] = i + NeighborSelectionFactor; nbrJ[19] = j + NeighborSelectionFactor; nbrK[19] = k + NeighborSelectionFactor;
		//
		nbrI[20] = i + NeighborSelectionFactor; nbrJ[20] = j - NeighborSelectionFactor; nbrK[20] = k - NeighborSelectionFactor;
		//
		nbrI[21] = i + NeighborSelectionFactor; nbrJ[21] = j - NeighborSelectionFactor; nbrK[21] = k + NeighborSelectionFactor;
		//
		nbrI[22] = i - NeighborSelectionFactor; nbrJ[22] = j + NeighborSelectionFactor; nbrK[22] = k - NeighborSelectionFactor;
		//
		nbrI[23] = i - NeighborSelectionFactor; nbrJ[23] = j + NeighborSelectionFactor; nbrK[23] = k + NeighborSelectionFactor;
		//
		nbrI[24] = i - NeighborSelectionFactor; nbrJ[24] = j - NeighborSelectionFactor; nbrK[24] = k - NeighborSelectionFactor;
		//
		nbrI[25] = i - NeighborSelectionFactor; nbrJ[25] = j - NeighborSelectionFactor; nbrK[25] = k + NeighborSelectionFactor;
		/////////////////////////////////////////
		
		// loop through all the neighbours
		
		for ( int l = 0; l < 26; l++) 
		{
			if ((nbrI[l] >= 0 && nbrI[l] < m_width) && (nbrJ[l] >= 0 && nbrJ[l] < m_height)&& (nbrK[l] >= 0 && nbrK[l] < m_depth)) 
			{
				int index = CUtility::CalculateIndex((int)nbrI[l],(int)nbrJ[l],(int)nbrK[l],m_width, m_height);
				if ((m_buffer[index] == 1) && connComp[l] == 0)
				{
					connComp[l] = labelVal;
					Point objectVoxel;
					objectVoxel.x = nbrI[l];
					objectVoxel.y = nbrJ[l];
					objectVoxel.z = nbrK[l];

					Q.push_back(objectVoxel);

//					labelVal = labelVal + 1;
					++labelVal;
				}
			}
		}
	}

	Q.clear();

	switch(direction)
	{
		case TOP_BOTTOM:
			if(connComp[4] != 0 && connComp[5] != 0 && connComp[4] != connComp[5])
				disconnected = true;
			break;
		case LEFT_RIGHT:
			if(connComp[0] != 0 && connComp[1] != 0 && connComp[0] != connComp[1])
				disconnected = true;
			break;
		case FRONT_BACK:
			if(connComp[2] != 0 && connComp[3] != 0 && connComp[2] != connComp[3])
				disconnected = true;
			break;
		default:
			break;
	}
	delete[] connComp;
	connComp = NULL;

	return disconnected;
}

} // namespace polygonization
} // namespace lssplib

