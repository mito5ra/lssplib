#include "Modeling/PointCloudGenerator/ThreeDDataProcessor.h"
#include <algorithm>
#include <math.h>

namespace lssplib
{
namespace polygonization
{

//! Constructor of CThreeDDataProcessor class
CThreeDDataProcessor::CThreeDDataProcessor(void)
: ThreeDNeighbours()
, PointsFromAlgm()
, normalsOfPoints()
, m_subvoxelListForPointGen()
, m_subvoxelListForNormalGen()
, m_voxel_x(0.0f)
, m_voxel_y(0.0f)
, m_voxel_z(0.0f)
, m_width(0)
, m_height(0)
, m_imageSize(0)
, m_buffer(NULL)
, m_thickData(NULL)
, m_count(0)
, m_divFactor(0)
, m_IsOutBoundSubvox(false)
, m_NeighborSelectionFactor(0.0f)
, m_ConnComp(new ConnectedComponentAnalyser())
, m_NormalFlipper(NULL)
, m_X_voxel_div(0.0f)
, m_Y_voxel_div(0.0f)
, m_Z_voxel_div(0.0f)
, m_viewpoint_x(0.0f)
, m_viewpoint_y(0.0f)
, m_viewpoint_z(0.0f)
{
	memset(WxFilterFront, 0, sizeof(WxFilterFront));
	memset(WxFilterMiddle, 0, sizeof(WxFilterMiddle));
	memset(WxFilterBack, 0, sizeof(WxFilterBack));

	memset(WyFilterFront, 0, sizeof(WyFilterFront));
	memset(WyFilterMiddle, 0, sizeof(WyFilterMiddle));
	memset(WyFilterBack, 0, sizeof(WyFilterBack));

	memset(WzFilterFront, 0, sizeof(WzFilterFront));
	memset(WzFilterMiddle, 0, sizeof(WzFilterMiddle));
	memset(WzFilterBack, 0, sizeof(WzFilterBack));

	memset(matrixInfront, 0, sizeof(matrixInfront));
	memset(matrixMiddle, 0, sizeof(matrixMiddle));
	memset(matrixBack, 0, sizeof(matrixBack));
}

CThreeDDataProcessor::CThreeDDataProcessor(CDataManager* dataManager)
: ThreeDNeighbours()
, PointsFromAlgm()
, normalsOfPoints()
, m_subvoxelListForPointGen()
, m_subvoxelListForNormalGen()
, m_voxel_x(dataManager->GetVoxelSizeX())
, m_voxel_y(dataManager->GetVoxelSizeY())
, m_voxel_z(dataManager->GetVoxelSizeZ())
, m_width(dataManager->GetVolumeWidth())
, m_height(dataManager->GetVolumeHeight())
, m_imageSize(dataManager->GetVolumeSize())
, m_buffer(dataManager->GetBinarydata())
, m_thickData(NULL)
, m_count(0)
, m_divFactor(0)
, m_IsOutBoundSubvox(false)
, m_NeighborSelectionFactor(0.0f)
, m_ConnComp(new ConnectedComponentAnalyser(dataManager))
, m_NormalFlipper(NULL)
, m_X_voxel_div(0.0f)
, m_Y_voxel_div(0.0f)
, m_Z_voxel_div(0.0f)
, m_viewpoint_x(0.0f)
, m_viewpoint_y(0.0f)
, m_viewpoint_z(0.0f)
{
	memset(WxFilterFront, 0, sizeof(WxFilterFront));
	memset(WxFilterMiddle, 0, sizeof(WxFilterMiddle));
	memset(WxFilterBack, 0, sizeof(WxFilterBack));

	memset(WyFilterFront, 0, sizeof(WyFilterFront));
	memset(WyFilterMiddle, 0, sizeof(WyFilterMiddle));
	memset(WyFilterBack, 0, sizeof(WyFilterBack));

	memset(WzFilterFront, 0, sizeof(WzFilterFront));
	memset(WzFilterMiddle, 0, sizeof(WzFilterMiddle));
	memset(WzFilterBack, 0, sizeof(WzFilterBack));

	memset(matrixInfront, 0, sizeof(matrixInfront));
	memset(matrixMiddle, 0, sizeof(matrixMiddle));
	memset(matrixBack, 0, sizeof(matrixBack));

	dataManager->GetDataForThickBuilder(&m_thickData);
}

//! Destructor of CThreeDDataProcessor class
CThreeDDataProcessor::~CThreeDDataProcessor(void)
{
	ClearBuffers();
	delete m_ConnComp;
}

//! Implementaion of void CThreeDDataProcessor::SetDivParams(int divFactor,float neighborFactor)
//! Sets the parameters required to calculate neighbors and divison factors
void CThreeDDataProcessor::SetDivParams(int divFactor,float neighborFactor)
{
	m_divFactor = divFactor;
	m_NeighborSelectionFactor = neighborFactor;
}

//! Implementaion of void CThreeDDataProcessor::SeIsSubvox()
//! Flag to identify sub voxel
void CThreeDDataProcessor::SeIsSubvox()
{
	m_IsOutBoundSubvox = true;
}

//! Implementaion of void CThreeDDataProcessor::SetListForPointGen(vector<Point> subvoxelListForPointGen )
//! Sets the list to check if a subvox is object in the point generation for subvoxels
void CThreeDDataProcessor::SetListForPointGen(vector<Point> subvoxelListForPointGen )
{
	m_subvoxelListForPointGen = subvoxelListForPointGen;
}

//! Implementaion of void CThreeDDataProcessor::SetListForNormalGen( vector<Point> subvoxelListForNormalGen)
//! Sets the list to check if a subvox is object in the normal generation for subvoxels
void CThreeDDataProcessor::SetListForNormalGen( vector<Point> subvoxelListForNormalGen)
{
	m_subvoxelListForNormalGen = subvoxelListForNormalGen;
}

//! Implementaion of void CThreeDDataProcessor::ResetIsOutBoundSubvox()
//! Flag to identify voxel
void CThreeDDataProcessor::ResetIsOutBoundSubvox()
{
	m_IsOutBoundSubvox = false;
}

//! Implementaion of void CThreeDDataProcessor::SetVoxelDivisionFactor()
//! Calculate voxel division factor to identify position of points to be placed 
void CThreeDDataProcessor::SetVoxelDivisionFactor()
{
	m_X_voxel_div = m_voxel_x/m_divFactor;
	m_Y_voxel_div = m_voxel_y/m_divFactor;
	m_Z_voxel_div = m_voxel_z/m_divFactor;
}

//! Implementaion of void CThreeDDataProcessor::Calculate3D_6Neighbours(float x,float y,float z)
//! Calculate three D six neighbours of a voxel or subvoxel
void CThreeDDataProcessor::Calculate3D_6Neighbours(float x,float y,float z)
{
	ClearNeighbourList();

	ThreeDNeighbours.clear();

	Point right ;
	right.x = x + m_NeighborSelectionFactor;
	right.y = y;
	right.z = z;
	ThreeDNeighbours.push_back(right);

	Point left ;
	left.x = x - m_NeighborSelectionFactor;
	left.y = y;
	left.z  = z;
	ThreeDNeighbours.push_back(left);

	Point front ;
	front.x = x;
	front.y = y;
	front.z = z - m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(front);

	Point back ;
	back.x = x;
	back.y = y;
	back.z = z + m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(back);

	Point up ;
	up.x = x;
	up.y = y - m_NeighborSelectionFactor;
	up.z = z;
	ThreeDNeighbours.push_back(up);

	Point down ;
	down.x = x;
	down.y = y + m_NeighborSelectionFactor;
	down.z = z;
	ThreeDNeighbours.push_back(down);
}

//! Implementaion of oid CThreeDDataProcessor::Calculate3D_18Neighbours(float x,float y,float z)
//! Calculate three D eighteen neighbours of a voxel or subvoxel
void CThreeDDataProcessor::Calculate3D_18Neighbours(float x,float y,float z)
{
	Calculate3D_6Neighbours(x,y,z);
	//Calculate for 12 edges
	
	//along vertical planes
	Point rightFront ;
	rightFront.x = x + m_NeighborSelectionFactor;
	rightFront.y = y;
	rightFront.z = z - m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(rightFront);

	Point leftFront ;
	leftFront.x = x - m_NeighborSelectionFactor;
	leftFront.y = y;
	leftFront.z = z - m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(leftFront);

	Point upFront ;
	upFront.x = x;
	upFront.y = y - m_NeighborSelectionFactor;
	upFront.z = z - m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(upFront);

	Point downFront ;
	downFront.x = x;
	downFront.y = y + m_NeighborSelectionFactor;
	downFront.z = z - m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(downFront);

	Point rightBack ;
	rightBack.x = x + m_NeighborSelectionFactor;
	rightBack.y = y;
	rightBack.z = z + m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(rightBack);

	Point leftBack ;
	leftBack.x = x - m_NeighborSelectionFactor;
	leftBack.y = y;
	leftBack.z = z + m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(leftBack);

	Point upBack ;
	upBack.x = x;
	upBack.y = y  - m_NeighborSelectionFactor;
	upBack.z = z + m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(upBack);

	Point downBack ;
	downBack.x = x;
	downBack.y = y + m_NeighborSelectionFactor;
	downBack.z = z + m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(downBack);

	//along upper and lower planes

	Point upperLeft ;
	upperLeft.x = x - m_NeighborSelectionFactor;
	upperLeft.y = y - m_NeighborSelectionFactor;
	upperLeft.z = z;
	ThreeDNeighbours.push_back(upperLeft);

	Point upperRight ;
	upperRight.x = x + m_NeighborSelectionFactor;
	upperRight.y = y - m_NeighborSelectionFactor;
	upperRight.z = z;
	ThreeDNeighbours.push_back(upperRight);

	Point lowerLeft ;
	lowerLeft.x = x - m_NeighborSelectionFactor;
	lowerLeft.y = y + m_NeighborSelectionFactor;
	lowerLeft.z = z;
	ThreeDNeighbours.push_back(lowerLeft);

	Point lowerRight ;
	lowerRight.x = x + m_NeighborSelectionFactor;
	lowerRight.y = y + m_NeighborSelectionFactor;
	lowerRight.z = z;
	ThreeDNeighbours.push_back(lowerRight);
}

//! Implementaion of void CThreeDDataProcessor::Calculate3D_26Neighbours(float x,float y,float z)
//! Calculate three D twenty six neighbours of a voxel or subvoxel
void CThreeDDataProcessor::Calculate3D_26Neighbours(float x,float y,float z)
{
	Calculate3D_18Neighbours(x,y,z);
	
	Point RightbelowFront;
	RightbelowFront.x = x + m_NeighborSelectionFactor;
	RightbelowFront.y = y + m_NeighborSelectionFactor;
	RightbelowFront.z = z - m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(RightbelowFront);
 
	Point RightBelowBack;
	RightBelowBack.x = x + m_NeighborSelectionFactor;
	RightBelowBack.y = y + m_NeighborSelectionFactor;
	RightBelowBack.z = z + m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(RightBelowBack);

	Point RightUpFront;
	RightUpFront.x = x + m_NeighborSelectionFactor;
	RightUpFront.y = y - m_NeighborSelectionFactor;
	RightUpFront.z = z - m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(RightUpFront);

	Point RightUpBack;
	RightUpBack.x = x + m_NeighborSelectionFactor;
	RightUpBack.y = y - m_NeighborSelectionFactor;
	RightUpBack.z = z + m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(RightUpBack);

	Point LeftBelowFront;	
	LeftBelowFront.x = x - m_NeighborSelectionFactor;
	LeftBelowFront.y = y + m_NeighborSelectionFactor;
	LeftBelowFront.z = z - m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(LeftBelowFront);
	
	Point LeftBelowBack;
	LeftBelowBack.x = x - m_NeighborSelectionFactor;
	LeftBelowBack.y = y + m_NeighborSelectionFactor;
	LeftBelowBack.z = z + m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(LeftBelowBack);
	
	Point LeftUpFront;
	LeftUpFront.x = x - m_NeighborSelectionFactor;
	LeftUpFront.y = y - m_NeighborSelectionFactor;
	LeftUpFront.z = z - m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(LeftUpFront);	

	Point LeftUpBack;
	LeftUpBack.x = x - m_NeighborSelectionFactor;
	LeftUpBack.y = y - m_NeighborSelectionFactor;
	LeftUpBack.z = z + m_NeighborSelectionFactor;
	ThreeDNeighbours.push_back(LeftUpBack);
}

//! Implementaion of void CThreeDDataProcessor::Create26NEighbourMatrices(float x,float y,float z)
//! Generates neighbour matrix for the generation of normal using 26 neighbours of a voxel or subvoxel
void CThreeDDataProcessor::Create26NEighbourMatrices(float x,float y,float z)
{
	
	Calculate3D_26Neighbours(x, y, z);
	matrixInfront[0][0] = ThreeDNeighbours[24];
	matrixInfront[0][1] = ThreeDNeighbours[8];
	matrixInfront[0][2] = ThreeDNeighbours[20];
	matrixInfront[1][0] = ThreeDNeighbours[7];
	matrixInfront[1][1] = ThreeDNeighbours[2];
	matrixInfront[1][2] = ThreeDNeighbours[6];
	matrixInfront[2][0] = ThreeDNeighbours[22];
	matrixInfront[2][1] = ThreeDNeighbours[9];
	matrixInfront[2][2] = ThreeDNeighbours[18];

	matrixMiddle[0][0] = ThreeDNeighbours[14];
	matrixMiddle[0][1] = ThreeDNeighbours[4];
	matrixMiddle[0][2] = ThreeDNeighbours[15];
	matrixMiddle[1][0] = ThreeDNeighbours[1];
	Point itSelf;
	itSelf.x = x;
	itSelf.y = y;
	itSelf.z = z;
	matrixMiddle[1][1] = itSelf;//x y z
	matrixMiddle[1][2] = ThreeDNeighbours[0];
	matrixMiddle[2][0] = ThreeDNeighbours[16];
	matrixMiddle[2][1] = ThreeDNeighbours[5];
	matrixMiddle[2][2] = ThreeDNeighbours[17];

	matrixBack[0][0] = ThreeDNeighbours[25];
	matrixBack[0][1] = ThreeDNeighbours[12];
	matrixBack[0][2] = ThreeDNeighbours[21];
	matrixBack[1][0] = ThreeDNeighbours[11];
	matrixBack[1][1] = ThreeDNeighbours[3];
	matrixBack[1][2] = ThreeDNeighbours[10];
	matrixBack[2][0] = ThreeDNeighbours[23];
	matrixBack[2][1] = ThreeDNeighbours[13];
	matrixBack[2][2] = ThreeDNeighbours[19];

	
}

//! Implementaion of void CThreeDDataProcessor::CreateWxFilter()
//! Generates The Weight matrix in the x direction for the generation of normal as per Tiede [Tie99]
void CThreeDDataProcessor::CreateWxFilter()
{
	
	WxFilterFront[0][0] = -WEIGHT26;
	WxFilterFront[0][1] =	0;
	WxFilterFront[0][2] = WEIGHT26;
	WxFilterFront[1][0] = -WEIGHT18;
	WxFilterFront[1][1] = 0;
	WxFilterFront[1][2] = WEIGHT18;
	WxFilterFront[2][0] = -WEIGHT26;
	WxFilterFront[2][1] = 0;
	WxFilterFront[2][2] = WEIGHT26;

	WxFilterMiddle[0][0] = -WEIGHT18;
	WxFilterMiddle[0][1] = 0;
	WxFilterMiddle[0][2] = WEIGHT18;
	WxFilterMiddle[1][0] = -WEIGHT6;
	WxFilterMiddle[1][1] = 0;
	WxFilterMiddle[1][2] = WEIGHT6;
	WxFilterMiddle[2][0] = -WEIGHT18;
	WxFilterMiddle[2][1] = 0;
	WxFilterMiddle[2][2] = WEIGHT18;

	WxFilterBack[0][0] = -WEIGHT26;
	WxFilterBack[0][1] = 0;
	WxFilterBack[0][2] = WEIGHT26;
	WxFilterBack[1][0] = -WEIGHT18;
	WxFilterBack[1][1] = 0;
	WxFilterBack[1][2] = WEIGHT18;
	WxFilterBack[2][0] = -WEIGHT26;
	WxFilterBack[2][1] = 0;
	WxFilterBack[2][2] = WEIGHT26;
}

//! Implementaion of void CThreeDDataProcessor::CreateWyFilter()
//! Generates The Weight matrix in the y direction for the generation of normal as per Tiede [Tie99]
void CThreeDDataProcessor::CreateWyFilter()
{
	
	WyFilterFront[0][0] = -WEIGHT26;
	WyFilterFront[0][1] = -WEIGHT18;
	WyFilterFront[0][2] = -WEIGHT26;
	WyFilterFront[1][0] = 0;
	WyFilterFront[1][1] = 0;
	WyFilterFront[1][2] = 0;
	WyFilterFront[2][0] = WEIGHT26;
	WyFilterFront[2][1] = WEIGHT18;
	WyFilterFront[2][2] = WEIGHT26;

	WyFilterMiddle[0][0] = -WEIGHT18;
	WyFilterMiddle[0][1] = -WEIGHT6;
	WyFilterMiddle[0][2] = -WEIGHT18;
	WyFilterMiddle[1][0] = 0;
	WyFilterMiddle[1][1] = 0;
	WyFilterMiddle[1][2] = 0;
	WyFilterMiddle[2][0] = WEIGHT18;
	WyFilterMiddle[2][1] = WEIGHT6;
	WyFilterMiddle[2][2] = WEIGHT18;

	WyFilterBack[0][0] = -WEIGHT26;
	WyFilterBack[0][1] = -WEIGHT18;
	WyFilterBack[0][2] = -WEIGHT26;
	WyFilterBack[1][0] = 0;
	WyFilterBack[1][1] = 0;
	WyFilterBack[1][2] = 0;
	WyFilterBack[2][0] = WEIGHT26;
	WyFilterBack[2][1] = WEIGHT18;
	WyFilterBack[2][2] = WEIGHT26;
}

//! Implementaion of void CThreeDDataProcessor::CreateWzFilter()
//! Generates The Weight matrix in the z direction for the generation of normal as per Tiede [Tie99]
void CThreeDDataProcessor::CreateWzFilter()
{
	WzFilterFront[0][0] = -WEIGHT26;
	WzFilterFront[0][1] = -WEIGHT18;
	WzFilterFront[0][2] = -WEIGHT26;
	WzFilterFront[1][0] = -WEIGHT18;
	WzFilterFront[1][1] = -WEIGHT6;
	WzFilterFront[1][2] = -WEIGHT18;
	WzFilterFront[2][0] = -WEIGHT26;
	WzFilterFront[2][1] = -WEIGHT18;
	WzFilterFront[2][2] = -WEIGHT26;

	WzFilterMiddle[0][0] = 0;
	WzFilterMiddle[0][1] = 0;
	WzFilterMiddle[0][2] = 0;
	WzFilterMiddle[1][0] = 0;
	WzFilterMiddle[1][1] = 0;
	WzFilterMiddle[1][2] = 0;
	WzFilterMiddle[2][0] = 0;
	WzFilterMiddle[2][1] = 0;
	WzFilterMiddle[2][2] = 0;

	WzFilterBack[0][0] = WEIGHT26;
	WzFilterBack[0][1] = WEIGHT18;
	WzFilterBack[0][2] = WEIGHT26;
	WzFilterBack[1][0] = WEIGHT18;
	WzFilterBack[1][1] = WEIGHT6;
	WzFilterBack[1][2] = WEIGHT18;
	WzFilterBack[2][0] = WEIGHT26;
	WzFilterBack[2][1] = WEIGHT18;
	WzFilterBack[2][2] = WEIGHT26;
}

//! Implementaion of bool CThreeDDataProcessor::GeneratePointsUsingAlgm(float x,float y,float z)
//! Implements Schumann's six step algorithm for voxel as well as sub voxel
bool CThreeDDataProcessor::GeneratePointsUsingAlgm(float x,float y,float z)
{
	ThreeDNeighbours.clear();
	PointsFromAlgm.clear();
	normalsOfPoints.clear();

	float xval, yval,zval ;

	xval = (m_voxel_x * x);
	yval = (m_voxel_y * y );
	zval = (m_voxel_z * z );

	int index[NUM_FACES] = {0,0,0,0,0,0};

	Calculate3D_6Neighbours(x, y, z);

	Point outBoundVoxel;
	outBoundVoxel.x = x;
	outBoundVoxel.y = y;
	outBoundVoxel.z = z;

	int flags[NUM_FACES] = {0,0,0,0,0,0}, anyNeighbour = 0,id = 0,i=0;

	m_count = 0;

	std::vector<Point>::iterator it;
	
	//int indx;

	if(!m_IsOutBoundSubvox)//checks if voxel or subvoxel
	{
		for(it = ThreeDNeighbours.begin(); it != ThreeDNeighbours.end(); ++it)
		{
			index[i] = CUtility::CalculateIndex((int)(it)->x, (int)(it)->y, (int)(it)->z, m_width, m_height);
			i++;
		}
		//calculates how many among three D six neighbours are object (thick objects)
		for(id=0 ; id<NUM_FACES ; id++)
		{
			if(CUtility::CheckIfObject(index[id], m_thickData, m_imageSize))
			{
				anyNeighbour = 1;
				flags[id] = 1;
				m_count = m_count + 1;
			}
		}
	}
	else //for subvoxel
	{
		for(it = ThreeDNeighbours.begin(); it != ThreeDNeighbours.end(); ++it)
		{
			std::vector<Point>::iterator iter = find(m_subvoxelListForPointGen.begin(), m_subvoxelListForPointGen.end(), *it);
			
			if (iter != m_subvoxelListForPointGen.end())
			{
				flags[id] = 1;
				m_count = m_count + 1;			
				anyNeighbour = 1;
			}
			id++;
		}
		
	}
	
	if(anyNeighbour == 1)//Any voxel among 3D_6 is a object voxel
	{
		if(m_count == 1)//One among 3D_6 is a object voxel
		{   
			int index;
			for(index =0 ; index<NUM_FACES ; index++)
			{
				if(flags[index] == 1)
				{
					PlacePoints(index ,xval, yval, zval);
					GetNormalForOnePoint(x, y, z);
					return true;
				}
			}
		}
		else if(m_count ==2)//Two among 3D_6 is a object voxel
		{
			if(flags[0] == 1 &&  flags[1] == 1) //Right and left
			{
				if(CheckIfDifferentStructure(LEFT_RIGHT, outBoundVoxel))
				{
					PlacePoints(0, 1, xval, yval, zval);
					
				}
				else
				{
					AddPoint(xval, yval, zval);
					GetNormalForOnePoint(x, y, z);
				}
			}
			else if(flags[2] == 1 &&  flags[3] == 1) //Front and back
			{
				if(CheckIfDifferentStructure(FRONT_BACK, outBoundVoxel))
				{
					PlacePoints(2, 3, xval, yval, zval);
				}
				else
				{
					AddPoint(xval, yval, zval);
					GetNormalForOnePoint(x, y, z);
				}
			}
			else if(flags[4] == 1 &&  flags[5] == 1) //up and down
			{
				if(CheckIfDifferentStructure(TOP_BOTTOM, outBoundVoxel))
				{
					PlacePoints(4, 5, xval, yval, zval);
				}
				else
				{	
					AddPoint(xval, yval, zval);
					GetNormalForOnePoint(x, y, z);
				}
			}
			else
			{
				AddPoint(xval, yval, zval);
				GetNormalForOnePoint(x, y, z);
			}
			return true;							
		}
		else if(m_count == 4)//Four among 3D_6 is a object voxel
		{
			if(flags[0] == 1 && flags[1] == 1  && flags[2] == 1 && flags[3] == 1)
				PlacePoints(0, 1, 2, 3, xval, yval, zval);
			else if(flags[0] == 1 && flags[1] == 1  && flags[4] == 1 && flags[5] == 1)
				PlacePoints(0, 1, 4, 5, xval, yval, zval);
			else if(flags[2] == 1 && flags[3] == 1  && flags[4] == 1 && flags[5] == 1)
				PlacePoints(2, 3, 4, 5, xval, yval, zval);
			else
			{
				AddPoint(xval, yval, zval);
				GetNormalForOnePoint(x, y, z);
			}
			return true;
								
		}
		else if(m_count == 5)//Five among 3D_6 is a object voxel
		{
			for(int i=0;i< 6; i++)
			{
				if(flags[i] == 0)
				{
					PlacePointsForFive(i, xval, yval, zval);
					GetNormalForOnePoint(x, y, z);
					return true;
				}
			}
		}
		else if(m_count == NUM_FACES)//Six among 3D_6 is a object voxel, it is ahole and is ignored
		{	
			return false;
		}
		else //Three among 3D_6 is a object voxel
		{
			AddPoint(xval, yval, zval);
			GetNormalForOnePoint(x, y, z);
			return true;
		}
	}
	else//No among 3D_6 is a object voxel
	{
		return false;
	}
	return false;

}

//! Implementaion of void CThreeDDataProcessor::PlacePoints(int neighbour, float x, float y, float z)
//! As per first step of algorithm  one point is placed where one object vox/subvox is encountered in the peer of candiadate vox/subvox
void CThreeDDataProcessor::PlacePoints(int neighbour, float x, float y, float z)
{
	switch(neighbour)
	{
		//right
	case 0:AddPoint(x + (m_X_voxel_div), y, z);
		break;
		//left
	case 1:AddPoint(x - (m_X_voxel_div), y, z);
		break;
		//front
	case 2:AddPoint(x, y, z-(m_Z_voxel_div));
		break;
		//back
	case 3:AddPoint(x, y, z+(m_Z_voxel_div));
		break;
		//up
	case 4:AddPoint(x, y-(m_Y_voxel_div), z);
		break;
		//down
	case 5:AddPoint(x, y+(m_Y_voxel_div), z);
		break;
	default:break;
	}

}

//! Implementaion of void CThreeDDataProcessor::PlacePoints(int nbr1, int nbr2, float x, float y,float z)
//! As per second step of algorithm  two points are placed where two object vox/subvox encounter in opposite sides of candidate voxel or subvoxel
void CThreeDDataProcessor::PlacePoints(int nbr1, int nbr2, float x, float y,float z)
{
	SetViewPoint(x, y, z);
	if(nbr1 == 0 && nbr2 == 1 )//right and left
	{
		AddPoint(x + (m_X_voxel_div),y, z);
		GetNormalForMorePoints(x + (m_X_voxel_div),y, z,LEFT);
		AddPoint(x - (m_X_voxel_div),y, z);
		GetNormalForMorePoints(x - (m_X_voxel_div),y, z,RIGHT);

	}
	else if(nbr1 == 2 && nbr2 == 3 )//front and back
	{
		AddPoint(x, y, z-(m_Z_voxel_div));
		GetNormalForMorePoints(x, y, z-(m_Z_voxel_div),FRONT);
		AddPoint(x, y, z+(m_Z_voxel_div));
		GetNormalForMorePoints(x, y, z+(m_Z_voxel_div),BACK);
	}
	else if(nbr1 == 4 && nbr2 == 5 )//up and down
	{
		AddPoint(x, y-(m_Y_voxel_div), z);
		GetNormalForMorePoints(x, y-(m_Y_voxel_div), z,UP);
		AddPoint(x, y+(m_Y_voxel_div), z);
		GetNormalForMorePoints(x, y+(m_Y_voxel_div), z,DOWN);
	}
	else
	{
		GetNormalForOnePoint(x, y, z);
		AddPoint(x,y,z);
	}

}

//! Implementaion of void CThreeDDataProcessor::PlacePoints(int nbr1,int  nbr2,int nbr3,int  nbr4, float x, float y, float z)
//! As per third step of algorithm four points are placed where four object vox/subvox are encountered in the neighborhood of vox/subvox
void CThreeDDataProcessor::PlacePoints(int nbr1,int  nbr2,int nbr3,int  nbr4, float x, float y, float z)
{
	SetViewPoint(x, y, z);
	//left right and front back
	if(nbr1 == 0 && nbr2 == 1 && nbr3 == 2 && nbr4 == 3)
	{
		AddPoint((m_X_voxel_div) + x , y, z);
		GetNormalForMorePoints((m_X_voxel_div) + x , y, z,RIGHT);
		AddPoint(x - (m_X_voxel_div), y, z);
		GetNormalForMorePoints(x - (m_X_voxel_div), y, z,LEFT);
		AddPoint(x, y, z - (m_Z_voxel_div));
		GetNormalForMorePoints(x, y, z - (m_Z_voxel_div),FRONT);
		AddPoint(x, y, z + (m_Z_voxel_div));
		GetNormalForMorePoints(x, y, z + (m_Z_voxel_div),BACK);
	}
	//left right up and down
	else if(nbr1 == 0 && nbr2 == 1 && nbr3 == 4 && nbr4 == 5)
	{
		AddPoint((m_X_voxel_div) + x , y, z);
		GetNormalForMorePoints((m_X_voxel_div) + x , y, z,RIGHT);
		AddPoint(x - (m_X_voxel_div), y, z);
		GetNormalForMorePoints(x - (m_X_voxel_div), y, z,LEFT);
		AddPoint(x, y - (m_Y_voxel_div), z);
		GetNormalForMorePoints(x, y - (m_Y_voxel_div), z,UP);
		AddPoint(x, y + (m_Y_voxel_div), z );
		GetNormalForMorePoints(x, y + (m_Y_voxel_div), z ,DOWN);

	}
	//up down front back
	else if(nbr1 == 2 && nbr2 == 3 && nbr3 == 4 && nbr4 == 5)
	{
		AddPoint(x, y - (m_Y_voxel_div), z);
		GetNormalForMorePoints(x, y - (m_Y_voxel_div), z,UP);
		AddPoint(x, y + (m_Y_voxel_div), z );
		GetNormalForMorePoints(x, y + (m_Y_voxel_div), z ,DOWN);
		AddPoint(x, y, z - (m_Z_voxel_div));
		GetNormalForMorePoints(x, y, z - (m_Z_voxel_div),FRONT);
		AddPoint(x, y, z + (m_Z_voxel_div));
		GetNormalForMorePoints(x, y, z + (m_Z_voxel_div),BACK);

	}
	else
	{
		GetNormalForOnePoint(x, y, z);
		AddPoint(x,y,z);
	}
}

//! Implementaion of void CThreeDDataProcessor::PlacePointsForFive(int notNeighbor, float x , float y , float z)
//! As per fourth step of algorithm one point is placed where five object vox/subvox are encountered in the neighborhood of vox/subvox
void CThreeDDataProcessor::PlacePointsForFive(int notNeighbor, float x , float y , float z)
{

	switch(notNeighbor)
	{
		//right one is not neighbour
	case 0:AddPoint(x - (m_X_voxel_div), y, z);
		break;
		//left one is not neighbour
	case 1:AddPoint(x + (m_X_voxel_div), y, z);
		break;
		//front one is not neighbour
	case 2:AddPoint(x, y, z+(m_Z_voxel_div));
		break;
		//back one is not neighbour
	case 3:AddPoint(x, y, z-(m_Z_voxel_div));
		break;
		//up one is not neighbour
	case 4:AddPoint(x, y+(m_Y_voxel_div), z);
		break;
		//down one is not neighbour
	case 5:AddPoint(x, y-(m_Y_voxel_div), z);
		break;
	default:break;
	}
}

//! Implementaion of void CThreeDDataProcessor::AddPoint(float x, float y, float z)
//! The computed points are added to the point list
void CThreeDDataProcessor::AddPoint(float x, float y, float z)
{
	Point point;
	point.x = x;
	point.y = y;
	point.z = z;
	PointsFromAlgm.push_back(point);
}

//! Implementaion of void CThreeDDataProcessor::GetNormalForOnePoint(float x,float y,float z)
//! In the case where one point is generated, normal is computed using weight matrix (pls refer: Tiede, U.: Realistische 3D-Visualisierung multiattributierter und multiparametrischer,Volumendaten, Universit舩 Hamburg, Fachbereich Informatik,Diss., 1999")
void CThreeDDataProcessor::GetNormalForOnePoint(float x,float y,float z)
{
	normalsOfPoints.clear();

	Create26NEighbourMatrices(x,y,z);
	CreateWxFilter();
	CreateWyFilter();
	CreateWzFilter();
	float Gradient_x = 0.0f,Gradient_y = 0.0f,Gradient_z = 0.0f;

	Gradient_x = Filter(matrixInfront,0) + Filter(matrixMiddle,1) + Filter(matrixBack,2);

	Gradient_y = Filter(matrixInfront,3) + Filter(matrixMiddle,4) + Filter(matrixBack,5);

	Gradient_z = Filter(matrixInfront,6) + Filter(matrixMiddle,7) + Filter(matrixBack,8);

	//Converting normal into world coordinates
	Gradient_x = Gradient_x * m_voxel_x;
	Gradient_y = Gradient_y * m_voxel_y;
	Gradient_z = Gradient_z * m_voxel_z;
	
	//normalisation of normals
	float normal_sum =  (float)sqrt((Gradient_x * Gradient_x) + (Gradient_y * Gradient_y) + (Gradient_z * Gradient_z));
	float normal_x = (Gradient_x/normal_sum) * -1;
	float normal_y = (Gradient_y/normal_sum) * -1;
	float normal_z = (Gradient_z/normal_sum) * -1;
	AddNormal(normal_x,normal_y, normal_z );
	
}

//! Implementaion of void CThreeDDataProcessor::GetNormalForMorePoints(float placed_x,float placed_y,float placed_z,int direction)
//! In the case more points than one is  generated, normal for each point is computed based on gradient
void CThreeDDataProcessor::GetNormalForMorePoints(float placed_x,float placed_y,float placed_z,int direction)
{

	float x1, y1, z1, x2, y2, z2, dx, dy, dz, normal_sum;
	Point PlacedPoint;
	PlacedPoint.x = placed_x;
	PlacedPoint.y = placed_y;
	PlacedPoint.z = placed_z;
	//To take relative points to calculate gradient based on direction of the point placed
	if(direction == 0 || direction == 1)//right left
	{
		x1 = placed_x;
		y1 = placed_y + m_Y_voxel_div;
		z1 = placed_z - m_Z_voxel_div;
		x2 = placed_x;
		y2 = placed_y + m_Y_voxel_div;
		z2 = placed_z + m_Z_voxel_div;
	}
	else if(direction == 2 || direction == 3)//front back
	{
		x1 = placed_x - m_X_voxel_div;
		y1 = placed_y + m_Y_voxel_div;
		z1 = placed_z;
		x2 = placed_x + m_X_voxel_div;
		y2 = placed_y + m_Y_voxel_div;
		z2 = placed_z;
	}
	else if(direction == 4 || direction == 5)//up down
	{
		x1 = placed_x - m_X_voxel_div;
		y1 = placed_y;
		z1 = placed_z - m_Z_voxel_div;
		x2 = placed_x + m_X_voxel_div;
		y2 = placed_y;
		z2 = placed_z - m_Z_voxel_div;
	}
	else return;

	dx = (y2 - y1) * (placed_z - z1) - (z2 - z1) * (placed_y - y1);
	dy = (z2 - z1) * (placed_x - x1) - (x2 - x1) * (placed_z - z1);
	dz = (x2 - x1) * (placed_y - y1) - (y2 - y1) * (placed_x - x1);
	
	normal_sum =  (float)sqrt((dx * dx) + (dy * dy) + (dz * dz));
	float normal_x = dx/normal_sum;
	float normal_y = dy/normal_sum;
	float normal_z = dz/normal_sum;
	Point generatedNormal;
	generatedNormal.x = normal_x;
	generatedNormal.y = normal_y;
	generatedNormal.z = normal_z;
	m_NormalFlipper->FlipNormalDirection(PlacedPoint, generatedNormal,m_viewpoint_x, m_viewpoint_y, m_viewpoint_z);//In order to set the normal pointing towards the voxel center(In case of more points)(page no 70: Visualisierung baumartiger anatomischer Strukturen mit MPU Implicits,section:Fall 2)
	AddNormal(generatedNormal.x,generatedNormal.y, generatedNormal.z );

}

//! Implementaion of float CThreeDDataProcessor::Filter(Point matrix[3][3],int filterSelector)
//! Calculate partial derivative using weight matrix and neighbor matrix for generating normal for one point placing case
float CThreeDDataProcessor::Filter(Point matrix[3][3],int filterSelector)
{
	int index;
	float Gradient = 0.0f;
	Point subVoxToSearch;

	float Filter[3][3];
	switch(filterSelector)
	{
		case 0:memcpy(Filter,WxFilterFront,sizeof(WxFilterFront));
			break;
		case 1:memcpy(Filter,WxFilterMiddle,sizeof(WxFilterMiddle));
			break;
		case 2:memcpy(Filter,WxFilterBack,sizeof(WxFilterBack));
			break;
		case 3:memcpy(Filter,WyFilterFront,sizeof(WyFilterFront));
			break;
		case 4:memcpy(Filter,WyFilterMiddle,sizeof(WyFilterMiddle));
			break;
		case 5:memcpy(Filter,WyFilterBack,sizeof(WyFilterBack));
			break;
		case 6:memcpy(Filter,WzFilterFront,sizeof(WzFilterFront));
			break;
		case 7:memcpy(Filter,WzFilterMiddle,sizeof(WzFilterMiddle));
			break;
		case 8:memcpy(Filter,WzFilterBack,sizeof(WzFilterBack));
		   break;

	}

	for(int i=0;i < FILTER_WINDOW_SIZE; i++)
	{
		for(int j=0; j< FILTER_WINDOW_SIZE; j++)
		{
			index = CUtility::CalculateIndex((int)matrix[i][j].x,(int) matrix[i][j].y, (int)matrix[i][j].z, m_width, m_height);
			if(!m_IsOutBoundSubvox)
			{
				if(CUtility::CheckIfObject(index, m_thickData, m_imageSize))
				{
					Gradient = Gradient + Filter[i][j];
				}
			}
			else      //for subvoxel
			{
				subVoxToSearch.x = matrix[i][j].x;
				subVoxToSearch.y = matrix[i][j].y;
				subVoxToSearch.z = matrix[i][j].z;
				std::vector<Point>::iterator iter = find(m_subvoxelListForNormalGen.begin(), m_subvoxelListForNormalGen.end(), subVoxToSearch);
				if (iter != m_subvoxelListForNormalGen.end())
				{
					Gradient = Gradient + Filter[i][j];
				}
			}
		}
	}
	return Gradient;
}

//! Implementaion of void CThreeDDataProcessor::setViewPoint(float x,float y,float z)
//! Sets the viewpoint towards which the normal to be flipped
//!
//! @param[in] x xの座標値
//! @param[in] y yの座標値
//! @param[in] z zの座標値
void CThreeDDataProcessor::SetViewPoint(float x,float y,float z)
{
	m_viewpoint_x = x;
	m_viewpoint_y = y;
	m_viewpoint_z = z;
}

//! Implementaion of void CThreeDDataProcessor::AddNormal(float x,float y,float z)
//! The computed normals are added to the normal list
void CThreeDDataProcessor::AddNormal(float x,float y,float z)
{
	Point normal;
	normal.x = x;
	normal.y = y;
	normal.z = z;
	normalsOfPoints.push_back(normal);
}

//! Implementaion of bool CThreeDDataProcessor::checkIfDifferentStructure(int direction, Point &outBoundVoxel)
//! Checks if the considering voxels in the opposite directions are connected or not
bool CThreeDDataProcessor::CheckIfDifferentStructure(int direction, Point &outBoundVoxel)
{	
	return m_ConnComp->CheckFloodfillConnectivity(direction, outBoundVoxel);
}

//! Implementaion of void CThreeDDataProcessor::GetPointsFromAlgm(vector<Point> &_PointsFromAlgm)
//! populates the vector of points generated after 6 step implementaion for each outer boundary vox/subvox
void CThreeDDataProcessor::GetPointsFromAlgm(vector<Point>& _PointsFromAlgm)
{
	_PointsFromAlgm = PointsFromAlgm;
}

//! Implementaion of void CThreeDDataProcessor::GetNormalsOfPoints(vector<Point> &_normalsOfPoints)
//! populates the vector of normals generated after 6 step implementaion for each outer boundary vox/subvox
void CThreeDDataProcessor::GetNormalsOfPoints(vector<Point> &_normalsOfPoints)
{
	_normalsOfPoints = normalsOfPoints;
}

//! Implementaion of vector<Point> CThreeDDataProcessor::GetNeighbourList()
//! Return the respective neighboulist(can be 3D-6, 3D-18 or 3D-26)
vector<Point> CThreeDDataProcessor::GetNeighbourList()
{
	return ThreeDNeighbours;
}

//! Implementaion of  CThreeDDataProcessor::ClearNeighbourList()
//! Clears the neighbor list
void CThreeDDataProcessor::ClearNeighbourList()
{	
	ThreeDNeighbours.clear();
}

//! Implementaion of void CThreeDDataProcessor::ClearPointList()
//! Clears the lists associated with point and normal generation
void CThreeDDataProcessor::ClearPointList()
{	
	PointsFromAlgm.clear();
	normalsOfPoints.clear();
	m_subvoxelListForNormalGen.clear();
	m_subvoxelListForPointGen.clear();
}

//! Implementaion of void CThreeDDataProcessor::ClearBuffers()
void CThreeDDataProcessor::ClearBuffers()
{
	ClearNeighbourList();
	ClearPointList();
}

} // namespace polygonization
} // namespace lssplib

