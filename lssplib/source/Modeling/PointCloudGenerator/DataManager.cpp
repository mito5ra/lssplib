#include "Modeling/PointCloudGenerator/DataManager.h"

namespace lssplib
{
namespace polygonization
{

//! Constructor of CDataManager class
CDataManager::CDataManager(void)
: m_binarydata(NULL)
, m_imageWidth(0)
, m_imageHeight(0)
, m_imageDepth(0)
, m_ImageSize(0)
, m_voxel_size_x(0.0f)
, m_voxel_size_y(0.0f)
, m_voxel_size_z(0.0f)
, m_bufferX()
, m_bufferY()
, m_bufferZ()
, m_objMorphOperator(new CMorphologicalOperator())
, m_thickData(NULL)
, m_thinData(NULL)
, m_erodedData(NULL)
, m_thinOutBoundData(NULL)
, m_thinOutBoundDataWithThin(NULL)
{
}

//! Destructor of CDataManager class
CDataManager::~CDataManager(void)
{
	delete m_objMorphOperator;
}

//! Initializing singleton Instance
//CDataManager*  CDataManager::m_DataManager = 0;

//!Implementaion of CDataManager* CDataManager::getInstance()
//CDataManager* CDataManager::getInstance()
//{
//	if ( ! m_DataManager )
//	{
//		m_DataManager    = new CDataManager();
//	}
//	return m_DataManager;
//}

//!Implementaion of unsigned char* CDataManager::GetBinarydata()
//unsigned char* CDataManager::GetBinarydata()
//{
//	return m_binarydata;
//}

//!Implementaion of unsigned char* CDataManager::GetBinarydata()
const unsigned char* CDataManager::GetBinarydata() const 
{
	return m_binarydata;
}


//!Implementaion of void CDataManager::SetBinarydata(unsigned char* binarydata)
void CDataManager::SetBinarydata(unsigned char* binarydata)
{
	m_binarydata = binarydata;
}

void CDataManager::SetBinarydata(const unsigned char* binarydata)
{
	m_binarydata = binarydata;
}

//!Implementaion of void CDataManager::SetImageParams(int width, int height, int depth,  float voxel_x,float voxel_y,float voxel_z)
void CDataManager::SetImageParams(int width, int height, int depth,  float voxel_x,float voxel_y,float voxel_z)
{
	m_imageWidth  = width;
	m_imageHeight = height;
	m_imageDepth  = depth;
	m_ImageSize = m_imageWidth * m_imageHeight * m_imageDepth;
	m_voxel_size_x = voxel_x;
	m_voxel_size_y = voxel_y;
	m_voxel_size_z = voxel_z;

	PopulateIndices();
	InitializeOutputData();
}

//!Implementaion of int CDataManager::GetVolumeWidth()
int CDataManager::GetVolumeWidth()
{
	return m_imageWidth;
}

//!Implementaion of int CDataManager::GetVolumeHeight()
int CDataManager::GetVolumeHeight()
{
	return m_imageHeight;
}

//!Implementaion of int CDataManager::GetVolumeDepth()
int CDataManager::GetVolumeDepth()
{
	return m_imageDepth;
}

//!Implementaion of int CDataManager::GetVolumeSize()
int CDataManager::GetVolumeSize()
{
	return m_ImageSize;
}

//!Implementaion of float CDataManager::GetVoxelSizeX()
float CDataManager::GetVoxelSizeX()
{
	return m_voxel_size_x;
}

//!Implementaion of float CDataManager::GetVoxelSizeY()
float CDataManager::GetVoxelSizeY()
{
	return m_voxel_size_y;
}

//!Implementaion of float CDataManager::GetVoxelSizeZ()
float CDataManager::GetVoxelSizeZ()
{
	return m_voxel_size_z;
}

//!Implementaion of void CDataManager::GenerateInputForPointBuilder()
void CDataManager::GenerateInputForPointBuilder()
{	
	InitializeMorphologicalData();
	
	GenerateThickData();
	
	GenerateThinData();
	
	GenerateThinOutBoundDataWithThinData();	
	
	GenerateThinOutBoundData();

	UnInitializeMorphologicalData();	

}

//!Implementaion of void CDataManager::GenerateThickData()
void CDataManager::GenerateThickData()
{
	m_objMorphOperator->PerformErosion(m_binarydata, m_erodedData,m_imageWidth,m_imageHeight,m_imageDepth);

	m_objMorphOperator->PerformDilation(m_erodedData, m_thickData,m_imageWidth,m_imageHeight,m_imageDepth);
}

//!Implementaion of void CDataManager::GenerateThinData()
void CDataManager::GenerateThinData()
{
	for(int i=0; i<m_ImageSize; i++)
		m_thinData[i] = m_binarydata[i] - m_thickData[i]; 
}

//!Implementaion of void CDataManager::GenerateThinOutBoundDataWithThinData()
void CDataManager::GenerateThinOutBoundDataWithThinData()
{
	m_objMorphOperator->PerformDilation(m_thinData, m_thinOutBoundDataWithThin, m_imageWidth,m_imageHeight,m_imageDepth);
	
}

//!Implementaion of void CDataManager::GenerateThinOutBoundData()
void CDataManager::GenerateThinOutBoundData()
{
	for(int i=0; i<m_ImageSize; i++)
	{
		if(m_thinOutBoundDataWithThin[i])
			m_thinOutBoundData[i] = m_thinOutBoundDataWithThin[i] - m_thinData[i] ; // removing thin datta from thinout bound data
	}

	for(int i=0; i<m_ImageSize; i++)
	{
		if(m_thinOutBoundData[i])
			m_thinOutBoundData[i] = m_thinOutBoundData[i] - m_thickData[i] ; // removing thick portions from thinout bound data
	}
}

//!Implementaion of void CDataManager::InitializeMorphologicalData()
void CDataManager::InitializeMorphologicalData()
{
	m_erodedData = new unsigned char[m_ImageSize];
	memset( m_erodedData, 0, m_ImageSize );
	
}

//!Implementaion of void CDataManager::UnInitializeMorphologicalData()
void CDataManager::UnInitializeMorphologicalData()
{
	if(m_erodedData != NULL)
	{
		delete[] m_erodedData;
		m_erodedData = NULL;
	}
}

//!Implementaion of void CDataManager::InitializeOutputData()
void CDataManager::InitializeOutputData()
{
	m_thickData = new unsigned char[m_ImageSize];
	memset( m_thickData, 0, m_ImageSize );

	m_thinData = new unsigned char[m_ImageSize];
	memset( m_thinData, 0, m_ImageSize );
	
	m_thinOutBoundDataWithThin = new unsigned char[m_ImageSize];
	memset( m_thinOutBoundDataWithThin, 0, m_ImageSize );

	m_thinOutBoundData = new unsigned char[m_ImageSize];
	memset( m_thinOutBoundData, 0, m_ImageSize );	
	
}

//!Implementaion of void CDataManager::UnInitializeOutputData()
void CDataManager::UnInitializeOutputData()
{
	
	if(m_thinData != NULL)
	{
		delete[] m_thinData;
		m_thinData = NULL;
	}
	if(m_thinOutBoundData != NULL)
	{
		delete[] m_thinOutBoundData;
		m_thinOutBoundData = NULL;
	}
	if(m_thickData != NULL)
	{
		delete[] m_thickData;
		m_thickData = NULL;
	}
	if(m_thinOutBoundDataWithThin != NULL)
	{
		delete[] m_thinOutBoundDataWithThin;
		m_thinOutBoundDataWithThin = NULL;
	}
}

//!Implementaion of void CDataManager::PopulateIndices()
void CDataManager::PopulateIndices()
{
	m_bufferX.clear();
	m_bufferY.clear();
	m_bufferZ.clear();
	for(int z = 0; z < m_imageDepth ; z++)
		for(int y = 0; y < m_imageHeight ; y++)
			for(int x = 0; x < m_imageWidth ; x++)
			{
				m_bufferX.push_back(x);
				m_bufferY.push_back(y);
				m_bufferZ.push_back(z);				
			}
}

//!Implementaion of const vector<int>& CDataManager::GetBufferX() const
const vector<int>& CDataManager::GetBufferX() const
{
	return m_bufferX;
}

//!Implementaion of const vector<int>& CDataManager::GetBufferY() const
const vector<int>& CDataManager::GetBufferY() const
{
	return m_bufferY;
}

//!Implementaion of const vector<int>& CDataManager::GetBufferZ() const
const vector<int>& CDataManager::GetBufferZ() const
{
	return m_bufferZ;
}

//!Implementaion of void CDataManager::GetThinOutBounData(unsigned char **thinOutBoundData)
void CDataManager::GetThinOutBounData(unsigned char **thinOutBoundData)
{
	*thinOutBoundData =  m_thinOutBoundData;
	
}

//!Implementaion of void CDataManager::GetDataForThickBuilder(unsigned char **thickData)
void CDataManager::GetDataForThickBuilder(unsigned char **thickData)
{
	*thickData = m_thickData;
}

//!Implementaion of void CDataManager::GetThinData(unsigned char **thinData)
void CDataManager::GetThinData(unsigned char **thinData)
{
	*thinData = m_thinData;
}

//!Implementaion of void CDataManager::ClearBuffers()
void CDataManager::ClearBuffers()
{
	UnInitializeOutputData();
	//if (  m_DataManager != NULL)
	//{
	//	delete m_DataManager;
	//	m_DataManager = NULL;
	//}
	
}

} // namespace polygonization
} // namespace lssplib

