#include "Modeling/PointCloudGenerator/ThinPointBuilder.h"

#include <algorithm>

namespace lssplib
{
namespace polygonization
{

//! Constructor of CThinPointBuilder class
CThinPointBuilder::CThinPointBuilder(void)
: m_GeneratedThinPoints()
, m_GeneratedNormals()
, m_EightSubvoxels()
, m_AllThinObjSubvoxels()
, m_objCThreeDDataProcessor(new CThreeDDataProcessor())
, m_Counter(0)
, m_dataManager(NULL)
{
}

CThinPointBuilder::CThinPointBuilder(CDataManager* dataManager)
: m_GeneratedThinPoints()
, m_GeneratedNormals()
, m_EightSubvoxels()
, m_AllThinObjSubvoxels()
, m_objCThreeDDataProcessor(new CThreeDDataProcessor(dataManager))
, m_Counter(0)
, m_dataManager(dataManager)
{
}

//! Destructor of CThinPointBuilder class
CThinPointBuilder::~CThinPointBuilder(void)
{
	delete m_objCThreeDDataProcessor;
}

//!Implementaion of bool CThinPointBuilder::AddPoints(void)
/*!
Generates and populates the point and normal lists in case of the thin structures
*/
bool CThinPointBuilder::AddPoints(void)
{
	cout<<"Generating points from thin structures.."<<endl;

	int width = m_dataManager->GetVolumeWidth();
	int height = m_dataManager->GetVolumeHeight();
	int depth = m_dataManager->GetVolumeDepth();
	int bufSize = width* height* depth;

	unsigned char* thinOutBoundData = NULL;
	m_dataManager->GetThinOutBounData(&thinOutBoundData);
	unsigned char* thinData = NULL;
	m_dataManager->GetThinData(&thinData);
	const unsigned char* objectBuffer = m_dataManager->GetBinarydata();
	unsigned char* thickData = NULL;
	m_dataManager->GetDataForThickBuilder(&thickData);
	
	std::vector<Point> ThreeDEighteenNeighbours;
	std::vector<Point>::iterator it;
	m_objCThreeDDataProcessor->ResetIsOutBoundSubvox();
	
	const vector<int>& bufferX = m_dataManager->GetBufferX();
	const vector<int>& bufferY = m_dataManager->GetBufferY();
	const vector<int>& bufferZ = m_dataManager->GetBufferZ();

	if(thinOutBoundData != NULL)
	{
		for(int Id = 0; Id < bufSize ; Id++)
		{
			if(thinOutBoundData[Id])//checks if it is an ounter boundary voxel of thin structure
			{
				m_AllThinObjSubvoxels.clear();
				//18 neighbours are calculated for out bound voxel and preparing list of object subvoxels of object voxels among 18 neighbors to use later
				m_objCThreeDDataProcessor->SetDivParams(VOX_DIV_FACTOR, VOX_NEIGHBOR_FACTOR);
				m_objCThreeDDataProcessor->SetVoxelDivisionFactor();
				m_objCThreeDDataProcessor->Calculate3D_18Neighbours((float)bufferX[Id], (float)bufferY[Id], (float)bufferZ[Id]);
				ThreeDEighteenNeighbours = m_objCThreeDDataProcessor->GetNeighbourList();
				for (it = ThreeDEighteenNeighbours.begin(); it!= ThreeDEighteenNeighbours.end(); ++it)
				{
					int index = ((int)(it)->z * width * height) + ((int)(it)->y * width ) + (int)(it)->x;
					if(CUtility::CheckIfObject(index, objectBuffer, bufSize))
					{
						CalculateSubvoxels((it)->x, (it)->y, (it)->z);
						m_AllThinObjSubvoxels.insert(m_AllThinObjSubvoxels.end(),m_EightSubvoxels.begin(),m_EightSubvoxels.end());
					}
				}
				ThreeDEighteenNeighbours.clear();
				//The voxels that represent cavities are excluded
				if(!CheckIfCavity(bufferX[Id], bufferY[Id], bufferZ[Id],objectBuffer, width, height, depth))
				{	
					DivideAndCheckSubvox((float)bufferX[Id], (float)bufferY[Id], (float)bufferZ[Id],width, height);								
				}
				m_EightSubvoxels.clear();
				
			}
		}
		m_AllThinObjSubvoxels.clear();
		GetPointsFromOutBoundVox(width, height, bufSize, objectBuffer);
	}
	else
	{
		return false;
	}
	
	return true;
}
//!Implementaion of void CThinPointBuilder::DivideAndCheckSubvox(float ParentX, float ParentY, float ParentZ,int width,int height)
/*!
Divides each voxel into eight subvoxels and checks if need to replace it as object
*/
void CThinPointBuilder::DivideAndCheckSubvox(float ParentX, float ParentY, float ParentZ,int width,int height)
{
	CalculateSubvoxels(ParentX, ParentY, ParentZ);
	std::vector<Point>::iterator it;
	m_objCThreeDDataProcessor->SetDivParams(SUBVOX_DIV_FACTOR, SUBVOX_NEIGHBOR_FACTOR);
	m_objCThreeDDataProcessor->SetVoxelDivisionFactor();
		
	int index = ((int)ParentZ * width * height) + ((int)ParentY * width ) + (int)ParentX;
	
	int outBoundAddFlag =0;
	Point ParentOfSubvox ;
	ParentOfSubvox.x = ParentX ;
	ParentOfSubvox.y = ParentY ;
	ParentOfSubvox.z = ParentZ ; 
	for (it = m_EightSubvoxels.begin(); it!= m_EightSubvoxels.end(); ++it)
	{		
		if(!CheckSubvoxToReplaceAsObj((it)->x ,(it)->y,(it)->z))//If subvoxel is not replaced it is retained as outbound subvox 
		{			
			if(!outBoundAddFlag)
			{
				//First element in the vector is retained as the parent of the out bound subvoxel
				ListOfOutBoundSubVoxPointsWithParent[m_Counter].push_back(ParentOfSubvox);	
				outBoundAddFlag = 1;	
			}
			ListOfOutBoundSubVoxPointsWithParent[m_Counter].push_back(*it); 	
			
		}
		else//the replaced subvoxels are kept in another map
		{
			ListOfReplacedPointsWithParent[index].push_back(*it);
		}		
	}
	if(outBoundAddFlag)
		m_Counter++;
}

//!Implementaion of bool CThinPointBuilder::CheckSubvoxToReplaceAsObj(float SubVoxelX ,float SubVoxelY,float SubVoxelZ)
/*!
Checks if each subvoxel need to be replaced as object
*/
bool CThinPointBuilder::CheckSubvoxToReplaceAsObj(float SubVoxelX ,float SubVoxelY,float SubVoxelZ)
{ 
	std::vector<Point>::iterator it;
	std::vector<Point> objectSubvoxels;

	std::vector<Point> ThreeDEighteenNeighbours;

	m_objCThreeDDataProcessor->Calculate3D_18Neighbours(SubVoxelX, SubVoxelY, SubVoxelZ);
	ThreeDEighteenNeighbours = m_objCThreeDDataProcessor->GetNeighbourList();
	//All the object subvoxels in the 3D-18 neighborhood of the candidate subvoxel is identified
	for (it = ThreeDEighteenNeighbours.begin(); it != ThreeDEighteenNeighbours.end(); ++it)
	{
		if(CheckIfObjectSubvoxel(*it))
			objectSubvoxels.push_back(*it);
	}

	std::vector<Point>::iterator iterator1;
	std::vector<Point>::iterator iterator2;

	Point CandidateSubvoxel ;
	CandidateSubvoxel.x = SubVoxelX;
	CandidateSubvoxel.y = SubVoxelY;
	CandidateSubvoxel.z = SubVoxelZ;

	//Iterate over each pair of object subvoxel  in the eighteen neighbourhood and check the if there is atleast one pair that satisfy three conditions suggested in the paper(page no:4)
	for (iterator1 = objectSubvoxels.begin(); iterator1 != objectSubvoxels.end(); ++iterator1)
	{
		for (iterator2 = iterator1 + 1; iterator2 != objectSubvoxels.end(); ++iterator2)
		{
			if(!SharesFaceWithConsideredSubVoxel(*iterator1, *iterator2 ))
			{
				if(SharesFaceWithCandidateSubVoxel(CandidateSubvoxel, *iterator1, *iterator2 ) )
				{
					objectSubvoxels.clear();
					return true;
				}
			}
		}
	}
	objectSubvoxels.clear();
	return false;

}
//!Implementaion of bool CThinPointBuilder::CheckIfObjectSubvoxel(Point& subvoxelToSearch)
/*!
Checks if the subvoxel received is object subvoxel using the list prepared before
*/
bool CThinPointBuilder::CheckIfObjectSubvoxel(Point& subvoxelToSearch)
{		
	std::vector<Point>::iterator it = find(m_AllThinObjSubvoxels.begin(), m_AllThinObjSubvoxels.end(), subvoxelToSearch);
	
	if (it != m_AllThinObjSubvoxels.end())
	{
		return true;		
	}
	return false;
}

//!Implementaion of bool CThinPointBuilder::SharesFaceWithConsideredSubVoxel(Point &objectSubvoxel1, Point &objectSubvoxel2)
/*!
Checks if the two object subvoxels are sharing any face or not using 3D-6 neighborhood
*/
bool CThinPointBuilder::SharesFaceWithConsideredSubVoxel(Point &objectSubvoxel1, Point &objectSubvoxel2)
{
	std::vector<Point> ThreeDSixNeighbours;

	m_objCThreeDDataProcessor->Calculate3D_6Neighbours(objectSubvoxel1.x, objectSubvoxel1.y, objectSubvoxel1.z);
	ThreeDSixNeighbours = m_objCThreeDDataProcessor->GetNeighbourList();

	std::vector<Point>::iterator iterator;
	for (iterator = ThreeDSixNeighbours.begin(); iterator != ThreeDSixNeighbours.end(); ++iterator)
	{
		if((iterator)->x == objectSubvoxel2.x && (iterator)->y == objectSubvoxel2.y && (iterator)->z == objectSubvoxel2.z)
		{
			return true;
		}
	}	
	return false;
}

//!Implementaion of bool CThinPointBuilder::SharesFaceWithCandidateSubVoxel(Point &candidatesubvoxel,Point &objectSubvoxel1, Point &objectSubvoxel2)
/*!
Checks if any one of the considering two subvoxels shares one face with the candidate subvoxel.
*/
bool CThinPointBuilder::SharesFaceWithCandidateSubVoxel(Point &candidatesubvoxel,Point &objectSubvoxel1, Point &objectSubvoxel2)
{
	std::vector<Point> ThreeDSixNeighbours;

	m_objCThreeDDataProcessor->Calculate3D_6Neighbours(candidatesubvoxel.x, candidatesubvoxel.y, candidatesubvoxel.z);
	ThreeDSixNeighbours = m_objCThreeDDataProcessor->GetNeighbourList(); 

	std::vector<Point>::iterator iterator;
	for (iterator = ThreeDSixNeighbours.begin(); iterator != ThreeDSixNeighbours.end(); ++iterator)
	{
		if(((iterator)->x == objectSubvoxel1.x && (iterator)->y == objectSubvoxel1.y && (iterator)->z == objectSubvoxel1.z) || 
			((iterator)->x == objectSubvoxel2.x && (iterator)->y == objectSubvoxel2.y && (iterator)->z == objectSubvoxel2.z))
		{			
			return true;
		}
	}	
	return false;
}

//!Implementaion of void CThinPointBuilder::GetPointsFromOutBoundVox(int width, int height,int bufSize, unsigned char* objectBuffer)
/*!
For each out bound subvoxel 6 step algorithm is implemented and points and normals are generated
*/
void CThinPointBuilder::GetPointsFromOutBoundVox(int width, int height,int bufSize, const unsigned char* objectBuffer)
{
	
	std::vector<Point> ThreeDSixNeighbours;
	std::vector<Point> ThreeDTwentySixNeighbours;
	std::vector<Point> objectSubvoxelsForPointGeneration;
	std::vector<Point> objectsubvoxelsForNormalGeneration;
	std::vector<Point> pointsFromAlgm;
	std::vector<Point> normalsOfPoints;
	std::vector<Point>::iterator iter;
	std::vector<Point> ParentWithsubvox;
	Point Parent;
	int index;
//	for(size_t i =0 ;i < ListOfOutBoundSubVoxPointsWithParent.size(); i++)
	for(int i =0 ;i < ListOfOutBoundSubVoxPointsWithParent.size(); i++)
	{
		ParentWithsubvox = ListOfOutBoundSubVoxPointsWithParent.find(i)->second;
		Parent = ParentWithsubvox[0];
		int parentIndex = ((int)Parent.z * width * height) + ((int)Parent.y * width ) + (int)Parent.x;
		objectSubvoxelsForPointGeneration.clear();
		objectsubvoxelsForNormalGeneration.clear();
		ThreeDSixNeighbours.clear();
		ThreeDTwentySixNeighbours.clear();
		

		m_objCThreeDDataProcessor->SetDivParams(VOX_DIV_FACTOR, VOX_NEIGHBOR_FACTOR);
		
		//List for normal generation is prepared 
		m_objCThreeDDataProcessor->Calculate3D_26Neighbours(Parent.x, Parent.y, Parent.z);
		ThreeDTwentySixNeighbours = m_objCThreeDDataProcessor->GetNeighbourList();
		for (iter = ThreeDTwentySixNeighbours.begin(); iter != ThreeDTwentySixNeighbours.end(); ++iter)
		{
			index = ((int)(iter)->z * width * height) + ((int)(iter)->y * width ) + (int)(iter)->x;
			if(CUtility::CheckIfObject(index, objectBuffer, bufSize))
			{
				CalculateSubvoxels((iter)->x, (iter)->y, (iter)->z);
				objectsubvoxelsForNormalGeneration.insert(objectsubvoxelsForNormalGeneration.end(),m_EightSubvoxels.begin(),m_EightSubvoxels.end());
			}
		}
		ThreeDSixNeighbours.insert(ThreeDSixNeighbours.begin(),ThreeDTwentySixNeighbours.begin(),ThreeDTwentySixNeighbours.begin() + SIXNEIGHBOR);
		//List for point generation is prepared 
		for (iter = ThreeDSixNeighbours.begin(); iter != ThreeDSixNeighbours.end(); ++iter)
		{
			index = ((int)(iter)->z * width * height) + ((int)(iter)->y * width ) + (int)(iter)->x;
			if(CUtility::CheckIfObject(index, objectBuffer, bufSize))
			{   
				CalculateSubvoxels((iter)->x, (iter)->y, (iter)->z);
				objectSubvoxelsForPointGeneration.insert(objectSubvoxelsForPointGeneration.end(),m_EightSubvoxels.begin(),m_EightSubvoxels.end());
				
			}
		}
		//list to use for point generation need to include the replaced object subvoxels too only if the parent of outbound subvox and replaced object subvox are the same 
		std::map < int, vector <Point> >::iterator it1 = ListOfReplacedPointsWithParent.find(parentIndex);
		if(it1 != ListOfReplacedPointsWithParent.end())
		{
			//Replaced subvoxels are also added to the list which is checked while point generation 
			//in case the parents of out bound subvoxel and replaced subvoxels are the same
			objectSubvoxelsForPointGeneration.insert(objectSubvoxelsForPointGeneration.end(),
				ListOfReplacedPointsWithParent[parentIndex].begin(),ListOfReplacedPointsWithParent[parentIndex].end());
		}	
		m_objCThreeDDataProcessor->SetListForPointGen(objectSubvoxelsForPointGeneration);
		m_objCThreeDDataProcessor->SetListForNormalGen(objectsubvoxelsForNormalGeneration);
		for(size_t j =1 ;j < ParentWithsubvox.size(); j++)
		{
			//Implelments the 6 step algorithm for out bound subvoxels as did for the voxels
			if(!objectSubvoxelsForPointGeneration.empty())
			{
				m_objCThreeDDataProcessor->SetDivParams(SUBVOX_DIV_FACTOR, SUBVOX_NEIGHBOR_FACTOR);
				m_objCThreeDDataProcessor->SeIsSubvox();
				
				m_objCThreeDDataProcessor->SetVoxelDivisionFactor();
				m_objCThreeDDataProcessor->GeneratePointsUsingAlgm(ParentWithsubvox[j].x, ParentWithsubvox[j].y, ParentWithsubvox[j].z);	
				
				m_objCThreeDDataProcessor->GetPointsFromAlgm(pointsFromAlgm);
				m_objCThreeDDataProcessor->GetNormalsOfPoints(normalsOfPoints);

				if(!pointsFromAlgm.empty())
				{					
					for(iter = pointsFromAlgm.begin(); iter != pointsFromAlgm.end(); ++iter)
					{
						m_GeneratedThinPoints.push_back(*iter);
					}
					pointsFromAlgm.clear();
				}

				if(!normalsOfPoints.empty())
				{
					for(iter = normalsOfPoints.begin(); iter != normalsOfPoints.end(); ++iter)
					{
						m_GeneratedNormals.push_back(*iter);
					}
					normalsOfPoints.clear();
				}
			}
		}
	}
	ListOfOutBoundSubVoxPointsWithParent.clear();
	ListOfReplacedPointsWithParent.clear();
}

//!Implementaion of void CThinPointBuilder::GetPoints(vector<Point> &thinData)
/*!
Returns the points generated for thin structures
*/
void CThinPointBuilder::GetPoints(vector<Point> &thinData)
{
	thinData = m_GeneratedThinPoints;
}

//!Implementaion of void CThinPointBuilder::GetNormals(vector<Point> &normals)
/*!
Returns the normals of the points generated for thin structures
*/
void CThinPointBuilder::GetNormals(vector<Point> &normals)
{
	normals = m_GeneratedNormals;
}

//!Implementaion of void CThinPointBuilder::CalculateSubvoxels(float x,float y,float z)
/*!
Calculates the eight subvoxels of a voxel
*/
void CThinPointBuilder::CalculateSubvoxels(float x,float y,float z)
{

	m_EightSubvoxels.clear();
	
	Point Left_upper_front;
	Left_upper_front.x = x - 0.25f;			//Zero: Left upper front
	Left_upper_front.y = y - 0.25f;
	Left_upper_front.z = z - 0.25f;
	m_EightSubvoxels.push_back(Left_upper_front);

	Point Right_Upper_front ;
	Right_Upper_front.x = x + 0.25f;	//One:Right Upper front
	Right_Upper_front.y = y - 0.25f;
	Right_Upper_front.z = z - 0.25f;
	m_EightSubvoxels.push_back(Right_Upper_front);

	Point Left_Lower_Front;
	Left_Lower_Front.x = x - 0.25f;			//Two:Left Lower Front
	Left_Lower_Front.y = y + 0.25f;
	Left_Lower_Front.z = z - 0.25f;
	m_EightSubvoxels.push_back(Left_Lower_Front);

	Point Right_Lower_front;
	Right_Lower_front.x = x + 0.25f;	//Three:Right Lower front
	Right_Lower_front.y = y + 0.25f;
	Right_Lower_front.z = z - 0.25f;
	m_EightSubvoxels.push_back(Right_Lower_front);

	Point Left_Upper_Back;
	Left_Upper_Back.x = x - 0.25f;			//Four:Left Upper Back
	Left_Upper_Back.y = y - 0.25f;
	Left_Upper_Back.z = z + 0.25f;
	m_EightSubvoxels.push_back(Left_Upper_Back);

	Point Right_Upper_Back ;
	Right_Upper_Back.x = x + 0.25f;	//Five:Right Upper Back
	Right_Upper_Back.y = y - 0.25f;
	Right_Upper_Back.z = z + 0.25f;
	m_EightSubvoxels.push_back(Right_Upper_Back);

	Point Left_Lower_Back;
	Left_Lower_Back.x = x - 0.25f;			//six: Left Lower Back
	Left_Lower_Back.y = y + 0.25f;
	Left_Lower_Back.z = z + 0.25f;
	m_EightSubvoxels.push_back(Left_Lower_Back);

	Point Right_Lower_Back;
	Right_Lower_Back.x = x + 0.25f;	//Seven:Right Lower Back
	Right_Lower_Back.y = y + 0.25f;
	Right_Lower_Back.z = z + 0.25f;
	m_EightSubvoxels.push_back(Right_Lower_Back);
}

//!Implementaion of bool CThinPointBuilder::CheckIfCavity(int x,int y,int z, unsigned char* objectBuffer,int width, int height ,int depth)
/*!
Checks if the voxel represent a cavity
*/
bool CThinPointBuilder::CheckIfCavity(int x,int y,int z, const unsigned char* objectBuffer,int width, int height ,int depth)
{
	int index[NUM_FACES] = {0,0,0,0,0,0};
	int count = 0,i= 0,id;
	std::vector<Point> ThreeDSixNeighbours;
	std::vector<Point>::iterator it;

	int imageSize = width * height * depth;
	m_objCThreeDDataProcessor->Calculate3D_6Neighbours((float)x, (float)y, (float)z);
	ThreeDSixNeighbours = m_objCThreeDDataProcessor->GetNeighbourList();
	for(it = ThreeDSixNeighbours.begin(); it != ThreeDSixNeighbours.end(); ++it)
	{
		index[i] = CUtility::CalculateIndex((int)(it)->x, (int)(it)->y, (int)(it)->z, width, height);
		i++;
	}
	for(id=0 ; id<NUM_FACES ; id++)
	{
		if(CUtility::CheckIfObject(index[id], objectBuffer, imageSize))
		{
			count = count + 1;
		}
	}
	if(count == (NUM_FACES-1))//if 5 neighbora among 3D-6 neighbors are objects,then it is a cavity
		return true;
	else
		return false;
}

//!Implementaion of void CThinPointBuilder::ClearSubvoxels()
/*!
clears the eight sub voxels list
*/
void CThinPointBuilder::ClearSubvoxels()
{	
	m_EightSubvoxels.clear();
	
}

//!Implementaion of void CThinPointBuilder::ClearBuffers()
/*!
clears the buffers associated with generating thin points and normals 
*/
void CThinPointBuilder::ClearBuffers()
{	
	ClearSubvoxels();
	m_AllThinObjSubvoxels.clear();
	m_GeneratedThinPoints.clear();
	m_GeneratedNormals.clear();
}

} // namespace polygonization
} // namespace lssplib

