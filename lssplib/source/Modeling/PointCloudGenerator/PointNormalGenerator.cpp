#include "Modeling/PointCloudGenerator/PointNormalGenerator.h"
#include "Modeling/PointCloudGenerator/Constants.h"
#include "Modeling/PointCloudGenerator/Utility.h"
#include <time.h>
#include <fstream>
using namespace std;

namespace lssplib
{
namespace polygonization
{

CPointNormalGenerator::CPointNormalGenerator(void)
{
}

CPointNormalGenerator::~CPointNormalGenerator(void)
{
}

//!Implementaion of void CPointNormalGenerator::FlipNormalDirection(Point pointPlaced, Point &generatedNormal ,float viewpoint_x,float viewpoint_y, float viewpoint_z)
/*!
Flips the direction of the normal towards the given view point
*/
void CPointNormalGenerator::FlipNormalDirection(Point pointPlaced, Point &generatedNormal ,float viewpoint_x,float viewpoint_y, float viewpoint_z)
{	
	FlipNormalTowardsViewpoint(pointPlaced, viewpoint_x, viewpoint_y, viewpoint_z, generatedNormal.x,generatedNormal.y,generatedNormal.z); 
}

//!Implementaion of void  CPointNormalGenerator::FlipNormalTowardsViewpoint (Point pointPlaced, float vp_x, float vp_y, float vp_z,float &nx, float &ny, float &nz)
void  CPointNormalGenerator::FlipNormalTowardsViewpoint (Point pointPlaced, float vp_x, float vp_y, float vp_z,float &nx, float &ny, float &nz)
  {
    // See if we need to flip any plane normals
    vp_x -= pointPlaced.x;
    vp_y -= pointPlaced.y;
    vp_z -= pointPlaced.z;

    // Dot product between the (viewpoint - point) and the plane normal
    float cos_theta = (vp_x * nx + vp_y * ny + vp_z * nz);

    // Flip the plane normal
    if (cos_theta < 0)
    {
      nx *= -1;
      ny *= -1;
      nz *= -1;
    }
  }

} // namespace polygonization
} // namespace lssplib

