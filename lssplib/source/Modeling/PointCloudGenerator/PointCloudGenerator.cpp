#pragma once

#include "Modeling/PointCloudGenerator/PointCloudGenerator.h"
#include "Modeling/PointCloudGenerator/Constants.h"

namespace lssplib
{
namespace polygonization
{

CPointCloudGenerator::CPointCloudGenerator(void)
{
}

CPointCloudGenerator::~CPointCloudGenerator(void)
{
}

//!Implementaion of bool CPointCloudGenerator::GeneratePoints(PointBuilder* objPointBuilder)
bool CPointCloudGenerator::GeneratePoints(PointBuilder* objPointBuilder)
{
	if(!objPointBuilder->AddPoints())
	{
		return false;
	}
	return true;
}

//!Implementaion of void CPointCloudGenerator::GetAllPoints(PointBuilder* thick, PointBuilder* thin, std::vector<Point> &m_points)
void CPointCloudGenerator::GetAllPoints(PointBuilder* thick, PointBuilder* thin, std::vector<Point> &m_points)
{
	vector<Point> thickPoints;
	thick->GetPoints(thickPoints);
	vector<Point> thinPoints;
	thin->GetPoints(thinPoints);
	
	thickPoints.insert(thickPoints.end(),thinPoints.begin(),thinPoints.end());

	m_points = thickPoints ;
	thickPoints.clear();
	thinPoints.clear();
}

//!Implementaion of void CPointCloudGenerator::GetAllNormals(PointBuilder* thick, PointBuilder* thin, std::vector<Point> &m_normals)
void CPointCloudGenerator::GetAllNormals(PointBuilder* thick, PointBuilder* thin, std::vector<Point> &m_normals)
{
	vector<Point> normalsFromThick;
	thick->GetNormals(normalsFromThick);
	vector<Point> normalsFromThin;
	thin->GetNormals(normalsFromThin);
	
	normalsFromThick.insert(normalsFromThick.end(),normalsFromThin.begin(),normalsFromThin.end());

	m_normals = normalsFromThick;
	normalsFromThick.clear();
	normalsFromThin.clear();

}

//!Implementaion of void CPointCloudGenerator::GetAllNormals(PointBuilder* thick, PointBuilder* thin, std::vector<Point> &m_normals)
void CPointCloudGenerator::ClearBuffers(PointBuilder* thick, PointBuilder* thin)
{
	thick->ClearBuffers();
	thin->ClearBuffers();

}

} // namespace polygonization
} // namespace lssplib

