#include "Modeling/PointCloudGenerator/MorphologicalOperator.h"
#include "Modeling/PointCloudGenerator/Utility.h"
#include <iostream>

namespace lssplib
{
namespace polygonization
{

//! Constructor of CMorphologicalOperator class
CMorphologicalOperator::CMorphologicalOperator(void)
{
	m_origX = 1;
	m_origY = 1;
	m_origZ = 1;
	CreateStructElement();
}

//! Constructor of CMorphologicalOperator class
CMorphologicalOperator::~CMorphologicalOperator(void)
{
	
}

//!	Implementation of void CMorphologicalOperator::CreateStructElement()
void CMorphologicalOperator::CreateStructElement()
{	
	for(int i =0 ;i < STRUCTURE_ELEMENT_SIZE; i++)
		m_structuringElement[i] = 1;

}

//!	Implementation of void CMorphologicalOperator::PerformDilation(unsigned char* buffer,unsigned char* bufferOut,int width, int height,int depth)
void CMorphologicalOperator::PerformDilation(unsigned char* buffer,unsigned char* bufferOut,int width, int height,int depth)
{
	
	int i, j, k, m, n, p; // Loop variables

	for (i=0; i<width; i++)
	{
		for (j=0; j<height; j++) 
		{
			for (k=0; k<depth; k++) 
			{
				int index = CUtility::CalculateIndex(i, j, k,width,height);
				if ((int)buffer[index]) 
				{       
					// if the foreground pixel is not zero, then fill in the pixel
					// covered by the s.e.
					for (m = 0; m < SE_WIDTH; m++)
					{
						for (n = 0; n < SE_HEIGHT; n++)
						{
							for (p = 0; p < SE_DEPTH; p++)
							{
								if ((i - m_origX + m) >= 0 && (j - m_origY + n)>=0 && (k - m_origZ + p) >=0 && (i - m_origX + m) < width && 
									(j - m_origY + n) < height && (k - m_origZ + p) < depth)
								{
									int BuffIndex = CUtility::CalculateIndex(i - m_origX + m, j - m_origY + n, k - m_origZ + p, width,height) ;
									if (!(int)bufferOut[BuffIndex])
									{										
										int SeIndex =  CUtility::CalculateIndex(m, n, p, SE_WIDTH,SE_HEIGHT);										
										bufferOut[BuffIndex] = ( (int)m_structuringElement[SeIndex] ? 1 : 0 );
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

//!	Implementation of void CMorphologicalOperator::PerformErosion(unsigned char* buffer,unsigned char* bufferOut,int width, int height,int depth) 
void CMorphologicalOperator::PerformErosion(const unsigned char* buffer,unsigned char* bufferOut,int width, int height,int depth) 
{
	int i, j,k, m, n,p;
	int sum, count,length;

	length = width * height * depth;

	sum = 0;

	for (i = 0; i < SE_WIDTH; i++)
	{
		for (j = 0; j < SE_HEIGHT; j++)
		{
			for (k = 0; k < SE_DEPTH; k++)
			{
				int SeIndex =  CUtility::CalculateIndex(i, j, k, SE_WIDTH, SE_HEIGHT);
				if ((int)m_structuringElement[SeIndex])
					sum++;
			}
		}
	}

	for (i = 0; i < width; i++)
	{
		for (j = 0; j < height; j++) 
		{
			for (k = 0; k < depth; k++) 
			{
				int index =  CUtility::CalculateIndex(i, j, k, width, height);
				if ((int)buffer[index])  // if the foreground pixel is 1
				{    
					count = 0;
					for (m = 0; m < SE_WIDTH; m++)
					{ 
						for (n = 0; n < SE_HEIGHT; n++) 
						{
							for (p = 0; p < SE_DEPTH; p++) 
							{
								if ((i - m_origX + m) >= 0 && (j - m_origY + n) >=0 && (k - m_origZ + p) >=0 &&
									(i - m_origX + m) < width && (j - m_origY + n) < height && (k - m_origZ + p) < depth)
								{
									int ImIndex = CUtility::CalculateIndex(i - m_origX + m, j - m_origY + n, k - m_origZ + p, width, height);
									int SeIndex = CUtility::CalculateIndex(m, n, p, SE_WIDTH, SE_HEIGHT);
									count += (int)buffer[ImIndex] && (int)m_structuringElement[SeIndex];
								}
							}
						}
					}
					// if all se foreground pixels are included in the foreground of 
					// the current pixel's neighborhood, then enable this pixel in erosion
					if (sum == count) 
					{
						int newIndex = CUtility::CalculateIndex(i, j, k, width, height );
						bufferOut[newIndex] = 1;
					}
				}
			}
		}
	}
}

} // namespace polygonization
} // namespace lssplib

