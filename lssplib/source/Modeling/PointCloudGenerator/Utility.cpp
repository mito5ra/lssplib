#include "Modeling/PointCloudGenerator/Utility.h"

namespace lssplib
{
namespace polygonization
{

CUtility::CUtility(void)
{
}

CUtility::~CUtility(void)
{
}

//!	Implementation of int CUtility::CalculateIndex(int x,int y,int z, int width, int height)
int CUtility::CalculateIndex(int x,int y,int z, int width, int height)
{
	return ((z * width * height) + (y * width ) + x);
}

//!	Implementation of bool CUtility::CheckIfObject(int index, unsigned char* buffer, int bufsize)
bool CUtility::CheckIfObject(int index, unsigned char* buffer, int bufsize)
{
	if(index <0 ||index >= bufsize)
		return false;
	if (buffer[index] == 1)
		return true;
	return false;

}

bool CUtility::CheckIfObject(int index, const unsigned char* buffer, int bufsize)
{
	if(index <0 ||index >= bufsize)
		return false;
	if (buffer[index] == 1)
		return true;
	return false;

}

} // namespace polygonization
} // namespace lssplib

