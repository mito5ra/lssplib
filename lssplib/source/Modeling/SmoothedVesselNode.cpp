#include "Modeling/SmoothedVesselNode.h"

namespace lssplib
{
namespace polygonization
{

//------------------------------------------------------------------------------
//! コンストラクタ
SmoothedVesselNode::SmoothedVesselNode()
: m_parent(NULL)
, m_children()
, m_position(0.0f, 0.0f, 0.0f)
, m_radius(0.0f)
, m_branchLevel(0)
{
}

//------------------------------------------------------------------------------
SmoothedVesselNode::SmoothedVesselNode( const SmoothedVesselNode& src )
: m_parent(src.m_parent)
, m_children(src.m_children)
, m_position(src.m_position)
, m_branchLevel(src.m_branchLevel)
, m_radius(src.m_radius)
{

}

//------------------------------------------------------------------------------
//! デストラクタ
SmoothedVesselNode::~SmoothedVesselNode()
{
}

//------------------------------------------------------------------------------
SmoothedVesselNode* SmoothedVesselNode::NextJunction()
{
	SmoothedVesselNode* currentNode = this;
	while (!currentNode->m_children.empty() &&
		currentNode->m_children.size() == 1)
	{
		currentNode = currentNode->m_children[0];
	}

	return currentNode;
}

//------------------------------------------------------------------------------
const SmoothedVesselNode* SmoothedVesselNode::NextJunction() const
{
	const SmoothedVesselNode* currentNode = this;
	while (!currentNode->m_children.empty() &&
		currentNode->m_children.size() == 1)
	{
		currentNode = currentNode->m_children[0];
	}

	return currentNode;
}

} // namespace polygonization
} // namespace lssplib

