#include "Modeling/TriangleData.h"

namespace lssplib
{
namespace polygonization
{
// === TriangleData ===

//! @brief コンストラクタ
TriangleData::TriangleData()
:m_hasBeenUsed(false)
{
	m_vertexIndex[0] = 0;
	m_vertexIndex[1] = 0;
	m_vertexIndex[2] = 0;

	m_edgeIndex[0] = 0;
	m_edgeIndex[1] = 0;
	m_edgeIndex[2] = 0;
}

//! @brief デストラクタ
TriangleData::~TriangleData()
{
}

//! @brief 辺のインデックスを取得する。
//! 
//! @param[in] ith 三角形のどの辺のインデックスを取得するのかを指定する数
//! @return 辺のインデックス
const size_t TriangleData::GetEdgeIndex(const size_t ith) const
{
	return m_edgeIndex[ith];
}

//! @brief 辺のインデックスをメンバ変数にセットする。
//! 
//! @param[in] edgeIndex 
//! @param[in] ith 三角形のどの辺にセットするのかを指定する数
void TriangleData::SetEdgeIndex(const size_t edgeIndex, const size_t ith)
{
	m_edgeIndex[ith] = edgeIndex;
}

//! @brief 頂点のインデックスをメンバ変数にセットする。
//! 
//! @param[in] vertexIndexOfTriangle 頂点インデックスの配列(3成分)へのポインタ
void TriangleData::SetVertexIndex(const size_t* vertexIndexOfTriangle)
{
	m_vertexIndex[0] = vertexIndexOfTriangle[0];
	m_vertexIndex[1] = vertexIndexOfTriangle[1];
	m_vertexIndex[2] = vertexIndexOfTriangle[2];
}

//! @brief 頂点インデックスを取得する。
//! 
//! @param[in] ith 三角形のどの頂点のインデックスを取得するのかを指定する数
//! @return 頂点インデックス
const size_t TriangleData::GetVertexIndex(const size_t ith) const
{
	return m_vertexIndex[ith];
}


// === EdgeData ===

//! @brief コンストラクタ
EdgeData::EdgeData()
:m_trianglesAroundEdge()
{
	m_edge[0] = 0;
	m_edge[1] = 0;
}
//! @brief デストラクタ
EdgeData::~EdgeData()
{
}

//! @brief 辺を共有する三角形を一つ追加する。
//! 
//! @param[in] triangleIndex 三角形のインデックス
void EdgeData::AddTriangleAroundEdge(const size_t triangleIndex)
{
	m_trianglesAroundEdge.push_back(triangleIndex);
}

//! @brief 辺が所有する二つの頂点を取得する。
//! 
//! @return 二つの頂点を持つメモリへのポインタ
const size_t* EdgeData::GetEdge() const
{
	return m_edge;
}

//! @brief 辺を共有する三角形を全て取得する。
//! 
//! @return 三角形のインデックスを要素とする配列への参照
const std::vector<size_t>& EdgeData::GetTriangleAroundEdge() const
{
	return m_trianglesAroundEdge;
}

//! @brief 辺が所有する頂点をセットする。
//! 
//! @param[in] edge 辺を所有する頂点へのポインタ
void EdgeData::SetEdge(const size_t* edge)
{
	m_edge[0] = edge[0];
	m_edge[1] = edge[1];
}


// === VertexData ===

//! @brief コンストラクタ
VertexData::VertexData()
:m_edgeIdAroundVertex()
,m_hasBeenUsed(false)
{
	m_positionOfVertex[0] = 0.0f;
	m_positionOfVertex[1] = 0.0f;
	m_positionOfVertex[2] = 0.0f;
};

//! @brief デストラクタ
VertexData::~VertexData()
{
};

//! @brief 頂点位置をセットする。
//! 
//! @param[in] position 頂点位置を所有するメモリへのポインタ
void VertexData::SetPositionOfVertex(const float* position)
{
	m_positionOfVertex[0] = position[0];
	m_positionOfVertex[1] = position[1];
	m_positionOfVertex[2] = position[2];
};

//! @brief 頂点位置を取得する。
//! 
//! @return 頂点位置を所有するメモリへのポインタ
const float* VertexData::GetPositionOfVertex() const
{
	return m_positionOfVertex;
}

//! @brief 頂点を共有する辺の全てを取得する。
//! 
//! @return 辺を要素とする配列への参照
const std::vector<size_t>& VertexData::GetEdgeAroundVertex() const
{
	return m_edgeIdAroundVertex;
}

//! @brief 頂点を共有する辺を一つ追加する。
//! 
//! @param[in] edgeIndex 辺のインデックス
void VertexData::AddEdgeAroundVertex(const size_t edgeIndex)
{
	m_edgeIdAroundVertex.push_back(edgeIndex);
}

} // namespace polygonization
} // namespace lssplib

