#include "Modeling/PolygonModelGenerator.h"

#include "Modeling/PointCloudGenerator/AppDataStructures.h"
#include "Modeling/PointCloudGenerator/DataManager.h"
#include "Modeling/PointCloudGenerator/ThickPointBuilder.h"
#include "Modeling/PointCloudGenerator/ThinPointBuilder.h"
#include "Modeling/PointCloudGenerator/PointCloudGenerator.h"
#include "Modeling/MPUI/PointSet.h"
#include "Modeling/MPUI/ImplicitOctTree.h"
#include "Modeling/BloomenthalPolygonizer.h"
#include "Modeling/SweepSmallPiecesOfTriangles.h"

namespace lssplib
{
namespace polygonization
{

PolygonModelGenerator::PolygonizerParameterSetter::PolygonizerParameterSetter()
{

}

PolygonModelGenerator::PolygonizerParameterSetter::~PolygonizerParameterSetter()
{

}

void PolygonModelGenerator::PolygonizerParameterSetter::operator()(
	const int numPoints,
	const float* min,
	const float* max,
	lssplib::polygonization::ImplicitPOU& func ) const
{
	int Nmin = static_cast<int>(numPoints/5.0);
	if (Nmin > 200) { Nmin = 200; }
	func.imp->_Nmin = Nmin;

	func.imp->_support = 0.75f;
	func.imp->_lambda = 0.2f;
	func.imp->_sharp = false;
	func.imp->_edgeT = 0.9f;
	func.imp->_cornerT = 0.7f;
	func._max_level = 8;
	func.count = 0;

	float sizeX = max[0] - min[0];
	float sizeY = max[1] - min[1];
	float sizeZ = max[2] - min[2];

	float size = static_cast<float>(sqrt(sizeX*sizeX + sizeY*sizeY + sizeZ*sizeZ));

	float maxError = 0.0225f;

	func._tol = maxError * size;

	func._iso = -0.0009f * size;
}



PolygonModelGenerator::PolygonModelGenerator()
: m_voxelSpacing(1.0f, 1.0f, 1.0f)
, m_cellSize(2.0f)
, m_boundsCoeff(5.0f)
, m_paramSetter(NULL)
{
}

PolygonModelGenerator::PolygonModelGenerator(PolygonizerParameterSetter* paramSetter)
: m_voxelSpacing(1.0f, 1.0f, 1.0f)
, m_cellSize(2.0f)
, m_boundsCoeff(5.0f)
, m_paramSetter(paramSetter)
{
}

PolygonModelGenerator::~PolygonModelGenerator()
{
}

void PolygonModelGenerator::SetVoxelSpacing( const Size3f& voxelSpacing )
{
	m_voxelSpacing = voxelSpacing;
}

void PolygonModelGenerator::SetPolygonizerParameters(float cellSize, float boundsCoeff)
{
	m_cellSize = cellSize;
	m_boundsCoeff = boundsCoeff;
}

bool PolygonModelGenerator::Generate(const Size3i& bufferSize, const unsigned char* buffer, Triangles& triangles, Vertices& vertices, bool enableThinPointBuilder )
{
	if (!m_paramSetter) return false;

	PointSet pointSet;
	CreatePointCloud(bufferSize, buffer, pointSet, enableThinPointBuilder);

	if (!PolygonizePointCloud(pointSet, triangles, vertices)) return false;

	SweepSmallPiecesOfPolygons sweeper(triangles, vertices);
	sweeper.SweepSmallPieces();

	return true;
}

void PolygonModelGenerator::CreatePointCloud(const Size3i& bufferSize, const unsigned char* buffer, PointSet& pointSet, bool enableThinPointBuilder ) const
{
	CDataManager dataManager;

	dataManager.SetBinarydata(buffer);

		dataManager.SetImageParams(
			bufferSize.width(), bufferSize.height(), bufferSize.depth(),
			m_voxelSpacing.width(), m_voxelSpacing.height(), m_voxelSpacing.depth()); // set buffer size and spacing

		dataManager.GenerateInputForPointBuilder();

		CPointCloudGenerator objPointCloudGenerator;
		CThickPointBuilder objPbThick(&dataManager);
		CThinPointBuilder objPbThin(&dataManager);

		if( objPointCloudGenerator.GeneratePoints(&objPbThick) )
		{
			if (enableThinPointBuilder) objPointCloudGenerator.GeneratePoints(&objPbThin);
		}

		PointList points;
		objPointCloudGenerator.GetAllPoints(&objPbThick, &objPbThin, points);

		NormalList normals;
		objPointCloudGenerator.GetAllNormals(&objPbThick, &objPbThin, normals);

		FillPointSet(points, normals, pointSet);

		dataManager.ClearBuffers();

		objPointCloudGenerator.ClearBuffers(&objPbThick, &objPbThin);
}

//! @brief 生成された点群データを調整し、クラス PointSet に格納させる。
//! @param[out] points 点群の頂点位置データ
//! @param[out] normals 点群の法線データ
//! @param [out] pointSet 点群情報を格納するPointSetオブジェクト
void PolygonModelGenerator::FillPointSet(const PointList& points, const NormalList& normals, PointSet& pointSet) const
{
	pointSet.setPointSize(static_cast<int>(points.size()));

	int count=0;
	for (PointList::const_iterator it = points.begin(); it!= points.end(); ++it, ++count)
	{
		pointSet.setPoint(count,(it)->x,(it)->y,(it)->z);
	}

	count = 0;
	int i = 0;
	float (*point)[3] = pointSet._point;
	float x,y,z;
	float invLen = 0.0f;
	for (NormalList::const_iterator it = normals.begin(); it!= normals.end(); ++it,++i)
	{
		x = (it)->x;
		y = (it)->y;
		z = (it)->z;

		if(x == 0 && y == 0 && z == 0)
			continue;

		invLen = static_cast<float>(1.0/sqrt(x*x + y*y + z*z));
		x *= invLen;
		y *= invLen;
		z *= invLen;

		pointSet.setPoint(count, point[i][0], point[i][1], point[i][2]);
		pointSet.setNormal(count, -x, -y, -z);
		++count;
	}

	pointSet._pointN = count;
}

//! @brief 点群データをもとに、ポリゴン化を行う。
//! @param [in] pointSet ポリゴン化を行う点群情報オブジェクト
//! @param[in] triangles 生成された三角形ポリゴンの配列
//! @param[in] vertices 生成されたポリゴンの頂点配列
//! return ポリゴン化に成功したかどうかを表すフラグ
bool PolygonModelGenerator::PolygonizePointCloud(PointSet& pointSet, Triangles& triangles, Vertices& vertices) const
{
	if (pointSet._pointN == 0) return false;

	//Bounding box data
	float min[3], max[3];
	pointSet.bound(min, max);

	float mid[3];
	mid[0] = 0.5f*(min[0] + max[0]);
	mid[1] = 0.5f*(min[1] + max[1]);
	mid[2] = 0.5f*(min[2] + max[2]);

	float sizeX = max[0] - min[0];
	float sizeY = max[1] - min[1];
	float sizeZ = max[2] - min[2];
	float size = std::max(sizeX, std::max(sizeY, sizeZ))*0.5f*1.2f;
	float maxC[3], minC[3];
	minC[0] = mid[0] - size;
	minC[1] = mid[1] - size;
	minC[2] = mid[2] - size;
	maxC[0] = mid[0] + size;
	maxC[1] = mid[1] + size;
	maxC[2] = mid[2] + size;

	ImplicitPOU func;
	func.imp = new ImplicitOctTree(&pointSet, minC, maxC); // this object will be deleted in the destructor of ImplicitPOU.

	(*m_paramSetter)(pointSet._pointN, min, max, func);

	BloomenthalPolygonizer polygonizer(&func, m_cellSize, static_cast<int>(m_boundsCoeff * size));
	if (!polygonizer.march(TET, mid[0], mid[1],mid[2])) return false;

	SetTrianglesFromPolygonizer(polygonizer, triangles, vertices);

	return true;
}

//! @brief ポリゴナイザーが生成したポリゴンデータをコピーする
//! @param [in] polygonizer Polygonizerオブジェクト
//! @param [out] triangles 三角形の頂点インデックスの集合
//! @param [out] vertexPositions 頂点位置の集合
void PolygonModelGenerator::SetTrianglesFromPolygonizer(
	const BloomenthalPolygonizer& polygonizer,
	Triangles& triangles, 
	Vertices& vertexPositions) const
{
	int numOfTriangles = polygonizer.no_triangles();

	if(numOfTriangles==0) return;

	for (int i=0; i<numOfTriangles; ++i)
	{
		const TRIANGLE& tempTriangle = polygonizer.get_triangle(i);
		TriangleVertexIndex tempTriangleVertexIndex;
		tempTriangleVertexIndex.vertex[0] = tempTriangle.v0;
		tempTriangleVertexIndex.vertex[1] = tempTriangle.v1;
		tempTriangleVertexIndex.vertex[2] = tempTriangle.v2;
		triangles.push_back(tempTriangleVertexIndex);
	}

	for (int i=0; i < polygonizer.no_vertices(); ++i)
	{
		const VECTOR& tempVertex = polygonizer.get_vertex(i);
		VertexPosition tempVertexPosition;

		tempVertexPosition.position[0] = tempVertex.x;
		tempVertexPosition.position[1] = tempVertex.y;
		tempVertexPosition.position[2] = tempVertex.z;

		vertexPositions.push_back(tempVertexPosition);
	}
}

} // polygonization
} // lssplib