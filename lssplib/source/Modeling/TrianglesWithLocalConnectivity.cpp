#include "Modeling/TrianglesWithLocalConnectivity.h"

namespace lssplib
{
namespace polygonization
{

//! @brief コンストラクタ
TrianglesWithLocalConnectivity::TrianglesWithLocalConnectivity()
:m_vertexData()
,m_edgeData()
,m_triangleData()
{
}

//! @brief デストラクタ
TrianglesWithLocalConnectivity::~TrianglesWithLocalConnectivity()
{
}

//! @brief 隣接関係を保有する三角形を保有する。
//! 
//! @param[in] inputTriangles 三角形の頂点全体
//! @param[in] inputVertices 頂点位置の全体
void TrianglesWithLocalConnectivity::MakeTrianglesWithLocalConnectivity(const std::vector<TriangleVertexIndex>& inputTriangles,
	const std::vector<VertexPosition>& inputVertices)
{
	size_t numTriangles = inputTriangles.size();
	m_triangleData.resize(numTriangles);

	size_t numVertices = inputVertices.size();
	m_vertexData.resize(numVertices);

	for(size_t i=0; i<numTriangles; ++i)
	{
		size_t triangle[3] = {inputTriangles[i].vertex[0], inputTriangles[i].vertex[1], inputTriangles[i].vertex[2]};
		m_triangleData[i].SetVertexIndex(triangle);
		InputDataByTriangle(triangle, i);
		InputPositionOfVertexIntoTriangleData(inputTriangles[i], inputVertices);
		InputTrueIntoFlagsIndicatingUsedOrNot(triangle);
	}
}

//! @brief 一つの三角形からデータを構成するために必要な情報を取得する。\n
//! データを構成するときには、一つの三角形を順番に処理して行く。その処理の一つ。
//! 
//! @param[in] triangle 三角形の頂点インデックス
//! @param[in] triangleIndex 三角形の頂点
void TrianglesWithLocalConnectivity::InputDataByTriangle(const size_t* triangle,
	const size_t triangleIndex)
{
	size_t edgeInTriangle[3][2] = {{triangle[0],triangle[1]},{triangle[1],triangle[2]},{triangle[2],triangle[0]}};
	for(int i=0; i<3; ++i)
	{
		InputDataByEdge(edgeInTriangle[i], triangleIndex, i);
	}
}

//! @brief 一つの辺からデータを構成するために必要な情報を取得する。
//! 
//! @param[in] currentPairOfVertices 辺が所有する二つの頂点
//! @param[in] triangleIndex 三角形のインデックス
//! @param[in] edgeLocation 三角形のどの辺なのかを指定する数
void TrianglesWithLocalConnectivity::InputDataByEdge(const size_t* currentPairOfVertices,
	const size_t triangleIndex,
	const size_t edgeLocation)
{
	if(HasPairOfVerticesBeenUsed(currentPairOfVertices)==true)
	{
		AddVertexPairToEdgeData_IfItHasNotBeenResgisteredAsEdge(currentPairOfVertices, triangleIndex, edgeLocation);
	}
	else
	{
		size_t edgeIndex = m_edgeData.size();
		m_triangleData[triangleIndex].SetEdgeIndex(edgeIndex, edgeLocation);
		m_vertexData[currentPairOfVertices[0]].AddEdgeAroundVertex(edgeIndex);
		m_vertexData[currentPairOfVertices[1]].AddEdgeAroundVertex(edgeIndex);
		EdgeData tempEdge;
		tempEdge.SetEdge(currentPairOfVertices);
		m_edgeData.push_back(tempEdge);
		m_edgeData[m_edgeData.size()-1].AddTriangleAroundEdge(triangleIndex);
	}
}

//! @brief データを構成するために必要な情報を取得する処理の一つ。\n
//! 二つの頂点が、辺データとして登録されていないならば、その二つの頂点を辺として登録する。\n
//! 既に登録されているならば、その辺を三角形の一辺として記録し、なおかつ三角形のインデックスを辺に対して共有三角形として記録する。\n
//! 
//! @param[in] currentPairOfVertices 二つの頂点
//! @param[in] triangleIndex 三角形のインデックス
//! @param[in] edgeLocation 三角形のどの辺なのかを指定する数
void TrianglesWithLocalConnectivity::AddVertexPairToEdgeData_IfItHasNotBeenResgisteredAsEdge(const size_t* currentPairOfVertices,
	const size_t triangleIndex,
	const size_t edgeLocation)
{
	const std::vector<size_t>& edgeAroundVertex = m_vertexData[currentPairOfVertices[0]].GetEdgeAroundVertex();
//	int counter =0;
	size_t counter = 0;
	size_t numEdgesAroundVertex = edgeAroundVertex.size();

	for (size_t i=0;i<numEdgesAroundVertex; ++i)
	{
		if (IsEdgeEqualToCurrentPairOfVertices(currentPairOfVertices, edgeAroundVertex[i])==true)
		{
			m_triangleData[triangleIndex].SetEdgeIndex(edgeAroundVertex[i], edgeLocation);
			m_edgeData[edgeAroundVertex[i]].AddTriangleAroundEdge(triangleIndex);
			break;
		}
		else
		{
			++counter;
			if (counter == edgeAroundVertex.size())
			{
				size_t edgeIndex = m_edgeData.size();
				m_triangleData[triangleIndex].SetEdgeIndex(edgeIndex, edgeLocation);
				m_vertexData[currentPairOfVertices[0]].AddEdgeAroundVertex(edgeIndex);
				m_vertexData[currentPairOfVertices[1]].AddEdgeAroundVertex(edgeIndex);
				EdgeData tempEdge;
				tempEdge.SetEdge(currentPairOfVertices);
				m_edgeData.push_back(tempEdge);
				m_edgeData[m_edgeData.size()-1].AddTriangleAroundEdge(triangleIndex);
			}
		}
	}
}

//! @brief 二つの頂点インデックスが、与えられた辺の所有する頂点インデックスと等しいかどうかを照合する。
//! 
//! @param[in] currentPairOfVertices 二つの頂点インデックス
//! @param[in] edgeIndex 辺のインデックス
//! @return 等しいかどうかを返す。
const bool TrianglesWithLocalConnectivity::IsEdgeEqualToCurrentPairOfVertices(const size_t* currentPairOfVertices,
	const size_t edgeIndex) const
{
	const size_t* edge = m_edgeData[edgeIndex].GetEdge();
	if (currentPairOfVertices[0]==edge[0] && currentPairOfVertices[1]==edge[1])
	{
		return true;
	}
	else if(currentPairOfVertices[0]==edge[1] && currentPairOfVertices[1]==edge[0])
	{
		return true;
	}
	else
	{
		return false;
	}
}

//! @brief 二つの頂点が処理済かどうかをフラグにより確認する。
//! 
//! @param[in] edge 辺が所有する二つのインデックスへのポインタ
//! @return 処理済かどうかを返す。
const bool TrianglesWithLocalConnectivity::HasPairOfVerticesBeenUsed(const size_t* edge) const
{
	if (m_vertexData[edge[0]].HasTheVertexBeenUsed()==true && m_vertexData[edge[1]].HasTheVertexBeenUsed()==true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//! @brief 三角形の頂点位置をデータに格納する。
//! 
//! @param[in] inputTriangle 三角形の頂点インデックスの全体
//! @param[in] inputVertices 頂点位置の全体
void TrianglesWithLocalConnectivity::InputPositionOfVertexIntoTriangleData(const TriangleVertexIndex& inputTriangle,
	const std::vector<VertexPosition>& inputVertices)
{
	for (int i=0;i<3;++i)
	{
		size_t vertexIndex = inputTriangle.vertex[i];
		float vertexPosition[3] = {inputVertices[vertexIndex].position[0], inputVertices[vertexIndex].position[1], inputVertices[vertexIndex].position[2]};
		m_vertexData[vertexIndex].SetPositionOfVertex(vertexPosition);
	}
}

//! @brief 三角形の各頂点に設けられたフラグを挙げる。
//! 
//! @param[in] triangle 三角形の頂点インデックスへのポインタ
void TrianglesWithLocalConnectivity::InputTrueIntoFlagsIndicatingUsedOrNot(const size_t* triangle)
{
	for(int i=0; i<3; ++i)
	{
		size_t vertexIndex = triangle[i];
		m_vertexData[vertexIndex].WriteTheVertexIsUsed();
	}
}

} // namespace polygonization
} // namespace lssplib

