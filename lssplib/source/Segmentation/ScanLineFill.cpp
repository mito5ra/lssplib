#include "Segmentation/ScanLineFill.h"

namespace lssplib
{
namespace segmentation
{

ScanLineFill::ScanLineFill(void)
: m_emptyValue(0)
, m_fillValue(0)
, m_imageSize()
, m_imageData(NULL)
{
}

ScanLineFill::~ScanLineFill(void)
{
}

//! @details 指定した位置のピクセルが空の時、同ピクセルを塗りつぶし、そして同位置情報を処理スタックに積む。
//! @param [in] pt 処理対象の位置
void ScanLineFill::UpdatePixel( const Point3i& pt )
{
	int idx = GetIndex(pt);
	if (m_imageData[idx] == m_emptyValue)
	{
		m_imageData[idx] = m_fillValue;
		m_queue.push(pt);
	}
}

//! @param [in] srcPoint ピクセルの位置
void ScanLineFill::MarkAdjacentPixels( const Point3i& srcPoint )
{
	// check +y
	if (srcPoint.y() < m_imageSize.height() - 1)
	{
		UpdatePixel(srcPoint + Point3i(0, 1, 0));
	}

	// check -y
	if (srcPoint.y() > 0)
	{
		UpdatePixel(srcPoint + Point3i(0, -1, 0));
	}

	// check +z
	if (srcPoint.z() < m_imageSize.depth() - 1)
	{
		UpdatePixel(srcPoint + Point3i(0, 0, 1));
	}

	// check -z
	if (srcPoint.z() > 0)
	{
		UpdatePixel(srcPoint + Point3i(0, 0, -1));
	}
}

//! @param [in] srcPoint ピクセルの更新の基点
//! @param [in] deltaX x方向位置の差分
void ScanLineFill::CheckDiagonalAndMarkAdjacentPixels(
	const Point3i& srcPoint, int deltaX )
{
	if (srcPoint.x() < 0 || srcPoint.x() >= m_imageSize.width()) return;

	// check +y diagonal
	if (srcPoint.y() < m_imageSize.height() - 1
		&& m_imageData[GetIndex(srcPoint + Point3i(deltaX, 1, 0))] != m_emptyValue)
	{
		UpdatePixel(srcPoint + Point3i(0, 1, 0));
	}

	// check -y diagonal
	if (srcPoint.y() > 0
		&& m_imageData[GetIndex(srcPoint + Point3i(deltaX, -1, 0))] != m_emptyValue)
	{
		UpdatePixel(srcPoint + Point3i(0, -1, 0));
	}

	// check +z diagonal
	if (srcPoint.z() < m_imageSize.depth() - 1
		&& m_imageData[GetIndex(srcPoint + Point3i(deltaX, 0, 1))] != m_emptyValue)
	{
		UpdatePixel(srcPoint + Point3i(0, 0, 1));
	}

	// check -z diagonal
	if (srcPoint.z() > 0
		&& m_imageData[GetIndex(srcPoint + Point3i(deltaX, 0, -1))] != m_emptyValue)
	{
		UpdatePixel(srcPoint + Point3i(0, 0, -1));
	}
}

//! @param [in] pt ピクセルの位置
//! @return 要素番号
int ScanLineFill::GetIndex( const Point3i& pt ) const
{
	return pt.x() + (pt.y() + pt.z() * m_imageSize.height()) * m_imageSize.width();
}

//! @details 塗りつぶし位置から画像データの塗りつぶし(Scan Line Fill)を開始する。
//! emptyValueの値を持つピクセルで塗りつぶしの対象となったピクセルは、fillValueの値で置換えられる。
//! @param [in] seedPt 塗りつぶし開始位置
//! @param [in] emptyValue 塗りつぶされるデータ値
//! @param [in] fillValue 塗りつぶすデータ値
//! @param [in] imageSize 画像サイズ
//! @param [in] imageData 画像データ
void ScanLineFill::Fill(
	const Point3i& seedPt,
	unsigned char emptyValue,
	unsigned char fillValue,
	const Size3i& imageSize,
	unsigned char* imageData )
{
	m_emptyValue = emptyValue;
	m_fillValue = fillValue;

	m_imageSize = imageSize;
	m_imageData = imageData;

	UpdatePixel(seedPt);

	Point3i basePoint;
	Point3i currentPoint;
	Point3i nextPoint;
	Point3i prevPoint;
	while (!m_queue.empty())
	{
		basePoint = m_queue.top();
		m_queue.pop();

		currentPoint = basePoint;
		nextPoint.x() = currentPoint.x() + 1;
		nextPoint.y() = currentPoint.y();
		nextPoint.z() = currentPoint.z();
		for(;;)
		{
			int idx = GetIndex(nextPoint);
			if (nextPoint.x() < m_imageSize.width() && m_imageData[idx] == emptyValue)
			{
				m_imageData[idx] = fillValue;
				CheckDiagonalAndMarkAdjacentPixels(currentPoint, 1);
			}
			else
			{
				MarkAdjacentPixels(currentPoint);
				break;
			}
			currentPoint =  nextPoint;
			++nextPoint.x();
		}

		currentPoint = basePoint;
		prevPoint.x() = currentPoint.x() - 1;
		prevPoint.y() = currentPoint.y();
		prevPoint.z() = currentPoint.z();
		for(;;)
		{
			int idx = GetIndex(prevPoint);
			if (prevPoint.x() >= 0 && m_imageData[idx] == emptyValue)
			{
				m_imageData[idx] = fillValue;
				CheckDiagonalAndMarkAdjacentPixels(currentPoint, -1);
			}
			else
			{
				MarkAdjacentPixels(currentPoint);
				break;
			}
			currentPoint =  prevPoint;
			--prevPoint.x();
		}

	}
}

} // namespace segmentation
} // namespace lssplib

