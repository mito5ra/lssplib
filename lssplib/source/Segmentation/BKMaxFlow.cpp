#include "Segmentation/BKMaxFlow.h"

#include <assert.h>

namespace lssplib
{
namespace segmentation
{

typedef Graph::Node::NodeGroup NodeGroupType;
typedef Graph::Node NodeType;

//-------------------------------------------------------------------
BKMaxFlow::BKMaxFlow()
: m_sourceNode(NULL)
, m_sinkNode(NULL)
, m_activeNodes()
, m_orphanNodes()
, m_flow(0.0) {}

//-------------------------------------------------------------------
BKMaxFlow::~BKMaxFlow() {}

//-------------------------------------------------------------------
//! @param [in] g グラフデータ
void BKMaxFlow::Execute(Graph* g)
{
	m_sourceNode = &g->SourceNode();
	m_sourceNode->SetGroup(NodeType::kNodeGroupSource);
	for (EdgePtr e = m_sourceNode->FirstEdge(); e != NULL; e = e->Next())
	{ // (source, p)
		if (e->Capacity() > 0.0)
		{
			NodePtr p = e->To();
			p->SetGroup(NodeType::kNodeGroupSource); // label the node as Source tree
			p->SetEdgeToParent(e->Reverse()); // the parent of the node is the source node
			m_activeNodes.push(p);
		}
	}

	m_sinkNode = &g->SinkNode();
	m_sinkNode->SetGroup(NodeType::kNodeGroupSink);
	for (EdgePtr e = m_sinkNode->FirstEdge(); e != NULL; e = e->Next())
	{
		NodePtr p = e->To();
		if (e->Reverse()->Capacity() > 0.0 &&
			p->Group() == NodeType::kNodeGroupFree)
		{
			p->SetGroup(NodeType::kNodeGroupSink); // label the node as Sink tree
			p->SetEdgeToParent(e->Reverse()); // the parent of the node is the sink node
			m_activeNodes.push(p);
		}
	}

	for(;;)
	{
		EdgePtr boundaryEdge = Growth();
		if (!boundaryEdge) break;

		Augmentation(boundaryEdge);

		Adoption();
	}
}

//-------------------------------------------------------------------
double BKMaxFlow::GetFlow() const { return m_flow; }

//-------------------------------------------------------------------
//! @details Source/Sinkの探索ツリーを成長させ、
//! SourceからSinkへ繋がる経路が出来た時、SourceとSinkの探索ツリーの境の辺を返す。
BKMaxFlow::EdgePtr BKMaxFlow::Growth()
{
	while(!m_activeNodes.empty())
	{
		NodePtr p = m_activeNodes.front();
		NodeGroupType nodeGroup = p->Group();

		if (nodeGroup == NodeType::kNodeGroupSource)
		{
			// edge (p,q)
			for (EdgePtr e = p->FirstEdge(); e != NULL; e = e->Next())
			{
				if (e->Capacity() <= 0.0) continue;

				NodePtr q = e->To(); // node q of the edge (p,q)
				if (q->Group() == NodeType::kNodeGroupSink) return e;
				else if (q->Group() == NodeType::kNodeGroupFree)
				{
					q->SetGroup(NodeType::kNodeGroupSource);
					q->SetEdgeToParent(e->Reverse());
					m_activeNodes.push(q);
				}
			}
		}
		else if (nodeGroup == NodeType::kNodeGroupSink)
		{
			// edge (p,q)
			for (EdgePtr e = p->FirstEdge(); e != NULL; e = e->Next())
			{
				if (e->Reverse()->Capacity() <= 0.0) continue;

				NodePtr q = e->To(); // node q of the edge (p,q)
				if (q->Group() == NodeType::kNodeGroupSource) return e;
				else if (q->Group() == NodeType::kNodeGroupFree)
				{
					q->SetGroup(NodeType::kNodeGroupSink);
					q->SetEdgeToParent(e->Reverse());
					m_activeNodes.push(q);
				}
			}
		}

		m_activeNodes.pop();
	}

	return NULL;
}

//-------------------------------------------------------------------
//! @details SourceとSinkの境の辺からSourceからSinkノードへ向かう経路を生成。
//! 経路上の辺の最小残留流量をグラフの流量に加え、
//! 経路上の辺の残留流量からその最小残留流量を引く。
//! 残留流量が0の辺のノードを孤児ノードとして登録する。
//! @param [in] boundaryEdge SourceとSinkの境の辺
void BKMaxFlow::Augmentation(EdgePtr boundaryEdge)
{
	if (boundaryEdge->From()->Group() == NodeType::kNodeGroupSink)
	{
		boundaryEdge = boundaryEdge->Reverse();
	}
	m_path.push_back(boundaryEdge);

	// get minimum residual capacity
	double minResidualCapacity = boundaryEdge->Capacity();
	NodePtr sourceSideNode = boundaryEdge->From();
	for (EdgePtr e = sourceSideNode->EdgeToParent();
		e != NULL; e = e->To()->EdgeToParent())
	{
		if (minResidualCapacity > e->Reverse()->Capacity())
		{
			minResidualCapacity = e->Reverse()->Capacity();
		}
		m_path.push_back(e->Reverse());
	}

	NodePtr sinkSideNode = boundaryEdge->To();
	for (EdgePtr e = sinkSideNode->EdgeToParent();
		e != NULL; e = e->To()->EdgeToParent())
	{
		if (minResidualCapacity > e->Capacity())
		{
			minResidualCapacity = e->Capacity();
		}
		m_path.push_back(e);
	}

	assert(minResidualCapacity > 0.0);

	// update flow
	m_flow += minResidualCapacity;

	// update residual capacity under path edges
	for (PathType::iterator itr = m_path.begin(); itr != m_path.end(); ++itr)
	{
		EdgePtr e = *itr;
		e->Capacity() -= minResidualCapacity;

		NodePtr p = e->From(); // source side node
		NodePtr q = e->To(); // sink side node

		if (e->Capacity() <= 0.0 && (p->Group() == q->Group()))
		{
			// add orphan node candidate to orphan node list
			if (p->Group() == NodeType::kNodeGroupSource)
			{
				m_orphanNodes.push(q);
			}
			else if (p->Group() == NodeType::kNodeGroupSink)
			{
				m_orphanNodes.push(p);
			}
		}

		if (p == m_sourceNode || q == m_sinkNode) continue; // ignore reverse edge for terminal nodes

		e->Reverse()->Capacity() += minResidualCapacity;
	}

	m_path.clear();
}

//-------------------------------------------------------------------
//! @details 孤児ノードの隣接ノードから新しい親ノードを探す。
//! 見つからない場合は、無所属のノードにする。
//! 孤児ノードを親とする隣接ノードは、孤児ノードとして登録する。
//! 孤児ノードの隣接ノードはアクティブノードとして登録。
void BKMaxFlow::Adoption()
{
	// look for new parent for orphan candidates and process orphans
	while (!m_orphanNodes.empty())
	{
		NodePtr p = m_orphanNodes.front();
		m_orphanNodes.pop();
		p->SetEdgeToParent(NULL);

		NodeGroupType nodeGroup = p->Group();

		// look for new parent for orphan node p
		if (nodeGroup == NodeType::kNodeGroupSource)
		{
			for (EdgePtr e = p->FirstEdge(); e != NULL; e = e->Next())
			{ // (p, q) where p is orphan node
				NodePtr q = e->To();

				// if p is source node, use (q, p) capacity = reverse of e
				if (q->Group() == NodeType::kNodeGroupSource &&
					e->Reverse()->Capacity() > 0.0 &&
					IsRootSource(q)) // p & q both from the same root
				{
					p->SetEdgeToParent(e);
					break;
				}
			}

			if (!p->EdgeToParent())
			{
				for (EdgePtr e = p->FirstEdge(); e != NULL; e = e->Next())
				{
					if (e->Capacity() <= 0.0) continue;
					NodePtr q = e->To(); // q node of edge (p, q)
					if (q->EdgeToParent() == e->Reverse())
					{
						q->SetEdgeToParent(NULL);
						m_orphanNodes.push(q);
					}

					if (q->Group() == NodeType::kNodeGroupSource)
					{ // res cap of edge (q, p)
						m_activeNodes.push(q);
					}
				}

				p->SetGroup(NodeType::kNodeGroupFree);
			}
		}
		else if (nodeGroup == NodeType::kNodeGroupSink)
		{
			for (EdgePtr e = p->FirstEdge(); e != NULL; e = e->Next())
			{ // (p, q) where p is orphan node
				NodePtr q = e->To();

				// if p is sink node, use (p, q) capacity
				if (q->Group() == NodeType::kNodeGroupSink &&
					e->Capacity() > 0.0 &&
					IsRootSink(q)) // p & q both from the same root
				{
					p->SetEdgeToParent(e);
					break;
				}
			}

			if (!p->EdgeToParent())
			{
				for (EdgePtr e = p->FirstEdge(); e != NULL; e = e->Next())
				{
					if (e->Reverse()->Capacity() <= 0.0) continue;
					NodePtr q = e->To(); // q node of edge (p, q)
					if (q->EdgeToParent() == e->Reverse())
					{
						q->SetEdgeToParent(NULL);
						m_orphanNodes.push(q);
					}

					if (q->Group() == NodeType::kNodeGroupSink)
					{ // outgoing edge from q
						m_activeNodes.push(q);
					}
				}

				p->SetGroup(NodeType::kNodeGroupFree);
			}
		}
	}
}

//! @param [in] node ノード
//! @return Sourceノードと繋がっていればtrue。それ以外はfalse。
bool BKMaxFlow::IsRootSource(NodePtr node)
{
	NodePtr root = node;
	while (root->EdgeToParent())
	{
		root = root->EdgeToParent()->To();
	}

	return (root == m_sourceNode);
}

//! @param [in] node ノード
//! @return Sinkノードと繋がっていればtrue。それ以外はfalse。
bool BKMaxFlow::IsRootSink(NodePtr node)
{
	NodePtr root = node;
	while (root->EdgeToParent())
	{
		root = root->EdgeToParent()->To();
	}

	return (root == m_sinkNode);
}

} // namespace segmentation
} // namespace lssplib

