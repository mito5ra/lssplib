#include "Segmentation/VesselTree.h"

#include <stack>

namespace lssplib
{
namespace segmentation
{

VesselTree::VesselTree()
: m_startNode(NULL)
, m_origin(0, 0, 0)
, m_size(0, 0, 0)
, m_spacing(0.0, 0.0, 0.0)
, m_type(kVesselTypeInvalid)
{ 

}

VesselTree::~VesselTree()
{
	DeleteBranches(m_startNode);
}

//! 血管構造データの座標で設定する。
//! @param [in] x x位置
//! @param [in] y y位置
//! @param [in] z z位置
//! @return 見つかったノード。該当がなければNULL。
VesselNode* VesselTree::FindNodeAt( int x, int y, int z ) const
{
	if (!m_startNode) return NULL;

	std::stack<VesselNode*> nodes;
	nodes.push(m_startNode);

	while (!nodes.empty())
	{
		VesselNode* currentNode = nodes.top();
		nodes.pop();

		if (
			currentNode->GetPosition().x() == x &&
			currentNode->GetPosition().y() == y &&
			currentNode->GetPosition().z() == z)
		{
			return currentNode;
		}

		for (size_t i = 0; i < currentNode->GetOutgoings().size(); ++i)
		{
			nodes.push(currentNode->GetOutgoings()[i]);
		}
	}

	return NULL;
}

//------------------------------------------------------------------------------
//! @param [in] aNode ノード
void VesselTree::DeleteBranches( VesselNode* aNode )
{
	if (!aNode) return;

	VesselNode* inNode = aNode->GetIncoming();

	if (inNode)
	{
		std::vector<VesselNode*>::iterator itr =
			std::find(inNode->GetOutgoings().begin(), inNode->GetOutgoings().end(), aNode);
		if (itr != inNode->GetOutgoings().end())
		{
			inNode->GetOutgoings().erase(itr);
		}
		if (!inNode->GetOutgoings().empty()) UpdateBranchLevel(inNode);
	}

	std::stack<VesselNode*> nodes;
	nodes.push(aNode);

	while (!nodes.empty())
	{
		VesselNode* currentNode = nodes.top();
		nodes.pop();

		for (size_t i = 0; i < currentNode->GetOutgoings().size(); ++i)
		{
			nodes.push(currentNode->GetOutgoings()[i]);
		}

		if (currentNode == m_startNode) m_startNode = NULL;
		delete currentNode;
	}
}

//------------------------------------------------------------------------------
void VesselTree::UpdateBranchLevel()
{
	UpdateBranchLevel(m_startNode);
}

//------------------------------------------------------------------------------
//! @param [in] aNode ノード
void VesselTree::UpdateBranchLevel( VesselNode* aNode )
{
	if (!aNode) return;

	VesselNode* node = aNode;
	std::stack<VesselNode*> nodeStack;
	nodeStack.push(aNode);
	while (!nodeStack.empty())
	{
		node = nodeStack.top();
		nodeStack.pop();

		if (node->GetIncoming() == NULL)
		{
			node->SetBranchLevel(0);
		}
		else if (node->GetIncoming()->GetOutgoings().size() > 1)
		{
			node->SetBranchLevel(node->GetIncoming()->GetBranchLevel() + 1);
		}
		else
		{
			node->SetBranchLevel(node->GetIncoming()->GetBranchLevel());
		}

		for (size_t n = 0; n < node->GetOutgoings().size(); ++n)
		{
			nodeStack.push(node->GetOutgoings()[n]);
		}
	}
}

//------------------------------------------------------------------------------
//! @param [in] node ノード
const VesselNode* VesselTree::GetBranchStart( const VesselNode* node ) const
{
	const VesselNode* currentNode = node;
	while (currentNode->GetIncoming())
	{
		if (currentNode->GetIncoming()->GetOutgoings().size() > 1)
		{
			return currentNode;
		}
		currentNode = currentNode->GetIncoming();
	}

	return m_startNode;
}

//------------------------------------------------------------------------------
//! @param [in] node ノード
VesselNode* VesselTree::GetBranchStart( VesselNode* node )
{
	VesselNode* currentNode = node;
	while (currentNode->GetIncoming())
	{
		if (currentNode->GetIncoming()->GetOutgoings().size() > 1)
		{
			return currentNode;
		}
		currentNode = currentNode->GetIncoming();
	}

	return m_startNode;
}

//------------------------------------------------------------------------------
//! @param [in] node ノード
const VesselNode* VesselTree::GetPreviousJunction( const VesselNode* node ) const
{
	const VesselNode* currentNode = node;
	while (currentNode->GetIncoming())
	{
		if (currentNode->GetIncoming()->GetOutgoings().size() > 1)
		{
			return currentNode->GetIncoming();
		}
		currentNode = currentNode->GetIncoming();
	}

	return m_startNode;
}

//------------------------------------------------------------------------------
//! @param [in] node ノード
const VesselNode* VesselTree::GetNextJunction( const VesselNode* node ) const
{
	std::stack<const VesselNode*> nodeStack;
	nodeStack.push(node);
	while (!nodeStack.empty())
	{
		const VesselNode* currentNode = nodeStack.top();
		nodeStack.pop();

		if (currentNode->GetOutgoings().empty() || currentNode->GetOutgoings().size() > 1)
		{
			return currentNode;
		}

		for (size_t n = 0; n < currentNode->GetOutgoings().size(); ++n)
		{
			nodeStack.push(currentNode->GetOutgoings()[n]);
		}
	}

	return NULL;
}

//------------------------------------------------------------------------------
//! @param [in] startNode ノード
//! @param [in] endNode もう一方のノード
//! @return ノード数
int VesselTree::CountNumberOfNodesBetween( const VesselNode* startNode, const VesselNode* endNode ) const
{
	// scan from start node toward origin
	int count = 1;
	const VesselNode* currentNode = startNode;
	while (currentNode->GetIncoming())
	{
		++count;
		if (currentNode->GetIncoming() == endNode)
		{
			return count;
		}
		currentNode = currentNode->GetIncoming();
	}

	// scan from end node toward origin
	count = 1;
	currentNode = endNode;
	while (currentNode->GetIncoming())
	{
		++count;
		if (currentNode->GetIncoming() == endNode)
		{
			return count;
		}
		currentNode = currentNode->GetIncoming();
	}

	return -1;
}

//------------------------------------------------------------------------------
//! @return ノード数
int VesselTree::GetNumberOfNodes() const
{
	if (!m_startNode) return 0;

	int count = 0;
//	const VesselNode* currentNode = m_startNode;
	std::stack<const VesselNode*> nodeStack;
	nodeStack.push(m_startNode);
	while (!nodeStack.empty())
	{
		const VesselNode* currentNode = nodeStack.top();
		nodeStack.pop();

		++count;

		for (size_t n = 0; n < currentNode->GetOutgoings().size(); ++n)
		{
			nodeStack.push(currentNode->GetOutgoings()[n]);
		}
	}

	return count;
}

//------------------------------------------------------------------------------
void VesselTree::UpdateNodeID()
{
	if (!m_startNode) return;

	int nodeID = 0;
	std::stack<VesselNode*> nodes;
	nodes.push(m_startNode);

	while (!nodes.empty())
	{
		VesselNode* currentNode = nodes.top();
		nodes.pop();

		currentNode->SetNodeId(nodeID);
		++nodeID;

		for (size_t i = 0; i < currentNode->GetOutgoings().size(); ++i)
		{
			nodes.push(currentNode->GetOutgoings()[i]);
		}
	}
}

//------------------------------------------------------------------------------
//! @return 複製された血管構造データのポインタ
VesselTree* VesselTree::Duplicate() const
{
	if (!m_startNode) return NULL;

	VesselNode* newStartNode = NULL;

	std::stack<VesselNode*> nodes;
	std::stack<VesselNode*> dupNodes;
	nodes.push(m_startNode);
	dupNodes.push(NULL);

	while (!nodes.empty())
	{
		VesselNode* currentNode = nodes.top();
		nodes.pop();

		VesselNode* prevDupNode = dupNodes.top();
		dupNodes.pop();

		VesselNode* dupNode = new VesselNode;
		dupNode->SetPosition(currentNode->GetPosition());
		dupNode->SetDirection(currentNode->GetDirection());
		dupNode->SetRadius(currentNode->GetRadius());
		dupNode->SetBranchLevel(currentNode->GetBranchLevel());
		dupNode->SetNodeId(currentNode->GetNodeId());
		if (prevDupNode)
		{
			dupNode->SetIncoming(prevDupNode);
			prevDupNode->GetOutgoings().push_back(dupNode);
		}
		else
		{
			newStartNode = dupNode;
		}

		for (size_t i = 0; i < currentNode->GetOutgoings().size(); ++i)
		{
			nodes.push(currentNode->GetOutgoings()[i]);

			dupNodes.push(dupNode);
		}
	}

	VesselTree* newTree = new VesselTree;
	newTree->SetOrigin(m_origin);
	newTree->SetSize(m_size);
	newTree->SetSpacing(m_spacing);
	newTree->SetStartNode(newStartNode);
	newTree->SetVesselType(m_type);

	return newTree;
}

//------------------------------------------------------------------------------
//! @return 妥当ならばtrue。問題があればfalseを返す。
bool VesselTree::ValidityCheck() const
{
	if (!m_startNode) return true;

	std::stack<VesselNode*> nodes;
	nodes.push(m_startNode);

	while (!nodes.empty())
	{
		VesselNode* currentNode = nodes.top();
		nodes.pop();

		if (currentNode->GetIncoming())
		{
			if ( (currentNode->GetPosition().cast<double>() - currentNode->GetIncoming()->GetPosition().cast<double>()).norm() > 5.0 )
			{
				return false;
			}
		}

		for (size_t i = 0; i < currentNode->GetOutgoings().size(); ++i)
		{
			nodes.push(currentNode->GetOutgoings()[i]);
		}
	}

	return true;
}

//------------------------------------------------------------------------------
VesselNode::Iterator VesselTree::Begin()
{
	return VesselNode::Iterator(m_startNode);
}

//------------------------------------------------------------------------------
VesselNode::ConstIterator VesselTree::ConstBegin() const
{
	return VesselNode::ConstIterator(m_startNode);
}

//------------------------------------------------------------------------------
VesselTree* VesselTree::Clone() const
{
	std::stack<VesselNode*> nodeStack;
	nodeStack.push(m_startNode);

	// cloned tree
	VesselNode* newRootNode = new VesselNode(*m_startNode);
	std::stack<VesselNode*> cloneStack;
	cloneStack.push(newRootNode);

	while(!nodeStack.empty())
	{
		VesselNode* currentNode = nodeStack.top();
		nodeStack.pop();

		VesselNode* newNode = cloneStack.top();
		cloneStack.pop();

		for (VesselNode::OutgoingNodeList::const_iterator childItr = currentNode->GetOutgoings().begin();
			childItr != currentNode->GetOutgoings().end(); ++childItr)
		{
			VesselNode* childNode = *childItr;
			VesselNode* cloneChildNode = new VesselNode(*childNode);
			newNode->AddOutgoing(cloneChildNode);
			cloneChildNode->SetIncoming(newNode);

			nodeStack.push(childNode);
			cloneStack.push(cloneChildNode);
		}
	}

	VesselTree* clonedTree = new VesselTree;
	clonedTree->SetStartNode(newRootNode);
	clonedTree->SetOrigin(m_origin);
	clonedTree->SetSize(m_size);
	clonedTree->SetSpacing(m_spacing);
	clonedTree->SetVesselType(m_type);

	return clonedTree;
}

//------------------------------------------------------------------------------
void VesselTree::Decimate( double targetInterval )
{

	std::vector<VesselNode*> nodesToBeRemoved;
	CollectNodesToDecimate(targetInterval, nodesToBeRemoved);

	for (std::vector<VesselNode*>::iterator itr = nodesToBeRemoved.begin();
		itr != nodesToBeRemoved.end(); ++itr)
	{
		VesselNode* aNode = (*itr);
		VesselNode* parent = aNode->GetIncoming();
		VesselNode* child = aNode->GetOutgoings()[0]; // junction not included
		aNode->Detach();
		parent->AddOutgoing(child);
		child->SetIncoming(parent);
		delete aNode;
	}

}

//------------------------------------------------------------------------------
void VesselTree::CollectNodesToDecimate(double targetInterval, std::vector<VesselNode*>& nodesToBeRemoved) const
{
	if (targetInterval <= 0.0 || !m_startNode) return;

	std::stack<double> distStack;
	distStack.push(0.0);
	std::stack<VesselNode*> nodeStack;
	nodeStack.push(m_startNode);
	while (!nodeStack.empty())
	{
		VesselNode* currentNode = nodeStack.top();
		nodeStack.pop();
		double currentDist = distStack.top();
		distStack.pop();

		for (VesselNode::OutgoingNodeList::const_iterator childItr = currentNode->GetOutgoings().begin();
			childItr != currentNode->GetOutgoings().end(); ++childItr)
		{
			VesselNode* childNode = *childItr;
			nodeStack.push(childNode);
			Vector3i posDiff = childNode->GetPosition() - currentNode->GetPosition();
			Vector3d posDiffReal(posDiff[0] * m_spacing[0], posDiff[1] * m_spacing[1], posDiff[2] * m_spacing[2]);
			double newDist = currentDist + posDiffReal.norm();
			if ( (static_cast<int>(currentDist/targetInterval) == static_cast<int>(newDist/targetInterval)) &&
				!childNode->IsJunction() && !childNode->IsTerminal())
			{
				nodesToBeRemoved.push_back(childNode);
			}

			distStack.push(newDist);
		}
	}

}


} // namespace segmentation
} // namespace lssplib
