#include "Segmentation/VesselNode.h"
#include <stack>

namespace lssplib
{
namespace segmentation
{
	//------------------------------------------------------------------------------
	VesselNode::Iterator::Iterator()
		: m_currentNode(NULL)
		, m_nodeStack() {}

	VesselNode::Iterator::Iterator(VesselNode* src)
		: m_currentNode(NULL)
		, m_nodeStack() { m_nodeStack.push_back(src); }

	VesselNode* VesselNode::Iterator::Next()
	{
		if (m_nodeStack.empty()) return NULL;

		m_currentNode = m_nodeStack.back();
		m_nodeStack.pop_back();

		for (size_t i = 0; i < m_currentNode->GetOutgoings().size(); ++i)
		{
			VesselNode* child = m_currentNode->GetOutgoings()[i];
			m_nodeStack.push_back(child);
		}

		return m_currentNode;
	}

	//------------------------------------------------------------------------------
	VesselNode::ConstIterator::ConstIterator()
		: m_currentNode(NULL)
		, m_nodeStack() {}

	VesselNode::ConstIterator::ConstIterator(const VesselNode* src)
		: m_currentNode(NULL)
		, m_nodeStack() { m_nodeStack.push_back(src); }

	const VesselNode* VesselNode::ConstIterator::Next()
	{
		if (m_nodeStack.empty()) return NULL;

		m_currentNode = m_nodeStack.back();
		m_nodeStack.pop_back();

		for (size_t i = 0; i < m_currentNode->GetOutgoings().size(); ++i)
		{
			const VesselNode* child = m_currentNode->GetOutgoings()[i];
			m_nodeStack.push_back(child);
		}

		return m_currentNode;
	}

//------------------------------------------------------------------------------
//! 根本側に接続しているノードの末端ノード配列から自身のポインタを消し、根本側ノードのポインタをクリア。
void VesselNode::Detach()
{
	OutgoingNodeList::iterator itr = std::find(m_in->GetOutgoings().begin(), m_in->GetOutgoings().end(), this);
	if (itr != m_in->GetOutgoings().end())
	{
		m_in->GetOutgoings().erase(itr);
	}
	m_in = NULL;
}

//------------------------------------------------------------------------------
//! 根本側のノードのポインタを入れ替え、そのノードの末端ノード配列に自身を加える。
//! @param [in] targetNode 接続先のノード
void VesselNode::Attach( VesselNode* targetNode )
{
	targetNode->AddOutgoing(this);
	m_in = targetNode;
}

//------------------------------------------------------------------------------
//! @return 枝の根本のノードのポインタ
const VesselNode* VesselNode::GetBranchStart() const
{
	const VesselNode* currentNode = this;
	while (currentNode->GetIncoming())
	{
		if (currentNode->GetIncoming()->GetOutgoings().size() > 1)
		{
			return currentNode;
		}
		currentNode = currentNode->GetIncoming();
	}

	return currentNode;
}

//------------------------------------------------------------------------------
//! @return 枝の根本のノードのポインタ
VesselNode* VesselNode::GetBranchStart()
{
	VesselNode* currentNode = this;
	while (currentNode->GetIncoming())
	{
		if (currentNode->GetIncoming()->GetOutgoings().size() > 1)
		{
			return currentNode;
		}
		currentNode = currentNode->GetIncoming();
	}

	return currentNode;
}

//------------------------------------------------------------------------------
//! @return 枝が繋がっている分岐ノードのポインタ。
const VesselNode* VesselNode::GetPreviousJunction() const
{
	const VesselNode* currentNode = this;
	while (currentNode->GetIncoming())
	{
		if (currentNode->GetIncoming()->GetOutgoings().size() > 1)
		{
			return currentNode->GetIncoming();
		}
		currentNode = currentNode->GetIncoming();
	}

	return NULL;
}

//------------------------------------------------------------------------------
//! @return 枝が繋がっている分岐ノードのポインタ。
const VesselNode* VesselNode::GetNextJunction() const
{
	std::stack<const VesselNode*> nodeStack;
	nodeStack.push(this);
	while (!nodeStack.empty())
	{
		const VesselNode* currentNode = nodeStack.top();
		nodeStack.pop();

		if (currentNode->GetOutgoings().empty() || currentNode->GetOutgoings().size() > 1)
		{
			return currentNode;
		}

		for (size_t n = 0; n < currentNode->GetOutgoings().size(); ++n)
		{
			nodeStack.push(currentNode->GetOutgoings()[n]);
		}
	}

	return NULL;
}

//------------------------------------------------------------------------------
//! @return 分岐点であるならばtrue、それ以外はfalse。
bool VesselNode::IsJunction() const
{
	return (m_out.size() > 1);
}

//------------------------------------------------------------------------------
//! @return 末端点であるならばtrue、それ以外はfalse。
bool VesselNode::IsTerminal() const
{
	return m_out.empty();
}

//------------------------------------------------------------------------------
//! @details ノードが所属する枝を構成するノード数を返す。
//! 枝の根本から次の分岐又は末端点までのノード数。
//! 根本側のノードは分岐点を含まず、分岐点の次のノード。
//! @return ノードの数
size_t VesselNode::GetNumNodesInBranch() const
{
	size_t numNodes = 0;
	const VesselNode* currentNode = this;
	while (currentNode)
	{
		++numNodes;

		if (currentNode->IsTerminal() || currentNode->IsJunction())
		{
			break;
		}

		currentNode = currentNode->GetOutgoings()[0];
	}

	currentNode = this->m_in;
	while(currentNode)
	{
		if (currentNode->IsJunction())
		{
			break;
		}

		++numNodes;

		currentNode = currentNode->GetIncoming();
	}

	return numNodes;
}

//------------------------------------------------------------------------------
VesselNode::Iterator VesselNode::Begin()
{
	return Iterator(this);
}

//------------------------------------------------------------------------------
VesselNode::ConstIterator VesselNode::ConstBegin() const
{
	return ConstIterator(this);
}

//------------------------------------------------------------------------------
void RetrieveVesselNodePoints( const VesselNode* rootNode, std::vector<Point3i>& pointList )
{
	VesselNode::ConstIterator itr = rootNode->ConstBegin();
	while (itr.Next())
	{
		const VesselNode* currentNode = itr.Get();
		pointList.push_back(currentNode->GetPosition());
	}
}

} // namespace segmentation
} // namespace lssplib
