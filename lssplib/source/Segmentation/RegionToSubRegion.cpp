#include "Segmentation/RegionToSubRegion.h"
#include "Segmentation/ScanLineFill.h"
#include <vector>

//! 領域情報の名称に前方に追加する文字列
#define REGION_NAME_PREFIX std::wstring(L"領域")
typedef unsigned int UINT;

namespace lssplib
{
namespace segmentation
{

//! @brief イメージ領域でerosion処理を行いサブ領域に分ける機能の実装クラス。
class RegionToSubRegionImpl
{
	enum
	{
		kRegionGrowOrigVal = 1, //!< 領域拡大処理と対象
		kErosionTempVal = 2, //!< Erosionで追加されたピクセル
		kRegionConnectedToExclusionPixelVal = 3, //!< 処理対象外
	};
public:
	//! @brief 標準コンストラクタ
	RegionToSubRegionImpl();

	//! @brief デストラクタ
	virtual ~RegionToSubRegionImpl();

	//! 領域情報の構造体
	struct RegionInfo
	{
		Point3i origin; //!< 原点
		Size3i size; //!< サイズ
		std::wstring name; //!< 名称
		UCHAR value; //!< 値
		float color[3]; //!< 色

		//! @brief コンストラクタ
		//!
		//! @param [in] _value 値
		//! @param [in] _name 名称
		//! @param [in] _color 色
		//! @param [in] _origin 原点
		//! @param [in] _size サイズ
		RegionInfo(UCHAR _value, const std::wstring& _name, const float* _color, const Point3i& _origin, const Point3i& _size) : name(_name), value(_value), origin(_origin), size(_size)
		{
			memcpy(color, _color, sizeof(float) * 3);
		}
	};

	//! サブ領域情報リストの型
	typedef std::vector<RegionInfo> RegionInfos;

	//! @brief 結果とかをクリアー
	void Clear();

	//! @brief 処理対象の領域をセット
	//! @brief ピクセルの値が0以外の場合は領域として処理される
	//!
	//! @param [in] pRegionBuffer 対象の領域のバッファへのポインタ
	//! @param [in] regionOrigin バッファの原点
	//! @param [in] regionDim バッファのx,y,z方向のサイズ
	void SetRegion(const UCHAR* pRegionBuffer, const Point3i& regionOrigin, const Size3i& regionDim);

	// pExculdeBuffer - binary buffer with labeling as 255(region) or 0(no region)
	//! @brief 処理対象外の領域をセット
	//! @brief ピクセルの値が255の場合は対象外の領域
	//!
	//! @param [in] pExcludeBuffer 対象外の領域のバッファへのポインタ
	//! @param [in] origin バッファの原点
	//! @param [in] size バッファのx,y,z方向のサイズ
	void SetRegionToExclude(const UCHAR* pExcludeBuffer, const Point3i& origin, const Size3i& size);

	//! @brief 一度erosion処理を行い、サブ領域を生成
	//!
	//! @return もう一度erosionでサブ領域生成される可能性がある場合はtrue
	bool TryToGenerateSubRegion();

	//! @brief サブ領域の数を取得
//	int GetSubRegionCount() const { return m_regions.size(); }
	size_t GetSubRegionCount() const { return m_regions.size(); }

	//! @brief サブ領域の原点を取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return サブ領域の原点
	Point3i GetSubRegionOrigin(size_t regionIndex) const { return m_regions[regionIndex].origin; }

	//! @brief サブ領域のサイズを取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return サブ領域のサイズ
	Point3i GetSubRegionSize(size_t regionIndex) const { return m_regions[regionIndex].size; }

	//! @brief サブ領域の色を取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return サブ領域の色(floatの3要素)
	Point3f GetSubRegionColor(size_t regionIndex) const { return Point3f(m_regions[regionIndex].color[0], m_regions[regionIndex].color[1], m_regions[regionIndex].color[2]); }

	//! @brief サブ領域の名称を取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return サブ領域の名称
	std::wstring GetSubRegionName(size_t regionIndex) const { return m_regions[regionIndex].name; }

	//! @brief 領域の原点を取得
	Point3i GetOrigin() const { return m_regionOrigin; }

	//! @brief 領域のサイズを取得
	Size3i GetDimension() const { return m_regionDim; }

	//! @brief 領域バッファへのポインタを取得
	const UCHAR* GetRegionBuffer() const { return &m_region[0]; }

	//! @brief サブ領域の値を取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return 領域の値
	UCHAR GetSubRegionValue(size_t regionIndex) const { return m_regions[regionIndex].value; }

	//! @brief 対象外のサブ領域の値を取得
	UCHAR GetExcludedRegionValue() const { return kRegionConnectedToExclusionPixelVal; }

	//! @brief 対象外のサブ領域の色を取得
	Point3f GetExcludedRegionColor() const { return Point3f(.0f, 0.99609375f, .0f); }

	//! @brief サブ領域を生成させる
	//!
	//! @return もう一度処理行ってサブ領域生成される可能性がある場合はtrue
	bool GenerateSubRegion();

	//! @brief サブ領域を探し、インデックスを取得
	//!
	//! @param [in] regionValue 領域の値
	//! @return サブ領域のインデックス
	int FindSubRegionIndex(UCHAR regionValue) const;

	//! @brief サブ領域を削除
	//!
	//! @param [in] regionValue 領域の値
	void EraseSubRegion(UCHAR regionValue);

	//! @brief サブ領域生成される可能性があるかどうか示すフラグを取得
	bool IsRegionExhausted() const { return m_isRegionExhausted; }

private:
	//! @brief サブ領域生成される可能性があるかどうか示すフラグの値を更新
	bool IsFurtherSubRegionPossible();

	//! @brief 3D配列を一次元配列に並び替えた時の近傍のオフセットを取得
	std::vector<int> GetNeighbourOffset(int& size) const;

	//! @brief erosion処理を行う
	void DoErosion();

	//! @brief ピクセルの値を入れ替える
	//!
	//! @param [in] currentValue 入れ替え前のピクセルの値
	//! @param [in] newValue 入れ替え後のピクセルの値
	void ReplaceRegionBufferValue(UCHAR currentValue, UCHAR newValue);

	//! @brief 領域情報のインデックスを取得
	int FindRegionInfo(int regionValue) const;

	//! @brief 領域拡大処理のために配列にラベルを付ける
	void PrepareRegionBufferToRegionGrow();

	//! @brief 領域拡大処理
	//!
	//! @param [in] seedPoint 領域拡大処理のための出発点
	//! @param [in] searchFor 領域の拡大するとき入れ替えるピクセルの値
	//! @param [in] replaceWith 領域拡大し、入れ替えたピクセルの値
	//! @return 常にtrue
	bool DoRegionGrowing(const Point3i& seedPoint, UCHAR searchFor, UCHAR replaceWith);

	//! @brief 領域対象外のピクセルと繋がっている領域を分ける
	void ClassifyRegionConnectedToExcludedPixels();

	//! @brief 領域を分ける処理
	//!
	//! @return 処理が成功したかどうかを示すフラグ
	bool ClassifyRegion();

	//! @brief 新規の領域情報を追加
	void AddNewRegionInfo(UCHAR regionValue, const std::wstring& _name = std::wstring(L""), const float* _color = NULL, const Point3i& origin = Point3i(-1, -1, -1), const Size3i& size = Size3i(0, 0, 0));

	//! @brief サブ領域のバウンディング ボックスを求める
	//!
	//! @param [in] regionValue サブ領域の値
	//! @return サブ領域のバウンディング ボックス
	Boxi CalculateRegionBoundingBox(int regionValue) const;

	//! @brief 領域のバウンディング バックスを更新
	void UpdateRegionBoundingBox();

	//! @brief MxNxKサイズの領域を除く処理
	void FilterSmallRegions();

private:
	static float m_region_colors[][3]; //!< 領域に割り振られる色のリスト
	std::vector<UCHAR> m_region; //!< 領域情報を書き込むための配列
	Point3i m_regionOrigin; //!< 領域の原点
	Size3i m_regionDim; //!< 領域情報配列のサイズ
	RegionInfos m_regions; //!< 各領域の情報
	std::vector<Point3i> m_pixelsToExclude; //!< 計算対象外の領域
	bool m_isRegionExhausted; //!< サブ領域処理のために領域が残っているかどうかを示すフラグ
};

//! 領域に割り振られる色のリストを初期化
float RegionToSubRegionImpl::m_region_colors[][3] =
{
	{0.283203125,	0.240234375,	0.544921875},	//colDarkSlateBlue
	{0,				0.748046875,	0.998046875},	//colDeepSkyBlue
	{0.134765625,	0.544921875,	0.134765625},	//colForestGreen
	{0.853515625,	0.646484375,	0.126953125},	//colGoldenRod
	{0.998046875,	0.412109375,	0.705078125},	//colHotPink
	{0.998046875,	0,				0.998046875},	//colMagenta
	{0.419921875,	0.556640625,	0.138671875},	//colOliveDrab
	{0.595703125,	0.982421875,	0.595703125},	//colPaleGreen
	{0.998046875,	0,				0},				//colRed
	{0.275390625,	0.509765625,	0.705078125},	//colSteelBlue
	{0.998046875,	0.998046875,	0},				//colYellow
};

RegionToSubRegionImpl::RegionToSubRegionImpl()
: m_regionDim(0, 0, 0)
, m_regionOrigin(0, 0, 0)
, m_isRegionExhausted(true)
{
}

RegionToSubRegionImpl::~RegionToSubRegionImpl()
{
}

void RegionToSubRegionImpl::Clear()
{
	m_region.clear();
	m_regionOrigin = Point3i(0, 0, 0);
	m_regionDim = Point3i(0, 0, 0);
	m_regions.clear();
	m_pixelsToExclude.clear();
}

void RegionToSubRegionImpl::SetRegion(const UCHAR* pRegionBuffer, const Point3i& regionOrigin, const Size3i& regionDim)
{
	m_isRegionExhausted = false;
	int voxelCount = regionDim.prod();
	if(pRegionBuffer == NULL || voxelCount <= 0)
	{
		m_regionOrigin = Point3i(0, 0, 0);
		m_regionDim = Point3i(0, 0, 0);
		m_region.clear();
		return;
	}

	m_regionOrigin = regionOrigin;
	m_regionDim = regionDim;
	m_region.resize(voxelCount);
	memcpy(&m_region[0], pRegionBuffer, sizeof(UCHAR) * voxelCount);
}

void RegionToSubRegionImpl::SetRegionToExclude(const UCHAR* pExcludeBuffer, const Point3i& origin, const Size3i& size)
{
	m_pixelsToExclude.clear();
	if(pExcludeBuffer == NULL)
	{
		return;
	}

	int voxelCount = size.prod();

	int reserveCount = int(ceil(voxelCount * 0.1));
	m_pixelsToExclude.reserve(reserveCount);

	const int& w = size.width();
	const int& h = size.height();
	const int& d = size.depth();
	const int& minx = origin.x();
	const int& miny = origin.y();
	const int& minz = origin.z();

	const UCHAR* pSrc = pExcludeBuffer;
	for(int z = 0; z < d; ++z)
	{
		for(int y = 0; y < h; ++y)
		{
			for(int x = 0; x < w; ++x, ++pSrc)
			{
				if(*pSrc == 255)
				{
					m_pixelsToExclude.push_back(Point3i(x + minx, y + miny, z + minz));
				}
			}
		}
	}

	size_t pointCount = m_pixelsToExclude.size();
	m_pixelsToExclude.resize(pointCount);
}

bool RegionToSubRegionImpl::TryToGenerateSubRegion()
{
	DoErosion();

	m_regions.clear();
	PrepareRegionBufferToRegionGrow();
	ClassifyRegionConnectedToExcludedPixels();
	ClassifyRegion();
	UpdateRegionBoundingBox();
	FilterSmallRegions();

	return IsFurtherSubRegionPossible();
}

bool RegionToSubRegionImpl::GenerateSubRegion()
{
	bool hasRegion;
	do
	{
		hasRegion = TryToGenerateSubRegion();

		if(m_regions.size() > 0)
		{
			return true;
		}

	}while(hasRegion == true);

	return false;
}

std::vector<int> RegionToSubRegionImpl::GetNeighbourOffset(int& size) const
{
	const int& w = m_regionDim.width();
	const int& h = m_regionDim.height();
	int s = w * h;

	enum {kN26 = 0, kN18, kN10, kN06, kN04} neighbour_type = kN06;

	int neighbour26[] = {-s-w-1, -s-w, -s-w+1, -s-1, -s, -s+1, -s+w-1, -s+w, -s+w+1, -w-1, -w, -w+1, -1, 1, w-1, w, w+1, s-w-1, s-w, s-w+1, s-1, s, s+1, s+w-1, s+w, s+w+1};
	int neighbour18[] = {-s-w, -s-1, -s, -s+1, -s+w, -w-1, -w, -w+1, -1, 1, w-1, w, w+1, s-w, s-1, s, s+1, s+w};
	int neighbour10[] = {-s, -w-1, -w, -w+1, -1, 1, w-1, w, w+1, s};
	int neighbour06[] = {-s, -w, -1, 1, w, s};
	int neighbour04[] = {-w, -1, 1, w};

	int* neighbourSelected = NULL;
	switch(neighbour_type)
	{
	case kN26:
		neighbourSelected = neighbour26;
		size = _countof(neighbour26);
		break;
	case kN18:
		neighbourSelected = neighbour18;
		size = _countof(neighbour18);
		break;
	case kN10:
		neighbourSelected = neighbour10;
		size = _countof(neighbour10);
		break;
	case kN06:
		neighbourSelected = neighbour06;
		size = _countof(neighbour06);
		break;
	case kN04:
		neighbourSelected = neighbour04;
		size = _countof(neighbour04);
		break;
	}

	std::vector<int> neighbour(size);
	memcpy(&neighbour[0], neighbourSelected, sizeof(int) * size);

	return neighbour;
}

void RegionToSubRegionImpl::DoErosion()
{
	int w = m_regionDim.width();
	int h = m_regionDim.height();
	int d = m_regionDim.depth();
	UCHAR* p = &m_region[0];
	UCHAR tempVal = kErosionTempVal;

	int neighbourCount = 0;
	std::vector<int> neighbour = GetNeighbourOffset(neighbourCount);

	for(int z = 0; z < d; ++z)
	{
		for(int y = 0; y < h; ++y)
		{
			for(int x = 0; x < w; ++x, ++p)
			{
				// set tempVal to boundary pixels and do nothing for pixels already with 0
				if(z <= 0 || z >= d - 1 || y <= 0 || y >= h - 1 || x <= 0 || x >= w - 1 || *p == 0)
				{
					if(*p != 0) *p = tempVal;
					continue;
				}

				for(int i = 0; i < neighbourCount; ++i)
				{
					if(*(p + neighbour[i]) == 0)
					{
						*p = tempVal;
						break;
					}
				}
			}
		}
	}

	ReplaceRegionBufferValue(tempVal, 0);
}

void RegionToSubRegionImpl::ReplaceRegionBufferValue(UCHAR currentValue, UCHAR newValue)
{
	int pixelCount = m_regionDim.prod();
	for(int i = 0; i < pixelCount; ++i)
	{
		if(m_region[i] == currentValue)
		{
			m_region[i] = newValue;
		}
	}
}

//! @param [in] regionValue 検索する領域の値
//! @return 領域情報のインデックス、見つからない場合は-1
int RegionToSubRegionImpl::FindRegionInfo(int regionValue) const
{
	int idxRegion = -1;
	int region_count = static_cast<int>(m_regions.size());
	for(int i = 0; i < region_count; ++i)
	{
		const RegionInfo& region = m_regions[i];
		if(region.value == regionValue)
		{
			idxRegion = i;
			break;
		}
	}

	return idxRegion;
}

void RegionToSubRegionImpl::PrepareRegionBufferToRegionGrow()
{
	int pixelCount = m_regionDim.prod();
	for(int i = 0; i < pixelCount; ++i)
	{
		if(m_region[i] != 0)
		{
			m_region[i] = kRegionGrowOrigVal;
		}
	}
}

bool RegionToSubRegionImpl::DoRegionGrowing(const Point3i& seedPoint, UCHAR searchFor, UCHAR replaceWith)
{
	ScanLineFill scanLineFill;
	scanLineFill.Fill(seedPoint, searchFor, replaceWith, m_regionDim, &m_region[0]);
	return true;
}

void RegionToSubRegionImpl::ClassifyRegionConnectedToExcludedPixels()
{
	if(m_pixelsToExclude.empty() == true)
	{
		return;
	}

	int width = m_regionDim.width();
	int height = m_regionDim.height();
	int rowStride = width * height;
	int pixelCount = m_regionDim.prod();

	size_t pixelsToExcludeCount = m_pixelsToExclude.size();
	for(size_t i = 0; i < pixelsToExcludeCount; ++i)
	{
		const Point3i& pt = m_pixelsToExclude[i] - m_regionOrigin;

		int index = pt.z() * rowStride + pt.y() * width + pt.x();
		if(!(index >= 0 && index < pixelCount))
		{
			continue;
		}

		UCHAR* pRegionBuffer = &m_region[index];
		if(*pRegionBuffer != kRegionGrowOrigVal)
		{
			continue;
		}

		DoRegionGrowing(pt, kRegionGrowOrigVal, kRegionConnectedToExclusionPixelVal);
	}
}

bool RegionToSubRegionImpl::ClassifyRegion()
{
	if(m_region.empty() == true)
	{
		return false;
	}

	int d = m_regionDim.depth();
	int h = m_regionDim.height();
	int w = m_regionDim.width();
	UCHAR regionValue = 255;
	UINT regionLimit = 245;
	UCHAR* pRegionBuffer = &m_region[0];
	int regionCount = 0;
	int colorCount = _countof(m_region_colors);
	for(int z = 0; z < d; ++z)
	{
		for(int y = 0; y < h; ++y)
		{
			for(int x = 0; x < w; ++x, ++pRegionBuffer)
			{
				if(*pRegionBuffer != kRegionGrowOrigVal)
				{
					continue;
				}

				Point3i pt = Point3i(x, y, z);
				DoRegionGrowing(pt, kRegionGrowOrigVal, regionValue);
				AddNewRegionInfo(regionValue, L"", m_region_colors[regionCount % colorCount]);
				--regionValue;
				++regionCount;

				if(m_regions.size() >= regionLimit)
				{
					// stop classification when limit is crossed
					x = w; y = h; z = d;
				}
			}
		}
	}

	return true;
}

void RegionToSubRegionImpl::AddNewRegionInfo(UCHAR regionValue, const std::wstring& _name /* =  */, const float* _color, const Point3i& origin /* = Point3i */, const Size3i& size /* = Size3i */)
{
	std::wstring name(_name);

	if(name.empty() == true)
	{
		std::wstringstream stringStream;
		stringStream << REGION_NAME_PREFIX << " - " << m_regions.size();
		name = stringStream.str();
	}

	float color[] = {0.119140625, 0.666015625, 0.998046875};
	if(_color != NULL)
	{
		memcpy(color, _color, sizeof(float) * 3);
	}

	m_regions.push_back(RegionInfo(regionValue, name, color, origin, size));
}

Boxi RegionToSubRegionImpl::CalculateRegionBoundingBox(int regionValue) const
{
	int dimx = m_regionDim.width();
	int dimy = m_regionDim.height();
	int dimz = m_regionDim.depth();
	int maxx = -1;
	int maxy = -1;
	int maxz = -1;
	int minx = dimx;
	int miny = dimy;
	int minz = dimz;

	const UCHAR* p = &m_region[0];
	for(int z = 0; z < dimz; ++z)
	{
		for(int y = 0; y < dimy; ++y)
		{
			for(int x = 0; x < dimx; ++x, ++p)
			{
				if(*p == regionValue)
				{
					if(minx > x) minx = x;
					if(miny > y) miny = y;
					if(minz > z) minz = z;
					if(maxx < x) maxx = x;
					if(maxy < y) maxy = y;
					if(maxz < z) maxz = z;
				}
			}
		}
	}

	Boxi regionBBox;

	regionBBox.origin() = Point3i(minx, miny, minz) + m_regionOrigin;
	regionBBox.size() = Point3i(maxx, maxy, maxz) - Point3i(minx, miny, minz) + Point3i(1, 1, 1);

	return regionBBox;
}

void RegionToSubRegionImpl::UpdateRegionBoundingBox()
{
	size_t region_count = m_regions.size();
	for(size_t i = 0; i < region_count; ++i)
	{
		RegionInfo& region = m_regions[i];
		Boxi dims = CalculateRegionBoundingBox(region.value);
		region.origin = dims.origin();
		region.size = dims.size();
	}
}

void RegionToSubRegionImpl::FilterSmallRegions()
{
	size_t regionCount = m_regions.size();
	std::vector<bool> shouldFilter;
	shouldFilter.resize(regionCount);
	Point3i smallAllowed(10, 10, 5);

	for(size_t i = 0; i < regionCount; ++i)
	{
		RegionInfo& region = m_regions[i];

		if((region.size.array() >= smallAllowed.array()).all())
		{
			continue;
		}

		shouldFilter[i] = true;
	}

	RegionInfos old_regions = m_regions;
	m_regions.clear();

	int color_count = _countof(m_region_colors);
	int new_region_count = 0;
	for(size_t i = 0; i < regionCount; ++i)
	{
		if(shouldFilter[i] == true) continue;
		RegionInfo& region = old_regions[i];
		float* color = &m_region_colors[new_region_count % color_count][0];
		AddNewRegionInfo(region.value, L"", color, region.origin, region.size);
		++new_region_count;
	}
}

int RegionToSubRegionImpl::FindSubRegionIndex(UCHAR regionValue) const
{
	int idexRegion = -1;
	RegionInfos::size_type regionCount = m_regions.size();
	for(RegionInfos::size_type i = 0; i < regionCount; ++i)
	{
		const RegionInfo& region = m_regions[i];
		if(region.value == regionValue)
		{
			idexRegion = static_cast<int>(i);
			break;
		}
	}

	return idexRegion;
}

void RegionToSubRegionImpl::EraseSubRegion(UCHAR regionValue)
{
	ReplaceRegionBufferValue(regionValue, 0);

	for(RegionInfos::iterator itr = m_regions.begin(); itr != m_regions.end(); ++itr)
	{
		if(itr->value == regionValue)
		{
			m_regions.erase(itr);
			break;
		}
	}
}

bool RegionToSubRegionImpl::IsFurtherSubRegionPossible()
{
	int pixels = m_regionDim.prod();
	bool hasRegion = false;
	for(int i = 0; i < pixels; ++i)
	{
		if(m_region[i] == kRegionConnectedToExclusionPixelVal)
		{
			hasRegion = true;
			break;
		}
	}

	m_isRegionExhausted = !hasRegion;

	return hasRegion;
}

//==============================================================================
RegionToSubRegion::RegionToSubRegion()
: m_pImpl(new RegionToSubRegionImpl)
{
}

RegionToSubRegion::~RegionToSubRegion()
{
	delete m_pImpl;
	m_pImpl = NULL;
}

void RegionToSubRegion::Clear()
{
	m_pImpl->Clear();
}

void RegionToSubRegion::SetRegion(const UCHAR* pRegionBuffer, const Point3i& regionOrigin, const Size3i& regionDim)
{
	m_pImpl->SetRegion(pRegionBuffer, regionOrigin, regionDim);
}

void RegionToSubRegion::SetRegionToExclude(const UCHAR *pExcludeBuffer, const Point3i &origin, const Size3i &size)
{
	m_pImpl->SetRegionToExclude(pExcludeBuffer, origin, size);
}

bool RegionToSubRegion::TryToGenerateSubRegion()
{
	return m_pImpl->TryToGenerateSubRegion();
}

size_t RegionToSubRegion::GetSubRegionCount() const
{
	return m_pImpl->GetSubRegionCount();
}

Point3i RegionToSubRegion::GetSubRegionOrigin(size_t regionIndex) const
{
	return m_pImpl->GetSubRegionOrigin(regionIndex);
}

Point3i RegionToSubRegion::GetSubRegionSize(size_t regionIndex) const
{
	return m_pImpl->GetSubRegionSize(regionIndex);
}

Point3f RegionToSubRegion::GetSubRegionColor(size_t regionIndex) const
{
	return m_pImpl->GetSubRegionColor(regionIndex);
}

std::wstring RegionToSubRegion::GetSubRegionName(size_t regionIndex) const
{
	return m_pImpl->GetSubRegionName(regionIndex);
}

Point3i RegionToSubRegion::GetOrigin() const
{
	return m_pImpl->GetOrigin();
}

Point3i RegionToSubRegion::GetDimension() const
{
	return m_pImpl->GetDimension();
}

const UCHAR* RegionToSubRegion::GetRegionBuffer() const
{
	return m_pImpl->GetRegionBuffer();
}

UCHAR RegionToSubRegion::GetSubRegionValue(size_t regionIndex) const
{
	return m_pImpl->GetSubRegionValue(regionIndex);
}

UCHAR RegionToSubRegion::GetExcludedRegionValue() const
{
	return m_pImpl->GetExcludedRegionValue();
}

Point3f RegionToSubRegion::GetExcludedRegionColor() const
{
	return m_pImpl->GetExcludedRegionColor();
}

bool RegionToSubRegion::GenerateSubRegion()
{
	return m_pImpl->GenerateSubRegion();
}

int RegionToSubRegion::FindSubRegionIndex(UCHAR regionValue) const
{
	return m_pImpl->FindSubRegionIndex(regionValue);
}

void RegionToSubRegion::EraseSubRegion(UCHAR regionValue)
{
	return m_pImpl->EraseSubRegion(regionValue);
}

bool RegionToSubRegion::IsRegionExhausted() const
{
	return m_pImpl->IsRegionExhausted();
}

} // namespace segmentation
} // namespace lssplib

