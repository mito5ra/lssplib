﻿#include "Segmentation/DistanceFieldGenerator.h"

namespace lssplib
{
namespace segmentation
{

DistanceFieldGenerator::DistanceFieldGenerator()
: m_kernel()
, m_imageSize()
, m_distanceMap()
, m_seeds0()
, m_seeds1()
, m_currentStack(NULL)
, m_nextStack(NULL)
, m_binaryImage(NULL)
{
	GenerateKernel();
}

DistanceFieldGenerator::~DistanceFieldGenerator()
{
}

/**
 * @brief 距離画像の生成を実行
 * @details 2値画像の非0の画素に対し、シード点群からの距離を算出したデータ配列を生成する。
 * 中心からの相対距離の3x3x3カーネルを使用する。スタックから次に処理する位置を取り出し、
 * 現在位置の距離にカーネルの値を足し周囲の距離の値を更新する。
 * 更新するピクセルに値が入っていない場合は、算出した距離をピクセルに記録する。
 * 既に値が入っている場合は、足した距離の値が既存のものより小さければ上書きする。
 * 
 * @param [in] w 距離画像の幅
 * @param [in] h 距離画像の高さ
 * @param [in] d 距離画像の深さ
 * @param [in] binaryImage 2値画像
 */
void DistanceFieldGenerator::Execute( int w, int h, int d, const unsigned char* binaryImage )
{
	m_imageSize.width() = w;
	m_imageSize.height() = h;
	m_imageSize.depth() = d;
	m_binaryImage = binaryImage;

	// allocate the same size of memory as binary image for distance map
	m_distanceMap.resize(m_imageSize.prod());
	std::fill(m_distanceMap.begin(), m_distanceMap.end(), -1.0f);

	GenerateInitialSeeds();

	m_currentStack = &m_seeds0;
	m_nextStack = &m_seeds1;
	for(;;)
	{
		while(!m_currentStack->empty())
		{
			const SeedPoint& pt = m_currentStack->top();
			UpdateDistanceMapAt(pt);
			m_currentStack->pop();
		}
		if (m_nextStack->empty()) break;
		std::swap(m_currentStack, m_nextStack);
	}
}

// 26 neighbors
/**
 * @brief 距離画像の為のカーネルを作る(26近傍)
 */
void DistanceFieldGenerator::GenerateKernel()
{
	m_kernel.resize(27);

	int idx = 0;
	for (int dz = -1; dz <= 1; ++dz)
	{
		for (int dy = -1; dy <= 1; ++dy)
		{
			for (int dx = -1; dx <= 1; ++dx)
			{
				m_kernel[idx] = sqrt(static_cast<double>(dx*dx+dy*dy+dz*dz));
				++idx;
			}
		}
	}
}

/**
 * @brief 初期シード点群を生成する
 * @details 0の画素で6近傍に非0の画素がある位置を距離0とし、位置をスタックに積む。
 */
void DistanceFieldGenerator::GenerateInitialSeeds()
{
	int w = m_imageSize.width();
	int h = m_imageSize.height();
	int d = m_imageSize.depth();
	int wh = w*h;

	SeedPoint seedPoint;
	for (int k = 0; k < d; ++k)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				int idx = i + j * w + k * wh;
				seedPoint.Set(i, j, k);
				if (m_binaryImage[idx] == 0 && IsInitialSeed(seedPoint))
				{
					m_seeds0.push(seedPoint);
					m_distanceMap[idx] = 0.0f;
				}
			}
		}
	}
}

/**
 * @brief 指定された位置の周囲6近傍の画素中の非0ピクセルの有無
 * 
 * @param [in] seed 2値画像内の位置
 * @return 6近傍に非0が有ればtrue。無ければfalse。
 */
bool DistanceFieldGenerator::IsInitialSeed(const SeedPoint& seed)
{
	int w = m_imageSize.width();
	int h = m_imageSize.height();
	int d = m_imageSize.depth();
	int wh = m_imageSize.width() * m_imageSize.height();

	if (seed.x > 0 && m_binaryImage[seed.x - 1 + seed.y * w + seed.z * wh])
	{
		return true;
	}
	else if (seed.x < w - 1 && m_binaryImage[seed.x + 1 + seed.y * w + seed.z * wh])
	{
		return true;
	}
	else if (seed.y > 0  && m_binaryImage[seed.x + (seed.y - 1) * w + seed.z * wh])
	{
		return true;
	}
	else if (seed.y < h - 1  && m_binaryImage[seed.x + (seed.y + 1) * w + seed.z * wh])
	{
		return true;
	}
	else if (seed.z > 0  && m_binaryImage[seed.x + seed.y * w + (seed.z - 1) * wh])
	{
		return true;
	}
	else if (seed.z < d - 1  && m_binaryImage[seed.x + seed.y * w + (seed.z + 1) * wh])
	{
		return true;
	}

	return false;
}

/**
 * @brief 指定位置の周囲の距離の更新
 * 
 * @param [in] pt 2値/距離画像内の位置
 */
void DistanceFieldGenerator::UpdateDistanceMapAt( const SeedPoint& pt )
{
	int w = m_imageSize.width();
	int h = m_imageSize.height();
	int d = m_imageSize.depth();
	int wh = w*h;

	SeedPoint seedPoint;
	DistanceValueType distanceAtTarget;
	DistanceValueType deltaDistance;
	DistanceValueType d0 = m_distanceMap[pt.x + pt.y * w + pt.z * wh];
	for (int dz = -1; dz <= 1; ++dz)
	{
		if (pt.z+dz < 0 || pt.z+dz >= d) continue;
		for (int dy = -1; dy <= 1; ++dy)
		{
			if (pt.y+dy < 0 || pt.y+dy >= h) continue;
			for (int dx = -1; dx <= 1; ++dx)
			{
				if ((pt.x+dx < 0 || pt.x+dx >= w) || (dx==0 && dy==0 && dz==0)) continue;
				int idx = (pt.x + dx) + (pt.y + dy) * w + (pt.z + dz) * wh;
				if (m_binaryImage[idx]) // search only object region
				{
					distanceAtTarget = m_distanceMap[idx];
					deltaDistance = static_cast<DistanceValueType>(m_kernel[(dx+1)*3 + (dy+1)*3 + (dz+1)*3]);
					if (distanceAtTarget == -1.0)
					{
						// push target pixel location only if the pixel is untouched
						seedPoint.Set(pt.x+dx, pt.y+dy, pt.z+dz);
						m_nextStack->push(seedPoint);
						m_distanceMap[idx] = d0 + deltaDistance;
					}
					else if (d0 + deltaDistance < distanceAtTarget)
					{
						// distance at the target pixel = distance at source pixel + kernel value
						m_distanceMap[idx] = d0 + deltaDistance;
					}
				}
			}
		}
	}
}

} // namespace segmentation
} // namespace lssplib

