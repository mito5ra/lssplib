#pragma warning( disable : 4996 )

#include "Segmentation/VesselThicknessInfo.h"

namespace lssplib
{
namespace segmentation
{

//------------------------------------------------------------------------------
VesselThicknessInfo::VesselThicknessInfo()
: m_vesselThickness(ThicknessImageType::New())
, m_origin(0, 0, 0)
, m_minIntensity(0)
, m_maxIntensity(0)
, m_liverId(-1)
, m_IVCId(-1)
{

}

//------------------------------------------------------------------------------
VesselThicknessInfo::~VesselThicknessInfo()
{

}

//------------------------------------------------------------------------------
/**
 * @brief 管状構造画像データの原点を設定
 * 
 * @param [in] pt 原点位置
 */
void VesselThicknessInfo::SetOrigin( const Point3i& pt )
{
	m_origin = pt;
}

//------------------------------------------------------------------------------
/**
 * @brief 輝度範囲を設定する(血管輝度強調用)
 * 
 * @param [in] minIntensity 輝度の下限
 * @param [in] maxIntensity 輝度の上限
 */
void VesselThicknessInfo::SetIntensityRange( int minIntensity, int maxIntensity )
{
	m_minIntensity = minIntensity;

	m_maxIntensity = maxIntensity;
}

//------------------------------------------------------------------------------
/**
 * @brief 肝臓マスクを登録する(IDのみ)
 * 
 * @param [in] liverMaskId 肝臓マスクID
 */
void VesselThicknessInfo::SetLiverId(int liverMaskId)
{
	m_liverId = liverMaskId;
}

//------------------------------------------------------------------------------
/**
 * @brief 下大静脈マスクを登録する(IDのみ)
 * 
 * @param [in] ivcMaskId 下大静脈マスクID
 */
void VesselThicknessInfo::SetIVCId(int ivcMaskId)
{
	m_IVCId = ivcMaskId;
}

//------------------------------------------------------------------------------
/**
 * @brief 管状構造画像データのポインタを取得
 * 
 * @return 管状構造画像データのポインタ
 */
VesselThicknessInfo::ThicknessImageType* VesselThicknessInfo::GetImagePointer()
{
	return m_vesselThickness.GetPointer();
}

//------------------------------------------------------------------------------
/**
 * @brief 管状構造画像データのポインタを取得
 * 
 * @return 管状構造画像データのポインタ
 */
const VesselThicknessInfo::ThicknessImageType* VesselThicknessInfo::GetImagePointer() const
{
	return m_vesselThickness.GetPointer();
}

//------------------------------------------------------------------------------
/**
 * @brief 管状構造画像データの原点を取得(3Dバッファの座標)
 * @return 原点
 */
const Point3i& VesselThicknessInfo::GetOrigin() const
{
	return m_origin;
}

//------------------------------------------------------------------------------
/**
 * @brief 輝度範囲の下限を取得
 * @return 輝度
 */
int VesselThicknessInfo::GetMinIntensity() const
{
	return m_minIntensity;
}

//------------------------------------------------------------------------------
/**
 * @brief 輝度範囲の上限を取得
 * @return 輝度
 */
int VesselThicknessInfo::GetMaxIntensity() const
{
	return m_maxIntensity;
}

//------------------------------------------------------------------------------
/**
 * @brief 肝臓マスクIDを取得
 * @return ID
 */
int VesselThicknessInfo::GetLiverId() const
{
	return m_liverId;
}

//------------------------------------------------------------------------------
/**
 * @brief 下大静脈マスクIDを取得
 * @return ID
 */
int VesselThicknessInfo::GetIVCId() const
{
	return m_IVCId;
}

//------------------------------------------------------------------------------
/**
 * @brief リセット(内部情報をクリア)
 */
void VesselThicknessInfo::Reset()
{
	m_vesselThickness = ThicknessImageType::New();
	m_origin.setZero();
	m_minIntensity = 0;
	m_maxIntensity = 0;
	m_liverId = -1;
	m_IVCId = -1;
}

//------------------------------------------------------------------------------
/**
 * @brief 管状構造画像の有無を確認する
 * 
 * @return true: 存在する。false: 作られていない。
 */
bool VesselThicknessInfo::IsEmpty() const
{
	float* ptr = m_vesselThickness->GetBufferPointer();
	if (!ptr) return true;

	return false;
}

} // namespace segmentation
} // namespace lssplib

