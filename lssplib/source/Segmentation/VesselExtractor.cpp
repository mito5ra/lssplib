#pragma warning( disable : 4996 )

#include "Segmentation/VesselExtractor.h"

#include <cassert>

#include "itkImportImageFilter.h"
#include "itkHessian3DToVesselnessMeasureImageFilter.h"
#include "itkHessianRecursiveGaussianImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkNearestNeighborInterpolateImageFunction.h"
#include "itkIdentityTransform.h"
#include "itkImageRegionIterator.h"
#include "itkDiscreteGaussianImageFilter.h"
#include "itkBinaryErodeImageFilter.h"
#include "itkBinaryDilateImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryMorphologicalOpeningImageFilter.h"

#include "Segmentation/VesselTree.h"
#include "Segmentation/VesselNode.h"
#include "Segmentation/VesselThicknessInfo.h"

#include "Segmentation/GraphCut.h"
#include "Segmentation/DistanceFieldGenerator.h"
#include "Segmentation/FastMarching.h"

//#define VESSELNESS_THRESHOLD 0.7
//#define VESSELNESS_THRESHOLD 0.9
//#define VESSELNESS_THRESHOLD 2.0

//! @brief ラインフィルタの出力に対する閾値
//! @details ラインフィルタ画像でこの閾値より高いものを血管とみなす。
#define VESSELNESS_THRESHOLD 5.0
//#define VESSELNESS_THRESHOLD 20.0

#define VESSEL_EXTRACTION_STOP_TIME 2.0

//! @brief 血管抽出時に、前処理として肝臓外周部をerosionする量。(mm)
//! @details 肝臓の外周付近には目立った血管の走行は無いので、抽出の対象外とする。
#define LIVER_EROSION_RADIUS 3.0

//! @brief 血管領域抽出の後処理で使用するラベル
//! @sa VesselExtractor::PostProcessSegmentedImage
#define POSTPROCESS_GRAPHCUT_REGION 7

//! @brief 血管領域抽出の後処理で使用するラベル
//! @sa VesselExtractor::PostProcessSegmentedImage
#define POSTPROCESS_VESSEL_REGION 8

namespace lssplib
{
namespace segmentation
{

//! @brief 血管領域抽出で経路周辺のマスクを除外するときの半径の下限値
//! @sa VesselExtractor::SubtractPathFromLiverMask
const double kMinVesselRadiusForSubtraction = 2.0;

//! @brief 血管領域抽出で経路周辺のマスクを除外するときの半径が下限値に到達する時の分岐レベル
//! @sa VesselExtractor::SubtractPathFromLiverMask
const int kMinRadiusBranchLevelForSubtraction = 10.0;

const double kMinVesselRadius = 0.5;

typedef itk::Image<unsigned char, 3> BinaryImageType;

namespace
{
//------------------------------------------------------------------------------
	/**
	 * @brief 座標が領域内が調べる
	 * 
	 * @param [in] regionSize 領域
	 * @param [in] x x位置
	 * @param [in] y y位置
	 * @param [in] z z位置
	 * @return true: 領域内, false: 領域外
	 */
	bool IsInside(const Size3i& regionSize, int x, int y, int z)
	{
		if (
			x < 0 || y < 0 || z < 0 ||
			x >= regionSize.width() ||
			y >= regionSize.height() ||
			z >= regionSize.depth())
		{
			return false;
		}

		return true;
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 参照画像と同じ原点、方向、ピクセル間隔、領域の画像を作る
	 * 
	 * @tparam TImageIn 参照するitk画像タイプ
 	 * @tparam TImageOut 出力itk画像タイプ
	 * @param [in] input 参照画像
	 * @param [out] output 出力画像
	 */
	template<typename TImageIn, typename TImageOut>
	void AllocateItkImage(const typename TImageIn* input, typename TImageOut* output)
	{
		output->SetOrigin(input->GetOrigin());
		output->SetDirection(input->GetDirection());
		output->SetSpacing(input->GetSpacing());
		output->SetRegions(input->GetLargestPossibleRegion());
		output->Allocate();
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 画像の空洞部分を塗りつぶす処理
	 * @details 入力マスク画像の空洞を塗りつぶす処理。
	 * 
	 * @tparam TImage itk画像タイプ
	 * @param [in] inputImg 入力マスク画像
	 * @param [out] outputImg 塗りつぶし後の画像
	 */
	template<typename TImage>
	void FillInterior(const TImage* inputImg, TImage* outputImg)
	{
		TImage::SizeValueType w = inputImg->GetLargestPossibleRegion().GetSize()[0];
		TImage::SizeValueType h = inputImg->GetLargestPossibleRegion().GetSize()[1];
		TImage::SizeValueType d = inputImg->GetLargestPossibleRegion().GetSize()[2];

		TImage::IndexType idx;
		for (int k = 0; k < d; ++k)
		{
			for (int j = 0; j < h; ++j)
			{
				for (int i = 0; i < w; ++i)
				{
					idx[0] = i;
					idx[1] = j;
					idx[2] = k;
					if ((*inputImg)[idx] != 0) break;
					else 
					{
						(*outputImg)[idx] = 2; // label as exteriror
					}
				}
				for (int i = w-1; i >= 0; --i)
				{
					idx[0] = i;
					idx[1] = j;
					idx[2] = k;
					if ((*inputImg)[idx] != 0) break;
					else 
					{
						(*outputImg)[idx] = 2; // label as exteriror
					}
				}
			}
		}

		for (int i = 0; i < w; ++i)
		{
			for (int k = 0; k < d; ++k)
			{
				for (int j = 0; j < h; ++j)
				{
					idx[0] = i;
					idx[1] = j;
					idx[2] = k;
					if ((*inputImg)[idx] != 0) break;
					else 
					{
						(*outputImg)[idx] = 2; // label as exteriror
					}
				}
				for (int j = h-1; j >= 0; --j)
				{
					idx[0] = i;
					idx[1] = j;
					idx[2] = k;
					if ((*inputImg)[idx] != 0) break;
					else 
					{
						(*outputImg)[idx] = 2; // label as exteriror
					}
				}
			}
		}

		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				for (int k = 0; k < d; ++k)
				{
					idx[0] = i;
					idx[1] = j;
					idx[2] = k;
					if ((*inputImg)[idx] != 0) break;
					else 
					{
						(*outputImg)[idx] = 2; // label as exteriror
					}
				}
				for (int k = d-1; k >= 0; --k)
				{
					idx[0] = i;
					idx[1] = j;
					idx[2] = k;
					if ((*inputImg)[idx] != 0) break;
					else 
					{
						(*outputImg)[idx] = 2; // label as exteriror
					}
				}
			}
		}

		itk::ImageRegionIterator<TImage> outputIterator(outputImg, outputImg->GetLargestPossibleRegion());
		while(!outputIterator.IsAtEnd())
		{
			if (outputIterator.Get() == 2)
			{
				outputIterator.Set(0);
			}
			else
			{
				outputIterator.Set(1);
			}
			++outputIterator;
		}

	}

//------------------------------------------------------------------------------
	/**
	 * @brief 距離画像を生成する
	 * 
	 * @tparam TImage 管状構造画像型
	 * @tparam TImageOut 出力画像(距離画像)型
	 * @param [in] inputImage 管状構造画像
	 * @param [out] outputImage 距離画像
	 */
	template<typename TImage, typename TImageOut>
	void GenerateDistanceMap(const TImage* inputImage, TImageOut* outputImage)
	{
		itk::ImageRegionConstIterator<TImage> inputItr(inputImage, inputImage->GetLargestPossibleRegion());

		BinaryImageType::Pointer binaryImage = BinaryImageType::New();
		AllocateItkImage(inputImage, binaryImage.GetPointer());

		itk::ImageRegionIterator<BinaryImageType> binImgItr(binaryImage, binaryImage->GetLargestPossibleRegion());

		inputItr.GoToBegin();
		binImgItr.GoToBegin();

		while (!inputItr.IsAtEnd())
		{
			if (inputItr.Get() > VESSELNESS_THRESHOLD) // 0.7?
			{
				binImgItr.Set(255);
			}
			else
			{
				binImgItr.Set(0);
			}
			++inputItr;
			++binImgItr;
		}

		BinaryImageType::SizeType sz = binaryImage->GetLargestPossibleRegion().GetSize();
		DistanceFieldGenerator distMapGen;
		distMapGen.Execute(sz[0], sz[1], sz[2], binaryImage->GetBufferPointer());

		AllocateItkImage(inputImage, outputImage);

		std::copy(distMapGen.GetDistanceMap().begin(),
			distMapGen.GetDistanceMap().end(),
			outputImage->GetBufferPointer());
	}

//------------------------------------------------------------------------------
	typedef itk::Image<float, 3> DistanceMapImageType; //!< 距離画像型

	/**
	 * @brief 末端点か調べる
	 * @details 管状構造の距離画像内で、現在位置の画素値が周辺の画素値(距離)よりも大きければ管の端点とみなす。
	 * 
	 * @param [in] distSrc 現在位置の画素値画素値
	 * @param [in] srcIndex 管状構造の距離画像内の現在の位置
	 * @param [in] img 管状構造の距離画像
	 * @return true: 末端点、 false: 末端点ではない
	 */
	bool IsVesselTip(
		const DistanceMapImageType::ValueType& distSrc,
		const DistanceMapImageType::IndexType& srcIndex,
		const DistanceMapImageType* img )
	{
		const DistanceMapImageType::SizeType& imgSize = img->GetLargestPossibleRegion().GetSize();
		DistanceMapImageType::IndexType imgIndex;
		int kernelRange = 5;
		bool isChecked = false;
		for (itk::IndexValueType dz = -kernelRange; dz <= kernelRange; ++dz)
		{
			if (srcIndex[2]+dz < 0 || srcIndex[2]+dz >= imgSize[2]) continue;
			for (itk::IndexValueType dy = -kernelRange; dy <= kernelRange; ++dy)
			{
				if (srcIndex[1]+dy < 0 || srcIndex[1]+dy >= imgSize[1]) continue;
				for (itk::IndexValueType dx = -kernelRange; dx <= kernelRange; ++dx)
				{
					if ((dx == 0 && dy == 0 && dz == 0) ||
						(srcIndex[0]+dx < 0 || srcIndex[0]+dx >= imgSize[0])) continue;
					imgIndex[0] = srcIndex[0]+dx;
					imgIndex[1] = srcIndex[1]+dy;
					imgIndex[2] = srcIndex[2]+dz;
					const DistanceMapImageType::PixelType& distTgt = img->GetPixel(imgIndex);
					if (distTgt > 10000.0f) continue;
					if (distSrc < distTgt)
					{
						return false;
					}
					isChecked = true;
				}
			}
		}

		if (!isChecked) return false;

		return true;
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 管状構造の末端点を見つける
	 * 
	 * @param [in] inputImage 管状構造の距離画像
	 * @param [out] vesselTipPosList 末端点のリスト
	 */
	void FindVesselTips(
		const DistanceMapImageType* inputImage,
		std::vector<DistanceMapImageType::IndexType>& vesselTipPosList)
	{
		int count = 0;
		DistanceMapImageType::ValueType distMax = 0.0f;

		// if no nearby points has smaller distance value, the point is considered as candidate.
		itk::ImageRegionConstIteratorWithIndex<DistanceMapImageType> itrImg(inputImage, inputImage->GetLargestPossibleRegion());
		itrImg.GoToBegin();
		for (; !itrImg.IsAtEnd(); ++itrImg/*, ++idx*/)
		{
			const DistanceMapImageType::ValueType& distSrc = itrImg.Get();
			if (distSrc > 0.0f && distSrc < 10000.0f)
			{
				const DistanceMapImageType::IndexType& imgIndex = itrImg.GetIndex();
				if (distSrc > distMax)
				{
					distMax = distSrc;
				}

				if (IsVesselTip(distSrc, imgIndex, inputImage))
				{
					// add current array index to tip list
					vesselTipPosList.push_back(imgIndex);
				}
				++count;
			}
		}
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 末端点のリスト
	 * @details 現在位置に隣接する(26近傍)ピクセルの中から最も距離(到達時間)の小さいものを選び次の位置とする。
	 * 同じ距離(到達時間)が複数ある場合は、進行方向(一つ前の位置と現在地から算出)にもっとも近いピクセルを選択する。
	 * この時一つ前の位置は含まない。
	 * 
	 * @param [in] currentPos 現在位置
	 * @param [in] previousPos 一つ前の位置
	 * @param [in] distanceMap 管状構造の距離画像
	 * @param [out] nextPos 次の位置
	 * @return 処理の成否。true: 成功、 false: 失敗(次の位置が得られなかった)
	 */
	bool GetNextIndex(
		const DistanceMapImageType::IndexType& currentPos,
		const DistanceMapImageType::IndexType& previousPos,
		const DistanceMapImageType* distanceMap,
		DistanceMapImageType::IndexType& nextPos
		)
	{
		const DistanceMapImageType::SizeType& imgSize =
			distanceMap->GetLargestPossibleRegion().GetSize();
		DistanceMapImageType::IndexType neighborIndex;
		DistanceMapImageType::IndexType minIndex = {{0, 0, 0}};
		const DistanceMapImageType::PixelType& centerPixelVal = distanceMap->GetPixel(currentPos);
		DistanceMapImageType::PixelType minPixelVal = centerPixelVal;

		double vecCurrentToNext[3];
		double vecPrevToCurrent[3];
		vecPrevToCurrent[0] = currentPos[0] - previousPos[0];
		vecPrevToCurrent[1] = currentPos[1] - previousPos[1];
		vecPrevToCurrent[2] = currentPos[2] - previousPos[2];
		double invnrm = 1.0/sqrt(vecPrevToCurrent[0]*vecPrevToCurrent[0] + vecPrevToCurrent[1]*vecPrevToCurrent[1] + vecPrevToCurrent[2]*vecPrevToCurrent[2]);
		vecPrevToCurrent[0] *= invnrm;
		vecPrevToCurrent[1] *= invnrm;
		vecPrevToCurrent[2] *= invnrm;

		double maxInnerProduct = -2.0;
		bool isChecked = false;
		for (itk::IndexValueType dz = -1; dz <= 1; ++dz)
		{
			for (itk::IndexValueType dy = -1; dy <= 1; ++dy)
			{
				for (itk::IndexValueType dx = -1; dx <= 1; ++dx)
				{
					neighborIndex[0] = currentPos[0] + dx;
					neighborIndex[1] = currentPos[1] + dy;
					neighborIndex[2] = currentPos[2] + dz;
					if (
						(neighborIndex[0] < 0 || neighborIndex[0] >= imgSize[0]) ||
						(neighborIndex[1] < 0 || neighborIndex[1] >= imgSize[1]) ||
						(neighborIndex[2] < 0 || neighborIndex[2] >= imgSize[2]) ||
						neighborIndex == previousPos ||
						neighborIndex == currentPos) // range check. don' go back. don't check self.
					{
						continue;
					}
					const DistanceMapImageType::PixelType& pixelVal = distanceMap->GetPixel(neighborIndex);
					if ( (pixelVal >= 0.0f && pixelVal < 10000.0f) && minPixelVal >= pixelVal )
					{
						if (minPixelVal == pixelVal)
						{
							vecCurrentToNext[0] = neighborIndex[0] - currentPos[0];
							vecCurrentToNext[1] = neighborIndex[1] - currentPos[1];
							vecCurrentToNext[2] = neighborIndex[2] - currentPos[2];
							double invnrm = 1.0/sqrt(vecCurrentToNext[0]*vecCurrentToNext[0] + vecCurrentToNext[1]*vecCurrentToNext[1] + vecCurrentToNext[2]*vecCurrentToNext[2]);
							vecCurrentToNext[0] *= invnrm;
							vecCurrentToNext[1] *= invnrm;
							vecCurrentToNext[2] *= invnrm;

							double innerProduct = vecPrevToCurrent[0]*vecCurrentToNext[0] +
								vecPrevToCurrent[1]*vecCurrentToNext[1] +
								vecPrevToCurrent[2]*vecCurrentToNext[2];

							if (maxInnerProduct < innerProduct)
							{
								maxInnerProduct = innerProduct;
								minPixelVal = pixelVal;
								minIndex = neighborIndex;
								isChecked = true;
							}
						}
						else
						{
							minPixelVal = pixelVal;
							minIndex = neighborIndex;
							isChecked = true;
						}
					}
				}
			}
		}

		if (!isChecked)
		{
			return false;
		}

		nextPos = minIndex;
		return true;
	}

//------------------------------------------------------------------------------
	// trace path for all vessel tips in the vesselTipPosList
	// and build a tree structure of vessels starting from seedPosition
	/**
	 * @brief 血管構造データを構築する
	 * @details 各管状構造の端点から距離(到達時間)画像を時間が小さくなる方向に経路探索を行い、
	 * 時間0のピクセルに到達させる。到達位置が門脈始点の場合は、門脈の血管構造データに登録し、
	 * それ以外の場合は、静脈の血管構造データに登録する。
	 * 経路探索中に、他の経路と合流した場合、その経路は合流先の血管構造データの一部になり、その時点で経路探索は終了する。
	 * 
	 * @param [in] seedPosition 門脈始点
	 * @param [in] vesselTipPosList 管状構造の端点リスト
	 * @param [in] distanceMap 管状構造の距離(到達時間)画像
	 * @param [out] vesselTree 門脈の血管構造データ
	 * @param [out] hepaticTrees 静脈の血管構造データの配列
	 */
	void BuildVesselTree_(
		const DistanceMapImageType::IndexType& seedPosition,
		const std::vector<DistanceMapImageType::IndexType>& vesselTipPosList,
		const DistanceMapImageType::Pointer& distanceMap,
		VesselTree& vesselTree,
		VesselExtractor::HepaticVeinTrees& hepaticTrees)
	{

		BinaryImageType::Pointer binaryImage = BinaryImageType::New();
		AllocateItkImage(distanceMap.GetPointer(), binaryImage.GetPointer());
		binaryImage->FillBuffer(0);

		typedef VesselNode NodeType;
		NodeType* origin = new NodeType(seedPosition[0], seedPosition[1], seedPosition[2]);
		vesselTree.SetStartNode(origin);

		// NodeType* currentNode = NULL;
		DistanceMapImageType::IndexType nextPos;
		std::vector<DistanceMapImageType::IndexType>::const_iterator tipItr = vesselTipPosList.begin();
		for (; tipItr != vesselTipPosList.end(); ++tipItr)
		{
			NodeType* currentNode = new NodeType((*tipItr)[0], (*tipItr)[1], (*tipItr)[2]);

			DistanceMapImageType::IndexType currentPos = (*tipItr);
			DistanceMapImageType::IndexType prevPos = (*tipItr);
			while (true)
			{
				if (GetNextIndex(currentPos, prevPos, distanceMap, nextPos))
				{
					if (nextPos == seedPosition) // portal vein
					{
						binaryImage->SetPixel(nextPos, 1);
						origin->AddOutgoing(currentNode);
						currentNode->SetIncoming(origin);
						break;
					}
					else if (binaryImage->GetPixel(nextPos) > 0)
					{
						// connect current path to the existing path
						NodeType* existingNode = vesselTree.FindNodeAt(nextPos[0], nextPos[1], nextPos[2]);
						if (existingNode == NULL)
						{
							for (int i = 0; i < hepaticTrees.size(); ++i)
							{
								existingNode = hepaticTrees[i]->FindNodeAt(nextPos[0], nextPos[1], nextPos[2]);
								if (existingNode) break;
							}
						}

						assert(existingNode != NULL);

						existingNode->AddOutgoing(currentNode);
						currentNode->SetIncoming(existingNode);
						break;
					}
					else if ( (*distanceMap)[nextPos] == 0.0f ) // hepatic vein
					{
						binaryImage->SetPixel(nextPos, 1);
						NodeType* newNode = new NodeType(nextPos[0], nextPos[1], nextPos[2]);
						newNode->AddOutgoing(currentNode);
						currentNode->SetIncoming(newNode);

						// create new tree
						VesselTree* newTree = new VesselTree;
						newTree->SetStartNode(newNode);
						hepaticTrees.push_back(newTree);
						break;
					}
					else
					{
						binaryImage->SetPixel(nextPos, 1);
						NodeType* newNode = new NodeType(nextPos[0], nextPos[1], nextPos[2]);
						newNode->AddOutgoing(currentNode);
						currentNode->SetIncoming(newNode);
						currentNode = newNode;
					}
					prevPos = currentPos;
					currentPos = nextPos;
				}
				else
				{
					assert(false);

					std::stack<NodeType*> nodes;
					nodes.push(currentNode);

					while (!nodes.empty())
					{
						NodeType* currentNode = nodes.top();
						nodes.pop();

						for (int i = 0; i < currentNode->GetOutgoings().size(); ++i)
						{
							nodes.push(currentNode->GetOutgoings()[i]);
						}
						DistanceMapImageType::IndexType pos;
						pos[0] = currentNode->GetPosition()[0];
						pos[1] = currentNode->GetPosition()[1];
						pos[2] = currentNode->GetPosition()[2];
						assert(binaryImage->GetPixel(pos) > 0);
						binaryImage->SetPixel(pos, 0);
						delete currentNode;
					}

					break;
				}
			}
		}

		vesselTree.UpdateBranchLevel();
		vesselTree.SetVesselType(kVesselTypePoratlVein);
		for (int i = 0; i < hepaticTrees.size(); ++i)
		{
			hepaticTrees[i]->UpdateBranchLevel();
			hepaticTrees[i]->SetVesselType(kVesselTypeHepaticVein);
		}
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 全ての血管構造データのノードIDを更新する
	 * @details 門脈と静脈全ての血管構造データのノードIDを付け直す。血管構造データの編集が行われた後で行う。
	 * 
	 * @param vesselTree 門脈の血管構造データ
	 * @param hepaticVeinTrees 静脈の血管構造データの配列
	 */
	void RefreshVesselNodeID(VesselTree* vesselTree,
		VesselExtractor::HepaticVeinTrees* hepaticVeinTrees)
	{
		typedef VesselNode NodeType;

		std::vector<VesselTree*> allVesselTrees(hepaticVeinTrees->size()+1);
		allVesselTrees[0] = vesselTree;
		std::copy(hepaticVeinTrees->begin(), hepaticVeinTrees->end(), allVesselTrees.begin()+1);

		std::vector<VesselTree*>::iterator itr = allVesselTrees.begin();
		for (; itr != allVesselTrees.end(); ++itr)
		{
			(*itr)->UpdateNodeID();
		}

	}

//------------------------------------------------------------------------------
	/**
	 * @brief 管状構造端点リストから肝臓マスク領域外にある端点を取り除く
	 * 
	 * @param [in] liverMask 肝臓マスクデータ
	 * @param [in,out] vesselTipPosList 管状構造端点リスト
	 */
	void EliminateVesselTipsOutOfRange(
		const VesselExtractor::MaskBufferType* liverMask,
		std::vector<DistanceMapImageType::IndexType>& vesselTipPosList)
	{
		VesselExtractor::MaskBufferType::IndexType maskIndex;
		const VesselExtractor::MaskBufferType::SizeType& maskSize = liverMask->GetLargestPossibleRegion().GetSize();

		std::vector<DistanceMapImageType::IndexType> tmpPosList;
		std::vector<DistanceMapImageType::IndexType>::const_iterator itr = vesselTipPosList.begin();
		for (; itr != vesselTipPosList.end(); ++itr)
		{
			maskIndex[0] = (*itr)[0];
			maskIndex[1] = (*itr)[1];
			maskIndex[2] = (*itr)[2];

			if (maskIndex[0] >= maskSize[0] || maskIndex[1] >= maskSize[1] || maskIndex[2] >= maskSize[2])
			{
				continue;
			}

			VesselExtractor::MaskBufferType::PixelType maskPixel = liverMask->GetPixel(maskIndex);
			if (maskPixel > 0)
			{
				tmpPosList.push_back(*itr);
			}
		}

		std::swap(vesselTipPosList, tmpPosList);
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 管状構造の端点で、経路が肝臓外のもを除外する
	 * @details 管状構造の端点で、門脈始点又は静脈表面までの経路が肝臓マスク外を通過する割合の大きいものを除外する門脈始点と静脈表面は管状構造の距離(到達時間)画像で0の場所。
	 * 
	 * @param [in] liverMask 肝臓マスク
	 * @param [in] distanceMap 管状構造の距離(到達時間)画像
	 * @param [in,out] vesselTipPosList 管状構造の端点リスト
	 */
	void EliminateVesselTipsInvalidPath(
		const VesselExtractor::MaskBufferType* liverMask,
		const DistanceMapImageType* distanceMap,
		std::vector<DistanceMapImageType::IndexType>& vesselTipPosList)
	{
		std::vector<DistanceMapImageType::IndexType> tmpTipPosList;

		int count = 0;
		DistanceMapImageType::IndexType nextPos;
		std::vector<DistanceMapImageType::IndexType>::const_iterator tipItr = vesselTipPosList.begin();
		for (; tipItr != vesselTipPosList.end(); ++tipItr)
		{
			DistanceMapImageType::IndexType currentPos = (*tipItr);
			DistanceMapImageType::IndexType prevPos = (*tipItr);
			int totalNumPixels = 0;
			int numPixelsInLiver = 0;
			while (true)
			{
				// increment total number of pixels
				++totalNumPixels;
				// check liver mask at current position
				// if mask, increment numPixelsInLiver
				if (liverMask->GetPixel(currentPos) > 0)
				{
					++numPixelsInLiver;
				}

				if (GetNextIndex(currentPos, prevPos, distanceMap, nextPos))
				{
					if ((*distanceMap)[nextPos] == 0.0f) break;
					prevPos = currentPos;
					currentPos = nextPos;
				}
				else
				{
					break;
				}
			}
			if (totalNumPixels > 0 && static_cast<double>(numPixelsInLiver)/totalNumPixels > 0.5)
			{
				tmpTipPosList.push_back(*tipItr);
			}
			++count;
		}

		vesselTipPosList.swap(tmpTipPosList);
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 血管構造データから短い枝を取り除く
	 * @details ノード数4以下の枝。枝は、根元側は分岐ノードの次のノード、末端側は、末端点。入力された血管構造データが書き換えられる。
	 * 
	 * @param [in,out] vesselTree 血管構造データ
	 */
	void EliminateShortEndBranches(VesselTree& vesselTree)
	{
		if (!vesselTree.GetStartNode()) return;

		std::stack<VesselNode*> vesselNodeStack;
		vesselNodeStack.push(vesselTree.GetStartNode());
		while (!vesselNodeStack.empty())
		{
			VesselNode* currentNode = vesselNodeStack.top();
			vesselNodeStack.pop();

			if (currentNode->GetOutgoings().empty())
			{
				size_t numNodes = currentNode->GetNumNodesInBranch();
				if (numNodes < 5)
				{
					VesselNode* startNode = currentNode->GetBranchStart();
					vesselTree.DeleteBranches(startNode);
				}
			}

			for (size_t n = 0; n < currentNode->GetOutgoings().size(); ++n)
			{
				vesselNodeStack.push(currentNode->GetOutgoings()[n]);
			}
		}

		vesselTree.UpdateBranchLevel();
		vesselTree.UpdateNodeID();
	}

	//------------------------------------------------------------------------------
	/**
	 * @brief 血管構造データから短い枝を取り除く
	 * @details ノード数4以下の枝。枝は、根元側は分岐ノードの次のノード、末端側は、末端点。入力された血管構造データが書き換えられる。
	 * 
	 * @param [in,out] vesselTree 門脈の血管構造データ
	 * @param [in,out] hepaticTrees 門脈の血管構造データ
	 */
	void EliminateShortEndBranches(
		VesselTree& vesselTree,
		VesselExtractor::HepaticVeinTrees& hepaticTrees)
	{
		EliminateShortEndBranches(vesselTree);
		for (int i = 0; i < hepaticTrees.size(); ++i)
		{
			EliminateShortEndBranches(*hepaticTrees[i]);
		}
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 画像の解像度を落とす
	 * 
	 * @tparam InImage 入力画像型
	 * @tparam OutImage 出力画像型
	 * @param [in] srcImage 元の画像
	 * @param [out] dstImage 解像度が落とされた画像
	 */
	template<typename InImage, typename OutImage>
	void DownSampleMaskImage(
		const InImage* srcImage,
		OutImage* dstImage)
	{
		InImage::SpacingType srcSpacing = srcImage->GetSpacing();
		OutImage::SpacingType dstSpacing = dstImage->GetSpacing();

		OutImage::SizeType dstSize = dstImage->GetLargestPossibleRegion().GetSize();

		OutImage::IndexType dstIndex;

		itk::ImageRegionConstIteratorWithIndex<InImage> itrSrc(srcImage, srcImage->GetLargestPossibleRegion());
		for (; !itrSrc.IsAtEnd(); ++itrSrc)
		{
			const InImage::IndexType& srcIndex = itrSrc.GetIndex();
			dstIndex[0] = srcIndex[0];
			dstIndex[1] = srcIndex[1];
			dstIndex[2] = static_cast<InImage::IndexValueType>(srcIndex[2] * srcSpacing[2] / dstSpacing[2]);
			if (dstIndex[2] >= dstSize[2]) continue;

			if (itrSrc.Get() > 0)
			{
				dstImage->SetPixel(dstIndex, 1);
			}
			else
			{
				dstImage->SetPixel(dstIndex, 0);
			}
		}
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 管状構造画像の距離(到達時間)画像を生成する
	 * 
	 * @param [in] distanceMap 管状構造画像
	 * @param [in] portalVeinPoint 門脈始点
	 * @param [in] ivcMask 下大静脈マスク
	 * @param [out] outputImage 距離(到達時間)画像
	 */
	void ApplyFastMarching(
		const DistanceMapImageType* distanceMap,
		const Point3i& portalVeinPoint,
		const VesselExtractor::MaskBufferType* ivcMask,
		DistanceMapImageType* outputImage)
	{

		DistanceMapImageType::SizeType imgSize = distanceMap->GetLargestPossibleRegion().GetSize();
		DistanceMapImageType::SpacingType imgSpacing = distanceMap->GetSpacing();

		typedef FastMarching<float, 3> FastMarchingType;

		FastMarchingType fm;
		FastMarchingType::Size fmSize;
		fmSize[0] = imgSize[0];
		fmSize[1] = imgSize[1];
		fmSize[2] = imgSize[2];
		fm.SetSpeedImage(fmSize, distanceMap->GetBufferPointer());

		FastMarchingType::Spacing fmSpacing;
		fmSpacing[0] = imgSpacing[0];
		fmSpacing[1] = imgSpacing[1];
		fmSpacing[2] = imgSpacing[2];

		fm.SetSpacing(fmSpacing);
		fm.AddSeed(FastMarchingType::Point(
			portalVeinPoint.x(), portalVeinPoint.y(), portalVeinPoint.z()));

		// add IVC mask pixels as seeds
		const VesselExtractor::MaskBufferType::SpacingType& ivcSpacing = ivcMask->GetSpacing();
		const VesselExtractor::MaskBufferType::PointType& ivcOrigin = ivcMask->GetOrigin();
		const VesselExtractor::MaskBufferType::RegionType& ivcRegion = ivcMask->GetLargestPossibleRegion();
		VesselExtractor::MaskBufferType::IndexType ivcIndex;

		const DistanceMapImageType::PointType& distOrigin = distanceMap->GetOrigin();

		VesselExtractor::MaskBufferType::IndexType ivcOffset;
		ivcOffset[0] = static_cast<VesselExtractor::MaskBufferType::IndexValueType>(distOrigin[0] - ivcOrigin[0]);
		ivcOffset[1] = static_cast<VesselExtractor::MaskBufferType::IndexValueType>(distOrigin[1] - ivcOrigin[1]);
		ivcOffset[2] = static_cast<VesselExtractor::MaskBufferType::IndexValueType>(distOrigin[2] - ivcOrigin[2]);

		itk::ImageRegionConstIteratorWithIndex<DistanceMapImageType> distImgItr(distanceMap, distanceMap->GetLargestPossibleRegion());
		distImgItr.GoToBegin();
		for (; !distImgItr.IsAtEnd(); ++distImgItr)
		{
			const DistanceMapImageType::IndexType& idx = distImgItr.GetIndex();
			ivcIndex[0] = idx[0] + ivcOffset[0];
			ivcIndex[1] = idx[1] + ivcOffset[1];
			ivcIndex[2] = static_cast<int>(idx[2] * imgSpacing[2]/ivcSpacing[2]) + ivcOffset[2];
			if (ivcRegion.IsInside(ivcIndex) && ivcMask->GetPixel(ivcIndex) > 0)
			{
				fm.AddSeed( FastMarchingType::Point(idx[0], idx[1], idx[2]) );
			}
		}

		fm.Execute();

		AllocateItkImage(distanceMap, outputImage);
		itk::ImageRegionIterator<DistanceMapImageType> outImgItr(outputImage, outputImage->GetLargestPossibleRegion());
		outImgItr.GoToBegin();

		FastMarchingType::TimeMapType::const_iterator timeMapItr = fm.GetTimeMap().begin();
		for (; !outImgItr.IsAtEnd(); ++outImgItr, ++timeMapItr)
		{
			outImgItr.Set(*timeMapItr);
		}
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 指定ノードの進行方向をZ軸とする座標系のベクトルを算出する
	 * 
	 * @param [in] node ノード
	 * @param [out] vecX x軸のベクトル
	 * @param [out] vecY y軸のベクトル
	 * @param [out] vecZ z軸のベクトル
	 */
	void LocalImageCoordinateVectors(
		const VesselNode* node,
		Vector3d& vecX,
		Vector3d& vecY,
		Vector3d& vecZ)
	{
		const VesselNode* nodeTail = node;
		for (int i = 0; i < 2; ++i)
		{
			if (nodeTail->GetIncoming())
			{
				nodeTail = nodeTail->GetIncoming();
			}
			else
			{
				break;
			}
		}

		const VesselNode* nodeHead = node;
		for (int i = 0; i < 2; ++i)
		{
			if (nodeHead->GetOutgoings().size() >= 1)
			{
				nodeHead = nodeHead->GetOutgoings()[0];
			}
			else
			{
				break;
			}
		}

		Vector3i nodeDir(nodeHead->GetPosition() - nodeTail->GetPosition());
		if (nodeDir[0] == 0 && nodeDir[1] == 0)
		{
			double val = (nodeDir[2] > 0) ? 1.0 : -1.0;
			vecX = Vector3d(1.0, 0.0, 0.0);
			vecY = Vector3d(0.0, val, 0.0);
			vecZ = Vector3d(0.0, 0.0, val);
		}
		else
		{
			vecZ = nodeDir.cast<double>();

			// vecX = zAxis x nodeDir
			vecX = Vector3d(0.0, 0.0, 1.0).cross(vecZ);
			vecX.normalize();

			// vecY = nodeDir x vecX
			vecY = vecZ.cross(vecX);
			vecY.normalize();
		}
	}

//------------------------------------------------------------------------------
	/**
	 * @brief 抽出した血管領域に対し、血管構造(血管の芯線)からの距離画像を生成する。
	 * 
	 * @param [in] segmentedImageSize 抽出領域の大きさ
	 * @param [in] segmentedImage 抽出された血管マスク
	 * @param [in] voxelSpacing 3Dバッファのピクセル間隔(マスクデータのピクセル間隔)
	 * @param [in] resampledVoxelSpacing 再サンプル画像のピクセル間隔
	 * @param [in] vesselTrees 血管構造データの配列
	 * @param [out] portalVeinDistanceMap 距離画像データ
	 */
	void GenerateDistanceMapFromVessels(
		const Size3i& segmentedImageSize,
		const unsigned char* segmentedImage,
		const Size3d& voxelSpacing, // for segmented image
		const Size3d& resampledVoxelSpacing, // for vessel node positions
		const std::vector<VesselTree*>& vesselTrees,
		std::vector<float>& portalVeinDistanceMap)
	{
		// prepare speed image for fast marching
		int numVoxels = segmentedImageSize.prod();
		std::vector<float> speedImage(numVoxels, 0.0f);
		for (int i = 0; i < numVoxels; ++i)
		{
			if (segmentedImage[i] > 0) speedImage[i] = 1;
		}

		// setup fast marching
		typedef FastMarching<float, 3> FastMarchingType;
		FastMarchingType fm;
		FastMarchingType::Size fmSize(segmentedImageSize[0], segmentedImageSize[1], segmentedImageSize[2]);
		fm.SetSpeedImage(fmSize, &speedImage[0]);

		FastMarchingType::Spacing fmSpacing(voxelSpacing[0], voxelSpacing[1], voxelSpacing[2]);
		fm.SetSpacing(fmSpacing);

		// add seed points to the fast marching
		std::vector<VesselTree*>::const_iterator itrVessels = vesselTrees.begin();
		for (; itrVessels != vesselTrees.end(); ++itrVessels)
		{
			if (!(*itrVessels)->GetStartNode()) continue;

			std::stack<const VesselNode*> vesselNodes;
			vesselNodes.push((*itrVessels)->GetStartNode());
			while (!vesselNodes.empty())
			{
				const VesselNode* currentNode = vesselNodes.top();
				vesselNodes.pop();

				int x = currentNode->GetPosition()[0];
				int y = currentNode->GetPosition()[1];
				int z = static_cast<int>( (currentNode->GetPosition()[2]+0.5)*resampledVoxelSpacing[2]/voxelSpacing[2] );
				if (z < segmentedImageSize.depth())
				{
					fm.AddSeed(FastMarchingType::Point(x, y, z));
				}

				for (int i = 0; i < currentNode->GetOutgoings().size(); ++i)
				{
					vesselNodes.push(currentNode->GetOutgoings()[i]);
				}
			}
		}

		fm.Execute();

		const FastMarchingType::TimeMapType& timeMap = fm.GetTimeMap();
		std::copy(timeMap.begin(), timeMap.end(), portalVeinDistanceMap.begin());

	}

//------------------------------------------------------------------------------
	/**
	 * @brief 各抽出ピクセルの血管の種類を識別する
	 * 
	 * @param [in] segmentedImageSize 抽出済み血管マスク画像の大きさ
	 * @param [in] portalVeinDistanceMap 門脈芯線からの距離画像
	 * @param [in] hepaticVeinDistanceMap 静脈芯線からの距離画像
	 * @param [out] vesselMask 血管の種類が識別された血管マスク画像
	 */
	void ClassifyVoxelsIntoVesselTypes(
		const Size3i& segmentedImageSize,
		// const unsigned char* segmentedImage,
		const std::vector<float>& portalVeinDistanceMap,
		const std::vector<float>& hepaticVeinDistanceMap,
		VesselExtractor::MaskBufferType* vesselMask)
	{
		VesselExtractor::MaskBufferType::PixelType* vesselMaskBuffer = vesselMask->GetBufferPointer();
		int numVoxels = segmentedImageSize.prod();
		for (int i = 0; i < numVoxels; ++i)
		{
			const float& distPV = portalVeinDistanceMap[i];
			const float& distHV = hepaticVeinDistanceMap[i];
			if (distPV < 0.0f && distHV < 0.0f) vesselMaskBuffer[i] = 0;
			else if (distHV < 0.0f) vesselMaskBuffer[i] = 1; // 1: portal vein
			else if (distPV < 0.0f) vesselMaskBuffer[i] = 2; // 2: hepatic vein
			else if (distPV < distHV) vesselMaskBuffer[i] = 1;
			else vesselMaskBuffer[i] = 2;
		}
	}
} // namespace

//------------------------------------------------------------------------------
VesselExtractor::VesselExtractor()
: m_buffer()
, m_resampledBuffer()
, m_vesselEnhancedBuffer()
, m_liverMask()
, m_resampledLiverMask()
, m_IVCMask()
, m_vesselnessImage()
, m_vesselThicknessInfo(NULL)
, m_output()
, m_portalVeinPoint()
, m_minIntensity(0)
, m_maxIntensity(0)
, m_voxelSpacing()
, m_vesselTree(NULL)
, m_hepaticVeinTrees(NULL)
, m_imageBuffer(NULL)
, m_imageBufferSize()
, m_origin()
{
}

//------------------------------------------------------------------------------
VesselExtractor::~VesselExtractor()
{
}

//------------------------------------------------------------------------------
/**
 * @brief 3Dバッファの画像データを取り込む
 * @details 外部で3Dバッファの部分データを取り出し保存したバッファから取り込む。
 * @param [in] editBufferBBoxOrigin 領域の原点の位置
 * @param [in] editBufferBBoxSize 領域の大きさ(幅、高さ、深さ)
 * @param [in] editBuffer 3Dバッファの画像データ
 */
void VesselExtractor::SetBuffer(const Point3i& editBufferBBoxOrigin, const Size3i& editBufferBBoxSize, short* editBuffer)
{
	typedef itk::ImportImageFilter< short, 3 > ImportFilterType;
	ImportFilterType::Pointer importer = ImportFilterType::New();

	ImageBufferType::PointType origin;
	origin[0] = editBufferBBoxOrigin.x();
	origin[1] = editBufferBBoxOrigin.y();
	origin[2] = editBufferBBoxOrigin.z();
	importer->SetOrigin(origin);

	ImageBufferType::IndexType start;
	start.Fill(0);

	ImageBufferType::SizeType regionSize;
	regionSize[0] = editBufferBBoxSize.width();
	regionSize[1] = editBufferBBoxSize.height();
	regionSize[2] = editBufferBBoxSize.depth();

	ImageBufferType::RegionType region;
	region.SetIndex(start);
	region.SetSize(regionSize);
	importer->SetRegion( region );

	const bool importFilterWillDeleteTheInputBuffer = false;

	typedef ImageBufferType::PixelType PixelType;
	PixelType * pixelData = static_cast< PixelType * >( editBuffer );

	const unsigned int totalNumberOfPixels = editBufferBBoxSize.prod();

	importer->SetImportPointer(
		pixelData,
		totalNumberOfPixels,
		importFilterWillDeleteTheInputBuffer );

	importer->Update();

	m_buffer = importer->GetOutput();
	m_buffer->DisconnectPipeline();
}

//------------------------------------------------------------------------------
/**
 * @brief 肝臓マスクのデータを取り込む
 * @param [in] bufferBBoxOrigin 領域の原点位置
 * @param [in] bufferBBoxSize 領域の大きさ(幅、高さ、深さ)
 * @param [in] buffer マスクデータ
 */
void VesselExtractor::SetLiverBuffer(const Point3i& bufferBBoxOrigin, const Size3i& bufferBBoxSize, unsigned char* buffer)
{
	typedef itk::ImportImageFilter< unsigned char, 3 > ImportFilterType;
	ImportFilterType::Pointer importer = ImportFilterType::New();

	MaskBufferType::PointType origin;
	origin[0] = bufferBBoxOrigin.x();
	origin[1] = bufferBBoxOrigin.y();
	origin[2] = bufferBBoxOrigin.z();
	importer->SetOrigin(origin);

	MaskBufferType::IndexType start;
	start.Fill(0);

	MaskBufferType::SizeType regionSize;
	regionSize[0] = bufferBBoxSize.width();
	regionSize[1] = bufferBBoxSize.height();
	regionSize[2] = bufferBBoxSize.depth();

	MaskBufferType::RegionType region;
	region.SetIndex(start);
	region.SetSize(regionSize);
	importer->SetRegion( region );

	const bool importFilterWillDeleteTheInputBuffer = false;

	typedef MaskBufferType::PixelType PixelType;
	PixelType * pixelData = static_cast< PixelType * >( buffer );

	const unsigned int totalNumberOfPixels = bufferBBoxSize.prod();

	importer->SetImportPointer(
		pixelData,
		totalNumberOfPixels,
		importFilterWillDeleteTheInputBuffer );

	importer->Update();

	m_liverMask = MaskBufferType::New();
	AllocateItkImage(importer->GetOutput(), m_liverMask.GetPointer());
	FillInterior(importer->GetOutput(), m_liverMask.GetPointer());
}

//------------------------------------------------------------------------------
/**
 * @brief 下大静脈マスクのデータを取り込む
 * @param [in] bufferBBoxOrigin 領域の原点位置
 * @param [in] bufferBBoxSize 領域の大きさ(幅、高さ、深さ)
 * @param [in] buffer マスクデータ
 */
void VesselExtractor::SetIVCBuffer(const Point3i& bufferBBoxOrigin, const Size3i& bufferBBoxSize, unsigned char* buffer)
{
	typedef itk::ImportImageFilter< unsigned char, 3 > ImportFilterType;
	ImportFilterType::Pointer importer = ImportFilterType::New();

	MaskBufferType::PointType origin;
	origin[0] = bufferBBoxOrigin.x();
	origin[1] = bufferBBoxOrigin.y();
	origin[2] = bufferBBoxOrigin.z();
	importer->SetOrigin(origin);

	MaskBufferType::IndexType start;
	start.Fill(0);

	MaskBufferType::SizeType regionSize;
	regionSize[0] = bufferBBoxSize.width();
	regionSize[1] = bufferBBoxSize.height();
	regionSize[2] = bufferBBoxSize.depth();

	MaskBufferType::RegionType region;
	region.SetIndex(start);
	region.SetSize(regionSize);
	importer->SetRegion( region );

	const bool importFilterWillDeleteTheInputBuffer = false;

	typedef MaskBufferType::PixelType PixelType;
	PixelType * pixelData = static_cast< PixelType * >( buffer );

	const unsigned int totalNumberOfPixels = bufferBBoxSize.prod();

	importer->SetImportPointer(
		pixelData,
		totalNumberOfPixels,
		importFilterWillDeleteTheInputBuffer );

	importer->Update();

	m_IVCMask = importer->GetOutput();
	m_IVCMask->DisconnectPipeline();
}

//------------------------------------------------------------------------------
/**
 * @brief 門脈始点を設定する
 * 
 * @param [in] portalVeinPoint 門脈始点の位置(3Dバッファの座標)
 */
void VesselExtractor::SetPortalVeinPoint( const Point3i& portalVeinPoint )
{
	m_portalVeinPoint = portalVeinPoint;
}

//------------------------------------------------------------------------------
/**
 * @brief 強調する輝度の範囲を設定する
 * 
 * @param [in] minIntensity 輝度の下限
 * @param [in] maxIntensity 輝度の上限
 */
void VesselExtractor::SetIntensityRange(int minIntensity, int maxIntensity)
{
	m_minIntensity = minIntensity;
	m_maxIntensity = maxIntensity;
}

//------------------------------------------------------------------------------
/**
 * @brief 3Dバッファのボクセル間隔
 * 
 * @param [in] voxelSpacing ボクセル間隔
 */
void VesselExtractor::SetVoxelSpacing( const Size3d& voxelSpacing )
{
	m_voxelSpacing = voxelSpacing;
}

//------------------------------------------------------------------------------
/**
 * @brief 血管抽出処理を実行する。
 * @return 処理の成否。
 */
bool VesselExtractor::Execute()
{
	if (m_voxelSpacing.prod() == 0.0 || !m_vesselTree || !m_vesselThicknessInfo) return false;

	try
	{
		Preprocess();

		// pixel position of the origin of the liver region in edit buffer coordinate.
		m_vesselTree->SetOrigin(m_origin);

		// Resampling process changes pixel pitch in z-direction. Finer than the original pitch.
		m_vesselTree->SetSize(m_resampledBufferSize);

		// voxels are isotropic after resampling. identical pixel pitch in all direction (x,y and z).
		m_vesselTree->SetSpacing(m_resampledVoxelSpacing);

		ResampleLiverMask();

		ErodeMask();

		if (m_vesselThicknessInfo->IsEmpty())
		{
			ResampleSourceImage();

			EnhanceVesselIntensity();

			GenerateVesselnessImage();

			GenerateThicknessInfo();
		}

		if (!BuildVesselTree()) return false;

		RefreshVesselNodeID(m_vesselTree, m_hepaticVeinTrees);

		if (!UpdateVesselRegions(false)) return false;

		return true;
	}
	catch(itk::ExceptionObject&)
	{
		return false;
	}
	catch(std::bad_alloc&)
	{
		return false;
	}

	return false;
}

//------------------------------------------------------------------------------
/**
 * @brief 血管領域を更新する
 * 
 * @param [in] performPreprocess 前処理の必要の有無。(true: 前処理実行。 false: 前処理を実行しない。)
 * @return 処理の成否。
 */
bool VesselExtractor::UpdateVesselRegions(bool performPreprocess)
{
	assert(m_minIntensity != m_maxIntensity);

	try
	{
		if (performPreprocess) Preprocess();

		if (!ExtractVesselRegions()) return false;

		ScanVesselRadius();

		m_output = m_vesselMask;

		return true;
	}
	catch(itk::ExceptionObject&)
	{
		return false;
	}
	catch(std::bad_alloc&)
	{
		return false;
	}

//	return false;
}


//------------------------------------------------------------------------------
/**
 * @brief 管状構造画像データオブジェクトのポインタを設定する
 * @details [long description]
 * 
 * @param [in] vesselThicknessInfo 管状構造画像データオブジェクトのポインタ
 */
void VesselExtractor::SetVesselThicknessInfoObject( VesselThicknessInfo* vesselThicknessInfo )
{
	m_vesselThicknessInfo = vesselThicknessInfo;
}

//------------------------------------------------------------------------------
/**
 * @brief 前処理
 * @details 通常のボクセル間隔の設定と、再サンプル後のボクセル間隔の設定を行う。
 * 
 */
void VesselExtractor::Preprocess()
{
	MaskBufferType::SpacingType maskSpacing;
	maskSpacing[0] = m_voxelSpacing[0];
	maskSpacing[1] = m_voxelSpacing[1];
	maskSpacing[2] = m_voxelSpacing[2];
	m_liverMask->SetSpacing(maskSpacing);
	m_IVCMask->SetSpacing(maskSpacing);

	ImageBufferType::SpacingType imageSpacing;
	imageSpacing[0] = m_voxelSpacing[0];
	imageSpacing[1] = m_voxelSpacing[1];
	imageSpacing[2] = m_voxelSpacing[2];
	m_buffer->SetSpacing(imageSpacing);

	m_resampledVoxelSpacing = m_voxelSpacing;
	m_resampledVoxelSpacing[2] = m_voxelSpacing[0];

	ImageBufferType::SizeType size = m_buffer->GetLargestPossibleRegion().GetSize();
	m_resampledBufferSize[0] = size[0];
	m_resampledBufferSize[1] = size[1];
	m_resampledBufferSize[2] = static_cast<ImageBufferType::SizeValueType>(size[2]*m_voxelSpacing[2] / m_resampledVoxelSpacing[2])+1;
}

//------------------------------------------------------------------------------
/**
 * @brief 肝臓マスクデータの再サンプル。
 * @details ｚ方向のボクセル間隔をxとyのボクセル間隔と合わせ、ボクセルを立方体にする。
 * 
 */
void VesselExtractor::ResampleLiverMask()
{
	typedef itk::ResampleImageFilter<MaskBufferType, MaskBufferType> ResamplingFilterType;
	ResamplingFilterType::Pointer resamplingFilter = ResamplingFilterType::New();

	typedef itk::IdentityTransform<double, 3> TransformType;
	TransformType::Pointer transform = TransformType::New();
	transform->SetIdentity();
	resamplingFilter->SetTransform( transform );

	typedef itk::NearestNeighborInterpolateImageFunction< MaskBufferType, double >  InterpolatorType;
	InterpolatorType::Pointer interpolator  = InterpolatorType::New();
	resamplingFilter->SetInterpolator( interpolator );

	resamplingFilter->SetOutputOrigin( m_buffer->GetOrigin() );
	resamplingFilter->SetOutputDirection( m_buffer->GetDirection() );

	ImageBufferType::SpacingType spacing;
	spacing[0] = m_resampledVoxelSpacing[0];
	spacing[1] = m_resampledVoxelSpacing[1];
	spacing[2] = m_resampledVoxelSpacing[2];

	ImageBufferType::SizeType size;
	size[0] = m_resampledBufferSize[0];
	size[1] = m_resampledBufferSize[1];
	size[2] = m_resampledBufferSize[2];

	resamplingFilter->SetOutputSpacing( spacing );
	resamplingFilter->SetSize( size );
	resamplingFilter->SetInput(m_liverMask);
	resamplingFilter->Update();

	m_resampledLiverMask = resamplingFilter->GetOutput();
	m_resampledLiverMask->DisconnectPipeline();
}

//------------------------------------------------------------------------------
/**
 * @brief マスクにErosion処理を施す
 * @details 肝臓の周辺部を含めないようにErosion処理で、一定量表面を削る。
 * 
 */
void VesselExtractor::ErodeMask()
{
	unsigned char maskVal = 0;
	unsigned char* ptr = m_resampledLiverMask->GetBufferPointer();
	MaskBufferType::SizeType maskSize = m_resampledLiverMask->GetLargestPossibleRegion().GetSize();

	for (int n = 0; n < maskSize[0]*maskSize[1]*maskSize[2]; ++n)
	{
		if (ptr[n] != 0) {
			maskVal = ptr[n];
			break;
		}
	}

	int erosion_radius = static_cast<int>(LIVER_EROSION_RADIUS/m_resampledLiverMask->GetSpacing()[0]+0.5);
	if (erosion_radius == 0) erosion_radius = 1;

	typedef itk::BinaryBallStructuringElement<
		MaskBufferType::PixelType, 3 > StructuringElementType;
	typedef itk::BinaryErodeImageFilter<
		MaskBufferType,
		MaskBufferType,
		StructuringElementType >  ErodeFilterType;

	StructuringElementType  structuringElement;
	structuringElement.SetRadius( erosion_radius );
	structuringElement.CreateStructuringElement();

	ErodeFilterType::Pointer  binaryErode  = ErodeFilterType::New();
	binaryErode->SetKernel(  structuringElement );
	binaryErode->SetInput(m_resampledLiverMask.GetPointer());
	binaryErode->SetErodeValue(maskVal);
	binaryErode->Update();

	m_resampledLiverMask = binaryErode->GetOutput();
	m_resampledLiverMask->DisconnectPipeline();
}

//------------------------------------------------------------------------------
/**
 * @brief 抽出対象画像を再サンプルする
 * @details ｚ方向のボクセル間隔をxとyのボクセル間隔と合わせ、ボクセルを立方体にする。
 */
void VesselExtractor::ResampleSourceImage()
{
	typedef itk::ResampleImageFilter<ImageBufferType, ImageBufferType> FilterType;
	FilterType::Pointer resamplingFilter = FilterType::New();

	typedef itk::IdentityTransform<double, 3> TransformType;
	TransformType::Pointer transform = TransformType::New();
	transform->SetIdentity();
	resamplingFilter->SetTransform( transform );

	typedef itk::LinearInterpolateImageFunction< ImageBufferType, double > InterpolatorType;
	InterpolatorType::Pointer interpolator  = InterpolatorType::New();
	resamplingFilter->SetInterpolator( interpolator );

	resamplingFilter->SetOutputOrigin( m_buffer->GetOrigin() );
	resamplingFilter->SetOutputDirection( m_buffer->GetDirection() );

	ImageBufferType::SpacingType spacing;
	spacing[0] = m_resampledVoxelSpacing[0];
	spacing[1] = m_resampledVoxelSpacing[1];
	spacing[2] = m_resampledVoxelSpacing[2];

	ImageBufferType::SizeType size;
	size[0] = m_resampledBufferSize[0];
	size[1] = m_resampledBufferSize[1];
	size[2] = m_resampledBufferSize[2];

	resamplingFilter->SetOutputSpacing( spacing );
	resamplingFilter->SetSize( size );
	resamplingFilter->SetInput(m_buffer);

	typedef itk::DiscreteGaussianImageFilter<ImageBufferType, ImageBufferType > GaussianFilterType;
	GaussianFilterType::Pointer gaussianFilter = GaussianFilterType::New();
	gaussianFilter->SetInput(resamplingFilter->GetOutput());
	gaussianFilter->SetVariance(1.0);
	gaussianFilter->Update();

	m_resampledBuffer = gaussianFilter->GetOutput();
	m_resampledBuffer->DisconnectPipeline();
}

//------------------------------------------------------------------------------
/**
 * @brief 指定輝度範囲を強調する。
 * @sa SetIntensityRange
 */
void VesselExtractor::EnhanceVesselIntensity()
{
	double i0 = (m_minIntensity+m_maxIntensity)/2;
	int intensityHalfWidth = (m_maxIntensity - i0)*1.2;
	double sigma = intensityHalfWidth/sqrt(2.0*log(2.0));
	double coef = 1.0/(2*sigma*sigma);

	m_vesselEnhancedBuffer = ImageBufferType::New();
	AllocateItkImage(m_resampledBuffer.GetPointer(), m_vesselEnhancedBuffer.GetPointer());

	itk::ImageRegionIterator<ImageBufferType> itrVesselEnhance(m_vesselEnhancedBuffer, m_vesselEnhancedBuffer->GetLargestPossibleRegion());
	itrVesselEnhance.GoToBegin();

	itk::ImageRegionConstIterator<ImageBufferType> itrForThresholding(m_resampledBuffer, m_resampledBuffer->GetLargestPossibleRegion());

	itrForThresholding.GoToBegin();
	while(!itrForThresholding.IsAtEnd())
	{
		double i = itrForThresholding.Get();
		if (i > 0)
		{
			itrVesselEnhance.Set(i0*exp(-coef*(i-i0)*(i-i0)));
		}
		++itrForThresholding;
		++itrVesselEnhance;
	}
}

//------------------------------------------------------------------------------
/**
 * @brief ラインフィルタ画像を生成する
 * @details 管状度合いの算出には、 itk::Hessian3DToVesselnessMeasureImageFilter を使用。
 */
void VesselExtractor::GenerateVesselnessImage()
{

	// apply vesselness filter
	typedef itk::HessianRecursiveGaussianImageFilter<ImageBufferType> HessianFilterType;
	typedef itk::Image<float, 3> FloatImageType;
	typedef itk::Hessian3DToVesselnessMeasureImageFilter<float> VesselnessFilterType;

#if 0
	typedef itk::MultiScaleHessianBasedMeasureImageFilter<FloatImageType,VesselnessFilterType> MultiScaleEnhancementFilterType;
	MultiScaleEnhancementFilterType::Pointer multiScaleEnhancementFilter = MultiScaleEnhancementFilterType::New();
	multiScaleEnhancementFilter->SetInput(vesselImage);
	multiScaleEnhancementFilter->SetSigmaMinimum(0.8);
	multiScaleEnhancementFilter->SetSigmaMaximum(1.2);
	multiScaleEnhancementFilter->SetNumberOfSigmaSteps(3);
	VesselnessFilterType* vesselnessFilter = dynamic_cast<VesselnessFilterType*>(multiScaleEnhancementFilter->GetHessianToMeasureFilter());
	vesselnessFilter->SetAlpha1(0.5);
	vesselnessFilter->SetAlpha2(2.0);

#else
	m_vesselnessImage = VesselnessImageType::New();
	AllocateItkImage(m_vesselEnhancedBuffer.GetPointer(), m_vesselnessImage.GetPointer());
	m_vesselnessImage->FillBuffer(0.0f);

	HessianFilterType::Pointer hessianFilter = HessianFilterType::New();
	hessianFilter->SetInput(m_vesselEnhancedBuffer);
	hessianFilter->SetNormalizeAcrossScale(true);

	VesselnessFilterType::Pointer vesselnessFilter = VesselnessFilterType::New();
	//		vesselnessFilter->SetAlpha1(0.25);
	//		vesselnessFilter->SetAlpha2(0.5);
	vesselnessFilter->SetAlpha1(0.5);
	vesselnessFilter->SetAlpha2(2.0);

	//	double sigma[] = {0.8, 1.0, 1.2, 1.4, 1.6};
	//	double sigma[] = {0.8, 1.0, 1.2, 1.4};
	//	double sigma[] = {0.8, 1.1, 1.4};
	// double sigma[] = {1.0, 1.2, 1.4};
//	double sigma[] = {0.8, 1.0, 1.2, 1.4};
//	double sigma[] = {0.6, 0.8, 1.0, 1.2, 1.4, 1.6};
	double sigma[] = {0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 2.0, 4.0};
	//	double sigma[] = {2.0, 4.0};
	//	double sigma[] = {1.5, 2.0, 2.5, 3.0};
	for (int i = 0; i < 8; ++i)
	{
		hessianFilter->SetSigma(sigma[i]);
		vesselnessFilter->SetInput( hessianFilter->GetOutput() );
		vesselnessFilter->Update();

		FloatImageType::ConstPointer outputImage = vesselnessFilter->GetOutput();

		itk::ImageRegionConstIterator<FloatImageType> inputIterator(outputImage, outputImage->GetLargestPossibleRegion());
		itk::ImageRegionIterator<FloatImageType> outputIterator(m_vesselnessImage, m_vesselnessImage->GetLargestPossibleRegion());

		inputIterator.GoToBegin();
		outputIterator.GoToBegin();

		while(!inputIterator.IsAtEnd())
		{
			float v = sigma[i]*inputIterator.Get();
			if (outputIterator.Get() < v)
			{
				outputIterator.Set(v);
			}
			++inputIterator;
			++outputIterator;
		}
	}
#endif
}

//------------------------------------------------------------------------------
/**
 * @brief 管状構造画像データを生成する
 * @details ラインフィルタで管と認識されたボクセルを血管とし、血管壁からの距離画像を作る。
 * 
 */
void VesselExtractor::GenerateThicknessInfo()
{
	VesselThicknessInfo::ThicknessImageType* thicknessImage = m_vesselThicknessInfo->GetImagePointer();
	GenerateDistanceMap(m_vesselnessImage.GetPointer(), thicknessImage);

	typedef itk::ImageRegionIterator<VesselThicknessInfo::ThicknessImageType> ThicknessImageIteratorType;
	ThicknessImageIteratorType thicknessImgItr(thicknessImage, thicknessImage->GetLargestPossibleRegion());

	thicknessImgItr.GoToBegin();
	for (; !thicknessImgItr.IsAtEnd(); ++thicknessImgItr)
	{
		if (thicknessImgItr.Get() == -1.0f)
		{
			thicknessImgItr.Set(0.0f);
		}
		else
		{
			thicknessImgItr.Set( thicknessImgItr.Get() * thicknessImgItr.Get() ); // squared
		}
	}
}

//------------------------------------------------------------------------------
/**
 * @brief 血管構造データを構築する
 * 
 * @return 処理の成否
 */
bool VesselExtractor::BuildVesselTree()
{

	Point3i seedPoint = m_portalVeinPoint;
	seedPoint[2] = static_cast<int>( (m_portalVeinPoint[2] * m_voxelSpacing[2]) / m_resampledVoxelSpacing[2] );

	DistanceMapImageType::Pointer fmImage = DistanceMapImageType::New();
	ApplyFastMarching(m_vesselThicknessInfo->GetImagePointer(), seedPoint, m_IVCMask.GetPointer(), fmImage.GetPointer());

	std::vector<DistanceMapImageType::IndexType> vesselTipPosList;
	FindVesselTips(fmImage.GetPointer(), vesselTipPosList);
	if (vesselTipPosList.empty()) return false;

	EliminateVesselTipsOutOfRange(m_resampledLiverMask, vesselTipPosList);

	EliminateVesselTipsInvalidPath(m_resampledLiverMask, fmImage, vesselTipPosList);

	DistanceMapImageType::IndexType seedPosition;
	seedPosition[0] = m_portalVeinPoint[0];
	seedPosition[1] = m_portalVeinPoint[1];
	seedPosition[2] = static_cast<DistanceMapImageType::IndexValueType>(
		(m_portalVeinPoint[2] * m_voxelSpacing[2]) / m_resampledVoxelSpacing[2] );
	BuildVesselTree_(seedPosition, vesselTipPosList, fmImage, *m_vesselTree, *m_hepaticVeinTrees);

	const MaskBufferType::SizeType& resampledSize = m_resampledLiverMask->GetLargestPossibleRegion().GetSize();
	HepaticVeinTrees::iterator itr = m_hepaticVeinTrees->begin();
	for (; itr != m_hepaticVeinTrees->end(); ++itr)
	{
		(*itr)->SetOrigin(m_origin);
		(*itr)->SetSize(Size3i(resampledSize[0], resampledSize[1], resampledSize[1]));
		(*itr)->SetSpacing(m_resampledVoxelSpacing);
	}

	EliminateShortEndBranches(*m_vesselTree, *m_hepaticVeinTrees);

	return true;
}

//------------------------------------------------------------------------------
/**
 * @brief 血管構造データを基に3Dバッファのデータから血管領域を抽出する
 * 
 * @return 処理の成否
 */
bool VesselExtractor::ExtractVesselRegions()
{
	const MaskBufferType::PointType& bufferOrigin = m_buffer->GetOrigin();
	const MaskBufferType::SizeType& bufferSize = m_buffer->GetLargestPossibleRegion().GetSize();
	Boxi imageBBox;
	imageBBox.origin().x() = bufferOrigin[0];
	imageBBox.origin().y() = bufferOrigin[1];
	imageBBox.origin().z() = bufferOrigin[2];
	imageBBox.size().width() = bufferSize[0];
	imageBBox.size().height() = bufferSize[1];
	imageBBox.size().depth() = bufferSize[2];
	std::vector<unsigned char> pathImage(imageBBox.size().prod(), 0);

	// draw vessel paths on pathImage. this image is used as foreground for graphcut.
	GeneratePathImage(imageBBox.size(), pathImage);

	// make a liver mask, then remove pixels around vessel paths.
	std::vector<unsigned char> subtractedLiverMask(imageBBox.size().prod(), 0); // used as background for graphcut
	double pathRadius = 10.0;
	SubtractPathFromLiverMask(pathRadius, subtractedLiverMask);

	// 1 to add pixels on IVC as background
	// 0 to disable the pixels on IVC
	ChangeMaskValueOnIVC(subtractedLiverMask, 0);

	// set 0 for each mask pixel if underlying image pixel is within the vessel intensity range
	// this process is for the graph cut operation to avoid vessel intensities
	// from being included in the background intensity distribution
	SubtractMaskInVesselIntensityRange(subtractedLiverMask);

	// prepare for graphcut operation
	GraphCut graphcut;
	graphcut.SetSourceImage(m_imageBufferSize, m_imageBuffer);
	graphcut.SetObjectMask(Boxi(imageBBox.origin(), imageBBox.size()), &pathImage[0]);
	graphcut.SetBackgroundMask(Boxi(imageBBox.origin(), imageBBox.size()), &subtractedLiverMask[0]);
	graphcut.SetErosionLevelInPostProcess(0); // don't perform opening in post processing
	graphcut.SetNeighborType(GraphCut::kGraphcutNeighborType18);
	graphcut.SetDataTermCoeff(1.0);
	graphcut.SetSmoothTermCoeff(500.0);

	// perform graphcut
	Boxi segmentedImageSize;
	unsigned char* segmentedImage = NULL;
	if (!graphcut.Execute(segmentedImageSize.origin(), segmentedImageSize.size(), segmentedImage))
	{
		return false;
	}

	try
	{
		// remove segmented voxels outside of the united mask. the remainings are vessel region.
		PostProcessSegmentedImage(pathRadius, segmentedImage);

		// allocate buffer for portal/hepatic vein mask if necessary
		if (m_vesselMask.IsNull())
		{
			m_vesselMask = MaskBufferType::New();
		}
		m_vesselMask->Initialize();
		AllocateItkImage(m_liverMask.GetPointer(), m_vesselMask.GetPointer());

		// make distance map from portal vein tree
		std::vector<VesselTree*> vesselTrees;
		vesselTrees.push_back(m_vesselTree);
		std::vector<float> portalVeinDistanceMap(segmentedImageSize.size().prod(), 0.0f);
		GenerateDistanceMapFromVessels(segmentedImageSize.size(), segmentedImage, m_voxelSpacing, m_resampledVoxelSpacing, vesselTrees, portalVeinDistanceMap);

		// make distance map from hepatic vein trees
		std::vector<float> hepaticVeinDistanceMap(segmentedImageSize.size().prod(), 0.0f);
		GenerateDistanceMapFromVessels(segmentedImageSize.size(), segmentedImage, m_voxelSpacing, m_resampledVoxelSpacing, *m_hepaticVeinTrees, hepaticVeinDistanceMap);

		// check the distance value for each voxel in segmented image and
		// determine the label (portal or hepatic vein) for the voxel.
		ClassifyVoxelsIntoVesselTypes(segmentedImageSize.size(), portalVeinDistanceMap, hepaticVeinDistanceMap, m_vesselMask);

		if (segmentedImage) delete [] segmentedImage;

		return true;
	}
	catch(itk::ExceptionObject&)
	{
		if (segmentedImage) delete [] segmentedImage;
		return false;
	}
	catch(std::bad_alloc&)
	{
		if (segmentedImage) delete [] segmentedImage;
		return false;
	}
}

//------------------------------------------------------------------------------
/**
 * @brief 抽出された血管領域から血管の半径を調べる処理
 * @details 抽出された血管領域の最外周からの距離画像を生成し、血管構造データのノードの位置の値を半径として記録する。
 * ノードの位置によっては実際の太さと異なる。
 * 
 */
void VesselExtractor::ScanVesselRadius()
{
	const MaskBufferType::SizeType& imageSize = m_vesselMask->GetLargestPossibleRegion().GetSize();
	int w = imageSize[0];
	int h = imageSize[1];
	int d = imageSize[2];

	// ensure that the mask pixels are 0 or 1
	typedef itk::Image<float, 3> FloatImageType;
	FloatImageType::Pointer speedImage = FloatImageType::New();
	AllocateItkImage(m_vesselMask.GetPointer(), speedImage.GetPointer());
	speedImage->FillBuffer(0.0f);

	itk::ImageRegionIterator<FloatImageType> itrSpeed(speedImage, speedImage->GetLargestPossibleRegion());
	itrSpeed.GoToBegin();
	itk::ImageRegionConstIterator<MaskBufferType> itrMask(m_vesselMask, m_vesselMask->GetLargestPossibleRegion());
	itrMask.GoToBegin();
	for (; !itrMask.IsAtEnd(); ++itrMask, ++itrSpeed)
	{
		if (itrMask.Get() > 0) itrSpeed.Set(1.0f);
	}

	FloatImageType::IndexType speedIndex;
	const FloatImageType::RegionType& speedRegion = speedImage->GetLargestPossibleRegion();
	typedef FastMarching<float, 3> FastMarchingType;
	FastMarchingType fm;
	fm.SetSpeedImage(FastMarchingType::Size(w, h, d), speedImage->GetBufferPointer());
	fm.SetSpacing(FastMarchingType::Spacing(m_voxelSpacing[0], m_voxelSpacing[1], m_voxelSpacing[2]));
	for (int k = 0; k < d; ++k)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				speedIndex[0] = i;
				speedIndex[1] = j;
				speedIndex[2] = k;
				if (speedImage->GetPixel(speedIndex) > 0.0f) continue;

				speedIndex[0] = i-1;
				if (speedRegion.IsInside(speedIndex) && speedImage->GetPixel(speedIndex) > 0.0f)
				{
					fm.AddSeed(FastMarchingType::Point(i, j, k));
					continue;
				}

				speedIndex[0] = i+1;
				if (speedRegion.IsInside(speedIndex) && speedImage->GetPixel(speedIndex) > 0.0f)
				{
					fm.AddSeed(FastMarchingType::Point(i, j, k));
					continue;
				}

				speedIndex[0] = i;
				speedIndex[1] = j-1;
				if (speedRegion.IsInside(speedIndex) && speedImage->GetPixel(speedIndex) > 0.0f)
				{
					fm.AddSeed(FastMarchingType::Point(i, j, k));
					continue;
				}

				speedIndex[1] = j+1;
				if (speedRegion.IsInside(speedIndex) && speedImage->GetPixel(speedIndex) > 0.0f)
				{
					fm.AddSeed(FastMarchingType::Point(i, j, k));
					continue;
				}

				speedIndex[1] = j;
				speedIndex[2] = k-1;
				if (speedRegion.IsInside(speedIndex) && speedImage->GetPixel(speedIndex) > 0.0f)
				{
					fm.AddSeed(FastMarchingType::Point(i, j, k));
					continue;
				}

				speedIndex[2] = k+1;
				if (speedRegion.IsInside(speedIndex) && speedImage->GetPixel(speedIndex) > 0.0f)
				{
					fm.AddSeed(FastMarchingType::Point(i, j, k));
					continue;
				}

			}
		}
	}

	fm.Execute();
	const FastMarchingType::TimeMapType& timeMap = fm.GetTimeMap();

	std::stack<VesselNode*> vesselNodes;

	HepaticVeinTrees allVesselTrees(m_hepaticVeinTrees->size());
	std::copy(m_hepaticVeinTrees->begin(), m_hepaticVeinTrees->end(), allVesselTrees.begin());
	allVesselTrees.push_back(m_vesselTree);

	Vector3d vecX;
	Vector3d vecY;
	Vector3d vecZ;

	HepaticVeinTrees::const_iterator itr = allVesselTrees.begin();
	for (; itr != allVesselTrees.end(); ++itr)
	{
		if (!(*itr)->GetStartNode()) continue;

		vesselNodes.push((*itr)->GetStartNode());
		while (!vesselNodes.empty())
		{
			VesselNode* currentNode = vesselNodes.top();
			vesselNodes.pop();

			LocalImageCoordinateVectors(currentNode, vecX, vecY, vecZ);
			vecZ.normalize();
			currentNode->SetDirection(vecZ);

			Point3i pt = ConvertResampledToOriginalPosition(currentNode->GetPosition());

			currentNode->SetRadius(timeMap[pt.x() + pt.y() * w + pt.z() * w * h]);
			if (currentNode->GetRadius() < 0.0) currentNode->SetRadius(0.0);

			for (int i = 0; i < currentNode->GetOutgoings().size(); ++i)
			{
				vesselNodes.push(currentNode->GetOutgoings()[i]);
			}
		}
	}
}

//------------------------------------------------------------------------------
/**
 * @brief 血管の経路画像を作る。
 * @details 入力された画像バッファに対し、血管構造データのノードの位置と対応するピクセルを非0にする。
 * 
 * @param [in] imageSize 画像の大きさ
 * @param [out] pathImage 出力画像
 */
void VesselExtractor::GeneratePathImage( const Size3i& imageSize, std::vector<unsigned char>& pathImage )
{
	int w = imageSize.width();
	int h = imageSize.height();
	int d = imageSize.depth();

	std::stack<VesselNode*> vesselNodes;

	HepaticVeinTrees allVesselTrees(m_hepaticVeinTrees->size());
	std::copy(m_hepaticVeinTrees->begin(), m_hepaticVeinTrees->end(), allVesselTrees.begin());
	allVesselTrees.push_back(m_vesselTree);

	HepaticVeinTrees::const_iterator itr = allVesselTrees.begin();
	for (; itr != allVesselTrees.end(); ++itr)
	{
		if (!(*itr)->GetStartNode()) continue;

		vesselNodes.push((*itr)->GetStartNode());
		while (!vesselNodes.empty())
		{
			VesselNode* currentNode = vesselNodes.top();
			vesselNodes.pop();

			Point3i pos = ConvertResampledToOriginalPosition(currentNode->GetPosition());
			if (pos.z() < d)
			{
				int idx = pos.x() + pos.y()*w + pos.z()*w*h;
				pathImage[idx] = 1;
			}

			for (int i = 0; i < currentNode->GetOutgoings().size(); ++i)
			{
				vesselNodes.push(currentNode->GetOutgoings()[i]);
			}
		}
	}
}

//------------------------------------------------------------------------------
// In liver mask, clear non-zero pixels within the radius less than pathRadius from vessel path.
// The subtracted result goes to subtractedLiverMask without changing the original.
/**
 * @brief 肝臓マスクから血管経路周辺を削除した画像を取得する
 * @details 肝臓マスクのコピーを作り、血管経路データを基に経路から一定半径内の肝臓マスクを削除する。\n
 * 半径は、根本から末端へいくにつれ小さくなる。2ミリが下限。
 * 
 * @param [in] pathRadius 削除半径の初期値
 * @param [out] subtractedLiverMask 出力画像
 */
void VesselExtractor::SubtractPathFromLiverMask( double pathRadius, std::vector<unsigned char>& subtractedLiverMask )
{
	const MaskBufferType::SizeType& liverMaskSize = m_liverMask->GetLargestPossibleRegion().GetSize();
	int w = liverMaskSize[0];
	int h = liverMaskSize[1];
	int d = liverMaskSize[2];
	Size3i regionSize(w, h, d);

	int xrange = static_cast<int>(pathRadius / m_voxelSpacing[0] + 0.5);
	int yrange = static_cast<int>(pathRadius / m_voxelSpacing[1] + 0.5);
	int zrange = static_cast<int>(pathRadius / m_voxelSpacing[2] + 0.5);

	MaskBufferType::IndexType liverMaskIndex;
	const MaskBufferType::RegionType& liverMaskRegion = m_liverMask->GetLargestPossibleRegion();


	///////////////////////////////////////////////////////
	// dilate mask
	unsigned char maskVal = 0;
	unsigned char* ptr = m_liverMask->GetBufferPointer();
	MaskBufferType::SizeType maskSize = m_liverMask->GetLargestPossibleRegion().GetSize();

	// find first non-zero mask value
	for (int n = 0; n < maskSize[0]*maskSize[1]*maskSize[2]; ++n)
	{
		if (ptr[n] != 0) {
			maskVal = ptr[n];
			break;
		}
	}

	typedef itk::BinaryBallStructuringElement<
		MaskBufferType::PixelType, 3 > StructuringElementType;
	typedef itk::BinaryDilateImageFilter<
		MaskBufferType,
		MaskBufferType,
		StructuringElementType > DilateFilterType;

	StructuringElementType  structuringElement;
	structuringElement.SetRadius( 2 );
	structuringElement.CreateStructuringElement();

	DilateFilterType::Pointer binaryDilate  = DilateFilterType::New();
	binaryDilate->SetKernel( structuringElement );
	binaryDilate->SetInput(m_liverMask.GetPointer());
	binaryDilate->SetDilateValue(maskVal);
	binaryDilate->Update();
	///////////////////////////////////////////////////////

	// copy entire liver mask
	std::vector<unsigned char>::iterator itrDst = subtractedLiverMask.begin();
	itk::ImageRegionConstIterator<MaskBufferType> itrLiverMask(binaryDilate->GetOutput(), binaryDilate->GetOutput()->GetLargestPossibleRegion());
	itrLiverMask.GoToBegin();
	for (; !itrLiverMask.IsAtEnd(); ++itrLiverMask, ++itrDst)
	{
		*itrDst = itrLiverMask.Get();
	}

	std::stack<VesselNode*> vesselNodes;
	HepaticVeinTrees allVesselTrees(m_hepaticVeinTrees->size());
	std::copy(m_hepaticVeinTrees->begin(), m_hepaticVeinTrees->end(), allVesselTrees.begin());
	allVesselTrees.push_back(m_vesselTree);

	HepaticVeinTrees::const_iterator itr = allVesselTrees.begin();
	for (; itr != allVesselTrees.end(); ++itr)
	{
		if (!(*itr)->GetStartNode()) continue;

		vesselNodes.push((*itr)->GetStartNode());
		while (!vesselNodes.empty())
		{
			VesselNode* currentNode = vesselNodes.top();
			vesselNodes.pop();

			// Decrease radius linearly as branch level increases.
			double sqrRadius = ((kMinVesselRadiusForSubtraction - pathRadius)/kMinRadiusBranchLevelForSubtraction) * currentNode->GetBranchLevel() + pathRadius;
			if (sqrRadius < kMinVesselRadiusForSubtraction) sqrRadius = kMinVesselRadiusForSubtraction;
			sqrRadius *= sqrRadius;

			Point3i pos = ConvertResampledToOriginalPosition(currentNode->GetPosition());

			for (int dz = -zrange; dz <= zrange; ++dz)
			{
				for (int dy = -yrange; dy <= yrange; ++dy)
				{
					for (int dx = -xrange; dx <= xrange; ++dx)
					{
						liverMaskIndex[0] = pos.x()+dx;
						liverMaskIndex[1] = pos.y()+dy;
						liverMaskIndex[2] = pos.z()+dz;

						if (!liverMaskRegion.IsInside(liverMaskIndex)) continue;

						double xdiff = dx*m_voxelSpacing[0];
						double ydiff = dy*m_voxelSpacing[1];
						double zdiff = dz*m_voxelSpacing[2];
						if (xdiff*xdiff + ydiff*ydiff + zdiff*zdiff <= sqrRadius)
						{
							int idx = pos.x()+dx + (pos.y()+dy)*w + (pos.z()+dz)*w*h;
							if (subtractedLiverMask[idx] != 0) // clear mask pixels if non-zero
							{
								subtractedLiverMask[idx] = 0;
							}
						}
					}
				}
			}

			for (int i = 0; i < currentNode->GetOutgoings().size(); ++i)
			{
				vesselNodes.push(currentNode->GetOutgoings()[i]);
			}
		}
	}

}

//------------------------------------------------------------------------------
/**
 * @brief 入力されたマスク画像の配列に下大静脈のマスクを上書きする
 * 
 * @param [in,out] subtractedLiverMask マスク画像配列
 */
void VesselExtractor::ChangeMaskValueOnIVC(
	std::vector<unsigned char>& subtractedLiverMask, unsigned char val)
{
	const MaskBufferType::PointType& liverMaskOrigin = m_liverMask->GetOrigin();
	const MaskBufferType::SizeType& liverMaskSize = m_liverMask->GetLargestPossibleRegion().GetSize();
	int w = liverMaskSize[0];
	int h = liverMaskSize[1];
	int d = liverMaskSize[2];

	const MaskBufferType::PointType& ivcOrigin = m_IVCMask->GetOrigin();
	const MaskBufferType::RegionType& ivcRegion = m_IVCMask->GetLargestPossibleRegion();

	MaskBufferType::IndexType ivcIndex;

	Point3i ivcOffset(
		liverMaskOrigin[0] - ivcOrigin[0],
		liverMaskOrigin[1] - ivcOrigin[1],
		liverMaskOrigin[2] - ivcOrigin[2]);

	for (int k = 0; k < d; ++k)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				ivcIndex[0] = i + ivcOffset.x();
				ivcIndex[1] = j + ivcOffset.y();
				ivcIndex[2] = k - ivcOffset.z();
				if (ivcRegion.IsInside(ivcIndex) && m_IVCMask->GetPixel(ivcIndex) > 0)
				{
					int idx = i + j * w + k * w * h;
					subtractedLiverMask[idx] = val; // disable mask
				}
			}
		}
	}
}

//------------------------------------------------------------------------------
void VesselExtractor::SubtractMaskInVesselIntensityRange( std::vector<unsigned char>& subtractedLiverMask )
{
#ifdef _DEBUG
	const ImageBufferType::SizeType& s = m_buffer->GetLargestPossibleRegion().GetSize();
	int numPixels = s[0] * s[1] * s[2];
	assert(numPixels == subtractedLiverMask.size());
#endif

	// size of m_buffer and subtractedLiverMask must be the same
	const ImageBufferType::PixelType* p = m_buffer->GetBufferPointer();
	for (int i = 0; i < subtractedLiverMask.size(); ++i) {
		//
		if (p[i] >= m_minIntensity && p[i] <= m_maxIntensity) subtractedLiverMask[i] = 0;
	}
}

//------------------------------------------------------------------------------
/**
 * @brief 血管抽出の後処理
 * @details 抽出後の領域から、血管経路周辺以外で抽出された領域を取り除く。\n
 * 取り除く対象は、血管系ロに対し、pathRadiusで設定された半径の外にある抽出領域。
 * 
 * @param [in] pathRadius 半径
 * @param [in,out] segmentedImage 抽出結果
 */
void VesselExtractor::PostProcessSegmentedImage(double pathRadius, unsigned char* segmentedImage )
{
	const MaskBufferType::SizeType& liverMaskSize = m_liverMask->GetLargestPossibleRegion().GetSize();
	int w = liverMaskSize[0];
	int h = liverMaskSize[1];
	int d = liverMaskSize[2];
	Size3i regionSize(w, h, d);

	int xrange = static_cast<int>(pathRadius / m_voxelSpacing[0] + 0.5);
	int yrange = static_cast<int>(pathRadius / m_voxelSpacing[1] + 0.5);
	int zrange = static_cast<int>(pathRadius / m_voxelSpacing[2] + 0.5);

	double sqrRadius = pathRadius*pathRadius;

	for (int i = 0; i < w*h*d; ++i)
	{
		if (segmentedImage[i] != 0) segmentedImage[i] = POSTPROCESS_GRAPHCUT_REGION;
	}

	std::stack<VesselNode*> vesselNodes;
	HepaticVeinTrees allVesselTrees(m_hepaticVeinTrees->size());
	std::copy(m_hepaticVeinTrees->begin(), m_hepaticVeinTrees->end(), allVesselTrees.begin());
	allVesselTrees.push_back(m_vesselTree);
	HepaticVeinTrees::const_iterator itr = allVesselTrees.begin();

	for (; itr != allVesselTrees.end(); ++itr)
	{
		if (!(*itr)->GetStartNode()) continue;

		vesselNodes.push((*itr)->GetStartNode());
		while (!vesselNodes.empty())
		{
			VesselNode* currentNode = vesselNodes.top();
			vesselNodes.pop();

			Point3i pos = ConvertResampledToOriginalPosition(currentNode->GetPosition());
			int idx = pos.x() + pos.y()*w + pos.z()*w*h;
			if (segmentedImage[idx] == POSTPROCESS_GRAPHCUT_REGION)
			{
				segmentedImage[idx] = POSTPROCESS_VESSEL_REGION;

				for (int dz = -zrange; dz <= zrange; ++dz)
				{
					for (int dy = -yrange; dy <= yrange; ++dy)
					{
						for (int dx = -xrange; dx <= xrange; ++dx)
						{
							if (dx == 0 && dy == 0 && dz == 0) continue;
							if (!IsInside(regionSize, pos.x()+dx, pos.y()+dy, pos.z()+dz)) continue;

							double xdiff = dx*m_voxelSpacing[0];
							double ydiff = dy*m_voxelSpacing[1];
							double zdiff = dz*m_voxelSpacing[2];
							if (xdiff*xdiff + ydiff*ydiff + zdiff*zdiff <= sqrRadius)
							{
								int idx = pos.x()+dx + (pos.y()+dy)*w + (pos.z()+dz)*w*h;
								if (segmentedImage[idx] == POSTPROCESS_GRAPHCUT_REGION)
								{
									segmentedImage[idx] = POSTPROCESS_VESSEL_REGION;
								}
							}
						}
					}
				}
			}

			for (int i = 0; i < currentNode->GetOutgoings().size(); ++i)
			{
				vesselNodes.push(currentNode->GetOutgoings()[i]);
			}
		}
	}

	for (int i = 0; i < w*h*d; ++i)
	{
		if (segmentedImage[i] != POSTPROCESS_VESSEL_REGION) segmentedImage[i] = 0;
	}
}

//------------------------------------------------------------------------------
/**
 * @brief 再サンプル画像の位置を元の画像の位置に変換する
 * @details 元画像とは、再サンプル画像の元とる画像データ。原点は再サンプル画像と同じ。3Dバッファと同じピクセル間隔を持つ。
 * @sa m_buffer
 * 
 * @param [in] resampledPosition 再サンプル画像の位置
 * @return 変換された位置
 */
Point3i VesselExtractor::ConvertResampledToOriginalPosition( const Point3i& resampledPosition ) const
{
	Point3i originalPosition = resampledPosition;
	originalPosition.z() = static_cast<int>( (originalPosition.z() + 0.5) * m_resampledVoxelSpacing[2] / m_voxelSpacing[2] );
	return originalPosition;
}

} // namespace segmentation
} // namespace lssplib
