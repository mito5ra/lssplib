#include "Segmentation/GraphCut.h"

#include <numeric>

#include "Segmentation/Graph.h"
#include "Segmentation/BKMaxFlow.h"

#include "Segmentation/ScanLineFill.h"

#define VOXEL_EMPTY 0 //!< 後処理で使用するボクセルの状態ラベル。空を表す。
#define VOXEL_SEGMENTED 1 //!< 後処理で使用するボクセルの状態ラベル。グラフカット処理で抽出されたピクセル又は前景マスクのピクセルを表す。
#define VOXEL_CONNECTED 2 //!< 後処理で使用するボクセルの状態ラベル。前景マスクのピクセルと接続している抽出済みピクセルを表す。
#define VOXEL_EXTERIOR 3 //!< 後処理で使用するボクセルの状態ラベル。最外周の領域を表す。

namespace lssplib
{
namespace segmentation
{

const static int kNumberOfBins = 100; //!< ヒストグラムのビン数

const static unsigned char kLabelErosion = 3; //!< Erosion処理のラベル
const static unsigned char kLabelDilation = 4; //!< Dilation処理のラベル

// node to node capacity = L*exp(-K*diff*diff)/dist;
//const static double kNodeToNodeCapacityCoefL = 40.0; //!< ノード間容量の係数
const static double kNodeToNodeCapacityCoefK = 0.1; //!< ノード間容量の係数
const static double kMaximumEdgeCapacity = 1000.0; //!< ノードとSource/Sink間の最大容量
const static double kMinimumEdgeCapacity = 0.0; //!< ノードとSource/Sink間の最小容量
const static int kMaxNumNodesPerCycle = 2000000; //!< 一度に処理するノードの最大数

//! @brief Erosion/Dilationの閾値のリスト
//! @details 6近傍の内有効なピクセル数(0から6)の1/3にあたる数。
const static int kCountThreshold[7] = {0, 1, 1, 1, 1, 2, 2};

//==============================================================================
namespace
{
//------------------------------------------------------------------------------
	//! @brief 指定した座標の内外判定
	//! @param [in] x x位置
	//! @param [in] y y位置
	//! @param [in] z z位置
	//! @param [in] bbox 領域
	//! @return 座標が領域内ならばtrue。それ以外ではfalse。
	bool IsInBox(int x, int y, int z, const Boxi& bbox)
	{
		if ( x < 0 || y < 0 || z < 0 ||
			x >= bbox.size().width() || y >= bbox.size().height() || z >= bbox.size().depth() )
		{
			return false;
		}
		return true;
	}

//------------------------------------------------------------------------------
	//! @brief 2つの領域を合成した領域を取得する
	//! @details 与えられた2つの領域を囲む一つの領域を返す。
	//! 合成された領域は、2つの領域の16の頂点内の、
	//! 最小と最大座標値で構成された2点を対角とする箱型の領域。
	//! @param [in] vol0 箱型領域
	//! @param [in] vol1 別の箱型領域
	//! @return 合成された領域
	Boxi ComputeCombinedVolume(const Boxi& vol0, const Boxi& vol1)
	{
		Point3i p[4];
		p[0] = vol0.origin();
		p[1] = vol0.origin() + vol0.size() - Point3i(1,1,1);
		p[2] = vol1.origin();
		p[3] = vol1.origin() + vol1.size() - Point3i(1,1,1);

		Point3i minPoint;
		Point3i maxPoint;
		minPoint[0] = std::min(std::min(p[0].x(), p[1].x()), std::min(p[2].x(), p[3].x()));
		maxPoint[0] = std::max(std::max(p[0].x(), p[1].x()), std::max(p[2].x(), p[3].x()));
		minPoint[1] = std::min(std::min(p[0].y(), p[1].y()), std::min(p[2].y(), p[3].y()));
		maxPoint[1] = std::max(std::max(p[0].y(), p[1].y()), std::max(p[2].y(), p[3].y()));
		minPoint[2] = std::min(std::min(p[0].z(), p[1].z()), std::min(p[2].z(), p[3].z()));
		maxPoint[2] = std::max(std::max(p[0].z(), p[1].z()), std::max(p[2].z(), p[3].z()));

		return Boxi(minPoint, maxPoint-minPoint+Point3i(1,1,1));
	}

	//! @brief 抽出対象領域をグラフカット処理するために分割する時の一セットのスライス数を算出する
	//! @param [in] bbox 抽出対象領域
	//! @param [in] numMaxNodes グラフカット処理の一回の最大ノード数
	//! @param [in] numOverlapSlices 重複させるスライス数
	//! @return 一度に処理するスライス数
	int ComputeNumberOfSlicesPerLoop(const Boxi& bbox, int numMaxNodes, int numOverlapSlices)
	{
		int numNodesPerSlice = bbox.size().width() * bbox.size().height();
		int totalNumNodes = numNodesPerSlice * bbox.size().depth();
		int numNodesOverlap = numOverlapSlices * numNodesPerSlice;

		int numDivisions = (totalNumNodes - numNodesOverlap)/(numMaxNodes - numNodesOverlap);
		if (numDivisions <= 0) return bbox.size().depth();

		if ( (totalNumNodes - numNodesOverlap)%(numMaxNodes - numNodesOverlap) > 0) ++numDivisions;

		int numNodesPerLoop = ((numDivisions - 1)*numNodesOverlap + totalNumNodes) / numDivisions;
		if ( ((numDivisions - 1)*numNodesOverlap + totalNumNodes) % numDivisions > 0) ++numNodesPerLoop;

		int numSlicesPerLoop = numNodesPerLoop / numNodesPerSlice;
		if ( numNodesPerLoop % numNodesPerSlice > 0) ++numSlicesPerLoop;

		return numSlicesPerLoop;
	}
}

//==============================================================================
//! グラフカット処理PIMPL実装オブジェクト
class GraphCutImpl
{
public:
	typedef std::vector<double> Histogram; //!< ヒストグラムデータ型

	typedef std::pair<short, short> MinMaxIntensity; //!< 輝度最大最小値ペア型

	GraphCutImpl()
	: m_imageBufferSize()
	, m_objectBBox()
	, m_backgroundBBox()
	, m_subVolume()
	, m_imageBuffer(NULL)
	, m_objectMask(NULL)
	, m_backgroundMask(NULL)
	, m_objectHistogram()
	, m_backgroundHistogram()
	, m_intensityMinMax()
	, m_erosionLevel(2)
	, m_sourceVertex(0)
	, m_sinkVertex(0)
	, m_graph()
	, m_dataTermCoeff(1.0)
	, m_smoothTermCoeff(1.0)
	, m_neighborType(GraphCut::kGraphcutNeighborType26)
	{}

	~GraphCutImpl() {}

	//! @brief 前景マスクを設定する
	//! @param [in] objectBBox 前景マスク領域
	//! @param [in] objectMask 前景マスクデータのポインタ
	void SetObjectMask( const Boxi& objectBBox, const unsigned char* objectMask )
	{
		m_objectBBox = objectBBox;
		m_objectMask = objectMask;
	}

	//! @brief 背景マスクを設定する
	//! @param [in] backgroundBBox 背景マスク領域
	//! @param [in] backgroundMask 背景マスクデータのポインタ
	void SetBackgroundMask(const Boxi& backgroundBBox, const unsigned char* backgroundMask)
	{
		m_backgroundBBox = backgroundBBox;
		m_backgroundMask = backgroundMask;
	}

	//! @brief 3Dバッファの情報設定
	//! @param [in] srcImageSize 3Dバッファの大きさ
	//! @param [in] srcImage 3Dバッファへのポインタ
	void SetSourceImage(const Size3i& srcImageSize, const short* srcImage)
	{
		m_imageBufferSize = srcImageSize;
		m_imageBuffer = srcImage;
	}

	//! @brief グラフカット処理を実行
	//! @param [in] subVolumeOrigin 抽出された領域の原点位置
	//! @param [in] subVolumeSize 抽出された領域の大きさ(幅、高さ、深さ)
	//! @param [in] outputBuffer 抽出データ
	//! @return 処理に成功したらtrue、失敗ならfalseを返す
	bool Execute(Point3i& subVolumeOrigin, Size3i& subVolumeSize, unsigned char*& outputBuffer);

	//! @brief 抽出領域の最小輝度を返す
	//! @return 最小輝度
	short GetMinIntensity() const { return m_intensityMinMax.first; }

	//! @brief 抽出領域の最大輝度を返す
	//! @return 最大輝度
	short GetMaxIntensity() const { return m_intensityMinMax.second; }

	//! @brief グラフカットの後処理で行うErosionの回数を設定する。
	//! @param [in] n 回数
	void SetErosionLevelInPostProcess( int n ) { m_erosionLevel = n; }

	//! @brief グラフの各ノードの近傍数を設定する。
	//! @param [in] neighborType 近傍数を示す列挙型(kGraphcutNeighborType6, kGraphcutNeighborType18又は、kGraphcutNeighborType26)
	void SetNeighborType(GraphCut::GraphcutNeighborType neighborType) { m_neighborType = neighborType; }

	//! @brief データ項の係数を設定する。(デフォルト値は1.0)
	//! @param [in] coeff 係数
	void SetDataTermCoeff(double coeff) { m_dataTermCoeff = coeff; }

	//! @brief 平滑化項の係数を設定する。(デフォルト値は1.0)
	//! @param [in] coeff 係数
	void SetSmoothTermCoeff(double coeff) { m_smoothTermCoeff = coeff; }
private:
	//! @brief コピーコンストラクタ
	GraphCutImpl(const GraphCutImpl&) {}

	//! @brief グラフデータを構築する
	//! @param [in] subVolume グラフカット処理対象領域(前景・背景マスクの合成領域)
	//! @return 処理に成功したらtrue、失敗ならfalseを返す
	bool BuildGraph(const Boxi& subVolume);

	//! @brief グラフデータをMaxFlow処理にかける
	void ComputeMaxFlow();

	//! @brief 出力用のバッファを確保する
	//! @details 前景・背景マスクの合成領域
	//! @return バッファのポインタ
	unsigned char* CreateOutputBuffer() const;

	//! @brief MaxFlowの処理結果を出力バッファに記録する
	//! @param [in] subSubVolume グラフカット処理1セット分の領域
	//! @param [in] buf グラフカット処理1セット分の領域データへのポインタ
	void FillBuffer(const Boxi& subSubVolume, unsigned char* buf) const;

	//! @brief グラフカットの後処理
	//! @details 次の4つの処理を行う。
	//! Erosion、前景マスクとの連結部取得、Dilation、空洞を埋める
	//! ErosionとDilationは、実行前に設定した回数分行われる。
	//! デフォルトでは2回行う。
	//! @param [in] buf 出力バッファのポインタ
	void PostProcess(unsigned char* buf) const;

	//! @brief 指定したバッファの位置にErosionのラベルを付ける
	//! 指定位置の6近傍を調べ、周囲1/3以上が空白ならErosionのラベルを付ける。
	//! @param [in] x 出力バッファのx位置
	//! @param [in] y 出力バッファのy位置
	//! @param [in] z 出力バッファのz位置
	//! @param [in] idx 出力バッファの要素番号
	//! @param [in,out] buf 出力バッファ
	void PutLabelErosion(int x, int y, int z, int idx, unsigned char* buf) const;

	//! @brief Erosion処理を行う
	//! @param [in,out] buf 出力バッファ
	void Erode(unsigned char* buf) const;

	//! @brief 指定したバッファの位置にDilationのラベルを付ける
	//! 指定位置の6近傍を調べ、周囲1/3以上が非空白ならDilationのラベルを付ける。
	//! @param [in] x 出力バッファのx位置
	//! @param [in] y 出力バッファのy位置
	//! @param [in] z 出力バッファのz位置
	//! @param [in] idx 出力バッファの要素番号
	//! @param [in,out] buf 出力バッファ
	void PutLabelDilation(int x, int y, int z, int idx, unsigned char* buf) const;

	//! @brief Dilation処理を行う
	//! @param [in,out] buf 出力バッファ
	void Dilate(unsigned char* buf) const;

	//! @brief 抽出データ内部にできた空洞を埋める
	//! @param [in,out] buf 出力バッファ
	void FillHoles(unsigned char* buf) const;

	//! @brief ノード間を接続する
	//! @param [in] subVolume 出力バッファの領域
	void ConnectNodeToNode(const Boxi& subVolume);

	//! @brief 各ノードとSource及びSinkノードを接続する
	//! @param [in] subVolume 出力バッファの領域
	void ConnectSourceToNodeToSink(const Boxi& subVolume);

	//! @brief ノード間の容量を算出する
	//! @details
	//! 容量 = L * exp( -k * ( I1 - I0 )^2 ) / D \n
	//! I0 : 接続元のピクセルの輝度\n
	//! I1 : 接続先のピクセルの輝度\n
	//! L : 係数 (#m_smoothTermCoeff)\n
	//! k : 係数 (kNodeToNodeCapacityCoefK)\n
	//! D : ピクセル間の距離
	//! @param [in] sourceIntensity 接続元の位置の輝度
	//! @param [in] destIntensity 接続先の位置の輝度
	//! @param [in] dist ノード間の距離
	//! @return 容量
	double ComputeNodeToNodeCapacity(int sourceIntensity, int destIntensity, double dist) const;

	//! @brief ノードとSource及びSinkノード間の容量を算出する
	//! @details 最大容量と最小容量には、
	//! kMaximumEdgeCapacity
	//! と
	//! kMinimumEdgeCapacity
	//! に設定された値を適用する。
	//! @param [in] subVolume 出力バッファの領域
	//! @param [in] x ノードのx位置
	//! @param [in] y ノードのy位置
	//! @param [in] z ノードのz位置
	//! @param [out] sourceCapacity Sourceノード間の容量
	//! @param [out] sinkCapacity Sinkノード間の容量
	void ComputeSourceToNodeToSinkCapacity(const Boxi& subVolume, int x, int y, int z, double& sourceCapacity, double& sinkCapacity) const;

	//! @brief 出力ノード領域の位置を3Dバッファの要素番号に変換する
	//! @param [in] subVolume 出力ノード領域
	//! @param [in] x ノードのx位置
	//! @param [in] y ノードのy位置
	//! @param [in] z ノードのz位置
	//! @return 3Dバッファの要素番号
	int ConvertSubVolumeToImageBufferIndex(const Boxi& subVolume, int x, int y, int z) const;

	//! @brief 出力ノード領域の位置を前景マスクの要素番号に変換する
	//! @param [in] subVolume 出力ノード領域
	//! @param [in] x ノードのx位置
	//! @param [in] y ノードのy位置
	//! @param [in] z ノードのz位置
	//! @param [out] idx 要素番号
	//! @return ノードの位置がマスク内ならtrueを返し、マスク外ならfalseを返す。
	bool ConvertSubVolumeToObjectMaskIndex(const Boxi& subVolume, int x, int y, int z, int& idx) const;

	//! @brief 出力ノード領域の位置を背景マスクの要素番号に変換する
	//! @param [in] subVolume 出力ノード領域
	//! @param [in] x ノードのx位置
	//! @param [in] y ノードのy位置
	//! @param [in] z ノードのz位置
	//! @param [out] idx 要素番号
	//! @return ノードの位置がマスク内ならtrueを返し、マスク外ならfalseを返す。
	bool ConvertSubVolumeToBackgroundMaskIndex(const Boxi& subVolume, int x, int y, int z, int& idx) const;

	//! @brief マスク領域の位置を出力ノード領域の要素番号に変換する
	//! @param [in] x x位置
	//! @param [in] y y位置
	//! @param [in] z z位置
	//! @param [in] maskBBox マスク領域
	//! @param [in] subVolume 出力ノード領域
	//! @return 要素番号
	int ConvertMaskToSubVolumeIndex(int x, int y, int z, const Boxi& maskBBox, const Boxi& subVolume) const;

	//! @brief グラフカット処理対象領域内の最小と最大の輝度をメンバ変数に記録する
	void ComputeMinMaxIntensity();

	//!@brief 輝度値をヒストグラムのビン番号に変換する
	//! @param [in] intensity 輝度
	//! @return ビン番号
	int ComputeBinNumber(short intensity) const;

	//! @brief 尤度ヒストグラムを生成する
	//! @param [in] maskVolume 入力マスクの領域
	//! @param [in] objectMask 入力マスク
	//! @param [out] histogram 出力先のヒストグラム
	//! @return 処理が成功したらtrue。
	bool CreateLikelihoodHistogram(
		const Boxi& maskVolume,
		const unsigned char* objectMask,
		Histogram& histogram);

	Size3i m_imageBufferSize; //!< 3Dバッファの大きさ
	Boxi m_objectBBox; //!< 前景マスクの領域
	Boxi m_backgroundBBox; //!< 背景マスクの領域
	Boxi m_subVolume; //!< 出力バッファの領域(前景と背景の合成領域)

	const short* m_imageBuffer; //!< 3Dバッファのポインタ
	const unsigned char* m_objectMask; //!< 前景マスクのデータ
	const unsigned char* m_backgroundMask; //!< 背景マスクのデータ

	Histogram m_objectHistogram; //!< 前景マスクから作られる尤度ヒストグラム
	Histogram m_backgroundHistogram; //!< 背景マスクから作られる尤度ヒストグラム

	MinMaxIntensity m_intensityMinMax; //!< 出力バッファ領域内の最小と最大輝度

	int m_erosionLevel; //!< 後処理で行うErosionの回数

	int m_sourceVertex; //!< Sourceノードの要素番号
	int m_sinkVertex; //!< Sinkノードの要素番号

	Graph m_graph; //!< グラフデータ

	double m_dataTermCoeff; //!< データ項の係数
	double m_smoothTermCoeff; //!< 平滑化項の係数
	GraphCut::GraphcutNeighborType m_neighborType; //!< ノードの近傍数のタイプ(6, 18又は26)
};

//------------------------------------------------------------------------------
bool GraphCutImpl::Execute(Point3i& subVolumeOrigin, Size3i& subVolumeSize, unsigned char*& outputBuffer)
{
	if (!m_imageBuffer || !m_objectMask || !m_backgroundMask) return false;

	m_subVolume = ComputeCombinedVolume(m_objectBBox, m_backgroundBBox);
	if (m_subVolume.size().prod() == 0) return false;

	ComputeMinMaxIntensity();

	CreateLikelihoodHistogram(m_objectBBox, m_objectMask, m_objectHistogram);

	CreateLikelihoodHistogram(m_backgroundBBox, m_backgroundMask, m_backgroundHistogram);

	unsigned char* buf = CreateOutputBuffer();
	if (!buf) return false; // return on error

	try
	{
		int numMaxNodes = kMaxNumNodesPerCycle;
		int numSlicesPerLoop = ComputeNumberOfSlicesPerLoop(m_subVolume, numMaxNodes, 1);
		int endSlice = m_subVolume.origin().z() + m_subVolume.size().depth();
		Boxi subSubVolume = m_subVolume;
		subSubVolume.size().depth() = numSlicesPerLoop;
		for(;;)
		{
			if (!BuildGraph(subSubVolume)) return false;

			ComputeMaxFlow();

			FillBuffer(subSubVolume, buf);

			if (subSubVolume.origin().z() + subSubVolume.size().depth() == endSlice)
			{
				break;
			}

			subSubVolume.origin().z() += (numSlicesPerLoop - 1); // overlap 1 slice
			if ( subSubVolume.origin().z() + numSlicesPerLoop > endSlice)
			{
				subSubVolume.size().depth() = endSlice - subSubVolume.origin().z();
			}

			m_graph.Reset();
		}
	}
	catch(std::bad_alloc&)
	{
		delete [] buf;
		return false;
	}

	PostProcess(buf);

	outputBuffer = buf;
	subVolumeOrigin = m_subVolume.origin();
	subVolumeSize = m_subVolume.size();

	return true;
}

//------------------------------------------------------------------------------
double GraphCutImpl::ComputeNodeToNodeCapacity(int sourceIntensity, int destIntensity, double dist) const
{
	int diff = sourceIntensity - destIntensity;
	return m_smoothTermCoeff*exp(-kNodeToNodeCapacityCoefK*diff*diff)/dist;
}

//------------------------------------------------------------------------------
void GraphCutImpl::ConnectNodeToNode(const Boxi& subVolume)
{
	if (m_neighborType == GraphCut::kGraphcutNeighborTypeInvalid) return;

	int w0 = m_imageBufferSize.width();
	int h0 = m_imageBufferSize.height();
	int wh0 = w0 * h0;
	int w = subVolume.size().width();
	int h = subVolume.size().height();
	int d = subVolume.size().depth();
	int wh = w*h;

	// loop for 6 neighbors
	// distance = 1.0
	for (int k = 0; k < d; ++k)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				int subVolumeSrcIdx = i + j*w + k*wh;
				int srcIdx = ConvertSubVolumeToImageBufferIndex(subVolume, i, j, k);
				if (k + 1 < d)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + wh;
					int tgtIdx = srcIdx + wh0;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], 1.0);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
				if (j + 1 < h)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + w;
					int tgtIdx = srcIdx + w0;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], 1.0);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
				if (i + 1 < w)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + 1;
					int tgtIdx = srcIdx + 1;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], 1.0);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
			}
		}
	}

	if (m_neighborType == GraphCut::kGraphcutNeighborType6) return;

	// loop for 18 neighbors
	double sqrt2 = sqrt(2.0); // distance from center to voxels at diagonal position
	for (int k = 0; k < d; ++k)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				int subVolumeSrcIdx = i + j*w + k*wh;
				int srcIdx = ConvertSubVolumeToImageBufferIndex(subVolume, i, j, k);
				if (k + 1 < d && i + 1 < w)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + wh + 1;
					int tgtIdx = srcIdx + wh0 + 1;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt2);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
				if (j + 1 < h && k + 1 < d)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + w + wh;
					int tgtIdx = srcIdx + w0 + wh0;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt2);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
				if (i + 1 < w && j + 1 < h)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + 1 + w;
					int tgtIdx = srcIdx + 1 + w0;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt2);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}

				if (k + 1 < d && i - 1 >= 0)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + wh - 1;
					int tgtIdx = srcIdx + wh0 - 1;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt2);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
				if (j + 1 < h && k - 1 >= 0)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + w - wh;
					int tgtIdx = srcIdx + w0 - wh0;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt2);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
				if (i + 1 < w && j - 1 >= 0)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + 1 - w;
					int tgtIdx = srcIdx + 1 - w0;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt2);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
			}
		}
	}

	if (m_neighborType == GraphCut::kGraphcutNeighborType18) return;

	// loop for 26 neighbors
	double sqrt3 = sqrt(3.0); // distance from center to corners of the 3x3x3 box
	for (int k = 0; k < d; ++k)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				int subVolumeSrcIdx = i + j*w + k*wh;
				int srcIdx = ConvertSubVolumeToImageBufferIndex(subVolume, i, j, k);
				if (k + 1 < d && j + 1 < h && i + 1 < w)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + wh + w + 1;
					int tgtIdx = srcIdx + wh0 + w0 + 1;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt3);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
				if (k + 1 < d && j + 1 < h && i - 1 >= 0)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + wh + w - 1;
					int tgtIdx = srcIdx + wh0 + w0 - 1;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt3);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
				if (k + 1 < d && j - 1 >= 0 && i + 1 < w)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + wh - w + 1;
					int tgtIdx = srcIdx + wh0 - w0 + 1;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt3);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
				if (k + 1 < d && j - 1 >= 0 && i - 1 >= 0)
				{
					int subVolumeTgtIdx = subVolumeSrcIdx + wh - w - 1;
					int tgtIdx = srcIdx + wh0 - w0 - 1;
					double cap = ComputeNodeToNodeCapacity(m_imageBuffer[srcIdx], m_imageBuffer[tgtIdx], sqrt3);
					m_graph.AddEdge(subVolumeSrcIdx, subVolumeTgtIdx, cap, cap);
				}
			}
		}
	}
}

//------------------------------------------------------------------------------
void GraphCutImpl::ComputeSourceToNodeToSinkCapacity(
	const Boxi& subVolume,
	int x, int y, int z,
	double& sourceCapacity,
	double& sinkCapacity) const
{
	// object mask
	int objMaskIndex;
	if (ConvertSubVolumeToObjectMaskIndex(subVolume, x, y, z, objMaskIndex) && m_objectMask[objMaskIndex])
	{
		sourceCapacity = kMaximumEdgeCapacity;
		sinkCapacity = kMinimumEdgeCapacity;
		return;
	}

	// background mask
	int bgMaskIndex;
	if (ConvertSubVolumeToBackgroundMaskIndex(subVolume, x, y, z, bgMaskIndex) && m_backgroundMask[bgMaskIndex])
	{
		sourceCapacity = kMinimumEdgeCapacity;
		sinkCapacity = kMaximumEdgeCapacity;
		return;
	}

	// otherwise
	int imageBufferIndex = ConvertSubVolumeToImageBufferIndex(subVolume, x, y, z);
	short intensity = m_imageBuffer[imageBufferIndex];
	int q = ComputeBinNumber(intensity);
	sourceCapacity = m_backgroundHistogram[q];
	sinkCapacity = m_objectHistogram[q];
}

//------------------------------------------------------------------------------
void GraphCutImpl::ConnectSourceToNodeToSink(const Boxi& subVolume)
{
	double srcCapacity = 0.0;
	double sinkCapacity = 0.0;
	int w = subVolume.size().width();
	int h = subVolume.size().height();
	int d = subVolume.size().depth();
	int wh = w*h;
	for (int k = 0; k < d; ++k)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				int nodeIdx = i + j*w + k*wh;
				ComputeSourceToNodeToSinkCapacity(subVolume, i, j, k, srcCapacity, sinkCapacity);
				m_graph.AddTEdge(nodeIdx, srcCapacity, sinkCapacity);
			}
		}
	}
}

//------------------------------------------------------------------------------
bool GraphCutImpl::BuildGraph(const Boxi& subVolume)
{
	m_sourceVertex = subVolume.size().prod();
	m_sinkVertex = subVolume.size().prod() + 1;

	size_t numEdges;
	switch(m_neighborType)
	{
	case GraphCut::kGraphcutNeighborType6:
		numEdges = 6;
		break;
	case GraphCut::kGraphcutNeighborType18:
		numEdges = 18;
		break;
	case GraphCut::kGraphcutNeighborType26:
		numEdges = 26;
		break;
	default:
		return false;
	}

	m_graph.Initialize(subVolume.size().prod()+2, (subVolume.size().prod()+2)*numEdges, m_sourceVertex, m_sinkVertex);

	ConnectNodeToNode(subVolume);

	ConnectSourceToNodeToSink(subVolume);

	return true;
}

//------------------------------------------------------------------------------
void GraphCutImpl::ComputeMaxFlow()
{
	BKMaxFlow maxFlow;
	maxFlow.Execute(&m_graph);
}


//------------------------------------------------------------------------------
unsigned char* GraphCutImpl::CreateOutputBuffer() const
{
	try
	{
		// allocate buffer and copy segmented data from the graph
		unsigned char* buf = new unsigned char[m_subVolume.size().prod()];
		memset(buf, 0, m_subVolume.size().prod() * sizeof(unsigned char) );
		return buf;
	}
  catch (std::bad_alloc&)
  {
		return NULL;
  }
	catch (...)
	{
		return NULL;
	}
}

//------------------------------------------------------------------------------
void GraphCutImpl::FillBuffer(const Boxi& subSubVolume, unsigned char* buf) const
{
	int numVoxelsPerSlice = subSubVolume.size().width() * subSubVolume.size().height();
	int offset = (subSubVolume.origin().z() - m_subVolume.origin().z()) * numVoxelsPerSlice;
	for (int i = 0; i < subSubVolume.size().prod(); ++i)
	{
		if (m_graph.GetNode(i).Group() == Graph::Node::kNodeGroupSource) buf[offset + i] = VOXEL_SEGMENTED;
	}
}

//------------------------------------------------------------------------------
void GraphCutImpl::PostProcess(unsigned char* buf) const
{
	for (int i = 0; i < m_erosionLevel; ++i) Erode(buf);

	for (int k = 0; k < m_objectBBox.size().depth(); ++k)
	{
		for (int j = 0; j < m_objectBBox.size().height(); ++j)
		{
			for (int i = 0; i < m_objectBBox.size().width(); ++i)
			{
				int subVolumeIdx = ConvertMaskToSubVolumeIndex(i, j, k, m_objectBBox, m_subVolume);
				int maskIdx = i + (j + k * m_objectBBox.size().height()) * m_objectBBox.size().width();
				if (m_objectMask[maskIdx] != 0)
				{
					buf[subVolumeIdx] = VOXEL_SEGMENTED;
				}
			}
		}
	}

	for(;;)
	{
		// look up seed point in object mask bbox
		Point3i seedPoint;
		bool allDone = true;
		for (int k = 0; k < m_objectBBox.size().depth(); ++k)
		{
			for (int j = 0; j < m_objectBBox.size().height(); ++j)
			{
				for (int i = 0; i < m_objectBBox.size().width(); ++i)
				{
					int subVolumeIdx = ConvertMaskToSubVolumeIndex(i, j, k, m_objectBBox, m_subVolume);
					int maskIdx = i + (j + k * m_objectBBox.size().height()) * m_objectBBox.size().width();
					if (m_objectMask[maskIdx] != 0 && buf[subVolumeIdx] == VOXEL_SEGMENTED)
					{
						seedPoint.x() = i;
						seedPoint.y() = j;
						seedPoint.z() = k;
						seedPoint += m_objectBBox.origin();
						seedPoint -= m_subVolume.origin();
						allDone = false;
						goto seedPointFound;
					}
				}
			}
		}

seedPointFound:
		if (allDone) break;

		// fill masks connected to the object mask
		ScanLineFill scanLineFill;
		scanLineFill.Fill(seedPoint, VOXEL_SEGMENTED, VOXEL_CONNECTED, m_subVolume.size(), buf);
	}

	// erase non-connected masks
	for (int i = 0; i < m_subVolume.size().prod(); ++i)
	{
		if (buf[i] != VOXEL_CONNECTED) buf[i] = VOXEL_EMPTY;
	}

	for (int i = 0; i < m_erosionLevel; ++i) Dilate(buf);

	// fill holes inside of the segmented body
	FillHoles(buf);

	// mark as empty at background masks
	for (int k = 0; k < m_backgroundBBox.size().depth(); ++k)
	{
		for (int j = 0; j < m_backgroundBBox.size().height(); ++j)
		{
			for (int i = 0; i < m_backgroundBBox.size().width(); ++i)
			{
				int subVolumeIdx = ConvertMaskToSubVolumeIndex(i, j, k, m_backgroundBBox, m_subVolume);
				int maskIdx = i + (j + k * m_backgroundBBox.size().height()) * m_backgroundBBox.size().width();
				if (m_backgroundMask[maskIdx] != 0)
				{
					buf[subVolumeIdx] = VOXEL_EMPTY;
				}
			}
		}
	}
}

//------------------------------------------------------------------------------
void GraphCutImpl::PutLabelErosion(int x, int y, int z, int idx, unsigned char* buf) const
{
	int count = 0;
	int totalCount = 0;
	const int& w = m_subVolume.size().width();
	const int& h = m_subVolume.size().height();
	const int& d = m_subVolume.size().depth();
	int wh = w*h;
	// 6 neighbor search
	if (x - 1 >= 0) { ++totalCount; if (buf[idx - 1] == 0) ++count; }
	if (x + 1 < w) { ++totalCount; if (buf[idx + 1] == 0) ++count; }
	if (y - 1 >= 0) { ++totalCount; if (buf[idx - w] == 0) ++count; }
	if (y + 1 < h) { ++totalCount; if (buf[idx + w] == 0) ++count; }
	if (z - 1 >= 0) { ++totalCount; if (buf[idx - wh] == 0) ++count; }
	if (z + 1 < d) { ++totalCount; if (buf[idx + wh] == 0) ++count; }
	if (count >= kCountThreshold[totalCount]) buf[idx] = kLabelErosion;
}

//------------------------------------------------------------------------------
void GraphCutImpl::Erode(unsigned char* buf) const
{
	for (int k = 0; k < m_subVolume.size().depth(); ++k)
	{
		for (int j = 0; j < m_subVolume.size().height(); ++j)
		{
			for (int i = 0; i < m_subVolume.size().width(); ++i)
			{
				int idx = i + (j + k * m_subVolume.size().height()) * m_subVolume.size().width();
				if (buf[idx] != 0)
				{
					PutLabelErosion(i, j, k, idx, buf);
				}
			}
		}
	}

	for (int i = 0; i < m_subVolume.size().prod(); ++i)
	{
		if (buf[i] == kLabelErosion) buf[i] = 0;
	}
}

//------------------------------------------------------------------------------
void GraphCutImpl::PutLabelDilation(int x, int y, int z, int idx, unsigned char* buf) const
{
	int count = 0;
	int totalCount = 0;
	const int& w = m_subVolume.size().width();
	const int& h = m_subVolume.size().height();
	const int& d = m_subVolume.size().depth();
	int wh = w*h;
	// 6 neighbor search
	if (x - 1 >= 0) { ++totalCount; if (buf[idx - 1] != kLabelDilation && buf[idx - 1] != 0) ++count; }
	if (x + 1 < w) { ++totalCount; if (buf[idx + 1] != kLabelDilation && buf[idx + 1] != 0) ++count; }
	if (y - 1 >= 0) { ++totalCount; if (buf[idx - w] != kLabelDilation && buf[idx - w] != 0) ++count; }
	if (y + 1 < h) { ++totalCount; if (buf[idx + w] != kLabelDilation && buf[idx + w] != 0) ++count; }
	if (z - 1 >= 0) { ++totalCount; if (buf[idx - wh] != kLabelDilation && buf[idx - wh] != 0) ++count; }
	if (z + 1 < d) { ++totalCount; if (buf[idx + wh] != kLabelDilation && buf[idx + wh] != 0) ++count; }
	if (count >= kCountThreshold[totalCount]) buf[idx] = kLabelDilation;
}

//------------------------------------------------------------------------------
void GraphCutImpl::Dilate(unsigned char* buf) const
{
	for (int k = 0; k < m_subVolume.size().depth(); ++k)
	{
		for (int j = 0; j < m_subVolume.size().height(); ++j)
		{
			for (int i = 0; i < m_subVolume.size().width(); ++i)
			{
				int idx = i + (j + k * m_subVolume.size().height()) * m_subVolume.size().width();
				if (buf[idx] == 0)
				{
					PutLabelDilation(i, j, k, idx, buf);
				}
			}
		}
	}

	for (int i = 0; i < m_subVolume.size().prod(); ++i)
	{
		if (buf[i] == kLabelDilation) buf[i] = 1;
	}
}

//------------------------------------------------------------------------------
void GraphCutImpl::FillHoles(unsigned char* buf) const
{
	ScanLineFill scanLineFill;
	int w = m_subVolume.size().width();
	int h = m_subVolume.size().height();
	int d = m_subVolume.size().depth();

	// scan empty voxel on top and bottom (x-y) of the buffer
	if (d > 1)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				int topIdx = i + j * w;
				if (buf[topIdx] == VOXEL_EMPTY)
				{
					Point3i seedPoint(i, j, 0);
					scanLineFill.Fill(seedPoint, VOXEL_EMPTY, VOXEL_EXTERIOR, m_subVolume.size(), buf);
				}

				int bottomIdx = topIdx + (d-1)*w*h;
				if (buf[bottomIdx] == VOXEL_EMPTY)
				{
					Point3i seedPoint(i, j, d-1);
					scanLineFill.Fill(seedPoint, VOXEL_EMPTY, VOXEL_EXTERIOR, m_subVolume.size(), buf);
				}
			}
		}
	}

	// scan empty voxel on side (y-z) of the buffer
	if (w > 1)
	{
		for (int k = 0; k < d; ++k)
		{
			for (int j = 0; j < h; ++j)
			{
				int lowIdx = j * w + k*w*h;
				if (buf[lowIdx] == VOXEL_EMPTY)
				{
					Point3i seedPoint(0, j, k);
					scanLineFill.Fill(seedPoint, VOXEL_EMPTY, VOXEL_EXTERIOR, m_subVolume.size(), buf);
				}

				int highIdx = lowIdx + (w-1);
				if (buf[highIdx] == VOXEL_EMPTY)
				{
					Point3i seedPoint(w-1, j, k);
					scanLineFill.Fill(seedPoint, VOXEL_EMPTY, VOXEL_EXTERIOR, m_subVolume.size(), buf);
				}
			}
		}
	}

	// scan empty voxel on side (z-x) of the buffer
	if (h > 1)
	{
		for (int i = 0; i < w; ++i)
		{
			for (int k = 0; k < d; ++k)
			{
				int lowIdx = i + k*w*h;
				if (buf[lowIdx] == VOXEL_EMPTY)
				{
					Point3i seedPoint(i, 0, k);
					scanLineFill.Fill(seedPoint, VOXEL_EMPTY, VOXEL_EXTERIOR, m_subVolume.size(), buf);
				}

				int highIdx = lowIdx + (h-1) * w;
				if (buf[highIdx] == VOXEL_EMPTY)
				{
					Point3i seedPoint(i, h-1, k);
					scanLineFill.Fill(seedPoint, VOXEL_EMPTY, VOXEL_EXTERIOR, m_subVolume.size(), buf);
				}
			}
		}
	}

	// for all non-exterior voxels, mark them as connected.
	for (int i = 0; i < m_subVolume.size().prod(); ++i)
	{
		if (buf[i] == VOXEL_EXTERIOR)
		{
			buf[i] = VOXEL_EMPTY;
		}
		else
		{
			buf[i] = VOXEL_CONNECTED;
		}
	}
}

//------------------------------------------------------------------------------
int GraphCutImpl::ConvertSubVolumeToImageBufferIndex(const Boxi& subVolume, int x, int y, int z) const
{
	return (subVolume.origin().x() + x) +
	(subVolume.origin().y() + y) * m_imageBufferSize.width() +
	(subVolume.origin().z() + z) * m_imageBufferSize.height()*m_imageBufferSize.width();
}

//------------------------------------------------------------------------------
bool GraphCutImpl::ConvertSubVolumeToObjectMaskIndex(const Boxi& subVolume, int x, int y, int z, int& idx ) const
{
	Point3i offset = subVolume.origin() - m_objectBBox.origin();
	x += offset.x();
	y += offset.y();
	z += offset.z();

	if (!IsInBox(x, y, z, m_objectBBox)) return false;

	idx = x + y * m_objectBBox.size().width() + z * m_objectBBox.size().width() * m_objectBBox.size().height();

	return true;
}

//------------------------------------------------------------------------------
bool GraphCutImpl::ConvertSubVolumeToBackgroundMaskIndex(const Boxi& subVolume, int x, int y, int z, int& idx) const
{
	Point3i offset = subVolume.origin() - m_backgroundBBox.origin();
	x += offset.x();
	y += offset.y();
	z += offset.z();

	if (!IsInBox(x, y, z, m_backgroundBBox)) return false;

	idx = x + y * m_backgroundBBox.size().width() + z * m_backgroundBBox.size().width() * m_backgroundBBox.size().height();

	return true;
}

//------------------------------------------------------------------------------
int GraphCutImpl::ConvertMaskToSubVolumeIndex(int x, int y, int z, const Boxi& maskBBox, const Boxi& subVolume) const
{
	x += maskBBox.origin().x();
	y += maskBBox.origin().y();
	z += maskBBox.origin().z();
	x -= subVolume.origin().x();
	y -= subVolume.origin().y();
	z -= subVolume.origin().z();
	return x + (y + z * subVolume.size().height()) * subVolume.size().width();
}

//------------------------------------------------------------------------------
void GraphCutImpl::ComputeMinMaxIntensity()
{
	short minIntensity = SHRT_MAX; // 32768
	short maxIntensity = SHRT_MIN; // -32768
	int w = m_subVolume.size().width();
	int h = m_subVolume.size().height();
	int d = m_subVolume.size().depth();
	for (int k = 0; k < d; ++k)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				int idx = ConvertSubVolumeToImageBufferIndex(m_subVolume, i, j, k);
				short intensity = m_imageBuffer[idx];
				if (intensity < minIntensity) minIntensity = intensity;
				else if (intensity > maxIntensity) maxIntensity = intensity;
			}
		}
	}

	m_intensityMinMax.first = minIntensity;
	m_intensityMinMax.second = maxIntensity;
}

//------------------------------------------------------------------------------
int GraphCutImpl::ComputeBinNumber(short intensity) const
{
	return static_cast<int>( (kNumberOfBins - 1) * static_cast<double>(intensity - m_intensityMinMax.first)/(m_intensityMinMax.second - m_intensityMinMax.first));
}

//------------------------------------------------------------------------------
bool GraphCutImpl::CreateLikelihoodHistogram(
															 const Boxi& maskVolume,
															 const unsigned char* objectMask,
															 Histogram& histogram)
{
	Histogram tempHistogram(kNumberOfBins, 0.1); // 100 bins filled with 0.1

	int x0 = maskVolume.origin().x();
	int y0 = maskVolume.origin().y();
	int z0 = maskVolume.origin().z();
	int w = maskVolume.size().width();
	int h = maskVolume.size().height();
	int d = maskVolume.size().depth();
	int wh = w*h;
	int w0 = m_imageBufferSize.width();
	int h0 = m_imageBufferSize.height();
	int wh0 = w0 * h0;
	for (int k = 0; k < d; ++k)
	{
		for (int j = 0; j < h; ++j)
		{
			for (int i = 0; i < w; ++i)
			{
				int idx = i + j * w + k * wh;
				if (objectMask[idx] > 0)
				{
					int targetIdx = (i + x0) + (j + y0) * w0 + (k + z0) * wh0;
					short intensity = m_imageBuffer[targetIdx];
					int binNumber = ComputeBinNumber(intensity);
					tempHistogram[binNumber] += 1.0;
				}
			}
		}
	}

	double sum = std::accumulate(tempHistogram.begin(), tempHistogram.end(), 0.0);
	Histogram::iterator itr = tempHistogram.begin();
	for (; itr != tempHistogram.end(); ++itr)
	{
		*itr = -log( (*itr)/sum )*m_dataTermCoeff;
	}

	histogram = tempHistogram;

	return true;
}

//==============================================================================
GraphCut::GraphCut(void)
: m_pImpl(new GraphCutImpl())
{
}

//------------------------------------------------------------------------------
GraphCut::~GraphCut(void)
{
	if (m_pImpl) delete m_pImpl;
}

//------------------------------------------------------------------------------
//! @param [in] objectBBox マスクの領域
//! @param [in] objectMask マスクデータ
void GraphCut::SetObjectMask( const Boxi& objectBBox, const unsigned char* objectMask )
{
	m_pImpl->SetObjectMask(objectBBox, objectMask);
}

//------------------------------------------------------------------------------
//! @param [in] backgroundBBox マスク領域
//! @param [in] backgroundMask マスクデータ
void GraphCut::SetBackgroundMask( const Boxi& backgroundBBox, const unsigned char* backgroundMask )
{
	m_pImpl->SetBackgroundMask(backgroundBBox, backgroundMask);
}

//------------------------------------------------------------------------------
//! @param [in] srcImageBBox 3Dバッファの大きさ
//! @param [in] srcImage 3Dバッファのポインタ
void GraphCut::SetSourceImage( const Size3i& srcImageBBox, const short* srcImage )
{
	m_pImpl->SetSourceImage(srcImageBBox, srcImage);
}

//------------------------------------------------------------------------------
//! グラフカット処理内で出力バッファが確保され返される。
//! このメモリ領域は受けた側で解放する必要がある。
//! @param [out] subVolumeOrigin 出力バッファの領域の原点位置
//! @param [out] subVolumeSize 出力バッファの領域の大きさ(幅、高さ、深さ)
//! @param [out] outputBuffer 出力バッファ
bool GraphCut::Execute(Point3i& subVolumeOrigin, Size3i& subVolumeSize, unsigned char*& outputBuffer)
{
	return m_pImpl->Execute(subVolumeOrigin, subVolumeSize, outputBuffer);
}

//------------------------------------------------------------------------------
//! @return 最小輝度
short GraphCut::GetMinIntensity() const
{
	return m_pImpl->GetMinIntensity();
}

//------------------------------------------------------------------------------
//! @return 最大輝度
short GraphCut::GetMaxIntensity() const
{
	return m_pImpl->GetMaxIntensity();
}

//------------------------------------------------------------------------------
//! @param [in] n Erosionを行う回数
void GraphCut::SetErosionLevelInPostProcess( int n )
{
	m_pImpl->SetErosionLevelInPostProcess(n);
}

//------------------------------------------------------------------------------
void GraphCut::SetNeighborType( GraphcutNeighborType neighborType )
{
	m_pImpl->SetNeighborType(neighborType);
}

//------------------------------------------------------------------------------
void GraphCut::SetDataTermCoeff( double coeff )
{
	m_pImpl->SetDataTermCoeff(coeff);
}

//------------------------------------------------------------------------------
void GraphCut::SetSmoothTermCoeff( double coeff )
{
	m_pImpl->SetSmoothTermCoeff(coeff);
}

} // namespace segmentation
} // namespace lssplib

