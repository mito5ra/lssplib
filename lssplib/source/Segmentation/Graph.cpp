#include "Segmentation/Graph.h"

#include <assert.h>
#include <cmath>

namespace lssplib
{
namespace segmentation
{

const static double kTolerance = 1e-8;

//-------------------------------------------------------------------
//------ Node
//-------------------------------------------------------------------
Graph::Node::Node()
: m_edge(NULL)
, m_edgeToParent(NULL)
, m_nodeGroup(kNodeGroupFree)
{}

//-------------------------------------------------------------------
Graph::Node::Node(const Node& src)
{
	*this = src;
}

//-------------------------------------------------------------------
Graph::Node::~Node() {}

//-------------------------------------------------------------------
Graph::Node& Graph::Node::operator=(const Node& src)
{
	m_edge = src.m_edge;
	m_edgeToParent = src.m_edgeToParent;
	m_nodeGroup = src.m_nodeGroup;

	return *this;
}

//-------------------------------------------------------------------
//! @param [in] q 隣接するノード
//! @return 始点が自身で終点がqとなる辺。見つからない場合はNULL。
Graph::Edge* Graph::Node::EdgeTo(const Graph::Node* q)
{
	Edge* e = FirstEdge();
	while (e && e->To() != q)
	{
		e = e->Next();
	}

	return e;
}

//-------------------------------------------------------------------
//! @param [in] q 隣接するノード
//! @return 始点がqで終点が自身となる辺。見つからない場合はNULL。
Graph::Edge* Graph::Node::EdgeFrom(const Graph::Node* q)
{
	Edge* e = EdgeTo(q);
	if (e)
	{
		return e->Reverse();
	}

	return NULL;
}

//-------------------------------------------------------------------
//------ Edge
//-------------------------------------------------------------------
Graph::Edge::Edge()
: m_capacity(0.0)
, m_nodeFrom(NULL)
, m_nodeTo(NULL)
, m_edgeReverse(NULL)
, m_edgeNext(NULL) {}

//-------------------------------------------------------------------
//! @param [in] from 始点ノード
//! @param [in] to 終点ノード
Graph::Edge::Edge(Node* from, Node* to)
: m_capacity(0.0)
, m_nodeFrom(from)
, m_nodeTo(to)
, m_edgeReverse(NULL)
, m_edgeNext(NULL) {}

//-------------------------------------------------------------------
Graph::Edge::Edge(const Edge& src)
{
	*this = src;
}

//-------------------------------------------------------------------
Graph::Edge::~Edge() {}

//-------------------------------------------------------------------
Graph::Edge& Graph::Edge::operator=(const Edge& src)
{
	m_capacity = src.m_capacity;
	m_nodeFrom = src.m_nodeFrom;
	m_nodeTo = src.m_nodeTo;
	m_edgeReverse = src.m_edgeReverse;
	m_edgeNext = src.m_edgeNext;
	return *this;
}

//-------------------------------------------------------------------
//------ Graph
//-------------------------------------------------------------------
Graph::Graph()
: m_nodeList()
, m_edgeList()
, m_sourceIdx(0)
, m_sinkIdx(0) {}

//-------------------------------------------------------------------
//! @details 必要なノードの数と辺の数を指定し、メモリを確保する。
//! @param [in] numNodes グラフ内のノード数
//! @param [in] numEdges グラフ生成に必要な辺の数
//! @param [in] srcIdx Sourceノードの配列要素番号
//! @param [in] sinkIdx Sinkノードの配列要素番号
Graph::Graph(size_t numNodes, size_t numEdges, size_t srcIdx, size_t sinkIdx)
: m_nodeList()
, m_edgeList()
, m_sourceIdx(0)
, m_sinkIdx(0)
{
	Initialize(numNodes, numEdges, srcIdx, sinkIdx);
}

//-------------------------------------------------------------------
Graph::Graph(const Graph& src)
: m_nodeList(src.m_nodeList)
, m_edgeList(src.m_edgeList)
, m_sourceIdx(src.m_sourceIdx)
, m_sinkIdx(src.m_sinkIdx)
{
}

//-------------------------------------------------------------------
Graph::~Graph()
{
	Reset();
}

//-------------------------------------------------------------------
//! @details 必要なノードの数と辺の数を指定し、メモリを確保する。
//! @param [in] numNodes グラフ内のノード数
//! @param [in] numEdges グラフ生成に必要な辺の数
//! @param [in] srcIdx Sourceノードの配列要素番号
//! @param [in] sinkIdx Sinkノードの配列要素番号
void Graph::Initialize(size_t numNodes, size_t numEdges, size_t srcIdx, size_t sinkIdx)
{
	Reset();

	m_nodeList.resize(numNodes); // allocate nodes

	m_edgeList.reserve(numEdges); // reserve memory for edges

	m_sourceIdx = srcIdx;

	m_sinkIdx = sinkIdx;
}

//-------------------------------------------------------------------
//! @details ノードと辺の配列を空にし、SourceとSinkノードの配列要素番号を0にする。
void Graph::Reset()
{
	NodeList().swap(m_nodeList);
	EdgeList().swap(m_edgeList);
	m_sourceIdx = 0;
	m_sinkIdx = 0;
}

//-------------------------------------------------------------------
//! @param [in] indexFrom 辺の始点ノードの配列要素番号
//! @param [in] indexTo 辺の終点ノードの配列要素番号
//! @param [in] capacity 辺の最大流量
//! @param [in] revCapacity 逆平行辺の最大流量
void Graph::AddEdge(size_t indexFrom, size_t indexTo, double capacity, double revCapacity)
{
	assert(m_edgeList.size() < m_edgeList.capacity());
	if (m_edgeList.size() >= m_edgeList.capacity()) return;

	capacity = (capacity < kTolerance) ? 0.0 : capacity;
	revCapacity = (revCapacity < kTolerance) ? 0.0 : revCapacity;
	if (capacity == 0.0 && revCapacity == 0.0) return;

	Node* nodeFrom = &m_nodeList[indexFrom];
	Node* nodeTo = &m_nodeList[indexTo];

	m_edgeList.push_back(Edge(nodeFrom, nodeTo));
	Edge* edge = &m_edgeList.back();
	m_edgeList.push_back(Edge(nodeTo, nodeFrom));
	Edge* revEdge = &m_edgeList.back();

	edge->SetReverse(revEdge);
	edge->SetCapacity(capacity);
	edge->SetNext(nodeFrom->FirstEdge());
	nodeFrom->SetFirstEdge(edge);

	revEdge->SetReverse(edge);
	revEdge->SetCapacity(revCapacity);
	revEdge->SetNext(nodeTo->FirstEdge());
	nodeTo->SetFirstEdge(revEdge);
}

//-------------------------------------------------------------------
//! @param [in] index ノードの配列要素番号
//! @param [in] sourceCapacity Sourceノードから指定ノードへの最大流量
//! @param [in] sinkCapacity 指定ノードからSinkノードへの最大流量
void Graph::AddTEdge(size_t index, double sourceCapacity, double sinkCapacity)
{
	if (sourceCapacity > sinkCapacity)
	{
		AddEdge(m_sourceIdx, index, sourceCapacity-sinkCapacity, 0.0);
	}
	else if (sourceCapacity < sinkCapacity)
	{
		AddEdge(index, m_sinkIdx, sinkCapacity-sourceCapacity, 0.0);
	}
}

} // namespace segmentation
} // namespace lssplib

