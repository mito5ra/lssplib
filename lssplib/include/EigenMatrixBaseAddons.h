EIGEN_STRONG_INLINE CoeffReturnType width() const { return (*this)[0]; }
EIGEN_STRONG_INLINE CoeffReturnType height() const { return (*this)[1]; }
EIGEN_STRONG_INLINE CoeffReturnType depth() const { return (*this)[2]; }
EIGEN_STRONG_INLINE CoeffReturnType fourth() const { return (*this)[3]; }

EIGEN_STRONG_INLINE Scalar& width() { return (*this)[0]; }
EIGEN_STRONG_INLINE Scalar& height() { return (*this)[1]; }
EIGEN_STRONG_INLINE Scalar& depth() { return (*this)[2]; }
EIGEN_STRONG_INLINE Scalar& fourth() { return (*this)[3]; }

inline ColXpr xaxis() { return ColXpr(derived(), 0); }
inline const ColXpr xaxis() const { return ColXpr(derived(), 0); }
inline ColXpr yaxis() { return ColXpr(derived(), 1); }
inline const ColXpr yaxis() const { return ColXpr(derived(), 1); }
inline ColXpr zaxis() { return ColXpr(derived(), 2); }
inline const ColXpr zaxis() const { return ColXpr(derived(), 2); }
inline ColXpr waxis() { return ColXpr(derived(), 3); }
inline const ColXpr waxis() const { return ColXpr(derived(), 3); }