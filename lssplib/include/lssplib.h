// 以下の ifdef ブロックは DLL からのエクスポートを容易にするマクロを作成するための 
// 一般的な方法です。この DLL 内のすべてのファイルは、コマンド ラインで定義された LSSPLIB_EXPORTS
// シンボルでコンパイルされます。このシンボルは、この DLL を使うプロジェクトで定義することはできません。
// ソースファイルがこのファイルを含んでいる他のプロジェクトは、 
// LSSPLIB_API 関数を DLL からインポートされたと見なすのに対し、この DLL は、このマクロで定義された
// シンボルをエクスポートされたと見なします。
#ifndef LSSPLIB_STATIC
#ifdef LSSPLIB_EXPORTS
#define LSSPLIB_API __declspec(dllexport)
#else
#define LSSPLIB_API __declspec(dllimport)
#endif
#else
#define LSSPLIB_API
#endif

