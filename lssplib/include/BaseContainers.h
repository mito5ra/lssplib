#pragma once

#undef EIGEN_MATRIXBASE_PLUGIN
#define EIGEN_MATRIXBASE_PLUGIN "EigenMatrixBaseAddons.h"

#include <Eigen/Core>
#include <Eigen/Dense>
#undef EIGEN_MATRIXBASE_PLUGIN

/*-------------------------------------------------------------------------*/
// Matrix, Vector and RowVector
#define MAKE_TYPEDEFS(Type, TypeSuffix, Size, SizeSuffix) \
  typedef Eigen::Matrix<Type, Size, Size, Eigen::DontAlign> Matrix##SizeSuffix##TypeSuffix; \
  typedef Eigen::Matrix<Type, Size,    1, Eigen::DontAlign> Vector##SizeSuffix##TypeSuffix; \
  typedef Eigen::Matrix<Type,    1, Size, Eigen::DontAlign> RowVector##SizeSuffix##TypeSuffix; \

#define MAKE_TYPEDEFS_ALIGNED(Type, TypeSuffix, Size, SizeSuffix) \
  typedef Eigen::Matrix<Type, Size, Size> Matrix##SizeSuffix##TypeSuffix##a; \
  typedef Eigen::Matrix<Type, Size,    1> Vector##SizeSuffix##TypeSuffix##a; \
  typedef Eigen::Matrix<Type,    1, Size> RowVector##SizeSuffix##TypeSuffix##a; \

#define MAKE_TYPEDEFS_ALL_SIZES(Type, TypeSuffix) \
  MAKE_TYPEDEFS(Type, TypeSuffix,              2, 2) \
  MAKE_TYPEDEFS(Type, TypeSuffix,              3, 3) \
  MAKE_TYPEDEFS(Type, TypeSuffix,              4, 4) \
  MAKE_TYPEDEFS(Type, TypeSuffix, Eigen::Dynamic, X) \
  MAKE_TYPEDEFS_ALIGNED(Type, TypeSuffix,              2, 2) \
  MAKE_TYPEDEFS_ALIGNED(Type, TypeSuffix,              3, 3) \
  MAKE_TYPEDEFS_ALIGNED(Type, TypeSuffix,              4, 4) \
  MAKE_TYPEDEFS_ALIGNED(Type, TypeSuffix, Eigen::Dynamic, X) \

MAKE_TYPEDEFS_ALL_SIZES(int,                  i)
MAKE_TYPEDEFS_ALL_SIZES(float,                f)
MAKE_TYPEDEFS_ALL_SIZES(double,               d)
MAKE_TYPEDEFS_ALL_SIZES(std::complex<float>,  cf)
MAKE_TYPEDEFS_ALL_SIZES(std::complex<double>, cd)

#undef MAKE_TYPEDEFS_ALL_SIZES
#undef MAKE_TYPEDEFS

/*-------------------------------------------------------------------------*/
// Point
typedef Eigen::Matrix<short,  2,              1, Eigen::DontAlign> Point2s;
typedef Eigen::Matrix<short,  3,              1, Eigen::DontAlign> Point3s;
typedef Eigen::Matrix<short,  4,              1, Eigen::DontAlign> Point4s;
typedef Eigen::Matrix<short,  Eigen::Dynamic, 1> PointXs;
typedef Eigen::Matrix<int,    2,              1, Eigen::DontAlign> Point2i;
typedef Eigen::Matrix<int,    3,              1, Eigen::DontAlign> Point3i;
typedef Eigen::Matrix<int,    4,              1, Eigen::DontAlign> Point4i;
typedef Eigen::Matrix<int,    Eigen::Dynamic, 1> PointXi;
typedef Eigen::Matrix<float,  2,              1, Eigen::DontAlign> Point2f;
typedef Eigen::Matrix<float,  3,              1, Eigen::DontAlign> Point3f;
typedef Eigen::Matrix<float,  4,              1, Eigen::DontAlign> Point4f;
typedef Eigen::Matrix<float,  Eigen::Dynamic, 1> PointXf;
typedef Eigen::Matrix<double, 2,              1, Eigen::DontAlign> Point2d;
typedef Eigen::Matrix<double, 3,              1, Eigen::DontAlign> Point3d;
typedef Eigen::Matrix<double, 4,              1, Eigen::DontAlign> Point4d;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> PointXd;
// Point alinged
typedef Eigen::Matrix<int,    2,              1> Point2ia;
typedef Eigen::Matrix<int,    3,              1> Point3ia;
typedef Eigen::Matrix<int,    4,              1> Point4ia;
typedef Eigen::Matrix<float,  2,              1> Point2fa;
typedef Eigen::Matrix<float,  3,              1> Point3fa;
typedef Eigen::Matrix<float,  4,              1> Point4fa;
typedef Eigen::Matrix<double, 2,              1> Point2da;
typedef Eigen::Matrix<double, 3,              1> Point3da;
typedef Eigen::Matrix<double, 4,              1> Point4da;

/*-------------------------------------------------------------------------*/
// Size
typedef Eigen::Matrix<int,    2,              1, Eigen::DontAlign> Size2i;
typedef Eigen::Matrix<int,    3,              1, Eigen::DontAlign> Size3i;
typedef Eigen::Matrix<int,    4,              1, Eigen::DontAlign> Size4i;
typedef Eigen::Matrix<int,    Eigen::Dynamic, 1> SizeXi;
typedef Eigen::Matrix<float,  2,              1, Eigen::DontAlign> Size2f;
typedef Eigen::Matrix<float,  3,              1, Eigen::DontAlign> Size3f;
typedef Eigen::Matrix<float,  4,              1, Eigen::DontAlign> Size4f;
typedef Eigen::Matrix<float,  Eigen::Dynamic, 1> SizeXf;
typedef Eigen::Matrix<double, 2,              1, Eigen::DontAlign> Size2d;
typedef Eigen::Matrix<double, 3,              1, Eigen::DontAlign> Size3d;
typedef Eigen::Matrix<double, 4,              1, Eigen::DontAlign> Size4d;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> SizeXd;
// Size alinged
typedef Eigen::Matrix<int,    2,              1> Size2ia;
typedef Eigen::Matrix<int,    3,              1> Size3ia;
typedef Eigen::Matrix<int,    4,              1> Size4ia;
typedef Eigen::Matrix<float,  2,              1> Size2fa;
typedef Eigen::Matrix<float,  3,              1> Size3fa;
typedef Eigen::Matrix<float,  4,              1> Size4fa;
typedef Eigen::Matrix<double, 2,              1> Size2da;
typedef Eigen::Matrix<double, 3,              1> Size3da;
typedef Eigen::Matrix<double, 4,              1> Size4da;

/*-------------------------------------------------------------------------*/
// Basis
typedef Eigen::Matrix<int,                 2,              2, Eigen::DontAlign> Basis2i;
typedef Eigen::Matrix<int,                 3,              3, Eigen::DontAlign> Basis3i;
typedef Eigen::Matrix<int,                 4,              4, Eigen::DontAlign> Basis4i;
typedef Eigen::Matrix<int,    Eigen::Dynamic, Eigen::Dynamic> BasisXi;
typedef Eigen::Matrix<float,               2,              2, Eigen::DontAlign> Basis2f;
typedef Eigen::Matrix<float,               3,              3, Eigen::DontAlign> Basis3f;
typedef Eigen::Matrix<float,               4,              4, Eigen::DontAlign> Basis4f;
typedef Eigen::Matrix<float,  Eigen::Dynamic, Eigen::Dynamic> BasisXf;
typedef Eigen::Matrix<double,              2,              2, Eigen::DontAlign> Basis2d;
typedef Eigen::Matrix<double,              3,              3, Eigen::DontAlign> Basis3d;
typedef Eigen::Matrix<double,              4,              4, Eigen::DontAlign> Basis4d;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> BasisXd;
// Basis aligned
typedef Eigen::Matrix<int,                 2,              2> Basis2ia;
typedef Eigen::Matrix<int,                 3,              3> Basis3ia;
typedef Eigen::Matrix<int,                 4,              4> Basis4ia;
typedef Eigen::Matrix<float,               2,              2> Basis2fa;
typedef Eigen::Matrix<float,               3,              3> Basis3fa;
typedef Eigen::Matrix<float,               4,              4> Basis4fa;
typedef Eigen::Matrix<double,              2,              2> Basis2da;
typedef Eigen::Matrix<double,              3,              3> Basis3da;
typedef Eigen::Matrix<double,              4,              4> Basis4da;

/*-------------------------------------------------------------------------*/
// Coordinate
template<typename _Type, int _Dimensions, int _Options = Eigen::DontAlign>
class __declspec(align(16)) Coordinate
{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef Eigen::Matrix<double, _Dimensions, _Dimensions, _Options> Basis;
  typedef Eigen::Matrix<_Type,  _Dimensions,           1, _Options> Point;

  Coordinate(): m_basis(), m_origin() {}
  ~Coordinate() {}

  int dimensions() const { return _Dimensions; }

  Point& origin() { return m_origin; }
  const Point& origin() const { return m_origin; }
  Basis& basis() { return m_basis; }
  const Basis& basis() const { return m_basis; }
  
  void reset() {
    m_basis.setIdentity(_Dimensions, _Dimensions);
    m_origin.setZero(_Dimensions, 1);
  }

private:
  Basis m_basis;
  Point m_origin;
};
// unaligned
typedef Coordinate<int,    2> Coordinate2i;
typedef Coordinate<float,  2> Coordinate2f;
typedef Coordinate<double, 2> Coordinate2d;
typedef Coordinate<int,    3> Coordinate3i;
typedef Coordinate<float,  3> Coordinate3f;
typedef Coordinate<double, 3> Coordinate3d;
typedef Coordinate<int,    4> Coordinate4i;
typedef Coordinate<float,  4> Coordinate4f;
typedef Coordinate<double, 4> Coordinate4d;
// aligned
typedef Coordinate<int,    2, Eigen::Aligned> Coordinate2ia;
typedef Coordinate<float,  2, Eigen::Aligned> Coordinate2fa;
typedef Coordinate<double, 2, Eigen::Aligned> Coordinate2da;
typedef Coordinate<int,    3, Eigen::Aligned> Coordinate3ia;
typedef Coordinate<float,  3, Eigen::Aligned> Coordinate3fa;
typedef Coordinate<double, 3, Eigen::Aligned> Coordinate3da;
typedef Coordinate<int,    4, Eigen::Aligned> Coordinate4ia;
typedef Coordinate<float,  4, Eigen::Aligned> Coordinate4fa;
typedef Coordinate<double, 4, Eigen::Aligned> Coordinate4da;

/*-------------------------------------------------------------------------*/
// OrthotopeBase
template<typename _Type, int _Dimensions, int _Options = Eigen::DontAlign>
class __declspec(align(16)) OrthotopeBase
{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
  typedef Eigen::Matrix<_Type, _Dimensions, 1, _Options> Point;
  typedef Eigen::Matrix<_Type, _Dimensions, 1, _Options> Size;

  OrthotopeBase(): m_origin(), m_size() {}
  OrthotopeBase(const Point& src_origin, const Size& src_size):
    m_origin(src_origin), m_size(src_size) {}
  ~OrthotopeBase() {}

  bool operator==(const OrthotopeBase<_Type, _Dimensions, Eigen::DontAlign>& src) const {
    return (m_origin == src.m_origin && m_size == src.m_size);
  }

  bool operator!=(const OrthotopeBase<_Type, _Dimensions, Eigen::DontAlign>& src) const {
    return (m_origin != src.m_origin || m_size != src.m_size);
  }

  bool operator==(const OrthotopeBase<_Type, _Dimensions, Eigen::Aligned>& src) const {
    return (m_origin == src.m_origin && m_size == src.m_size);
  }

  bool operator!=(const OrthotopeBase<_Type, _Dimensions, Eigen::Aligned>& src) const {
    return (m_origin != src.m_origin || m_size != src.m_size);
  }

  int dimensions() const { return _Dimensions; }

  Point& origin() { return m_origin; }
  const Point& origin() const { return m_origin; }
  Size& size() { return m_size; }
  const Size& size() const { return m_size; }
  
  void reset() {
    m_origin.setZero(_Dimensions,1);
    m_size.setZero(_Dimensions,1);
  }

private:
  Point m_origin;
  Size m_size;
};

// Rect
typedef OrthotopeBase<int,    2> Recti;
typedef OrthotopeBase<float,  2> Rectf;
typedef OrthotopeBase<double, 2> Rectd;
// Rect aligned
typedef OrthotopeBase<int,    2, Eigen::Aligned> Rectia;
typedef OrthotopeBase<float,  2, Eigen::Aligned> Rectfa;
typedef OrthotopeBase<double, 2, Eigen::Aligned> Rectda;

// Box
typedef OrthotopeBase<int,    3> Boxi;
typedef OrthotopeBase<float,  3> Boxf;
typedef OrthotopeBase<double, 3> Boxd;
// Box aligned
typedef OrthotopeBase<int,    3, Eigen::Aligned> Boxia;
typedef OrthotopeBase<float,  3, Eigen::Aligned> Boxfa;
typedef OrthotopeBase<double, 3, Eigen::Aligned> Boxda;

/*-------------------------------------------------------------------------*/
// Orthotope
template<typename _Type, int _Dimensions, int _Options = Eigen::DontAlign>
class __declspec(align(16)) Orthotope: public OrthotopeBase<_Type, _Dimensions, _Options>
{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef Eigen::Matrix<double, _Dimensions, _Dimensions, _Options> Basis;

  Orthotope(): m_basis() {}
  Orthotope(const Point& src_origin, const Size& src_size,
               const Basis& src_basis):
    OrthotopeBase<_Type, _Dimensions, _Options>(src_origin, src_size),
    m_basis(src_basis) {}
  ~Orthotope() {}

  Basis& basis() { return m_basis; }
  const Basis& basis() const { return m_basis; }
  
  void reset() {
    OrthotopeBase<_Type, _Dimensions, _Options>::reset();
    m_basis.setIdentity(_Dimensions, _Dimensions);
  }

private:
  Basis m_basis;
};

// GenericRect
typedef Orthotope<int,    2> GenericRecti;
typedef Orthotope<float,  2> GenericRectf;
typedef Orthotope<double, 2> GenericRectd;
// GenericRect aligned
typedef Orthotope<int,    2, Eigen::Aligned> GenericRectia;
typedef Orthotope<float,  2, Eigen::Aligned> GenericRectfa;
typedef Orthotope<double, 2, Eigen::Aligned> GenericRectda;

// GenericBox
typedef Orthotope<int,    3> GenericBoxi;
typedef Orthotope<float,  3> GenericBoxf;
typedef Orthotope<double, 3> GenericBoxd;
// GenericBox aligned
typedef Orthotope<int,    3, Eigen::Aligned> GenericBoxia;
typedef Orthotope<float,  3, Eigen::Aligned> GenericBoxfa;
typedef Orthotope<double, 3, Eigen::Aligned> GenericBoxda;

