#pragma once

#include <vector>

namespace lssplib
{
namespace polygonization
{

//! @brief 線分データのインデックスを保持するクラス。
class SourceIndexHolder
{
public:
	SourceIndexHolder();

	~SourceIndexHolder();

	const std::vector<unsigned int>& GetSourceIndices() const;

	void AddSourceIndex(const unsigned int indexToBeAdded);

private:
	std::vector<unsigned int> m_sourceIndices; //!< 線分データのインデックス

};

} // namespace polygonization
} // namespace lssplib

