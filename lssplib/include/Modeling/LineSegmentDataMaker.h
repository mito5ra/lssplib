#pragma once

#include "SourceLineSegment.h"
#include "BaseContainers.h"
#include <vector>

namespace lssplib
{
namespace segmentation
{

class VesselTree;
class VesselNode;

} // namespace segmentation

namespace polygonization
{

class SmoothedVesselNode;

//! @brief 線分データを作成するクラス
class LineSegmentDataMaker
{
public:
	//! 処理するためのパラメータ保持する構造体
	struct Params
	{
		size_t timesToSmooth; //!< スムーズ処理の回数
		double maxRadiusAtEnd; //!< 末端の最大半径
		size_t maxNodesFromBranchToEnd; //!< 分岐から末端の最大ノードの数
		double maxRadiusAtBranchToEnd; //!< 分岐から末端の最大半径

		//! コンストラクタ
		Params(size_t _timesToSmooth,
			double _maxRadiusAtEnd,
			size_t _maxNodesFromBranchToEnd,
			double _maxRadiusAtBranchToEnd)
			: timesToSmooth(_timesToSmooth)
			, maxRadiusAtEnd(_maxRadiusAtEnd)
			, maxNodesFromBranchToEnd(_maxNodesFromBranchToEnd)
			, maxRadiusAtBranchToEnd(_maxRadiusAtBranchToEnd) {}

		Params(const Params& src)
			: timesToSmooth(src.timesToSmooth)
			, maxRadiusAtEnd(src.maxRadiusAtEnd)
			, maxNodesFromBranchToEnd(src.maxNodesFromBranchToEnd)
			, maxRadiusAtBranchToEnd(src.maxRadiusAtBranchToEnd) {}

		Params& operator=(const Params& src)
		{
			timesToSmooth = src.timesToSmooth;
			maxRadiusAtEnd = src.maxRadiusAtEnd;
			maxNodesFromBranchToEnd = src.maxNodesFromBranchToEnd;
			maxRadiusAtBranchToEnd = src.maxRadiusAtBranchToEnd;
		}
	};

	LineSegmentDataMaker(const segmentation::VesselTree* startNode, const Size3d& voxelSize, const Params& params);

	LineSegmentDataMaker::LineSegmentDataMaker(const Params& params, const Size3d& voxelSize);

	virtual ~LineSegmentDataMaker();

	void MakeLineSegmentData(
		std::vector<SourceLineSegment>& lineSegments);

private:

	void ClearSmoothedTree();

	void SetEndsAndRadiusFromSmoothedNodes(std::vector<SourceLineSegment>& lineSegments) const;

	const segmentation::VesselTree* m_vesselTree; //!< 血管抽出機能側から与えられる血管の芯線データ

	Size3d m_voxelSize; //!< 画像の voxel scale 等を保持するクラスへのポインタ

	SmoothedVesselNode* m_startNode; //!< 平滑化に使われる血管の芯線データの始点

	Params m_params; //!< 処理に使用するパラメータ
};

} // namespace polygonization
} // namespace lssplib

