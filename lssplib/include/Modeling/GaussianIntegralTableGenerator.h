#pragma once

#include "../lssplib.h"
#include <vector>

namespace lssplib
{
namespace polygonization
{

//! @brief ガウス積分テーブルを生成するクラス。積分の数値計算結果を素早く処理するために、あらかじめ計算結果を実行し、格納する。
class GaussianIntegralTableGenerator
{
public:
	LSSPLIB_API GaussianIntegralTableGenerator();

	LSSPLIB_API ~GaussianIntegralTableGenerator();

	LSSPLIB_API void GenerateIntegralTable();

	LSSPLIB_API const std::vector<double>& GetIntegralTable() const;

private:

	std::vector<double> m_table; //!< 積分テーブル

	bool m_hasBeenGenerated; //!< 既に積分テーブルを生成したかどうかを記録する変数
};

} // namespace polygonization
} // namespace lssplib

