#pragma once

#include "../lssplib.h"
#include "../BaseContainers.h"

namespace lssplib
{
namespace segmentation
{

class VesselNode;

} // namespace segmentation

namespace polygonization
{

class SmoothedVesselNode;

class SmoothedVesselNodeGenerator
{
public:
	LSSPLIB_API SmoothedVesselNodeGenerator();

	LSSPLIB_API explicit SmoothedVesselNodeGenerator(size_t n);

	LSSPLIB_API virtual ~SmoothedVesselNodeGenerator();

	LSSPLIB_API void SetParams(
		size_t maxNodesFromBranchToEnd,
		float maxRadiusAtEnd,
		float maxRadiusAtBranchToEnd);

	LSSPLIB_API SmoothedVesselNode* Convert(
		const segmentation::VesselNode* vesselNode,
		const Point3f& vesselOrigin,
		const Size3f& voxelSize);

	LSSPLIB_API static void ClearNodeTree(SmoothedVesselNode*& startNode);

private:

	void ConvertPixelCoordToRealCoord(
		const segmentation::VesselNode* vesselNode,
		const Point3f& vesselOrigin,
		const Size3f& voxelSize);

	void UpdateBranchLevels();

	void SetBranchLevelFromTerminalNode(SmoothedVesselNode* terminalNode);

	void CollectTerminalNodes(std::vector<SmoothedVesselNode*>& terminalNodes) const;

	void ReduceRadiiOfFirstLevelJunctions();

	void CollectFirstLevelJunctions(std::vector<SmoothedVesselNode*>& firstLevelJunctions) const;

	void SetRadiiOfBranches();

	void CollectJunctionNodes(std::vector<SmoothedVesselNode*>& junctionNodes);

	bool ExchangeRadiusAtJunctions(SmoothedVesselNode* junctionNode) const;

	void ReduceRadiiOfTerminalNodes();

	void SetRadiusByMonotomicallyDecreasingManner();

	void DecreaseRadiusGradually(SmoothedVesselNode* junctionNode);

	void CollectBranchNodes(SmoothedVesselNode* branchStartNode, std::vector<SmoothedVesselNode*>& nodesInBranch);

	void DecreaseRadiusTowardNextJunction(const std::vector<SmoothedVesselNode*>& nodesInBranch);

	void SmoothPositions();

	size_t m_numSmoothing;

	Size3f m_voxelSize;

	SmoothedVesselNode* m_smoothedVesselNode;

	size_t m_maxNodesFromBranchToEnd; //!< 分岐から末端の最大ノードの数
	float m_maxRadiusAtEnd; //!< 末端の最大半径
	float m_maxRadiusAtBranchToEnd; //!< 分岐から末端の最大半径
};

} // namespace polygonization
} // namespace lssplib
