#pragma once

#include "TriangleData.h"
#include "TrianglesWithLocalConnectivity.h"
#include "../lssplib.h"

namespace lssplib
{
namespace polygonization
{

//! @brief ポリゴンの小片を取り除くクラス。一番大きな連結グループだけを残し、それ以外を除去する。
class SweepSmallPiecesOfPolygons
{
public:
	LSSPLIB_API SweepSmallPiecesOfPolygons(std::vector<TriangleVertexIndex>& inputTriangles, std::vector<VertexPosition>& inputVertices);

	virtual~SweepSmallPiecesOfPolygons();

	LSSPLIB_API void SweepSmallPieces();

private:
	void CollectConnectingTriangle(const size_t startIndex, std::vector<size_t>& connectingTriangle);

	void DivideTrianglesIntoConnectingGroups(std::vector<std::vector<size_t>>& connectingGroups);

	const size_t FindBiggestGroup(const std::vector<std::vector<size_t>>& connectingGroup) const;

	const bool FindStartIndex(size_t& startIndex) const;

	void MakeArrayOfVertexIndexToBeRemoved_ArrangedInAscendingOrder(const std::vector<std::vector<size_t>>& connectingGroups,
		const size_t indexOfBiggestGroup, 
		std::vector<size_t>& arrayOfVertexIndex) const;

	void MakeArrayOfTriangleIndexToBeRemoved_ArrangedInAscendingOrder(const std::vector<std::vector<size_t>>& connectingGroups,
		const size_t indexOfBiggestGroup, 
		std::vector<size_t>& arrayOfTriangleIndex) const;

	void UpdateTriangles(const std::vector<size_t>& vertexIndexToBeRemoved,
		const std::vector<size_t>& triangleIndexToBeRemoved);

	TrianglesWithLocalConnectivity m_trianglesWithConnectivity; //!< 連結関係を保有する三角形データ

	std::vector<TriangleVertexIndex>* m_triangles; //!< 三角形のインデックス集合

	std::vector<VertexPosition>* m_vertices; //!< 頂点座標の集合

//	void output(); // for testing
};

} // namespace polygonization
} // namespace lssplib

