#ifndef LOCALAPPROX_H 
#define LOCALAPPROX_H 

#include <stdio.h>
#include <math.h>

namespace lssplib
{
namespace polygonization
{

class LocalApprox{
public:
	virtual ~LocalApprox() {}

  virtual float value(float x, float y, float z)=0;
  
  virtual void gradient(float g[3], float x, float y, float z)=0;
  
  virtual float valueAndGradient(float g[3], float x, float y, float z)=0;
  
};

} // namespace polygonization
} // namespace lssplib

#endif