#pragma once

#include <vector>

namespace lssplib
{
namespace polygonization
{

//! @brief 三角形頂点を保持する構造体
struct TriangleVertexIndex
{
	size_t vertex[3]; //!< 三角形の頂点インデックス

	//! @brief コンストラクタ
	TriangleVertexIndex()
	{
		vertex[0] = 0;
		vertex[1] = 0;
		vertex[2] = 0;
	}

	//! @brief デストラクタ
	~TriangleVertexIndex(){}
};

//! @brief 頂点位置を保持する構造体
struct VertexPosition
{
	float position[3]; //!< 頂点位置

	//! @brief コンストラクタ
	VertexPosition()
	{
		position[0] = 0;
		position[1] = 0;
		position[2] = 0;
	}

	//! @brief デストラクタ
	~VertexPosition(){}
};

//! @brief 三角形データを保持するクラス。
class TriangleData
{
public:
	TriangleData();

	~TriangleData();

	const size_t GetEdgeIndex(const size_t ith) const;

	const size_t GetVertexIndex(const size_t ith) const;

	//! @brief フラグを取得する。
	//! 
	//! @return フラグ
	const bool HasTheTriangleBeenUsed()const {return m_hasBeenUsed;}

	void SetEdgeIndex(const size_t edgeIndex, const size_t ith);

	void SetVertexIndex(const size_t* vertexIndexOfTriangle);

	//! @brief フラグをセットする。
	//! 
	//! @param[in] usedOrNot フラグ
	void WriteUsedOrNot(const bool usedOrNot){m_hasBeenUsed = usedOrNot;}

private:

	size_t m_edgeIndex[3]; //!< 三角形が所有する辺のインデックス

	size_t m_vertexIndex[3]; //!< 三角形が所有する頂点のインデックス

	bool m_hasBeenUsed; //!< フラグ。処理済みかどうかを記録する。
};

//! @brief 三角形の辺データを保持するクラス。
class EdgeData
{
public:
	EdgeData();

	~EdgeData();

	void AddTriangleAroundEdge(const size_t triangleID);

	const size_t* GetEdge() const;

	const std::vector<size_t>& GetTriangleAroundEdge() const;

	void SetEdge(const size_t* edge);

private:
	size_t m_edge[2]; //!< 辺を構成する頂点のインデックス

	std::vector<size_t> m_trianglesAroundEdge; //!< 辺を共有する三角形のインデックス
};

//! @brief 三角形の頂点データを保持するクラス
class VertexData
{
public:
	VertexData();

	virtual~VertexData();

	void AddEdgeAroundVertex(const size_t id);

	const std::vector<size_t>& GetEdgeAroundVertex() const;

	const float* GetPositionOfVertex() const;

	//! @brief フラグを取得する。
	//! 
	//! @return フラグ
	const bool HasTheVertexBeenUsed() const {return m_hasBeenUsed;}

	void SetPositionOfVertex(const float* position);

	//! @brief フラグに true を記録する。
	//! 
	//! @return フラグ
	void WriteTheVertexIsUsed(){m_hasBeenUsed = true;}

private:
	float m_positionOfVertex[3]; //!< 頂点の位置

	std::vector<size_t> m_edgeIdAroundVertex; //!< 頂点を共有する辺のインデックス

	bool m_hasBeenUsed; //!< 処理済かどうかを記録するフラグ
};

} // namespace polygonization
} // namespace lssplib

