//! Defines the constants

#define TOP_BOTTOM 0
#define LEFT_RIGHT 1
#define FRONT_BACK 2

#define SE_WIDTH 3
#define SE_HEIGHT 3
#define SE_DEPTH 3

#define NUM_FACES 6
#define WEIGHT6 1.0f
#define WEIGHT18 0.54f
#define WEIGHT26 0.183f

#define LEFT 0
#define RIGHT 1
#define FRONT 2
#define BACK 3
#define UP 4
#define DOWN 5

#define MAXERRORCONST 0.86602540

#define FILTER_WINDOW_SIZE 3

#define STRUCTURE_ELEMENT_SIZE 27

#define SIXNEIGHBOR 6

#define SUBVOX_DIV_FACTOR 4
#define VOX_DIV_FACTOR 2

#define SUBVOX_NEIGHBOR_FACTOR 0.5f
#define VOX_NEIGHBOR_FACTOR 1.0f