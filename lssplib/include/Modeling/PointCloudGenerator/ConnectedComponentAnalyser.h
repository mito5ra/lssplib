#pragma once
#include  "AppDataStructures.h"
#include <iostream>
#include "DataManager.h"
#include "Utility.h"
#include "Constants.h"

using namespace std;

namespace lssplib
{
namespace polygonization
{

class ConnectedComponentAnalyser
{
	//! unsigned char* variable
	/*!It keeps labels after identifying connected components*/
	unsigned char* connComp;

	//! int variable
	/*!It will have the width of the volume*/
	int m_width;

	//! int variable
	/*!It will have the height of the volume*/
	int m_height;

	//! int variable
	/*!It will have the depth of the volume*/
	int m_depth;

	//! unsigned char* variable
	/*!It will have the binary data read*/
	const unsigned char* m_buffer;
	
public:
	//! Constructor of ConnectedComponentAnalyser class
	ConnectedComponentAnalyser(void);

	//! Constructor
	ConnectedComponentAnalyser(CDataManager* dataManager);

	//! Destructor of ConnectedComponentAnalyser class
	~ConnectedComponentAnalyser(void);

	//! CheckFloodfillConnectivity() checks if the considering voxels in the opposite directions are connected or not
	/*!
	\param[in] direction the direction along which the opposite voxels or subvoxels are residing.
	\returns true if the vox/subvox are not connected(ie,belonging to different structures) or else false
	*/
	bool CheckFloodfillConnectivity(int direction, Point &outBoundVoxel);
};

} // namespace polygonization
} // namespace lssplib

