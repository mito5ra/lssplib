#pragma once

namespace lssplib
{
namespace polygonization
{

struct Point
{
public:
	float x; //!< x coordinate
	float y; //!< y coordinate
	float z; //!< z coordinate

	//! equality operator
	bool operator==(const Point& p) const
	{
		return ((p.x == x) && (p.y == y) && (p.z == z));
	}
};

} // namespace polygonization
} // namespace lssplib

