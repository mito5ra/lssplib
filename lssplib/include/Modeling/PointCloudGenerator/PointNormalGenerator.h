#pragma once
#include "DataManager.h"

namespace lssplib
{
namespace polygonization
{

class CPointNormalGenerator
{
private:
	
public:
	CPointNormalGenerator(void);
	~CPointNormalGenerator(void);

	//! FlipNormalDirection inturn calls function FlipNormalTowardsViewpoint that flips the direction of the normal received
	/*!
	\param[in] pointPlaced the point for which normal is generated
	\param[in] generatedNormal the generated normal
	\param[in] viewpoint_x the x cordinate of the viewpoint towards which the direction of normal to be flipped
	\param[in] viewpoint_y the y cordinate of the viewpoint towards which the direction of normal to be flipped
	\param[in] viewpoint_z the z cordinate of the viewpoint towards which the direction of normal to be flipped
	*/
	void FlipNormalDirection(Point pointPlaced, Point &generatedNormal ,float viewpoint_x,float viewpoint_y, float viewpoint_z);

	//! FlipNormalTowardsViewpoint flips the direction of the normal received toward the given viewpoint
	/*!
	\param[in] pointPlaced the point for which normal is generated
	\param[in] vp_x the x cordinate of the viewpoint towards which the direction of normal to be flipped
	\param[in] vp_y the y cordinate of the viewpoint towards which the direction of normal to be flipped
	\param[in] vp_z the z cordinate of the viewpoint towards which the direction of normal to be flipped
	\param[in] nx the x cordinate of the generated normal
	\param[in] ny the y cordinate of the generated normal
	\param[in] nz the z cordinate of the generated normal
	*/
	void FlipNormalTowardsViewpoint (Point pointPlaced, float vp_x, float vp_y, float vp_z,float &nx, float &ny, float &nz);
};

} // namespace polygonization
} // namespace lssplib

