#pragma once
#include "Constants.h"

namespace lssplib
{
namespace polygonization
{

class CMorphologicalOperator
{
	//! int array
	/*!It defines the Sructuring element for Morphological operations*/
	int m_structuringElement[STRUCTURE_ELEMENT_SIZE];

	//! int variable
	/*!It defines the Origin X of Sructuring element*/
	int m_origX;
	
	//! int variable
	/*!It defines the Origin Y of Sructuring element*/
	int m_origY;
	
	//! int variable
	/*!It defines the Origin Z of Sructuring element*/
	int m_origZ;

	//! CreateStructElement() defines the array m_structuringElement
	void CreateStructElement();
	
public:

	//! Constructor of CMorphologicalOperator class
	CMorphologicalOperator(void);
	
	//! Constructor of CMorphologicalOperator class
	~CMorphologicalOperator(void);
	
	//! PerformDilation() perfoms dilation operation on the Input Buffer
	/*!
	\param[in] buffer input buffer
	\param[out] bufferOut the buffer after dilation.
	\param[in] width of the input volume data.
	\param[in] height of the input volume data.
	\param[in] depth of the input volume data.
	*/
	void PerformDilation(unsigned char* buffer,unsigned char* bufferOut,int width, int height,int depth);
	
	//! PerformErosion() perfoms erosion operation on the Input Buffer
	/*!
	\param[in] buffer input buffer
	\param[out] bufferOut the buffer after dilation.
	\param[in] width of the input volume data.
	\param[in] height of the input volume data.
	\param[in] depth of the input volume data.
	*/void PerformErosion(const unsigned char* buffer,unsigned char* bufferOut,int width, int height,int depth);
};

} // namespace polygonization
} // namespace lssplib

