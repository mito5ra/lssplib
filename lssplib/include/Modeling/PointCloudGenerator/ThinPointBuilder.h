#pragma once
#include "PointBuilder.h"
#include "DataManager.h"
#include "Utility.h"
#include "ThreeDDataProcessor.h"
#include  "AppDataStructures.h"
#include <map>
#include <iostream>
#include "../../lssplib.h"

using namespace std;

namespace lssplib
{
namespace polygonization
{

class CThinPointBuilder:
	public PointBuilder
{
	//! vector of Point type variable
	/*!Stores all the points generated from thin structures*/
	vector<Point> m_GeneratedThinPoints;

	//! vector of Point type variable
	/*!Stores all the normals generated from thin structures*/
	vector<Point> m_GeneratedNormals;

	//! vector of Point type variable
	/*!Stores the eightsubvoxels of a defenite voxel*/
	vector<Point> m_EightSubvoxels;

	//! vector of Point type variable
	/*!Stores the subvoxels of object voxels in 3D 18 neighbours of a candidate voxel to use later for point generation*/
	vector<Point> m_AllThinObjSubvoxels;
	
	//! pointer variable
	/*!Points to threeDDataprocesoor class*/
	CThreeDDataProcessor *m_objCThreeDDataProcessor;

	//! int variable
	/*!counter that keeps track of the out bound subvoxels that are added to the map ListOfOutBoundSubVoxPointsWithParent*/
	int m_Counter;

	//! Pointer variable to object of CDataManager
	CDataManager* m_dataManager;
	
	//! GetPointsFromOutBoundVox()generate points for the out bound subvoxels through 6 step algorithm
	/*!
	\param[in] width of the input volume data.
	\param[in] height of the input volume data.
	\param[in] bufSize the size of the volume
	\param[in] objectBuffer contains the actual segmented binary data read from raw file. 
	\returns nothing
	*/
	void GetPointsFromOutBoundVox(int width, int height,int bufSize, const unsigned char* objectBuffer);

	//! CalculateSubvoxels()calculates subvoxels for the voxel received
	/*!
	\param[in] x cordinate of the voxel to be divided to subvoxel
	\param[in] y cordinate of the voxel to be divided to subvoxel
	\param[in] z cordinate of the voxel to be divided to subvoxel
	\returns nothing
	*/
	void CalculateSubvoxels(float x, float y, float z);


	//! CheckIfCavity()checks if the received voxel is a cavity or not
	/*!
	\param[in] x cordinate of the voxel to be checked for cavity
	\param[in] y cordinate of the voxel to be checked for cavity
	\param[in] z cordinate of the voxel to be checked for cavity
	\param[in] objectBuffer contains the actual segmented binary data read from raw file 
	\param[in] width of the input volume data.
	\param[in] height of the input volume data.
	\param[in] depth of the input volume data.
	\returns true if the candidate voxel represents a voxel or else false
	*/
	bool CheckIfCavity(int x,int y,int z, const unsigned char* objectBuffer,int width, int height ,int depth);

	//! DivideAndCheckSubvox()divides each voxel given into eight and checks each one if need to be replaced as object subvoxel
	/*!
	\param[in] ParentX x cordinate of the parent voxel to be divided to subvoxel
	\param[in] ParentY y cordinate of the parent voxel to be divided to subvoxel
	\param[in] ParentZ z cordinate of the parent voxel to be divided to subvoxel
	\param[in] width of the input volume data.
	\param[in] height of the input volume data.
	\returns nothing
	*/
	void DivideAndCheckSubvox(float ParentX, float ParentY, float ParentZ,int width, int height);

	//! CheckSubvoxToReplaceAsObj()checks if a given subvoxel needs to be replaced as object subvoxel
	/*!
	\param[in] SubVoxelX x cordinate of the subvoxel to be checked
	\param[in] SubVoxelY y cordinate of the subvoxel to be checked
	\param[in] SubVoxelZ z cordinate of the subvoxel to be checked
	\returns true if the subvoxel to be replaced as object subvox or else false
	*/
	bool CheckSubvoxToReplaceAsObj(float SubVoxelX ,float SubVoxelY,float SubVoxelZ);

	//! SharesFaceWithConsideredSubVoxel()checks if a given subvoxels shares any face with eachother
	/*!
	\param[in] objectSubvoxel1 first object subvoxel in the 18 neighbourhood of the candidate subvox
	\param[in] objectSubvoxel2 second object subvoxel in the 18 neighbourhood of the candidate subvox
	\returns true if the subvoxels share any face with eachother or else false
	*/
	bool SharesFaceWithConsideredSubVoxel(Point &objectSubvoxel1, Point &objectSubvoxel2);

	//! SharesFaceWithCandidateSubVoxel()checks if a given subvoxels shares any face with the candidate subvoxel
	/*!
	\param[in] candidatesubvoxel the candidate subvoxel
	\param[in] objectSubvoxel1 first object subvoxel in the 18 neighbourhood of the candidate subvox
	\param[in] objectSubvoxel2 second object subvoxel in the 18 neighbourhood of the candidate subvox
	\returns true if any of the two object subvoxels share a face with the candidate subvoxel or else false
	*/
	bool SharesFaceWithCandidateSubVoxel(Point &candidatesubvoxel,Point &objectSubvoxel1, Point &objectSubvoxel2);

	//! CheckIfObjectSubvoxel()checks if the given subvoxel is a part of the object or not using a list prepared earlier(allthinobjsubvoxels)
	/*!
	\param[in] subvoxelToSearch to be checked if object or not
	\returns true if the subvoxel is an object or else false
	*/
	bool CheckIfObjectSubvoxel(Point &subvoxelToSearch);

	//! ClearSubvoxels() clears the eight sub voxels list
	void ClearSubvoxels();
	
	//! map of vector of points variable with integer key
	/*!Store all the object subvoxels to be replaced*/
	std::map<int,std::vector<Point>> ListOfReplacedPointsWithParent;

	//! map of vector of points variable with integer key
	/*!Stores all the out bound sub voxels for which points to be placed using 6 step algorithm later*/
	std::map<int,std::vector<Point>> ListOfOutBoundSubVoxPointsWithParent;
public:
	//! Constructor of CThinPointBuilder class
	LSSPLIB_API CThinPointBuilder(void);

	//! Constructor
	LSSPLIB_API CThinPointBuilder(CDataManager* dataManager);

	//! Destructor of CThinPointBuilder class
	~CThinPointBuilder(void);

	//! AddPoints()generates points and normals from thin structures through algorithm implementation
	/*!
	\returns true on successful point generation or else false
	*/
	bool AddPoints();

	//! GetPoints()gets all the points generated and populates thin point list
	/*!
	\param[in] thinData vector of thin points to be populated
	\returns nothing
	*/
	void GetPoints(vector<Point> &thinData);

	//! GetNormals()gets all the normals generated and populates thin point list
	/*!
	\param[in] normals vector of thin points to be populated
	\returns nothing
	*/
	void GetNormals(vector<Point> &normals);

	//! ClearBuffers() clears the buffers associated with generating thin points and normals
	/*!
	\returns nothing
	*/
	void ClearBuffers();
};

} // namespace polygonization
} // namespace lssplib

