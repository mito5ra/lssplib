#pragma once
#include "DataManager.h"
#include "PointBuilder.h"
#include "AppDataStructures.h"
#include "../../lssplib.h"
using namespace std;

namespace lssplib
{
namespace polygonization
{

class CPointCloudGenerator
{

public:
	LSSPLIB_API CPointCloudGenerator(void);
	LSSPLIB_API ~CPointCloudGenerator(void);

	//! GeneratePoints() can call thick and thin point generation functions using point builder (parent) object
	/*!
	\param[in] pb pointbuilder pointer variable
	\returns true on succesful point generation or else false
	*/
	LSSPLIB_API bool GeneratePoints(PointBuilder* pb);

	//! GetAllPoints() gets all the thick and thin points generated and populates point list
	/*!
	\param[in] thick Pointbuilder pointer variable corresponding to thick object
	\param[in] thin Pointbuilder pointer variable corresponding to thin object
	\param[in] m_points vector of points to be filled 
	*/
	LSSPLIB_API void GetAllPoints(PointBuilder* thick, PointBuilder* thin, std::vector<Point> &m_points);

	//! GetAllNormals() gets all the thick and thin point normals generated and populates normal list
	/*!
	\param[in] thick Pointbuilder pointer variable corresponding to thick object
	\param[in] thin Pointbuilder pointer variable corresponding to thin object
	\param[in] m_normals vector of normals to be filled 
	*/
	LSSPLIB_API void GetAllNormals(PointBuilder* thick, PointBuilder* thin, std::vector<Point> &m_normals);

	//! ClearBuffers() clears the buffers associated with thick and thin point and normal generation
	/*!
	\param[in] thick Pointbuilder pointer variable corresponding to thick object
	\param[in] thin Pointbuilder pointer variable corresponding to thin object 
	*/
	LSSPLIB_API void ClearBuffers(PointBuilder* thick, PointBuilder* thin);
};

} // namespace polygonization
} // namespace lssplib

