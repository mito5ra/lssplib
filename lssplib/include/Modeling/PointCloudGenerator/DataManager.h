//! CDataManager singleton class.
/*!
class for reading binary data and preparing inputs for point builder classes
*/
#pragma once
#include "AppDataStructures.h"
#include "MorphologicalOperator.h"
#include <list>
#include <vector>
#include "../../lssplib.h"
using namespace std;

namespace lssplib
{
namespace polygonization
{

class CDataManager
{
	//! unsigned char* variable
	/*!It will have the binary data read*/
	const unsigned char* m_binarydata;

	//! int variable
	/*!It will have the width of the volume*/
	int m_imageWidth;

	//! int variable
	/*!It will have the height of the volume*/
	int m_imageHeight;

	//! int variable
	/*!It will have the depth of the volume*/
	int m_imageDepth;

	//! int variable
	/*!It will have the size of the volume*/
	int m_ImageSize;

	//! float variable
	/*!It will have the voxel size in x direction*/
	float m_voxel_size_x;

	//! float variable
	/*!It will have the voxel size in y direction*/
	float m_voxel_size_y;

	//! float variable
	/*!It will have the voxel size in z direction*/
	float m_voxel_size_z;

	//! vector of integers variable
	/*!It will have the x co-ordinate values of the voxels*/
	vector<int> m_bufferX;

	//! vector of integers variable
	/*!It will have the y co-ordinate values of the voxels*/
	vector<int> m_bufferY;

	//! vector of integers variable
	/*!It will have the z co-ordinate values of the voxels*/
	vector<int> m_bufferZ;

	//! Pointer variable
	/*!to create object of singleton class CDataManager*/
//	static CDataManager* m_DataManager;

	//! Pointer variable
	/*!to create object of  class MorphologicalOperator*/
	CMorphologicalOperator* m_objMorphOperator;

	//! unsigned char* variable
	/*!It will have the binary input for thick point buider*/
	unsigned char* m_thickData;

	//! unsigned char* variable
	/*!It will have data for thin structures alone for thin point buider*/
	unsigned char* m_thinData;

	//! unsigned char* variable
	/*!It will have the binary data after erosion*/
	unsigned char* m_erodedData;

	//! unsigned char* variable
	/*!Thin out bound data without thin*/
	unsigned char* m_thinOutBoundData;

	//! unsigned char* variable
	/*!Thin out bound data along with thin data*/
	unsigned char* m_thinOutBoundDataWithThin;

	//! PopulateIndices() calculate x y z co-ordinates of each voxel and index beforehand to use later
	/*!
	\returns nothing
	*/
	void PopulateIndices();

	//! InitializeMorphologicalData()intializes and clears the buffers associated with morphological operations
	/*!
	\returns nothing.
	*/
	void InitializeMorphologicalData();

	//! InitializeOutputData() initializes the intermediate buffers required for generating inputs for point builder
	/*!
	\returns nothing.
	*/
	void InitializeOutputData();

	//! UnInitializeOutputData() unintializes and clears the intermediate buffers used while generating inputs for point builder
	/*!
	\returns nothing.
	*/
	void UnInitializeOutputData();

	//! GenerateThinData() gets the binary data read from the input raw file
	/*!
	\returns nothing.
	*/
	void GenerateThinData();

	//! UnInitializeMorphologicalData() unintializes and clears the buffers associated with morphological operations
	/*!
	\returns nothing.
	*/
	void UnInitializeMorphologicalData();

	//! GenerateThickData() generates the data for thick point builder
	/*!
	\returns nothing.
	*/
	void GenerateThickData();

	//! GenerateThinOutBoundData() generates the data of outer boundary voxels for thin point builder
	/*!
	\returns nothing.
	*/
	void GenerateThinOutBoundData();

	//! Generates thin outbound data including thin data
	/*!
	\returns nothing.
	*/
	void GenerateThinOutBoundDataWithThinData();

public:

	//! Constructor of CDataManager class
	LSSPLIB_API CDataManager(void);

	//! Destructor of CDataManager class
	LSSPLIB_API ~CDataManager(void);

	//! SetBinarydata() sets the binary data read from the input raw file
	/*!
	\param[in] binarydata contains the binary data read.
	*/
	LSSPLIB_API void SetBinarydata(unsigned char* binarydata);

	//! SetBinarydata() sets the binary data read from the input raw file
	/*!
	\param[in] binarydata contains the binary data read.
	*/
	void SetBinarydata(const unsigned char* binarydata);
	
	//! GetBinarydata() gets the binary data read from the input raw file
	/*!
	\returns the binary data read.
	*/
	const unsigned char* GetBinarydata() const;

	//! SetImageParams() sets the dimension informations of the input data
	/*!
	\param[in] width of the input volume data.
	\param[in] height of the input volume data.
	\param[in] depth of the input volume data.
	\param[in] voxel_x voxel size in x direction of the input volume data.
	\param[in] voxel_y voxel size in y direction of the input volume data.
	\param[in] voxel_z voxel size in z direction of the input volume data.
	*/
	LSSPLIB_API void SetImageParams(int width, int height, int depth, float voxel_x,float voxel_y,float voxel_z);

	//! GetVolumeWidth() gets the width of the input volume data
	/*!
	\returns the width of the input volume data
	*/
	int GetVolumeWidth();
	
	//! GetVolumeHeight() gets the height of the input volume data
	/*!
	\returns the height of the input volume data
	*/
	int GetVolumeHeight();

	//! GetVolumeDepth() gets the depth of the input volume data
	/*!
	\returns the depth of the input volume data
	*/
	int GetVolumeDepth();

	//! GetVolumeSize() gets the size of the input volume data
	/*!
	\returns the size of the input volume data
	*/
	int GetVolumeSize();

	//! GetVoxelSizeX() gets voxel size in x direction of the input volume data.
	/*!
	\returns the voxel size in x direction of the input volume data.
	*/
	float GetVoxelSizeX();

	//! GetVoxelSizeY() gets voxel size in y direction of the input volume data.
	/*!
	\returns the voxel size in y direction of the input volume data.
	*/
	float GetVoxelSizeY();

	//! GetVoxelSizeZ() gets voxel size in z direction of the input volume data.
	/*!
	\returns the voxel size in z direction of the input volume data.
	*/
	float GetVoxelSizeZ();

	//! GetBufferX() gets x co-ordinate value of the voxels
	/*!
	\returns the x co-ordinate value of the voxels
	*/
	const vector<int>& GetBufferX() const;

	//! GetBufferY() gets y co-ordinate value of the voxels
	/*!
	\returns the y co-ordinate value of the voxels
	*/
	const vector<int>& GetBufferY() const;

	//! GetBufferZ() gets z co-ordinate value of the voxels
	/*!
	\returns the z co-ordinate value of the voxels
	*/
	const vector<int>& GetBufferZ() const;
	
	//! GenerateInputForPointBuilder() generates input buffers required for point builder class
	LSSPLIB_API void GenerateInputForPointBuilder();
	
	//! GenerateInputForPointBuilder() gets the buffer of outer boundary voxels
	/*!
	\param[out] thinOutBoundData the buffer of outer boundary voxels
	*/
	void GetThinOutBounData(unsigned char **thinOutBoundData);

	//! GetThinData() gets the buffer of voxels of  thin structures
	/*!
	\param[out] thinData the buffer of voxels of  thin structures
	*/
	void GetThinData(unsigned char **thinData);

	//! GetDataForThickBuilder() gets the buffer of voxels of  thick structures
	/*!
	\param[out] thickData the buffer of voxels of  thick structures
	*/
	void GetDataForThickBuilder(unsigned char **thickData);

	//! ClearBuffers() clears the buffers associated in generating intermediate inputs
	LSSPLIB_API void ClearBuffers();

};

} // namespace polygonization
} // namespace lssplib

