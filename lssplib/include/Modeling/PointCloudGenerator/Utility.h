#pragma once

namespace lssplib
{
namespace polygonization
{

class CUtility
{
	//! Constructor of CUtility class
	CUtility(void);
	//! Destructor of CUtility class
	~CUtility(void);
public:
	//! CalculateIndex() returns the index(when considering as 1D array) from xyz values of a 3D array 
	//!
	//! @param [in] x index
	//! @param [in] y index
	//! @param [in] z index
	//! @param [in] width of the input volume data.
	//! @param [in] height of the input volume data.
	//! @return the index
	static int CalculateIndex(int x, int y, int z, int width, int height);

	//! CheckIfObject() returns the checks whether the given index contains a valid data
	//!
	//! @param [in] index to be checked
	//! @param [in] buffer to be checked
	//! @param [in] bufsize size of buffer
	//! @return the result
	static bool CheckIfObject(int index, unsigned char* buffer, int bufsize);

	//! CheckIfObject() returns the checks whether the given index contains a valid data
	//!
	//! @param [in] index to be checked
	//! @param [in] buffer to be checked
	//! @param [in] bufsize size of buffer
	//! @return the result
	static bool CheckIfObject(int index, const unsigned char* buffer, int bufsize);


};

} // namespace polygonization
} // namespace lssplib

