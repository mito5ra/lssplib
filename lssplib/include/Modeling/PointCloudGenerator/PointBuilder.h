#pragma once
#include "AppDataStructures.h"
#include <vector>

using namespace std;

namespace lssplib
{
namespace polygonization
{

class PointBuilder
{
public:
	virtual bool AddPoints()=0;
	virtual void  GetPoints(vector<Point> &points)=0;
	virtual void GetNormals(vector<Point> &normals) = 0;
	virtual void ClearBuffers()=0;
	virtual ~PointBuilder(){}
};

} // namespace polygonization
} // namespace lssplib

