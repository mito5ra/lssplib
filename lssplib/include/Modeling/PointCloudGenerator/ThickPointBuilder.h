#pragma once
#include "PointBuilder.h"
#include "DataManager.h"
#include <iostream>
#include "ThreeDDataProcessor.h"
#include "AppDataStructures.h"
#include "../../lssplib.h"

namespace lssplib
{
namespace polygonization
{

using namespace std;

class CThickPointBuilder:
	public PointBuilder
{
	//! pointer variable
	/*!Points to threeDDataprocesoor class*/
	CThreeDDataProcessor *m_objCThreeDDataProcessor;

	//! vector of Point type variable
	/*!Stores all the points generated from thick structures*/
	vector<Point> m_GeneratedThickPoints;

	//! vector of Point type variable
	/*!Stores all the normals generated from thick structures*/
	vector<Point> m_GeneratedNormals;

	//! Pointer variable to object of CDataManager
	CDataManager* m_dataManager;
	
public:
	//! Constructor of CThickPointBuilder class
	CThickPointBuilder(void);

	//! Constructor
	LSSPLIB_API CThickPointBuilder(CDataManager* dataManager);

	//! Destructor of CThickPointBuilder class
	~CThickPointBuilder(void);

	//! AddPoints()generates points and normals from thick structures through algorithm implementation
	/*!
	\returns true on successful point generation or else false
	*/
	bool AddPoints();

	//! GetPoints()gets all the points generated and populates thick point list
	/*!
	\param[in] thickData vector of thick points to be populated
	\returns nothing
	*/
	void GetPoints(vector<Point> &thickData);

	//! GetNormals()gets all the normals generated and populates thick point list
	/*!
	\param[in] normals vector of thick points to be populated
	\returns nothing
	*/
	void GetNormals(vector<Point> &normals);

	//! ClearBuffers() clears the buffers associated with generating thick points and normals
	void ClearBuffers();
};

} // namespace polygonization
} // namespace lssplib

