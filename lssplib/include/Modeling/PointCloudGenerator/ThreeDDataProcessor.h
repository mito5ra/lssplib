#pragma once
#include "ConnectedComponentAnalyser.h"
#include "PointNormalGenerator.h"
#include "DataManager.h"
#include "Constants.h"
#include "AppDataStructures.h"

namespace lssplib
{
namespace polygonization
{

using namespace std;
class CThreeDDataProcessor
{
	//! vector of Point type variable
	/*!Stores all the 3D neighbours of a voxel or subvoxel*/
	vector<Point> ThreeDNeighbours;

	//! vector of Point type variable
	/*!Stores all the points placed for voxel or subvoxel*/
	vector<Point> PointsFromAlgm;

	//! vector of Point type variable
	/*!Stores all the normals generated for the points placed for voxel or subvoxel*/
	vector<Point> normalsOfPoints;

	//! vector of Point type variable
	/*!Stores all the object subvoxels in the 3D-6 neighbours of a voxel to check if an arbitary subvoxel is object or not later in point generation*/
	vector<Point> m_subvoxelListForPointGen;

	//! vector of Point type variable
	/*!Stores all the object subvoxels in the 3D-26 neighbours of a voxel to check if an arbitary subvoxel is object or not later in normal generation*/
	vector<Point> m_subvoxelListForNormalGen;
	
	//! float variable
	/*!It will have the voxel size in x direction*/
	float m_voxel_x;

	//! float variable
	/*!It will have the voxel size in y direction*/
	float m_voxel_y;

	//! float variable
	/*!It will have the voxel size in z direction*/
	float m_voxel_z;

	//! int variable
	/*!It will have the width of the volume*/
	int m_width;

	//! int variable
	/*!It will have the height of the volume*/
	int m_height;

	//! float variable
	/*!It will have the voxel size in x direction*/
	int m_imageSize;

	//! unsigned char* variable
	/*!It will have the binary data read*/
	const unsigned char* m_buffer;

	//! unsigned char* variable
	/*!It will have the binary input for thick point buider*/
	unsigned char* m_thickData;

	//! int variable
	/*!It will have the number of object vox/subvox in the neighborhood of the candidate outbound vox/subvox*/
	int m_count;

	//! int variable
	/*!The factor that decides positions of the additional generated points seperately for vox/subvox*/
	int m_divFactor;

	//! bool variable
	/*!The flag that differentiates between voxel and subvoxel*/
	bool m_IsOutBoundSubvox;

	//! float variable
	/*!The factor that helps to calculate the neighbors of voxel and subvoxel differently*/
	float m_NeighborSelectionFactor;

	//! pointer variable
	/*!Points to ConnectedComponentAnalyser class*/
	ConnectedComponentAnalyser* m_ConnComp;

	//! pointer variable
	/*!Points to CPointNormalGenerator class*/
	CPointNormalGenerator* m_NormalFlipper;

	//! int variable
	/*!The factor that decides positions of the additional generated points  in x direction (to shift the point to be placed exactly in the center of the boundary faces)*/
	float m_X_voxel_div;

	//! int variable
	/*!The factor that decides positions of the additional generated points  in y direction*/
	float m_Y_voxel_div;

	//! int variable
	/*!The factor that decides positions of the additional generated points  in z direction*/
	float m_Z_voxel_div;

	//! float variable
	/*!The front weight matrix Wx inorder to generate normal(as suggested in Tiede [Tie99])*/
	float WxFilterFront[3][3];

	//! float variable
	/*!The middle weight matrix Wx to generate normal*/
	float WxFilterMiddle[3][3];

	//! float variable
	/*!The back weight matrix Wx to generate normal*/
	float WxFilterBack[3][3];

	//! float variable
	/*!The front weight matrix Wy to generate normal*/
	float WyFilterFront[3][3];

	//! float variable
	/*!The middle weight matrix Wy to generate normal*/
	float WyFilterMiddle[3][3];

	//! float variable
	/*!The back weight matrix Wy to generate normal*/
	float WyFilterBack[3][3];

	//! float variable
	/*!The front weight matrix Wz to generate normal*/
	float WzFilterFront[3][3];

	//! float variable
	/*!The middle weight matrix Wz to generate normal*/
	float WzFilterMiddle[3][3];

	//! float variable
	/*!The back weight matrix Wz to generate normal*/
	float WzFilterBack[3][3];

	//! float variable
	/*!The front layer matrix generated using 26 neighbors of the voxel/subvox inorder to calculate normals of the points placed for it*/
	Point matrixInfront[3][3];

	//! float variable
	/*!The middle layer matrix generated using 26 neighbors of the voxel/subvox inorder to calculate normals of the points placed for it*/
	Point matrixMiddle[3][3];

	//! float variable
	/*!The back layer matrix generated using 26 neighbors of the voxel/subvox inorder to calculate normals of the points placed for it*/
	Point matrixBack[3][3];

	//! float variable
	/*!The x cordinate of the viewpoint towards which the normal is to be flipped*/
	float m_viewpoint_x;

	//! float variable
	/*!The y cordinate of the viewpoint towards which the normal is to be flipped*/
	float m_viewpoint_y;

	//! float variable
	/*!The z cordinate of the viewpoint towards which the normal is to be flipped*/
	float m_viewpoint_z;

private:
	//! PlacePoints: As per first step of algorithm  one point is placed where one object vox/subvox is encountered in the peer of candiadate vox/subvox
	/*!
	\param[in] neighbour the direction where the object vox/subvox is encountered in the neighborhood like right,left etc
	\param[in] x cordinate of the candidate vox/subvox
	\param[in] y cordinate of the candidate vox/subvox
	\param[in] z cordinate of the candidate vox/subvox
	*/
	void PlacePoints(int neighbour, float x, float y, float z );

	//! PlacePoints :As per second step of algorithm  two points are placed where two object vox/subvox encounter in opposite sides of candidate voxel or subvoxel
	/*!
	\param[in] nbr1 the direction where the first object vox/subvox is encountered in the neighborhood like right,left etc
	\param[in] nbr2 the direction where the second object vox/subvox is encountered in the neighborhood like right,left etc
	\param[in] x cordinate of the candidate vox/subvox
	\param[in] y cordinate of the candidate vox/subvox
	\param[in] z cordinate of the candidate vox/subvox
	*/
	void PlacePoints(int nbr1, int nbr2, float x, float y,float z);

	//! PlacePoints: As per third step of algorithm four points are placed where four object vox/subvox are encountered in the neighborhood of vox/subvox
	/*!
	\param[in] nbr1 the direction where the first object vox/subvox is encountered in the neighborhood like right,left etc
	\param[in] nbr2 the direction where the second object vox/subvox is encountered in the neighborhood 
	\param[in] nbr3 the direction where the third object vox/subvox is encountered in the neighborhood 
	\param[in] nbr4 the direction where the fourth object vox/subvox is encountered in the neighborhood
	\param[in] x cordinate of the candidate vox/subvox
	\param[in] y cordinate of the candidate vox/subvox
	\param[in] z cordinate of the candidate vox/subvox
	*/
	void PlacePoints(int nbr1,int  nbr2,int nbr3,int  nbr4, float x, float y, float z);

	//! PlacePointsForFive: As per fourth step of algorithm one point is placed where five object vox/subvox are encountered in the neighborhood of vox/subvox
	/*!
	\param[in] notNeighbor the direction where the neighbor is not an object among 3D-6 neighbors of the candidate vox/subvox
	\param[in] x cordinate of the candidate vox/subvox
	\param[in] y cordinate of the candidate vox/subvox
	\param[in] z cordinate of the candidate vox/subvox
	*/
	void PlacePointsForFive(int notNeighbor, float x , float y , float z);

	//! AddPoint The computed points are added to the point list
	/*!
	\param[in] x cordinate of the point to be placed(ie,generated)
	\param[in] y cordinate of the point to be placed(ie,generated)
	\param[in] z cordinate of the point to be placed(ie,generated)
	*/
	void AddPoint(float x, float y, float z);

	//! Create26NEighbourMatrices: Generates neighbour matrix for the generation of normal using 26 neighbours of a voxel or subvoxel
	/*!
	\param[in] x cordinate of the candidate vox/subvox
	\param[in] y cordinate of the candidate vox/subvox
	\param[in] z cordinate of the candidate vox/subvox
	*/
	void Create26NEighbourMatrices(float x,float y,float z);

	//! CheckIfDifferentStructure:checks if the considering voxels in the opposite directions are connected or not
	/*!
	\param[in] direction the opposite directions where the voxels/subvoxels to be checked if belonging to single structure reside like Top_bottom,left_right etc
	\param[in] outBoundVoxel the candidate vox/subvox having object neighbors in opposite directions that need to be checked if belnging to different structures 
	\returns true if the neigbors in opposite directions are belonging to different structures(ie, disconnected) or else false
	*/
	bool CheckIfDifferentStructure(int direction, Point &outBoundVoxel);

	//! GetNormalForMorePoints: In the case more points than one is  generated for an outbound vox/subvox, normal for each point is computed based on gradient
	/*!
	\param[in] placed_x x cordinate value of the point generated newly for the candidate outbound vox/subvox
	\param[in] placed_y y cordinate value of the point generated newly for the candidate outbound vox/subvox
	\param[in] placed_z z cordinate value of the point generated newly for the candidate outbound vox/subvox
	\param[in] direction the direction of the point placed to take relative points to calculate gradient
	*/
	void GetNormalForMorePoints(float placed_x,float placed_y,float placed_z,int direction);

	//! CreateWxFilter: Generates The Weight matrix in the x direction for the generation of normal as per Tiede [Tie99]
	void CreateWxFilter();

	//! CreateWyFilter: Generates The Weight matrix in the y direction for the generation of normal as per Tiede [Tie99]
	void CreateWyFilter();

	//! CreateWzFilter: generates The Weight matrix in the z direction for the generation of normal as per Tiede [Tie99]
	void CreateWzFilter();

	//! ClearPointList: Clears the lists associated with point and normal generation  
	void ClearPointList();

	//! AddNormal:The computed normals are added to the normal list
	/*!
	\param[in] x cordinate of the normal generated
	\param[in] y cordinate of the normal generated
	\param[in] z cordinate of the normal generated
	*/
	void AddNormal(float x,float y,float z);

	//! Filter:Calculate partial derivative using weight matrix and neighbor matrix for generating normal for one point placing case
	/*!
	\param[in] matrix the matrix generated with 3D 26 neighbours of the voxel like matrixInfront ,matrixmiddle etc
	\param[in] filterSelector the flag that identifies which weight matrix to be used to multiply with the above matrix of neighbors like WyFilterFront
	*/
	float Filter(Point matrix[3][3],int filterSelector);

	//! SetViewPoint: Sets the viewpoint towards which the normal to be flipped
	/*!
	\param[in] x cordinate of the viewpoint
	\param[in] y cordinate of the viewpoint
	\param[in] z cordinate of the viewpoint
	*/
	void SetViewPoint(float x,float y,float z);
	
	//! GetNormalForOnePoint:In the case where one point is generated, normal is computed using weight matrix (pls refer: Tiede, U.: Realistische 3D-Visualisierung multiattributierter und multiparametrischer,Volumendaten, Universitšt Hamburg, Fachbereich Informatik,Diss., 1999")
	/*!
	\param[in] x cordinate of the point generated
	\param[in] y cordinate of the point generated
	\param[in] z cordinate of the point generated
	*/
	void GetNormalForOnePoint(float x,float y,float z);

public:

	//! Constructor of CThreeDDataProcessor class
	CThreeDDataProcessor(void);

	//! Constructor
	CThreeDDataProcessor(CDataManager* dataManager);

	//! Destructor of CThreeDDataProcessor class
	~CThreeDDataProcessor(void);

	//! SetVoxelDivisionFactor: Calculate voxel divison factor to identify position of points to be placed
	void SetVoxelDivisionFactor();

	//! GeneratePointsUsingAlgm:Implements Schumann's six step algorithm for voxel as well as sub voxel
	/*!
	\param[in] x cordinate of the out bound vox/subvox
	\param[in] y cordinate of the out bound vox/subvox
	\param[in] z cordinate of the out bound vox/subvox
	\returns true if any point is placed according to the algorithm for the vox/subvox or else false.
	*/
	bool GeneratePointsUsingAlgm(float x,float y,float z);

	//! SetListForPointGen: Sets the list to check if a subvox is object in the point generation for subvoxels
	/*!
	\param[in] subvoxelListForPointGen vector of the object subvoxels generated using 3D-6 neighbors of the outbound subvox to check if a arbitary subvoxel is object or not later
	*/
	void SetListForPointGen(vector<Point> subvoxelListForPointGen);

	//! SetListForNormalGen: Sets the list to check if a subvox is object in the normal generation for subvoxels
	/*!
	\param[in] subvoxelListForNormalGen vector of the object subvoxels generated using 3D-26 neighbors of the outbound subvox to check if a arbitary subvoxel is object or not later     
	*/
	void SetListForNormalGen( vector<Point> subvoxelListForNormalGen);

	//! SeIsSubvox: sets flag to true to identify sub voxel
	void SeIsSubvox();
	
	//! SetDivParams: Sets the parameters required to calculate neighbors and divison factors
	/*!
	\param[in] divFactor the division factor that helps in identifying position of the points to be placed on boundary faces
	\param[in] neighborFactor the flag that identifies between neighbor of voxel or subvoxel
	*/
	void SetDivParams(int divFactor,float neighborFactor);

	//! SeIsSubvox: resets flag to falag to identify it is not a sub voxel
	void ResetIsOutBoundSubvox();

	//! GetPointsFromAlgm populates the vector of points generated after 6 step implementaion for each outer boundary vox/subvox
	/*!
	\param[in] _PointsFromAlgm vector of the generated points  which is populated afer implementing algorithm
	*/
	void  GetPointsFromAlgm(vector<Point>& _PointsFromAlgm );

	//! GetNormalsOfPoints populates the vector of normals generated after 6 step implementaion for each outer boundary vox/subvox
	/*!
	\param[in] _normalsOfPoints vector of the generated normals that is populated
	*/
	void GetNormalsOfPoints(vector<Point>& _normalsOfPoints);

	//! GetNeighbourList Return the respective neighboulist(can be 3D-6, 3D-18 or 3D-26)
	vector<Point> GetNeighbourList();

	//! Calculate3D_6Neighbours:Calculate three D six neighbours of a voxel or subvoxel
	/*!
	\param[in] x cordinate of the vox/subvox for which 3D 6 neighbors are calculated
	\param[in] y cordinate of the vox/subvox for which 3D 6 neighbors are calculated
	\param[in] z cordinate of the vox/subvox for which 3D 6 neighbors are calculated
	\returns the respective neighboulist.
	*/
	void Calculate3D_6Neighbours(float x,float y,float z);

	//! Calculate3D_18Neighbours:Calculate three D eighteen neighbours of a voxel or subvoxel
	/*!
	\param[in] x cordinate of the vox/subvox for which 3D 18 neighbors are calculated
	\param[in] y cordinate of the vox/subvox for which 3D 18 neighbors are calculated
	\param[in] z cordinate of the vox/subvox for which 3D 18 neighbors are calculated
	\returns the respective neighboulist.
	*/
	void Calculate3D_18Neighbours(float x,float y,float z);

	//! Calculate3D_26Neighbours:Calculate three D twentySix neighbours of a voxel or subvoxel
	/*!
	\param[in] x cordinate of the vox/subvox for which 3D 26 neighbors are calculated
	\param[in] y cordinate of the vox/subvox for which 3D 26 neighbors are calculated
	\param[in] z cordinate of the vox/subvox for which 3D 26 neighbors are calculated
	\returns the respective neighboulist.
	*/
	void Calculate3D_26Neighbours(float x,float y,float z);

	//! ClearNeighbourList: clears the lists associated with generating thin points and normals
	void ClearNeighbourList();

	//! ClearBuffers: calls clearpointlist and clearbuffers to clear all the related lists and buffers associated with point and normal calculation
	void ClearBuffers();
};

} // namespace polygonization
} // namespace lssplib

