#pragma once

#include "TriangleData.h"

namespace lssplib
{
namespace polygonization
{

//! @brief 隣接関係を保有する三角形データを構成するクラス。
class TrianglesWithLocalConnectivity
{
public:
	TrianglesWithLocalConnectivity();

	virtual~TrianglesWithLocalConnectivity();

	void MakeTrianglesWithLocalConnectivity(const std::vector<TriangleVertexIndex>& inputTriangles,
		const std::vector<VertexPosition>& inputVertices);

	//! @brief 頂点データ全体を取得する。
	//! 
	//! @return 頂点データを要素とする配列への参照
	const std::vector<VertexData>& GetVertexData() const {return m_vertexData;};

	//! @brief 辺データ全体を取得する。
	//! 
	//! @return 辺データを要素とする配列
	const std::vector<EdgeData>& GetEdgeData() const {return m_edgeData;};

	//! @brief 三角形データ全体を取得する。
	//! 
	//! @return 三角形データを要素とする配列
	const std::vector<TriangleData>& GetTriangleData() const {return m_triangleData;};

	//! @brief 三角形データ全体
	//! 
	//! @return 三角形データを要素とする配列
	std::vector<TriangleData>& GetTriangleData() {return m_triangleData;};

private:
	void AddVertexPairToEdgeData_IfItHasNotBeenResgisteredAsEdge(const size_t* currentPairOfVertices,
		const size_t triangleIndex,
		const size_t edgeLocation);

	const bool HasPairOfVerticesBeenUsed(const size_t* edge) const;

	const bool IsEdgeEqualToCurrentPairOfVertices(const size_t* currentEdge,
		const size_t edgeIndex) const;

	void InputDataByEdge(const size_t* currentPairOfVertices,
		const size_t triangleIndex,
		const size_t edgeLocation);

	void InputDataByTriangle(const size_t* triangle, const size_t triangleIndex);

	void InputPositionOfVertexIntoTriangleData(const TriangleVertexIndex& inputTriangle,
		const std::vector<VertexPosition>& inputVertices);

	void InputTrueIntoFlagsIndicatingUsedOrNot(const size_t* triangle);

	std::vector<TriangleData> m_triangleData; //!< 三角形データの全体

	std::vector<EdgeData> m_edgeData; //!< エッジデータの全体

	std::vector<VertexData> m_vertexData; //!< 頂点データの全体
};

} // namespace polygonization
} // namespace lssplib

