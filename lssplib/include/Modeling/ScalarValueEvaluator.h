#pragma once

#include "BloomenthalPolygonizer.h"
#include "SourceIndexHolder.h"
#include "SourceLineSegment.h"
#include "SourceIndexCellGenerator.h"

namespace lssplib
{
namespace polygonization
{

//! @brief 各セルのインデックスを保持する構造体
// struct CellIndex
// {
// 	int xIndex; //!< インデックスの x 成分
// 	int yIndex; //!< インデックスの y 成分
// 	int zIndex; //!< インデックスの z 成分
// 
// 	//! @brief コンストラクタ
// 	CellIndex()
// 	{
// 		xIndex = INT_MAX;
// 		yIndex = INT_MAX;
// 		zIndex = INT_MAX;
// 	}
// };

//! @brief スカラー分布を計算するクラス。与えられた座標に応じて、スカラー値を出力する。
class ScalarValueEvaluator:public ImplicitFunction
{
public:
	LSSPLIB_API ScalarValueEvaluator();

	LSSPLIB_API virtual ~ScalarValueEvaluator();

	float value(float x, float y, float z);

	LSSPLIB_API void SetLineSegments(const std::vector<SourceLineSegment>& lineSegments);

	LSSPLIB_API void SetGaussianIntegralTable(const std::vector<double>& table);

	LSSPLIB_API void SetCells(SourceIndexHolder**** sourceIndexCells);

	//! @brief セルの大きさをメンバ変数にセットする。
	//! 
	//! @param[in] cellScale セルの大きさ
	void SetCellScale(const float cellScale){m_cellScale = cellScale;}

//	LSSPLIB_API void SetBoundingBoxEnclosingAllLineSegments(const float* boundingBox);
	LSSPLIB_API void SetBoundingBoxEnclosingAllLineSegments(const Boxf& boundingBox);

private:
//	const float ComputeDistanceFilter(const unsigned int lineSegmentIndex, const float x, const float y, const float z) const;
	float ComputeDistanceFilter(const SourceLineSegment& lineSegment, const Point3f& srcPoint) const;

//	const float ComputeDistanceFromPointToLineOnLineSegment(const unsigned int lineSegmentIndex, const float x, const float y, const float z) const;
//	float ComputeDistanceFromPointToLineOnLineSegment(const SourceLineSegment& lineSegment, const Point3f& srcPoint) const;

	void ComputeDistancesFromProjectionToLineSegmentEnd(const SourceLineSegment& lineSegment, 
		const Point3f& srcPoint,
		float& distanceFromProjectionToOneEnd,
		float& distanceFromProjectionToOtherEnd) const;
	//void ComputeDistancesFromProjectionToLineSegmentEnd(const unsigned int lineSegmentIndex, 
	//	const float x, const float y, const float z,
	//	float& distanceFromProjectionToOneEnd,
	//	float& distanceFromProjectionToOtherEnd) const;

	float ComputeIntegralFilter(const SourceLineSegment& lineSegment, const Point3f& srcPoint) const;
//	const float ComputeIntegralFilter(const unsigned int lineSegmentIndex, const float x, const float y, const float z) const;

	void ComputeIntegralRange(const SourceLineSegment& lineSegment, 
		const Point3f& srcPoint,
		float& lowerBoundOfIntegralRange, float& upperBoundOfIntegralRange) const;
	//void ComputeIntegralRange(const unsigned int lineSegmentIndex, 
	//	const float x, const float y, const float z, 
	//	float& lowerBoundOfIntegralRange, float& upperBoundOfIntegralRange) const;

	void ComputePointProjectionOnLine(const Point3f& pointOnLine, const Vector3f& pointVector, 
		const Point3f& srcPoint,
		Point3f& projectionOfTargetPointOnLine) const;
	//void ComputePointProjectionOnLine(const float* pointOnLine, const float* pointVector, 
	//	const float x, const float y, const float z, 
	//	float* projectionOfTargetPointOnLine) const;

	//const float ComputeScalarValueFromAllContributingLineSegments(const std::vector<unsigned int>& contributingLineSegments,
	//	const float x, const float y, const float z) const;
	float ComputeScalarValueFromAllContributingLineSegments(
		const std::vector<unsigned int>& contributingLineSegments,
		const Point3f& srcPoint) const;

	//const float ComputeScalarValueFromSingleLineSegment(const unsigned int lineSegmentIndex, 
	//	const float x, const float y, const float z) const;
	float ComputeScalarValueFromSingleLineSegment(
		const SourceLineSegment& lineSegment,
		const Point3f& srcPoint) const;

//	const float EstimateLineSegmentThicknessAtPoint(const unsigned int lineSegmentIndex, const float x, const float y, const float z) const;
	float EstimateLineSegmentThicknessAtPoint(const SourceLineSegment& lineSegment, const Point3f& srcPoint) const;

	float GetGaussianIntegral(const float integralEndPoint) const;

//	void GetIndicesOfContributingLineSegment(const CellIndex& cellIndex,
//		std::vector<unsigned int>& lineSegmentIndices) const;
	void GetIndicesOfContributingLineSegment(const Point3i& cellIndex,
		std::vector<unsigned int>& lineSegmentIndices) const;

//	void GetCellContainingGivenPoint(const float x, const float y, const float z, CellIndex& cellIndex) const;
	void GetCellContainingGivenPoint(const Point3f& srcPoint, Point3i& cellIndex) const;

//	const bool IsPointInsideOfBoundingBox(const float x, const float y, const float z) const;
	bool IsPointInsideOfBoundingBox(const Point3f& srcPoint) const;

	const std::vector<double>* m_integralLookUpTable; //!< ガウス積分の結果を保持する積分テーブルへのポインタ

	SourceIndexHolder**** m_sourceIndexCells; //!< 三次元セルの先頭の要素へのポインタ。このポインタから三次元セルの各要素へアクセスすることができる。

	const std::vector<SourceLineSegment>* m_lineSegments; //!< 線分データへのポインタ

	float m_cellScale; //!< セルの大きさ

	Boxf m_boundingBox; //!< 線分データの境界。線分データを包含するバウンディングボックス。
//	float m_boundingBox[6]; //!< 線分データの境界。線分データを包含するバウンディングボックス。
};

} // namespace polygonization
} // namespace lssplib

