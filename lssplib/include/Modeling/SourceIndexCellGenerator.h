#pragma once

#include "SourceLineSegment.h"
#include "../BaseContainers.h"
#include "../lssplib.h"
#include <vector>

namespace lssplib
{
namespace polygonization
{

class SourceIndexHolder;

//! @brief セルデータを生成するクラス。各セルは近傍の線分データインデックスを所有する。
class SourceIndexCellGenerator
{
public:
	LSSPLIB_API SourceIndexCellGenerator();

	LSSPLIB_API virtual ~SourceIndexCellGenerator();

	void DeleteCells();

	LSSPLIB_API void GenerateCells();

	//! @brief 各方向のセル数を取得する。
	//! 
	//! @return 各方向のセル数
	const Size3i GetArraySize() const { return m_cellArraySize; }

	//! @brief 線分データ全体のバウンディングボックスを取得する。
	//! 
	//! @return バウンディングボックス
	const Boxf& GetBoundingBox() const { return m_boudningBox; }

	//! @brief セルの一辺の長さを取得する。
	//! 
	//! @return 一辺の長さ
	const float GetCellScale() const {return m_cellScale;}

	//! @brief セルデータへのポインタを取得する。
	//! 
	//! @return セルデータへのポインタ
	SourceIndexHolder**** GetCells() const {return m_sourceIndexCells;}

	LSSPLIB_API void Reset();

	//! @brief セルの一辺の長さをメンバ変数にセットする。
	//! 
	//! @param[in] cellScale セルの一辺の長さ
	void SetCellScale(const float cellScale){m_cellScale = cellScale;}

	//! @brief 線分データをセットする。
	//! 
	//! @param[in] lineSegments 線分データ
	void SetLineSegments(const std::vector<SourceLineSegment>& lineSegments){m_lineSegments = &(lineSegments);}

private:
	void AllocateCells();

	void ComputeBoundingBoxEnclosingAllLineSegments();

	void ExtendBoundingBox();

	const float GetMaximumCylinderRadius() const;

	void InitializeCells();

	const bool IsCellInsideCylinder(const unsigned int lineSegmentIndex, 
		const unsigned int cellIndexX, const unsigned int cellIndexY, unsigned int cellIndexZ) const;

	void RegisterSingleLineSegmentToCells(const unsigned int lineSegmentIndex, const Point3i& minIdx, const Point3i& maxIdx) const;

	void RegisterAllLineSegmentsToCells();

	void SetCellArraySize();

//	void output(); // テスト用

//	void outputMultiContributedVoxels(); // テスト用

//	void ouputVoxelsAroundSingleEdge(unsigned int lineSegmentIndex); // テスト用

	const std::vector<SourceLineSegment>* m_lineSegments; //!< 線分データへのポインタ

	SourceIndexHolder**** m_sourceIndexCells; //!< セルデータへのポインタ

	Boxf m_boudningBox; //!< 線分データ全体のバウンディングボックス

	Size3i m_cellArraySize; //!< 各方向のセル数。

	float m_cellScale; //!< セルの一辺の長さ
};

} // namespace polygonization
} // namespace lssplib

