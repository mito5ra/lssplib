#pragma once

#include <vector>
#include "../lssplib.h"
#include "ScalarValueEvaluator.h"
#include "GaussianIntegralTableGenerator.h"
#include "../BaseContainers.h"

namespace lssplib
{

namespace segmentation
{
	class VesselTree;
} // namespace segmentation

namespace polygonization
{

struct TriangleVertexIndex;
struct VertexPosition;
class BloomenthalPolygonizer;

//! @brief 血管のポリゴンモデルを生成するクラス。
class PolygonFromVesselTree
{
public:
	//! ポリゴン生成処理のパラメータ保持する構造体
	struct Params
	{
		int timesToSmooth; //!< スムーズ処理の回数
		double maxRadiusAtEnd; //!< 末端の最大半径
		int maxNodesFromBranchToEnd; //!< 分岐から末端の最大ノードの数
		double maxRadiusAtBranchToEnd; //!< 分岐から末端の最大半径
		double cubeSize; //!< ポリゴン生成するときのセルのサイズ
		double boundsCoeff; //!< the limit to how far away the polygonizer will look for components of the implicit surface

		//! コンストラクタ
		Params(int _timesToSmooth,
			double _maxRadiusAtEnd,
			int _maxNodesFromBranchToEnd,
			double _maxRadiusAtBranchToEnd,
			double _cubeSize,
			double _boundsCoeff)
			: timesToSmooth(_timesToSmooth)
			, maxRadiusAtEnd(_maxRadiusAtEnd)
			, maxNodesFromBranchToEnd(_maxNodesFromBranchToEnd)
			, maxRadiusAtBranchToEnd(_maxRadiusAtBranchToEnd)
			, cubeSize(_cubeSize)
			, boundsCoeff(_boundsCoeff)
		{
		}
	};

	//! 生成処理の結果の列挙型
	enum ErrorStatus
	{
		kErrorStatus_NoError = 0, //!< エラー無し
		kErrorStatus_SomePortionFailed, //!< 一部のモデルの生成に失敗
		kErrorStatus_Failed //!< モデル生成に失敗
	};

	//! @brief コンストラクタ
	//!
	//! @param[in] voxelSize ボクセルのサイズ
	//! @param[in] params 処理のために必要なパラメータ
	LSSPLIB_API PolygonFromVesselTree(const Size3d& voxelSize, const Params& params);

	//! @brief デストラクタ
	LSSPLIB_API ~PolygonFromVesselTree();

	LSSPLIB_API ErrorStatus Generate(const std::vector<const segmentation::VesselTree*>& vesselTrees, std::vector<TriangleVertexIndex>& triangles, std::vector<VertexPosition>& vertices);

private:
	void MergeIntoSingleGroupOfPolygons(const BloomenthalPolygonizer* polygonizer, std::vector<VertexPosition>& vertexPositions, std::vector<TriangleVertexIndex>& triangles);

	void RemoveOutlyingFaces(const std::vector<VertexPosition>& vertexPositions, std::vector<TriangleVertexIndex>& triangles) const;

	void GenerateCells();

//	const float GetMinimumScaleOfVoxel() const;

	const bool Polygonize(std::vector<VertexPosition>& vertexPositions, std::vector<TriangleVertexIndex>& triangles);

	void SetParametersToScalarValueEvaluator(ScalarValueEvaluator& valueEvaluator) const;

	std::vector<SourceLineSegment> m_sourceLineSegments; //!< 線分データ

	SourceIndexCellGenerator m_cellGenerator; //!< セルデータを生成するオブジェクト

	GaussianIntegralTableGenerator m_integralTableGenerator; //!< 積分テーブルを生成するオブジェクト

	Size3d m_voxelSize; //!< ボクセルのサイズ
	Params m_params; //!< 生成するためのパラメータ
};

} // namespace polygonization
} // namespace lssplib
