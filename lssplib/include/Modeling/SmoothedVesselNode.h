#pragma once

#include <vector>

#include "../BaseContainers.h"

namespace lssplib
{
namespace polygonization
{

//! @brief 血管の芯線データ(親子関係を持つ node)を保持するクラス。芯線を編集するときに使われる。
class SmoothedVesselNode
{
public:
	class Iterator
	{
	public:
		Iterator(): m_nodeStack(), m_currentNode(NULL) {};

		Iterator(SmoothedVesselNode* node): m_nodeStack(), m_currentNode(NULL)
		{
			m_nodeStack.push(node);
		}

		Iterator(const Iterator& src)
		{
			m_nodeStack = src.m_nodeStack;
			m_currentNode = src.m_currentNode;
		}

		~Iterator() {}

		Iterator& operator=(const Iterator& src)
		{
			m_nodeStack = src.m_nodeStack;
			m_currentNode = src.m_currentNode;
			return *this;
		}

		SmoothedVesselNode* Next()
		{
			if (m_nodeStack.empty())
			{
				m_currentNode = NULL;
				return NULL;
			}

			m_currentNode = m_nodeStack.top();
			m_nodeStack.pop();
			const std::vector<SmoothedVesselNode*>& children = m_currentNode->GetChildren();
			for (size_t i = 0; i < children.size(); ++i)
			{
				m_nodeStack.push(children[i]);
			}

			return m_currentNode;
		}



		SmoothedVesselNode* Get() { return m_currentNode; }

		const SmoothedVesselNode* Get() const { return m_currentNode; }

	private:
		std::stack<SmoothedVesselNode*> m_nodeStack;
		SmoothedVesselNode* m_currentNode;
	};

	class ConstIterator
	{
	public:
		ConstIterator(): m_nodeStack(), m_currentNode(NULL) {};

		ConstIterator(const SmoothedVesselNode* node): m_nodeStack(), m_currentNode(NULL)
		{
			m_nodeStack.push(node);
		}

		ConstIterator(const ConstIterator& src)
		{
			m_nodeStack = src.m_nodeStack;
			m_currentNode = src.m_currentNode;
		}

		~ConstIterator() {}

		ConstIterator& operator=(const ConstIterator& src)
		{
			m_nodeStack = src.m_nodeStack;
			m_currentNode = src.m_currentNode;
			return *this;
		}

		const SmoothedVesselNode* Next()
		{
			if (m_nodeStack.empty())
			{
				m_currentNode = NULL;
				return NULL;
			}

			m_currentNode = m_nodeStack.top();
			m_nodeStack.pop();
			const std::vector<SmoothedVesselNode*>& children = m_currentNode->GetChildren();
			for (size_t i = 0; i < children.size(); ++i)
			{
				m_nodeStack.push(children[i]);
			}

			return m_currentNode;
		}

		const SmoothedVesselNode* Get() const { return m_currentNode; }

	private:
		std::stack<const SmoothedVesselNode*> m_nodeStack;
		const SmoothedVesselNode* m_currentNode;
	};

	SmoothedVesselNode();

	SmoothedVesselNode(const SmoothedVesselNode& src);

	virtual ~SmoothedVesselNode();

	//! @brief 子ポインタを追加する。
	//! 
	//! @param[in] child 子ポインタ
	void AddChild(SmoothedVesselNode* child){m_children.push_back(child);}

	//! @brief 子ポインタ全体を取得する。
	//! 
	//! @return 子ポインタ配列への参照
	const std::vector<SmoothedVesselNode*>& GetChildren() const {return m_children;}

	//! @brief 分岐レベル取得する。
	const unsigned short GetLevel() const {return m_branchLevel;}

	//! @brief 親ポインタを取得する。
	//! 
	//! @return 親ポインタ
	const SmoothedVesselNode* GetParent() const {return m_parent;}

	//! @brief 親ポインタを取得する。
	//! 
	//! @return 親ポインタ
	SmoothedVesselNode* GetParent() {return m_parent;}

	//! @brief node の位置を取得する。
	//! 
	//! @return node 位置へのポインタ
//	const float* GetPosition()const{return m_position;}
	const Point3f& GetPosition() const {return m_position;}

	//! @brief node に置かれている半径を取得する。
	//! 
	//! @return node の半径
	const float& GetRadius() const {return m_radius;}

	//! @brief 分岐レベルをセットする。
	//!
	//! @param[in] level 分岐レベル
	void SetBranchLevel(unsigned short level){m_branchLevel = level;}

	//! @brief node に半径をセットする。
	//! 
	//! @param[in] radius 半径
	void SetRadius(float radius){m_radius = radius;}

	//! @brief 親ポインタをセットする。
	//! 
	//! @param[in] parent 親ポインタ
	void SetParent(SmoothedVesselNode* parent){m_parent = parent;}

	//! @brief node に位置をセットする。
	//! 
	//! @param[in] position 位置
// 	void SetPosition(const float* position)
// 	{
// 		m_position[0] = position[0]; 
// 		m_position[1] = position[1]; 
// 		m_position[2] = position[2];
// 	}
	void SetPosition(const Point3f& position)
	{
		m_position = position;
	}

	SmoothedVesselNode* NextJunction();

	const SmoothedVesselNode* NextJunction() const;

	Iterator Begin()
	{
		return Iterator(this);
	}

	const ConstIterator Begin() const
	{
		return ConstIterator(this);
	}

private:
	SmoothedVesselNode* m_parent; //!< 親ポインタ

	std::vector<SmoothedVesselNode*> m_children; //!< 子ポインタ全体

//	float m_position[3]; //!< node の位置
	Point3f m_position; //!< node の位置

	float m_radius; //!< node が保持する半径データ。血管の太さを定義する材料。

	unsigned short m_branchLevel; //!< 末端からルートに向けて数えられた分岐の数。分岐レベル。

};

} // namespace polygonization
} // namespace lssplib

