#pragma once

#include <vector>

#include "../lssplib.h"
#include "../BaseContainers.h"
#include "TriangleData.h"

namespace lssplib
{
namespace polygonization
{

class BloomenthalPolygonizer;
class PointSet;
struct TriangleVertexIndex;
struct VertexPosition;
struct Point;
class CDataManager;
class ImplicitPOU;

//! @brief マスクデータからポリゴンモデルを生成するクラス
class PolygonModelGenerator
{
public:
	typedef std::vector<TriangleVertexIndex> Triangles; //!< 三角形データ配列型
	typedef std::vector<VertexPosition> Vertices; //!< 頂点配列型
	typedef std::vector<Point> PointList; //!< Pointの配列型
	typedef std::vector<Point> NormalList; //!< Pointの配列型

	//! @brief ポリゴン化処理パラメータ設定オブジェクト
	class LSSPLIB_API PolygonizerParameterSetter
	{
	public:
		//! @brief コンストラクタ
		PolygonizerParameterSetter();

		//! @brief デストラクタ
		virtual ~PolygonizerParameterSetter();

		//! @brief オペーレータ(陰関数オブジェクトにパラメータを設定する)
		//! @param [in] numPoints Point Cloudの点の数
		//! @param [in] min Point Cloudのバウンディングボックスの角の位置
		//! @param [in] max Point Cloudのバウンディングボックスの角の位置
		//! @param [out] func 陰関数オブジェクト
		virtual void operator()(const int numPoints, const float* min, const float* max, lssplib::polygonization::ImplicitPOU& func) const;
	private:
	};

	//! @brief コンストラクタ
	LSSPLIB_API PolygonModelGenerator();

	//! @brief コンストラクタ
	//! @param [in] paramSetter ポリゴン化処理パラメータ設定オブジェクト
	LSSPLIB_API PolygonModelGenerator(PolygonizerParameterSetter* paramSetter);

	//! @brief デストラクタ
	LSSPLIB_API ~PolygonModelGenerator();

	//! @brief ボクセルの大きさを登録する
	LSSPLIB_API void SetVoxelSpacing(const Size3f& voxelSpacing);

	//! @brief BloomenthalPolygonizerのパラメータ設定
	//! @param [in] cellSize the size of the polygonizing cell
	//! @param [in] boundsCoeff the limit to how far away the polygonizer look for components of the implicit surface
	LSSPLIB_API void SetPolygonizerParameters(float cellSize, float boundsCoeff);

	//! @brief マスクデータからポリゴンモデルを生成する
	LSSPLIB_API bool Generate(const Size3i& bufferSize, const unsigned char* buffer, Triangles& triangles, Vertices& vertices, bool enableThinPointBuilder = true );

private:

	//! @brief マスクデータから点群データを生成する
	void CreatePointCloud(const Size3i& bufferSize, const unsigned char* buffer, PointSet& pointSet, bool enableThinPointBuilder ) const;

	void FillPointSet(const PointList& points, const NormalList& normals, PointSet& pointSet) const;

	bool PolygonizePointCloud(PointSet& pointSet, Triangles& triangles, Vertices& vertices) const;

	void SetTrianglesFromPolygonizer(
		const BloomenthalPolygonizer& polygonizer,
		Triangles& triangles, 
		Vertices& vertexPositions) const;

	Size3f m_voxelSpacing; //!< ボクセルの大きさ(縦, 横, 高さ)
	float m_cellSize; //!< size of the polygonizing cell
	float m_boundsCoeff; //!< the limit to how far away we will look for components of the implicit surface
	PolygonizerParameterSetter* m_paramSetter; //!< 陰関数パラメータ設定オブジェクト
};

} // polygonization
} // lssplib