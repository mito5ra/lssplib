#pragma once

#include "../BaseContainers.h"
#include "../lssplib.h"

namespace lssplib
{
namespace polygonization
{

const float kTolerance = 1e-6f;

//! @brief 線分データを保持するクラス
class SourceLineSegment
{
public:
	LSSPLIB_API SourceLineSegment();

	LSSPLIB_API SourceLineSegment(
		const Point3f& p0,
		const Point3f& p1,
		float thickness0,
		float thickness1);

	LSSPLIB_API ~SourceLineSegment();

	const float& GetBoundingCylinderRadius() const;

	const float& GetLineSegmentLength() const;

	LSSPLIB_API const Size2f& GetThickness() const;

	LSSPLIB_API const Point3f& GetEndPosition(unsigned int idx) const;

	const Vector3f& GetLineSegmentVector() const;

	const Vector3f& GetNormalizedLineSegmentVector() const;

	LSSPLIB_API void SetEndPositions(const Point3f& p0, const Point3f& p1);

	LSSPLIB_API void SetThickness(const float& thickness0, const float& thickness1);

	float ComputeDistanceFromPoint(const Point3f& p) const;

	Point3f ComputePointOfProjection(const Point3f& p) const;

private:
	void MakeNormalizedVector();

	Point3f m_p0; //! 線分の始点の位置

	Point3f m_p1; //! 線分の終点の位置

	Vector3f m_lineSegmentVector;

	Vector3f m_normalizedLineSegmentVector; //!< 規格化された方向ベクトル

	float m_boundingCylinderRadius; //!< 円筒の半径。線分は、自分の近傍領域を定義する円筒を保持する。

	float m_length; //!< 線分の長さ

	Size2f m_thickness; //!< 線分のそれぞれの端に与えられた太さ。血管モデルの太さを定義するときに使われる。
};

} // namespace polygonization
} // namespace lssplib

