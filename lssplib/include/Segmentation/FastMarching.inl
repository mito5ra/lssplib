#include <iostream>
#include <assert.h>

namespace lssplib
{
namespace segmentation
{

const double kTimeAtFarLabel = -1.0; //!< 未処理のピクセルの時刻は-1.0に設定。

template<typename DataType, unsigned int NDim>
FastMarching<DataType, NDim>::FastMarching()
: m_size()
, m_speed(NULL)
, m_constantSpeed(0.0)
, m_stopTime(0.0)
, m_stopPoint()
, m_timeMap()
, m_stateImage()
, m_seeds()
, m_spacing()
, m_invSquaredSpacing()
, m_heap()
, m_hasStopPoint(false)
, m_secondOrderEnabled(false)
{
	for (int i = 0; i < NDim; ++i)
	{
		m_size[i] = 0;
		m_spacing[i] = 1;
		m_invSquaredSpacing[i] = 1;
	}
}

template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::Clear()
{
	TimeMapType().swap(m_timeMap);
	PixelStateImageType().swap(m_stateImage);
	SeedPoints().swap(m_seeds);
	m_heap.Clear();
}

//! @param [in] pt シード点
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::AddSeed(const Point& pt)
{
	m_seeds.push_back(pt);
}

//! @param [in] sz バッファ(速度画像)の大きさ
//! @param [in] speedImage 速度画像データ。
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::SetSpeedImage(const Size& sz, const DataType* speedImage)
{
	m_size = sz;
	m_speed = speedImage;
}

//! @param [in] sz バッファの大きさ
//! @param [in] speed 伝搬速度
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::SetConstantSpeed( const Size& sz, DataType speed )
{
	m_size = sz;
	m_constantSpeed = speed;
	m_speed = NULL;
}

//! @param [in] stopTime 処理を終了させる時刻。
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::SetStoppingTime(DataType stopTime)
{
	m_stopTime = stopTime;
}

//! @param [in] stopPoint 処理を終了させる場所。
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::SetStopPoint(const Point& stopPoint)
{
	m_stopPoint = stopPoint;
	m_hasStopPoint = true;
}

template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::UnsetStopPoint()
{
	m_stopPoint = Point();
	m_hasStopPoint = false;
}

//! @param [in] spacing ピクセル間隔
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::SetSpacing(const Spacing& spacing)
{
	m_spacing = spacing;
	for (unsigned int i = 0; i < NDim; ++i)
	{
		m_invSquaredSpacing[i] = DataType(1.0)/(spacing[i]*spacing[i]);
	}
}

//! @return 到達時刻データ
template<typename DataType, unsigned int NDim>
inline const typename FastMarching<DataType, NDim>::TimeMapType&
FastMarching<DataType, NDim>::GetTimeMap() const
{ return m_timeMap; }

template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::Execute()
{
	Initialize();

	while (!m_heap.IsEmpty())
	{
		Point pt = m_heap.Top();

		m_heap.Pop(); // pop and update heap

		if (m_stopTime > 0.0 && GetArrivalTimeAt(pt) > m_stopTime) break;

		if (m_hasStopPoint && GetLabelAt(m_stopPoint) == kAccepted) break;

		if (GetLabelAt(pt) != kAccepted)
		{
			Update(pt);
		}
	}

	Finalize();
}

template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::EnableSecondOrder()
{
  m_secondOrderEnabled = true;
}

template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::DisableSecondOrder()
{
	m_secondOrderEnabled = false;
}

template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::Initialize()
{
	// initialize time map
	int totalNumPixels = 1;
	for (unsigned int i = 0; i < NDim; ++i)
	{
		totalNumPixels *= m_size[i];
	}
	m_timeMap.resize(totalNumPixels);
	std::fill(m_timeMap.begin(), m_timeMap.end(), static_cast<DataType>(kTimeAtFarLabel));

	// initialize pixel state array
	m_stateImage.resize(totalNumPixels);
	std::fill(m_stateImage.begin(), m_stateImage.end(), kFarAway);

	// initialize binary heap
	m_heap.GetEvaluator().size = m_size;
	m_heap.GetEvaluator().timeMap = &m_timeMap;

	// place seed points on state array and time map
	for (typename SeedPoints::const_iterator itr = m_seeds.begin(); itr != m_seeds.end(); ++itr)
	{
		SetLabelAt(*itr, kAccepted); // change label to accepted at seed locations
		SetArrivalTimeAt(*itr, 0.0); // set time=0.0 for all seed locations
	}

	// update pixels adjacent to seed points (initial narrow band points)
	for (typename SeedPoints::const_iterator itr = m_seeds.begin(); itr != m_seeds.end(); ++itr)
	{
		// compute and set the arrival time at the neighbor of the seed
		UpdateNeighbors(*itr);
	}
}

template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::Finalize()
{
	typename TimeMapType::iterator itr = m_timeMap.begin();
	typename PixelStateImageType::const_iterator citr = m_stateImage.begin();
	for (; citr != m_stateImage.end(); ++citr, ++itr)
	{
		if (*citr == kNarrowBand) { *itr = -1.0; }
	}
}

//! @param [in] pt 画像上の位置
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::Update(const Point& pt)
{
	// change label at pt to Accepted
	SetLabelAt(pt, kAccepted);

	UpdateNeighbors(pt);
}

//! @param [in] pt 画像上の位置
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::UpdateNeighbors(const Point& pt)
{
	Point neighbor;
	for (unsigned int dirType = kXDir; dirType < NDim; ++dirType) {
		neighbor = pt;
		--neighbor[dirType];
		if (neighbor[dirType] >= 0)
		{
			UpdateSingleNeighbor(neighbor);
		}

		neighbor[dirType] += 2;
		if (neighbor[dirType] < m_size[dirType])
		{
			UpdateSingleNeighbor(neighbor);
		}
	}
}

//! @param [in] neighbor 画像上の位置
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::UpdateSingleNeighbor(const Point& neighbor)
{
	const PixelState& state = GetLabelAt(neighbor);
	if (state == kAccepted) return;
	else if (state == kFarAway) {
		SetLabelAt(neighbor, kNarrowBand);
	}

	DataType arrivalTime = ComputeArrivalTime(neighbor);
	if (arrivalTime > 0.0)
	{
		SetArrivalTimeAt(neighbor, arrivalTime);
		m_heap.Push(neighbor);
	}
}

// a x^2 + b x + c = 1/F^2 where F is speed value at the given point
//! @param [in] pt 画像上の位置
template<typename DataType, unsigned int NDim>
inline DataType FastMarching<DataType, NDim>::ComputeArrivalTime(const Point& pt)
{
	DataType speed = GetSpeedAt(pt);
	if (speed <= 0.0) return -1.0;

	DataType suma = 0.0;
	DataType sumb = 0.0;
	DataType sumc = 0.0;
	DataType tmpA, tmpB, tmpC;
	for (unsigned int dirType = kXDir; dirType < NDim; ++dirType)
	{
		for (int n = -1; n <= 1; n += 2)
		{
			Point neighborPt = pt;
			neighborPt[dirType] += n;
			const PixelState& pixelState = GetLabelAt(neighborPt);
			if (pixelState != kAccepted) continue;
			else if (pixelState == kAccepted)
			{
				DataType arrivalTime = GetArrivalTimeAt(neighborPt);
				Point neighbor2ndPt = neighborPt;
				neighbor2ndPt[dirType] += n;
				if (m_secondOrderEnabled && GetLabelAt(neighbor2ndPt) == kAccepted)
				{
					DataType arrivalTime2 = GetArrivalTimeAt(neighbor2ndPt);
					ComputeQuadraticCoefficients2ndOrder(
						static_cast<DirectionType>(dirType),
						arrivalTime, arrivalTime2, tmpA, tmpB, tmpC);
				}
				else
				{
					ComputeQuadraticCoefficients1stOrder(
						static_cast<DirectionType>(dirType),
						arrivalTime, tmpA, tmpB, tmpC);
				}
				suma += tmpA;
				sumb += tmpB;
				sumc += tmpC;
			}
		}
	}
	if (suma == 0.0) return -1.0;

	sumc -= DataType(1.0)/(speed*speed);

	DataType discriminant = sumb*sumb - 4*suma*sumc;
	if (discriminant < 0.0) return -1.0;

	DataType t = (-sumb + sqrt(discriminant))/(2*suma);

	return t;
}

//! @param [in] pt 画像上の位置
//! @param [in] state 更新するピクセルの状態
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::SetLabelAt(const Point& pt, PixelState state)
{
	m_stateImage[GetLinearIndex(pt)] = state;
}

//! @param [in] pt 画像上の位置
template<typename DataType, unsigned int NDim>
inline typename FastMarching<DataType, NDim>::PixelState FastMarching<DataType, NDim>::GetLabelAt(const Point& pt) const
{
	if (IsPointInRange(pt))
	{
		return m_stateImage[GetLinearIndex(pt)];
	}

	return kFarAway;
}

//! @param [in] pt 画像上の位置
//! @param [in] arrivalTime 到達時刻
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::SetArrivalTimeAt(const Point& pt, DataType arrivalTime)
{
	m_timeMap[GetLinearIndex(pt)] = arrivalTime;
}

//! @param [in] pt 画像上の位置
//! @return 到達時刻
template<typename DataType, unsigned int NDim>
inline DataType FastMarching<DataType, NDim>::GetArrivalTimeAt(const Point& pt) const
{
	if (IsPointInRange(pt))
	{
		return m_timeMap[GetLinearIndex(pt)];
	}
	return -1.0;
}

//! @param [in] pt 画像上の位置
//! @return 速度
template<typename DataType, unsigned int NDim>
inline DataType FastMarching<DataType, NDim>::GetSpeedAt(const Point& pt) const
{
	if (IsPointInRange(pt))
	{
		if (m_speed)
		{
			return m_speed[GetLinearIndex(pt)];
		}
		else
		{
			return m_constantSpeed;
		}
	}

	return 0.0;
}

//! 到達時刻はtは2次関数(a t^2 + b t + c = 0)の解。
//! 計算対象位置からdirTypeで与えられる方向で隣接する位置の時刻情報を使い、
//! この2次関数の係数a, b, cを計算する。
//! @param [in] dirType 向き(ピクセル間隔を取得する為)
//! @param [in] value 計算対象のピクセルの近傍の時刻データ
//! @param [out] a 係数
//! @param [out] b 係数
//! @param [out] c 係数
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::ComputeQuadraticCoefficients1stOrder(
	DirectionType dirType,
	const DataType& value,
	DataType& a, DataType& b, DataType& c )
{
	const DataType& invSqrSpacing = m_invSquaredSpacing[dirType];
	a = invSqrSpacing; // 1.0/(dx*dx);
	b = DataType(-2.0) * invSqrSpacing * value; // -2.0*value/(dx*dx);
	c = invSqrSpacing * value * value; // value/(dx*dx);
}

//! 到達時刻はtは2次関数(a t^2 + b t + c = 0)の解。
//! 計算対象位置からdirTypeで与えられる方向の2つ先までの時刻情報を使い、
//! この2次関数の係数a, b, cを計算する。
//! @param [in] dirType 向き(ピクセル間隔を取得する為)
//! @param [in] value0 計算対象のピクセルの近傍(直ぐ隣)の時刻データ
//! @param [in] value1 計算対象のピクセルの近傍(2つ先)の時刻データ
//! @param [out] a 係数
//! @param [out] b 係数
//! @param [out] c 係数
template<typename DataType, unsigned int NDim>
inline void FastMarching<DataType, NDim>::ComputeQuadraticCoefficients2ndOrder(
	DirectionType dirType,
	const DataType& value0,
	const DataType& value1,
	DataType& a, DataType& b, DataType& c )
{
	const DataType& invSqrSpacing = m_invSquaredSpacing[dirType];
	DataType K = DataType(4.0)*value0 - value1;
	a = DataType(9.0/4.0)*invSqrSpacing;
	b = DataType(-3.0/2.0)*invSqrSpacing*K;
	c = invSqrSpacing * K * K / DataType(4.0);
}

//! @param [in] pt 画像上の位置
//! @return 配列要素番号
template<typename DataType, unsigned int NDim>
inline int FastMarching<DataType, NDim>::GetLinearIndex(const Point& pt) const
{
	int sum = pt[NDim-1];
	for (int i = NDim-2; i >= 0; --i)
	{
		sum *= m_size[i];
		sum += pt[i];
	}
	return sum;
}

//! @param [in] pt 画像上の位置
//! @return 位置が画像内ならtrue。画像外ならfalse。
template<typename DataType, unsigned int NDim>
inline bool FastMarching<DataType, NDim>::IsPointInRange(const Point& pt) const
{
	for (int i = 0; i < NDim; ++i) {
		if (pt[i] < 0 || pt[i] >= m_size[i]) {
			return false;
		}
	}
	return true;
}

// InternalFixedArray
template<typename DataType, unsigned int NDim>
template<typename _ValueType>
inline FastMarching<DataType, NDim>::
InternalFixedArray<_ValueType>::
InternalFixedArray()
{ for (int i = 0; i < NDim; ++i) elements[i] = 0; }

//! @param _x 最初の要素の初期値
//! @param _y 2番めの要素の初期値
template<typename DataType, unsigned int NDim>
template<typename _ValueType>
inline FastMarching<DataType, NDim>::
InternalFixedArray<_ValueType>::
InternalFixedArray(const _ValueType& _x, const _ValueType& _y)
{ elements[0] = _x; elements[1] = _y; }

//! @param _x 最初の要素の初期値
//! @param _y 2番めの要素の初期値
//! @param _z 3番めの要素の初期値
template<typename DataType, unsigned int NDim>
template<typename _ValueType>
inline FastMarching<DataType, NDim>::
InternalFixedArray<_ValueType>::
InternalFixedArray(const _ValueType&  _x, const _ValueType& _y, const _ValueType& _z)
{ elements[0] = _x; elements[1] = _y; elements[2] = _z; }

template<typename DataType, unsigned int NDim>
template<typename _ValueType>
inline _ValueType& FastMarching<DataType, NDim>::
InternalFixedArray<_ValueType>::
operator[](int n)
{ return elements[n]; }

template<typename DataType, unsigned int NDim>
template<typename _ValueType>
inline const _ValueType& FastMarching<DataType, NDim>::
InternalFixedArray<_ValueType>::
operator[](int n) const
{ return elements[n]; }

// BinaryHeapEvaluator
//! @param [in] pt0 画像上の位置
//! @param [in] pt1 画像上の位置
//! @return 2点の到達時刻を比較し、pt0がpt1より小さければtrueを返す。その他はfalseを返す。
template<typename DataType, unsigned int NDim>
inline bool FastMarching<DataType, NDim>::BinaryHeapEvaluator::operator()(const Point& pt0, const Point& pt1) const
{
	return ((*timeMap)[GetLinearIndex(pt0)] < (*timeMap)[GetLinearIndex(pt1)]);
}

//! @param [in] pt 画像上の位置
//! @return 配列要素番号。
template<typename DataType, unsigned int NDim>
inline size_t FastMarching<DataType, NDim>::BinaryHeapEvaluator::GetLinearIndex(const Point& pt) const
{
	size_t sum = pt[NDim-1];
	for (int i = NDim-2; i >= 0; --i)
	{
		sum *= size[i];
		sum += pt[i];
	}
	return sum;
}

} // namespace segmentation
} // namespace lssplib

