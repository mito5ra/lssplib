#pragma once

#include <vector>

#include "../BaseContainers.h"
#include "../lssplib.h"
#include "VesselNode.h"

namespace lssplib
{
namespace segmentation
{

class VesselNode;

//! 血管構造データの種類
enum VesselType
{
	kVesselTypeInvalid, //!< 無効
	kVesselTypePoratlVein, //!< 門脈
	kVesselTypeHepaticVein, //!< 静脈
};

//! 血管構造データを保持するクラス
class VesselTree
{
public:

	LSSPLIB_API VesselTree();

	LSSPLIB_API ~VesselTree();

	//! 根本のノードを設定する
	LSSPLIB_API void SetStartNode(VesselNode* org);

	//! 根本ノードを取得する
	LSSPLIB_API VesselNode* GetStartNode();

	//! 根本ノードを取得する
	LSSPLIB_API const VesselNode* GetStartNode() const;

	//! 血管構造データの原点を設定する。(3Dバッファの座標)
	LSSPLIB_API void SetOrigin(const Point3i& pt);

	//! 血管構造データの原点を取得する。(3Dバッファの座標)
	LSSPLIB_API const Point3i& GetOrigin() const;

	//! 血管構造データの大きさを設定する。(ピクセルサイズ)
	LSSPLIB_API void SetSize(const Size3i& sz);

	//! 血管構造データの大きさを取得する。(ピクセルサイズ)
	LSSPLIB_API const Size3i& GetSize() const;

	//! 血管構造データのピクセル間隔を設定する。
	LSSPLIB_API void SetSpacing(const Vector3d& spacing);

	//! 血管構造データのピクセル間隔を取得する。
	LSSPLIB_API const Vector3d& GetSpacing() const;

	//! 血管構造データの血管の種類を設定する。
	LSSPLIB_API void SetVesselType(VesselType vesselType);

	//! 血管構造データの血管の種類を取得する。
	LSSPLIB_API VesselType GetVesselType() const;

	//! 指定した位置の血管構造データのノードを探す。
	LSSPLIB_API VesselNode* FindNodeAt(int x, int y, int z) const;

	//! 指定したノードから末端側にある全てのノードを消す。
	LSSPLIB_API void DeleteBranches(VesselNode* aNode);

	//! 全てノードの分岐レベルを更新する。
	LSSPLIB_API void UpdateBranchLevel();

	//! 指定したノードから末端側のノードの分岐レベルを更新する。
	LSSPLIB_API void UpdateBranchLevel(VesselNode* aNode);

	//! 指定したノードがある枝の根本のノードを取得する。
	LSSPLIB_API const VesselNode* GetBranchStart(const VesselNode* node) const;

	//! 指定したノードがある枝の根本のノードを取得する。
	LSSPLIB_API VesselNode* GetBranchStart(VesselNode* node);

	//! 指定したノードがある枝の根本側で接続している分岐部のノードを取得する。
	LSSPLIB_API const VesselNode* GetPreviousJunction(const VesselNode* node) const;

	//! 指定したノードがある枝の末端側で接続している分岐部のノードを取得する。
	LSSPLIB_API const VesselNode* GetNextJunction(const VesselNode* node) const;

	//! 2つのノード間のノード数を取得する。
	LSSPLIB_API int CountNumberOfNodesBetween(const VesselNode* startNode, const VesselNode* endNode) const;

	//! 全ノード数を取得する。
	LSSPLIB_API int GetNumberOfNodes() const;

	//! 全ノードのIDを更新する。
	LSSPLIB_API void UpdateNodeID();

	//! 複製を生成する。
	LSSPLIB_API VesselTree* Duplicate() const;

	//! 血管構造データの妥当性を調べる。
	LSSPLIB_API bool ValidityCheck() const;

	LSSPLIB_API VesselNode::Iterator Begin();

	LSSPLIB_API VesselNode::ConstIterator ConstBegin() const;

	LSSPLIB_API VesselTree* Clone() const;

	LSSPLIB_API void Decimate(double targetInterval);

	LSSPLIB_API void CollectNodesToDecimate(double targetInterval, std::vector<VesselNode*>& nodesToBeRemoved) const;


private:
	//! 根本のノード
	VesselNode* m_startNode;

	//! 血管構造データの原点とする3Dバッファ上の位置。
	Point3i m_origin;

	// pixel size of the re-sampled region size of the liver
	// pixel pitch in z direction is different from the original image data.
	// most likely finer pitch than the original.
	//! 血管構造データの大きさ。(ピクセルサイズ)
	Size3i m_size;

	// length of each side of the re-sampled voxels.
	//! 血管構造データのピクセル間隔。(3Dバッファのものとは異なる)
	Vector3d m_spacing;

	//! 血管の種類
	VesselType m_type;
};

//! 一つのノードとそれを保持する血管構造データの情報を保持する構造体。
struct LiverNodeInfo
{
	VesselNode* node; //!< ノードのポインタ

	VesselTree* parentTree; //!< ノードが所属する血管構造データのポインタ

	//! ノードと血管構造データのポインタを設定する。
	void Set(
		VesselNode* aNode,
		VesselTree* aTree)
	{ node = aNode; parentTree = aTree; }

	//! ノードと血管構造データのポインタをクリアする。
	void Reset() { node = NULL; parentTree = NULL; }

	//! ノードと血管構造データの有効性を返す。
	bool IsValid() const { return (node && parentTree); }

	LiverNodeInfo(): node(NULL), parentTree(NULL) {}

	//! コンストラクタ(ノードと血管構造データを入力)
	LiverNodeInfo(
		VesselNode* aNode,
		VesselTree* aTree)
		: node(aNode), parentTree(aTree) {}

	//! コピーコンストラクタ
	LiverNodeInfo(const LiverNodeInfo& src): node(src.node), parentTree(src.parentTree) {}

	//! 代入演算子
	LiverNodeInfo& operator=(const LiverNodeInfo& src)
	{
		node = src.node;
		parentTree = src.parentTree;
		return *this;
	}
};

//! @param [in] org ノード
inline void VesselTree::SetStartNode( VesselNode* org )
{
	m_startNode = org;
}

//! @return 根本のノード
inline VesselNode* VesselTree::GetStartNode()
{
	return m_startNode;
}

//! @return 根本のノード
inline const VesselNode* VesselTree::GetStartNode() const
{
	return m_startNode;
}

//! @param [in] pt 原点(3Dバッファのピクセル位置)
inline void VesselTree::SetOrigin(const Point3i& pt)
{
	m_origin = pt;
}

//! @return 原点(3Dバッファのピクセル位置)
inline const Point3i& VesselTree::GetOrigin() const
{
	return m_origin;
}

//! @param [in] sz 領域の大きさ。
inline void VesselTree::SetSize(const Size3i& sz)
{
	m_size = sz;
}

//! @return 領域の大きさ。
inline const Size3i& VesselTree::GetSize() const
{
	return m_size;
}

//! @param [in] spacing ピクセル間隔
inline void VesselTree::SetSpacing(const Vector3d& spacing)
{
	m_spacing = spacing;
}

//! @return ピクセル間隔
inline const Vector3d& VesselTree::GetSpacing() const
{
	return m_spacing;
}

//! @param [in] vesselType 血管の種類
inline void VesselTree::SetVesselType( VesselType vesselType )
{
	m_type = vesselType;
}

//! @return 血管の種類
inline VesselType VesselTree::GetVesselType() const
{
	return m_type;
}

} // namespace segmentation
} // namespace lssplib
