﻿#pragma once

#include "BaseContainers.h"
#include "lssplib.h"
#include <stack>
#include <vector>

namespace lssplib
{
namespace segmentation
{

/**
 * @brief 距離画像の生成処理オブジェクト
 */
class DistanceFieldGenerator
{
public:
	/**
	 * @brief 距離画像のシード点を保持するオブジェクト
	 */
	struct SeedPoint
	{
		int x; //!< x位置
		int y; //!< y位置
		int z; //!< z位置
		SeedPoint(): x(0), y(0), z(0) {}

		//! @brief コンストラクタ
		//! @param [in] xpos x位置
		//! @param [in] ypos y位置
		//! @param [in] zpos z位置
		SeedPoint(int xpos, int ypos, int zpos): x(xpos), y(ypos), z(zpos) {}

		/**
		 * @brief 座標値を設定する
		 * 
		 * @param [in] xpos x位置
		 * @param [in] ypos y位置
		 * @param [in] zpos z位置
		 */
		void Set(int xpos, int ypos, int zpos)
		{
			x = xpos;
			y = ypos;
			z = zpos;
		}
	};

	typedef std::vector<double> Kernel; //!< 距離生成カーネル型
	typedef std::stack<SeedPoint> SeedStack; //!< シード位置のスタック
	typedef float DistanceValueType; //!< 距離画像の画素のデータ型
	typedef std::vector<DistanceValueType> DistanceMap; //!< 距離画像型

	LSSPLIB_API DistanceFieldGenerator(void);
	LSSPLIB_API virtual ~DistanceFieldGenerator(void);

	//! @brief 距離画像の生成を実行する
	//! @param [in] w 元画像の幅
	//! @param [in] h 元画像の高さ
	//! @param [in] d 元画像の深さ
	//! @param [in] binaryImage 元画像
	LSSPLIB_API void Execute(int w, int h, int d, const unsigned char* binaryImage);

	//! @brief 距離画像を取得する
	LSSPLIB_API const DistanceMap& GetDistanceMap() const { return m_distanceMap; }

private:
	void GenerateKernel();

	void GenerateInitialSeeds();

	bool IsInitialSeed(const SeedPoint& pt);

	void UpdateDistanceMapAt(const SeedPoint& pt);

	std::vector<double> m_kernel; //!< 距離生成のカーネル
	Size3i m_imageSize; //!< 距離画像の大きさ
	DistanceMap m_distanceMap; //!< 距離画像データ
	SeedStack m_seeds0; //!< 処理位置を保持するスタック
	SeedStack m_seeds1; //!< 処理位置を保持するスタック
	SeedStack* m_currentStack; //!< 現在処理中のスタックを示すポインタ
	SeedStack* m_nextStack; //!< 次のループで処理するスタックを示すポインタ

	const unsigned char* m_binaryImage; //!< 距離画像を生成するための元画像データ
};

} // namespace segmentation
} // namespace lssplib

