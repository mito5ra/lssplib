namespace lssplib
{
namespace segmentation
{

//! @return ヒープ配列が空ならtrue、それ以外ならfalse。
template<typename DataType, class Compare>
inline bool BinaryHeap<DataType, Compare>::IsEmpty() const
{ return m_data.empty(); }

//! @return ヒープ配列の大きさ。
template<typename DataType, class Compare>
inline size_t BinaryHeap<DataType, Compare>::GetSize() const
{ return m_data.size(); }

//! @param [in] value データ
template<typename DataType, class Compare>
inline void BinaryHeap<DataType, Compare>::Push(const DataType& value)
{
	m_data.push_back(value);
	size_t n = GetSize()-1;
	UpHeap(n);
}

template<typename DataType, class Compare>
inline void BinaryHeap<DataType, Compare>::Pop()
{
	m_data.front() = m_data.back();
	m_data.pop_back();
	DownHeap(0);
}

//! @return ヒープ配列の先頭データの参照。
template<typename DataType, class Compare>
inline const DataType& BinaryHeap<DataType, Compare>::Top() const
{ return m_data.front(); }

template<typename DataType, class Compare>
inline void BinaryHeap<DataType, Compare>::Clear()
{ std::vector<DataType>().swap(m_data); }

//! @return 比較関数オブジェクトを返す。
template<typename DataType, class Compare>
inline Compare& BinaryHeap<DataType, Compare>::GetEvaluator()
{ return evaluator; }

//! @param [in] childIndex ヒープ配列の要素番号。
template<typename DataType, class Compare>
inline void BinaryHeap<DataType, Compare>::UpHeap(size_t childIndex)
{
	if (childIndex == 0) return;

	const DataType& childData = m_data[childIndex];

	size_t parentIndex = (childIndex+1)/2-1;
	const DataType& parentData = m_data[parentIndex];
	// childData < parentData
	if ( evaluator(childData, parentData) )
	{
		std::swap(m_data[childIndex], m_data[parentIndex]);
		UpHeap(parentIndex);
	}
}

//! @param [in] parentIndex ヒープ配列の要素番号。
template<typename DataType, class Compare>
inline void BinaryHeap<DataType, Compare>::DownHeap(size_t parentIndex)
{
	size_t childIndex0 = 2*(parentIndex+1)-1;
	if (childIndex0 >= GetSize()) return;

	const DataType& parentData = m_data[parentIndex];

	const DataType& childData0 = m_data[childIndex0];

	size_t childIndex1 = 2*(parentIndex+1);
	if (childIndex1 < GetSize())
	{
		const DataType& childData1 = m_data[childIndex1]; //

		// parentData <= childData0 && parentData <= childData1
		if (!evaluator(childData0, parentData) &&
			!evaluator(childData1, parentData) ) return;
		//childData0 < childData1
		else if ( evaluator(childData0, childData1) )
		{
			std::swap(m_data[parentIndex], m_data[childIndex0]);
			DownHeap(childIndex0);
		}
		else
		{
			std::swap(m_data[parentIndex], m_data[childIndex1]);
			DownHeap(childIndex1);
		}
	}
	else
	{
		// childData0 < parentData
		if ( evaluator(childData0, parentData) )
		{
			std::swap(m_data[parentIndex], m_data[childIndex0]);
			DownHeap(childIndex0);
		}
	}
}

} // namespace segmentation
} // namespace lssplib

