#pragma once

#include "../BaseContainers.h"
#include "../lssplib.h"
#include <vector>

namespace lssplib
{
namespace segmentation
{

//! 血管構造データのノード情報を保持するクラス。
class VesselNode
{
public:
	//! 末端側に接続するノードのポインタの配列型
	typedef std::vector<VesselNode*> OutgoingNodeList;

	class Iterator {
	public:
		LSSPLIB_API Iterator();

		LSSPLIB_API Iterator(VesselNode* src);

		LSSPLIB_API ~Iterator() {}

		LSSPLIB_API VesselNode* Next();

		LSSPLIB_API VesselNode* Get() { return m_currentNode; }

	private:
		VesselNode* m_currentNode;
		std::vector<VesselNode*> m_nodeStack;
	};

	class ConstIterator {
	public:
		LSSPLIB_API ConstIterator();

		LSSPLIB_API ConstIterator(const VesselNode* src);

		LSSPLIB_API ~ConstIterator() {}

		LSSPLIB_API const VesselNode* Next();

		LSSPLIB_API const VesselNode* Get() { return m_currentNode; }
	private:
		const VesselNode* m_currentNode;
		std::vector<const VesselNode*> m_nodeStack;
	};

	//! デフォルトコンストラクタ
	LSSPLIB_API VesselNode()
		: m_in(NULL)
		, m_out()
		, m_position(0, 0, 0)
		, m_direction(0.0, 0.0, 0.0)
		, m_radius(0.0)
		, m_branchLevel(0)
		, m_nodeId(0) {}

	//! コンストラクタ(ノードの位置を指定)
	//! @param [in] x x位置
	//! @param [in] y y位置
	//! @param [in] z z位置
	LSSPLIB_API VesselNode(int x, int y, int z)
		: m_in(NULL)
		, m_out()
		, m_position(x,y,z)
		, m_direction(0.0, 0.0, 0.0)
		, m_radius(0.0)
		, m_branchLevel(0)
		, m_nodeId(0) {}

	LSSPLIB_API VesselNode(const VesselNode& src)
		: m_in(NULL)
		, m_out()
		, m_position(src.m_position)
		, m_direction(src.m_direction)
		, m_radius(src.m_radius)
		, m_branchLevel(src.m_branchLevel)
		, m_nodeId(src.m_nodeId) {}

	//! 根本側のノード情報のポインタを登録する。
	LSSPLIB_API void SetIncoming(VesselNode* aNode) { m_in = aNode; }

	//! 根本側のノード情報のポインタを取得する。
	LSSPLIB_API VesselNode* GetIncoming() { return m_in; }

	//! 根本側のノード情報のポインタを取得する。
	LSSPLIB_API const VesselNode* GetIncoming() const { return m_in; }

	//! 末端側のノード情報のポインタを追加する。
	LSSPLIB_API void AddOutgoing(VesselNode* aNode) { m_out.push_back(aNode); }

	//! 末端側のノード情報のポインタの配列を取得する。
	LSSPLIB_API OutgoingNodeList& GetOutgoings() { return m_out; }

	//! 末端側のノード情報のポインタの配列を取得する。
	LSSPLIB_API const OutgoingNodeList& GetOutgoings() const { return m_out; }

	//! @brief ノードの位置を設定する。
	//! 血管構造で設定されている原点からの位置。
	LSSPLIB_API void SetPosition(const Point3i& pos) { m_position = pos; }

	//! ノードの位置を取得する。
	//! 血管構造で設定されている原点からの位置。
	LSSPLIB_API const Point3i& GetPosition() const { return m_position; }

	//! ノードの向きを設定する。
	LSSPLIB_API void SetDirection(const Vector3d& dir) { m_direction = dir; }

	//! ノードの向きを取得する。
	LSSPLIB_API const Vector3d& GetDirection() const { return m_direction; }

	//! ノードの位置での血管半径を設定する。
	LSSPLIB_API void SetRadius(double r) { m_radius = r; }

	//! ノードの位置での血管半径を取得する。
	LSSPLIB_API double GetRadius() const { return m_radius; }

	//! 血管の分岐レベルを設定する。
	LSSPLIB_API void SetBranchLevel(int n) { m_branchLevel = n; }

	//! 血管の分岐レベルを取得する。
	LSSPLIB_API int GetBranchLevel() const { return m_branchLevel; }

	//! ノードのIDを設定する。
	LSSPLIB_API void SetNodeId(int id) { m_nodeId = id; }

	//! ノードのIDを取得する。
	LSSPLIB_API int GetNodeId() const { return m_nodeId; } 

	//! 根側のノードから切り離す。
	LSSPLIB_API void Detach();

	//! 指定したノードに接続する。
	LSSPLIB_API void Attach(VesselNode* targetNode);

	//! 枝の根本側のノードを取得。(根本側を探索。分岐点の一つ手前のノード。)
	LSSPLIB_API const VesselNode* GetBranchStart() const;

	//! 枝の根本側のノードを取得。(根本側を探索。分岐点の一つ手前のノード。)
	LSSPLIB_API VesselNode* GetBranchStart();

	//! 枝の根本側の分岐点のノードを取得。(根本側で探索。分岐点のノード。)
	LSSPLIB_API const VesselNode* GetPreviousJunction() const;

	//! 枝の根本側の分岐点のノードを取得。(根本側で探索。分岐点のノード。)
	LSSPLIB_API const VesselNode* GetNextJunction() const;

	//! 分岐点であるかどうかの確認
	LSSPLIB_API bool IsJunction() const;

	//! 末端点かどうかの確認
	LSSPLIB_API bool IsTerminal() const;

	//! @brief 枝のノードの数を取得する
	LSSPLIB_API size_t GetNumNodesInBranch() const;

	LSSPLIB_API Iterator Begin();

	LSSPLIB_API ConstIterator ConstBegin() const;

private:
	VesselNode* m_in; //!< 根側で接続するノードのポインタ。

	OutgoingNodeList m_out; //!< 末端側に接続するノードのポインタの配列。

	//! ノードの位置
	Point3i m_position;

	//! ノードの向き
	Vector3d m_direction;

	//! 血管の半径
	double m_radius;

	//! 血管の分岐レベル
	int m_branchLevel;

	//! ノードのID
	int m_nodeId;
};

void RetrieveVesselNodePoints(const VesselNode* aNode, std::vector<Point3i>& pointList);

} // namespace segmentation
} // namespace lssplib
