#pragma once

#include <queue>

#include "Graph.h"

namespace lssplib
{
namespace segmentation
{

//! @brief Boykov Kolmogorov Max Flowの処理クラス
//! @details 参考資料: \n
//! Yuri Boykov and Vladimir Kolmogorov\n
//! An Experimental Comparison of Min-Cut/Max-Flow Algorithms for Energy Minimization in Vision\n
//! In IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 9, pp. 1124-1137, Sept. 2004.\n
//! \n
//! 井尻敬\n
//! グラフカット画像領域分割法, 第2章, Computer Graphics Gems JP 2013/2014 〜コンピュータグラフィックス技術の最前線〜, 2013年12月
class BKMaxFlow
{
public:
	typedef Graph::Node* NodePtr; //!< ノードのポインタ型
	typedef Graph::Edge* EdgePtr; //!< 辺のポインタ型
	typedef std::vector<EdgePtr> PathType; //!< 辺のポインタのリスト型
	typedef std::queue<NodePtr> NodeQueue; //!< ノードのポインタのFIFO配列型

	BKMaxFlow();

	~BKMaxFlow();

	//! @brief Max Flow処理を実行する
	void Execute(Graph* g);

	//! @brief 最大流量を取得する(Max Flow処理の結果)
	double GetFlow() const;

private:
	//! SourceとSinkの探索ツリーを成長させる
	EdgePtr Growth();

	//! 流量を増加させる
	void Augmentation(EdgePtr boundaryEdge);

	//! 孤児ノードの新しい親ノードを探す
	void Adoption();

	//! 指定ノードの親を辿りSourceノードに辿り着くか確認する
	bool IsRootSource(NodePtr node);

	//! 指定ノードの親を辿りSinkノードに辿り着くか確認する
	bool IsRootSink(NodePtr node);

	NodePtr m_sourceNode; //!< Sourceノード

	NodePtr m_sinkNode; //!< Sinkノード

	NodeQueue m_activeNodes; //!< アクティブノードのリスト

	NodeQueue m_orphanNodes; //!< 孤児ノードのリスト

	PathType m_path; //!< Growth処理で見つかった経路の辺のリスト

	double m_flow; //!< 流量
};

} // namespace segmentation
} // namespace lssplib

