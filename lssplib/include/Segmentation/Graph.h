#pragma once

#include <vector>

namespace lssplib
{
namespace segmentation
{

//! グラフ情報クラス
class Graph
{
public:
	class Edge;

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	//! ノード情報クラス
	class Node
	{
	public:
		//! ノードの状態の列挙型
		enum NodeGroup {
			kNodeGroupFree, //!< 検索ツリーに属さない(無所属)
			kNodeGroupSource, //!< Source側の検索ツリーに属する
			kNodeGroupSink, //!< Sink側の検索ツリーに属する
		};

		Node();

		//! @brief コピーコンストラクタ
		Node(const Node& src);

		~Node();

		//! @brief 代入演算子
		Node& operator=(const Node& src);

		//! @brief ノードから発する全ての辺を辿るための最初の辺を登録する
		void SetFirstEdge(Edge* e);

		//! @brief ノードから発する全ての辺を辿るための最初の辺を取得する
		Edge* FirstEdge();

		//! @brief ノードの状態を登録する
		void SetGroup(NodeGroup nodeGroup);

		//! @brief ノードの状態を取得する
		NodeGroup Group() const;

		//! @brief 親ノードに繋がる辺を登録する
		void SetEdgeToParent(Edge* edgeToParent);

		//! @brief 親ノードに繋がる辺を取得する
		Edge* EdgeToParent();

		//! @brief 指定したノードが隣接している場合に、指定ノードに繋がる辺を返す。
		Edge* EdgeTo(const Node* q);

		//! @brief 指定したノードが隣接している場合に、指定ノードに繋がる辺の逆平行辺を返す。
		Edge* EdgeFrom(const Node* q);

	private:
		Edge* m_edge; //!< ノードから発する全ての辺を辿るための最初の辺

		Edge* m_edgeToParent; //!< 親ノードへ向かう辺

		NodeGroup m_nodeGroup; //!< ノードの状態(Source/Sink/無所属)
	};

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	//! 辺情報クラス
	class Edge
	{
	public:
		Edge();

		Edge(Node* from, Node* to);

		//! @brief コピーコンストラクタ
		Edge(const Edge& src);

		~Edge();

		//! @brief 代入演算子
		Edge& operator=(const Edge& src);

		//! @brief 辺の始点のノード
		Node* From();

		//! @brief 辺の終点のノード
		Node* To();

		//! @brief 逆平行辺
		Edge* Reverse();

		//! @brief 逆平行辺を登録する
		void SetReverse(Edge* revEdge);

		//! @brief 始点が同じノードの辺
		Edge* Next();

		//! @brief 始点が同じノードの辺を登録する
		void SetNext(Edge* nextEdge);

		//! @brief 辺の残りの流量
		const double& Capacity() const;

		//! @brief 辺の残りの流量
		double& Capacity();

		//! @brief 辺の残りの流量を登録する
		void SetCapacity(double capacity);

	private:
		double m_capacity; //!< 辺の残りの流量

		Node* m_nodeFrom; //!< 始点ノード
		Node* m_nodeTo; //!< 終点ノード

		Edge* m_edgeReverse; //!< 逆平行辺
		Edge* m_edgeNext; //!< 始点が同じノードの辺
	};

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	typedef std::vector<Node> NodeList; //!< ノードの配列型
	typedef std::vector<Edge> EdgeList; //!< 辺の配列型

	Graph();

	//! @brief コンストラクタ
	Graph(size_t numNodes, size_t numEdges, size_t srcIdx, size_t sinkIdx);

	//! @brief コピーコンストラクタ
	Graph(const Graph& src);

	~Graph();

	//! @brief 初期化
	void Initialize(size_t numNodes, size_t numEdges, size_t srcIdx, size_t sinkIdx);

	//! @brief 内部データを空にする
	void Reset();

	//! @brief 指定した要素番号のノードを取得する
	const Node& GetNode(size_t idx) const;

	//! @brief 指定した要素番号のノードを取得する
	Node& GetNode(size_t idx);

	//! @brief Sourceノードを取得する
	const Node& SourceNode() const;

	//! @brief Sourceノードを取得する
	Node& SourceNode();

	//! @brief Sinkノードを取得する
	const Node& SinkNode() const;

	//! @brief Sinkノードを取得する
	Node& SinkNode();

	//! @brief 辺を追加する
	void AddEdge(size_t indexFrom, size_t indexTo, double capacity, double revCapacity);

	//! @brief 末端辺を追加する(Source/Sinkと接続する辺)
	void AddTEdge(size_t index, double sourceCapacity, double sinkCapacity);

	//! @brief ノード数を取得する
	size_t GetNumNodes() const;

	//! @brief 登録された辺の数を取得する
	size_t GetNumEdges() const;

	//! @brief ノードの配列を取得する
	const NodeList& Nodes() const;

	//! @brief 辺の配列を取得する
	const EdgeList& Edges() const;

	//! @brief 辺の配列を取得する
	EdgeList& Edges();

private:
	NodeList m_nodeList; //!< ノードの配列

	EdgeList m_edgeList; //!< 辺の配列

	size_t m_sourceIdx; //!< Sourceノードの配列要素番号

	size_t m_sinkIdx; //!< Sinkノードの配列要素番号
};

} // namespace segmentation
} // namespace lssplib

#include "Graph.inl"
