#pragma once

#include "../BaseContainers.h"
#include "../lssplib.h"

namespace lssplib
{
namespace segmentation
{

class GraphCutImpl;

//! グラフカット法で領域抽出処理を行うクラス
class LSSPLIB_API GraphCut
{
public:
	//! @brief ノードの近傍数の列挙型
	enum GraphcutNeighborType
	{
		kGraphcutNeighborTypeInvalid, //!< 無効
		kGraphcutNeighborType6, //!< 6近傍
		kGraphcutNeighborType18, //!< 18近傍
		kGraphcutNeighborType26, //!< 26近傍
	};
	GraphCut(void);

	virtual ~GraphCut(void);

	//! 前景マスクを登録する
	void SetObjectMask(const Boxi& objectBBox, const unsigned char* objectMask);

	//! 背景マスクを登録する
	void SetBackgroundMask(const Boxi& backgroundBBox, const unsigned char* backgroundMask);

	//! 3Dバッファの大きさと画像データのポインタを登録する
	void SetSourceImage(const Size3i& srcImageBBox, const short* srcImage);

	//! 抽出領域の輝度の最小値を取得する
	short GetMinIntensity() const;

	//! 抽出領域の輝度の最大値を取得する
	short GetMaxIntensity() const;

	//! グラフカット処理を実行する
	bool Execute(Point3i& subVolumeOrigin, Size3i& subVolumeSize, unsigned char*& outputBuffer);

	//! 抽出の後処理で行うErosionの回数を設定する
	void SetErosionLevelInPostProcess(int n);

	//! @brief グラフの各ノードの近傍数を設定する。
	void SetNeighborType(GraphcutNeighborType neighborType);

	//! @brief データ項の係数を設定する。(デフォルト値は1.0)
	void SetDataTermCoeff(double coeff);

	//! @brief 平滑化項の係数を設定する。(デフォルト値は1.0)
	void SetSmoothTermCoeff(double coeff);

private:
	//! コピーコンストラクタ
	GraphCut(const GraphCut&) {}

	//! グラフカットを実行する内部クラスのポインタ
	GraphCutImpl* m_pImpl;
};

} // namespace segmentation
} // namespace lssplib

