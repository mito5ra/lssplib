#pragma once

#include "itkImage.h"
#include "../BaseContainers.h"
#include "../lssplib.h"

namespace lssplib
{
namespace segmentation
{

class VesselTree;

class VesselThicknessInfo;

/**
 * @brief 血管抽出処理を行うクラス
 */
class VesselExtractor
{
public:
	typedef itk::Image<short, 3> ImageBufferType; //!< 3Dバッファ画像型
	typedef itk::Image<unsigned char, 3> MaskBufferType; //!< マスクデータ型
	typedef itk::Image<float, 3> VesselnessImageType; //!< ラインフィルタデータ型
	typedef itk::Image<unsigned char, 3> OutputImageType; //!< 出力画像型
	typedef std::tr1::shared_ptr<VesselExtractor> Pointer; //!< LiverVesselExtractorのshared_ptr型
	typedef std::vector<VesselTree*> HepaticVeinTrees; //!< 静脈の血管構造データのリストの配列型

	LSSPLIB_API VesselExtractor();

	LSSPLIB_API ~VesselExtractor();

	// subset of edit buffer
	LSSPLIB_API void SetBuffer(const Point3i& editBufferBBoxOrigin, const Size3i& editBufferBBoxSize, short* editBuffer);

	// use the same portion of volume as the subset of the edit buffer
	LSSPLIB_API void SetLiverBuffer(const Point3i& bufferBBoxOrigin, const Size3i& bufferBBoxSize, unsigned char* buffer);

	// use the same portion as the liver mask buffer
	LSSPLIB_API void SetIVCBuffer(const Point3i& bufferBBoxOrigin, const Size3i& bufferBBoxSize, unsigned char* buffer);

	// position from the subset buffer origin
	LSSPLIB_API void SetPortalVeinPoint(const Point3i& portalVeinPoint);

	LSSPLIB_API void SetIntensityRange(int minIntensity, int maxIntensity);

	LSSPLIB_API void SetVoxelSpacing(const Size3d& voxelSpacing);

	/**
	 * @brief 抽出対象領域の原点を設定する。3Dバッファの座標。
	 * 
	 * @param [in] org 原点
	 */
	LSSPLIB_API void SetOrigin(const Point3i& org) { m_origin = org; }

	/**
	 * @brief 3Dバッファの大きさとポインタを登録する
	 * 
	 * @param [in] bufSize 大きさ
	 * @param [in] imageBuf バッファのポインタ
	 */
	LSSPLIB_API void SetImageBuffer(const Size3i& bufSize, const short* imageBuf)
	{
		m_imageBufferSize = bufSize;
		m_imageBuffer = const_cast<short*>(imageBuf);
	}

	LSSPLIB_API bool Execute();

	LSSPLIB_API bool UpdateVesselRegions(bool performPreprocess = true);

	/**
	 * @brief 抽出結果の画像バッファを取得する
	 * 
	 * @return 抽出結果画像
	 */
	//OutputImageType::Pointer GetOutput() { return m_output; }

	/**
	 * @brief 抽出結果の画像バッファを取得する
	 * 
	 * @return 抽出結果画像
	 */
	LSSPLIB_API const OutputImageType::Pointer GetOutput() const { return m_output; }

	/**
	 * @brief 血管構造データを登録する。
	 * @details 空のデータを登録する。
	 * 
	 * @param [in] vesselTree 門脈の血管構造データのポインタ
	 * @param [in] hepaticTrees 静脈の血管構造データのリストのポインタ
	 */
	LSSPLIB_API void SetLiverVesselTree(VesselTree* vesselTree, HepaticVeinTrees* hepaticTrees)
	{
		m_vesselTree = vesselTree;
		m_hepaticVeinTrees = hepaticTrees;
	}

	LSSPLIB_API void SetVesselThicknessInfoObject(VesselThicknessInfo* vesselThicknessInfo);

private:
	void Preprocess();

	void ResampleLiverMask();
	void ErodeMask();
	void ResampleSourceImage();
	void EnhanceVesselIntensity();
	void GenerateVesselnessImage();
	void GenerateThicknessInfo();
	bool BuildVesselTree();
	bool ExtractVesselRegions();
	void ScanVesselRadius();

	void GeneratePathImage(const Size3i& imageSize, std::vector<unsigned char>& pathImage);
	void SubtractPathFromLiverMask(double pathRadius, std::vector<unsigned char>& subtractedLiverMask);
	void ChangeMaskValueOnIVC(std::vector<unsigned char>& subtractedLiverMask, unsigned char val);
	void SubtractMaskInVesselIntensityRange(std::vector<unsigned char>& subtractedLiverMask);
	void PostProcessSegmentedImage(double pathRadius, unsigned char* segmentedImage);

	Point3i ConvertResampledToOriginalPosition(const Point3i& resampledPosition) const;

	ImageBufferType::Pointer m_buffer;//!< 抽出対象領域の画像データ(3Dバッファ内の該当する領域がコピーされたもの)
	ImageBufferType::Pointer m_resampledBuffer; //!< 再サンプル画像(抽出対象領域の画像を立方体ボクセル化したもの。z方向を補間)
	ImageBufferType::Pointer m_vesselEnhancedBuffer; //!< 血管輝度強調画像(再サンプル画像の輝度範囲を強調させた画像)
	MaskBufferType::Pointer m_liverMask; //!< 肝臓マスク
	MaskBufferType::Pointer m_resampledLiverMask; //!< 再サンプル肝臓マスク
	MaskBufferType::Pointer m_IVCMask; //!< 下大静脈マスク
	MaskBufferType::Pointer m_vesselMask; //!< 抽出画像
	VesselnessImageType::Pointer m_vesselnessImage; //!< ラインフィルタ画像
	VesselThicknessInfo* m_vesselThicknessInfo; //!< 管状構造画像データ
	OutputImageType::Pointer m_output; //!< 出力用抽出画像

	Point3i m_portalVeinPoint; //!< 門脈始点

	int m_minIntensity; //!< 血管輝度強調の下限
	int m_maxIntensity; //!< 血管輝度強調の上限

	// original spacing
	Size3d m_voxelSpacing; //!< 3Dバッファのボクセル間隔

	// spacing after re-sample
	Size3d m_resampledVoxelSpacing; //!< 再サンプル画像のボクセル間隔
	Size3i m_resampledBufferSize; //!< 再サンプル画像の大きさ

	VesselTree* m_vesselTree; //!< 門脈の血管構造データのポインタ
	HepaticVeinTrees* m_hepaticVeinTrees; //!< 静脈の血管構造データのリストのポインタ

	short* m_imageBuffer; //!< 3Dバッファのポインタ
	Size3i m_imageBufferSize; //!< 3Dバッファの大きさ

	// pixel position of the origin of the liver region in edit buffer coordinate.
	Point3i m_origin; //!< 抽出対象領域の原点(3Dバッファの座標)
};

} // namespace segmentation
} // namespace lssplib

