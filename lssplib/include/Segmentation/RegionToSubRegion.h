#pragma once

#include "../BaseContainers.h"
#include "../lssplib.h"

namespace lssplib
{
namespace segmentation
{
typedef unsigned char UCHAR;
class RegionToSubRegionImpl;

//! @brief イメージ領域でerosion処理を行いサブ領域に分けるクラスのラッパ。
class LSSPLIB_API RegionToSubRegion
{
public:
	//! @brief 標準コンストラクタ
	RegionToSubRegion();

	//! @brief デストラクタ
	virtual ~RegionToSubRegion();

	//! @brief 結果とかをクリアー
	void Clear();

	//! @brief 処理対象の領域をセット
	//! @brief ピクセルの値が0以外の場合は領域として処理される
	//!
	//! @param [in] pRegionBuffer 対象の領域のバッファへのポインタ
	//! @param [in] regionOrigin バッファの原点
	//! @param [in] regionDim バッファのx,y,z方向のサイズ
	void SetRegion(const UCHAR* pRegionBuffer, const Point3i& regionOrigin = Point3i(0, 0, 0), const Size3i& regionDim = Size3i(0, 0, 0));

	// pExculdeBuffer - binary buffer with labeling as 255(region) or 0(no region)
	//! @brief 処理対象外の領域をセット
	//! @brief ピクセルの値が255の場合は対象外の領域
	//!
	//! @param [in] pExcludeBuffer 対象外の領域のバッファへのポインタ
	//! @param [in] origin バッファの原点
	//! @param [in] size バッファのx,y,z方向のサイズ
	void SetRegionToExclude(const UCHAR* pExcludeBuffer, const Point3i& origin = Point3i(0, 0, 0), const Size3i& size = Size3i(0, 0, 0));

	//! @brief 一度erosion処理を行い、サブ領域を生成
	//!
	//! @return もう一度erosionでサブ領域生成される可能性がある場合はtrue
	bool TryToGenerateSubRegion();

	//! @brief サブ領域の数を取得
	size_t GetSubRegionCount() const;

	//! @brief サブ領域の原点を取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return サブ領域の原点
	Point3i GetSubRegionOrigin(size_t regionIndex) const;

	//! @brief サブ領域のサイズを取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return サブ領域のサイズ
	Point3i GetSubRegionSize(size_t regionIndex) const;

	//! @brief サブ領域の色を取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return サブ領域の色(floatの3要素)
	Point3f GetSubRegionColor(size_t regionIndex) const;

	//! @brief サブ領域の名称を取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return サブ領域の名称
	std::wstring GetSubRegionName(size_t regionIndex) const;

	//! @brief 領域の原点を取得
	Point3i GetOrigin() const;

	//! @brief 領域のサイズを取得
	Point3i GetDimension() const;

	//! @brief 領域バッファへのポインタを取得
	const UCHAR* GetRegionBuffer() const;

	//! @brief 最大生成される領域の数を取得
	int GetMaxPossibleRegionValue() const { return 255; }

	//! @brief サブ領域の値を取得
	//!
	//! @param [in] regionIndex 領域のインデックス
	//! @return 領域の値
	UCHAR GetSubRegionValue(size_t regionIndex) const;

	//! @brief 対象外のサブ領域の値を取得
	UCHAR GetExcludedRegionValue() const;

	//! @brief 対象外のサブ領域の色を取得
	Point3f GetExcludedRegionColor() const;

	//! @brief サブ領域を生成させる
	//!
	//! @return もう一度処理行ってサブ領域生成される可能性がある場合はtrue
	bool GenerateSubRegion();

	//! @brief サブ領域を探し、インデックスを取得
	//!
	//! @param [in] regionValue 領域の値
	//! @return サブ領域のインデックス
	int FindSubRegionIndex(UCHAR regionValue) const;

	//! @brief サブ領域を削除
	//!
	//! @param [in] regionValue 領域の値
	void EraseSubRegion(UCHAR regionValue);

	//! @brief サブ領域生成される可能性があるかどうか示すフラグを取得
	bool IsRegionExhausted() const;

private:
	//! 処理を行う内部クラスへのポインタ
	RegionToSubRegionImpl* m_pImpl;
};

} // namespace segmentation
} // namespace lssplib

