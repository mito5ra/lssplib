#pragma once

#include <functional>
#include <vector>

namespace lssplib
{
namespace segmentation
{

//! @brief 二分ヒープ
//! @tparam DataType 内部のデータ型
//! @tparam Compare 比較オブジェクト
template< typename DataType, class Compare = std::less< DataType > >
class BinaryHeap
{
public:
	BinaryHeap(): m_data() {}

	~BinaryHeap() {}

	//! 配列が空なら真を返す。
	bool IsEmpty() const;

	//! 配列のサイズ
	size_t GetSize() const;

	//! データを追加する。
	void Push(const DataType& value);

	//! 先頭のデータを消す。
	void Pop();

	//! 先頭のデータを取得する。
	const DataType& Top() const;

	//! 配列を空にする。
	void Clear();

	//! 比較オブジェクトを取得する。
	Compare& GetEvaluator();

private:
	//! Up-Heap操作を行う。
	void UpHeap(size_t childIndex);

	//! Down-Heap操作を行う。
	void DownHeap(size_t parentIndex);

	std::vector<DataType> m_data; //!< データを保持する配列。

	Compare evaluator; //!< データの更新で使われる比較オブジェクト。
};

} // namespace segmentation
} // namespace lssplib

#include "BinaryHeap.inl"
