#pragma once

#include "BaseContainers.h"
#include <stack>

namespace lssplib
{
namespace segmentation
{

//! 塗りつぶし処理(Scan Line Fill)
class ScanLineFill
{
public:
	ScanLineFill(void);
	virtual ~ScanLineFill(void);

//! @brief 塗りつぶし処理を実行する
void Fill(
					const Point3i& seedPt,
					unsigned char emptyValue,
					unsigned char fillValue,
					const Size3i& imageSize,
					unsigned char* imageData);

private:
	//! @brief 指定した位置のピクセルを塗りつぶし、スタックに積む。
	void UpdatePixel(const Point3i& pt);

	//! @brief 指定ピクセルの周辺の更新
	void MarkAdjacentPixels(const Point3i& srcPoint);

	//! @brief 基点から(+/-)x方向にdeltaX分先のピクセルのYとZで隣接するピクセルの更新処理
	void CheckDiagonalAndMarkAdjacentPixels(const Point3i& srcPoint, int deltaX);

	//! @brief ピクセルの位置情報(x,y,z)を画像データ配列の要素番号に変換する
	int GetIndex(const Point3i& pt) const;

	unsigned char m_emptyValue; //!< データ内で塗りつぶされる対象の値

	unsigned char m_fillValue; //!< 塗りつぶし後の値

	Size3i m_imageSize; //!< 画像データの大きさ

	unsigned char* m_imageData; //!< 画像データバッファ

	//! @brief 処理する位置のスタック
	std::stack<Point3i> m_queue;
};

} // namespace segmentation
} // namespace lssplib

