#pragma once

#include "itkImage.h"
#include "../BaseContainers.h"
#include "../lssplib.h"

namespace lssplib
{
namespace segmentation
{

/**
 * @brief @brief Çó\¢æðÛ·éIuWFNg
 */
class VesselThicknessInfo
{
public:
	typedef itk::Image<float, 3> ThicknessImageType; //!< Çó\¢æf[^^

	LSSPLIB_API VesselThicknessInfo();

	LSSPLIB_API ~VesselThicknessInfo();

	LSSPLIB_API void SetOrigin(const Point3i& pt);

	LSSPLIB_API void SetIntensityRange(int minIntensity, int maxIntensity);

	LSSPLIB_API void SetLiverId(int liverMask);

	LSSPLIB_API void SetIVCId(int ivcMaskId);

	LSSPLIB_API ThicknessImageType* GetImagePointer();

	LSSPLIB_API const ThicknessImageType* GetImagePointer() const;

	LSSPLIB_API const Point3i& GetOrigin() const;

	LSSPLIB_API int GetMinIntensity() const;

	LSSPLIB_API int GetMaxIntensity() const;

	LSSPLIB_API int GetLiverId() const;

	LSSPLIB_API int GetIVCId() const;

	LSSPLIB_API void Reset();

	LSSPLIB_API bool IsEmpty() const;

private:
	ThicknessImageType::Pointer m_vesselThickness; //!< Çó\¢æ

	Point3i m_origin; //!<Çó\¢æf[^Ì´_

	int m_minIntensity; //!< PxºÀ(ÇPx­²p)

	int m_maxIntensity; //!< PxãÀ(ÇPx­²p)

	int m_liverId; //!< Ì}XNID

	int m_IVCId; //!< IVC}XNID
};

} // namespace segmentation
} // namespace lssplib

