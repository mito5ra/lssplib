#pragma once

namespace lssplib
{
namespace segmentation
{

//-------------------------------------------------------------------
//--------- Graph::Node
//-------------------------------------------------------------------
inline void Graph::Node::SetFirstEdge(Edge* e) { m_edge = e; }

inline Graph::Edge* Graph::Node::FirstEdge() { return m_edge; }

inline void Graph::Node::SetGroup(NodeGroup nodeGroup) { m_nodeGroup = nodeGroup; }

inline Graph::Node::NodeGroup Graph::Node::Group() const { return m_nodeGroup; }

inline void Graph::Node::SetEdgeToParent(Edge* edgeToParent) { m_edgeToParent = edgeToParent; }

inline Graph::Edge* Graph::Node::EdgeToParent() { return m_edgeToParent; }


//-------------------------------------------------------------------
//--------- Graph::Edge
//-------------------------------------------------------------------
inline Graph::Node* Graph::Edge::From() { return m_nodeFrom; }

inline Graph::Node* Graph::Edge::To() { return m_nodeTo; }

inline Graph::Edge* Graph::Edge::Reverse() { return m_edgeReverse; }

inline void Graph::Edge::SetReverse(Edge* revEdge) { m_edgeReverse = revEdge; }

inline Graph::Edge* Graph::Edge::Next() { return m_edgeNext; }

inline void Graph::Edge::SetNext(Edge* nextEdge) { m_edgeNext = nextEdge; }

inline const double& Graph::Edge::Capacity() const { return m_capacity; }

inline double& Graph::Edge::Capacity() { return m_capacity; }

inline void Graph::Edge::SetCapacity(double capacity) { m_capacity = capacity; }

//-------------------------------------------------------------------
//--------- Graph
//-------------------------------------------------------------------
inline const Graph::Node& Graph::GetNode(size_t idx) const { return m_nodeList[idx]; }

inline Graph::Node& Graph::GetNode(size_t idx) { return m_nodeList[idx]; }

inline const Graph::Node& Graph::SourceNode() const { return m_nodeList[m_sourceIdx]; }

inline Graph::Node& Graph::SourceNode() { return m_nodeList[m_sourceIdx]; }

inline const Graph::Node& Graph::SinkNode() const { return m_nodeList[m_sinkIdx]; }

inline Graph::Node& Graph::SinkNode() { return m_nodeList[m_sinkIdx]; }

inline size_t Graph::GetNumNodes() const { return m_nodeList.size(); }

inline size_t Graph::GetNumEdges() const { return m_edgeList.size(); }

inline const Graph::NodeList& Graph::Nodes() const { return m_nodeList; }

inline const Graph::EdgeList& Graph::Edges() const { return m_edgeList; }

inline Graph::EdgeList& Graph::Edges() { return m_edgeList; }

} // namespace segmentation
} // namespace lssplib

