#pragma once 

#include "BinaryHeap.h"

namespace lssplib
{
namespace segmentation
{

//! @brief Fast Marching処理を行うクラス。
//! @tparam DataType 内部のデータ型
//! @tparam NDim 次元
template<typename DataType, unsigned int NDim>
class FastMarching
{
public:

	//! Fast Marchingで処理するピクセルの状態を表す。
	enum PixelState
	{
		kFarAway, //!< 未到達のピクセル
		kNarrowBand, //!< 到達時刻を処理中のピクセル
		kAccepted, //!< 到達時刻が確定したピクセル
	};

	//! 現在位置から処理する方向を示す。
	enum DirectionType
	{
		kXDir = 0, //!< X方向
		kYDir = 1, //!< Y方向
		kZDir = 2, //!< Z方向
	};

	//! @brief 内部で使用する固定長の配列。
	//! @tparam _ValueType データ型
	template<typename _ValueType>
	struct InternalFixedArray
	{
		_ValueType elements[NDim]; //!< データ配列

		//! デフォルトコンストラクタ
		InternalFixedArray();

		//! 初期値を指定したコンストラクタ
		InternalFixedArray(const _ValueType& _x, const _ValueType& _y);

		//! 初期値を指定したコンストラクタ
		InternalFixedArray(const _ValueType& _x, const _ValueType& _y, const _ValueType& _z);

		//! 指定した要素番号のデータを返す。
		_ValueType& operator[](int n);

		//! 指定した要素番号のデータを返す。
		const _ValueType& operator[](int n) const;
	};

	typedef InternalFixedArray<DataType> Spacing; //!< ピクセル間隔を表すデータ型

	typedef InternalFixedArray<int> Size; //!< 大きさを表すデータ型

	typedef InternalFixedArray<int> Point; //!< 位置を表すデータ型

	typedef std::vector< Point > SeedPoints; //!< シード点群を表すデータ型

	typedef DataType PixelValueType; //!< ピクセルのデータ型

	typedef std::vector<PixelValueType> TimeMapType; //!< 時刻データ配列型

	typedef std::vector<PixelState> PixelStateImageType; //!< ピクセル状態配列型

	FastMarching();

	~FastMarching() {}

	//! 内部データをクリアする。
	void Clear();

	//! シード点を追加する。
	void AddSeed(const Point& pt);

	//!バッファの大きさと速度画像を登録する。
	void SetSpeedImage(const Size& imgSize, const DataType* speedImage);

	//! バッファの大きさと伝搬速度を設定する。
	void SetConstantSpeed(const Size& imgSize, DataType speed);

	//! 処理を終了させる時刻を登録する。
	void SetStoppingTime(DataType stopTime);

	//! 処理を終了させる画像上の位置を定める。
	void SetStopPoint(const Point& pt);

	//! 処理を終了させる画像上の位置を削除する。
	void UnsetStopPoint();

	//! バッファのピクセル間隔を登録する。
	void SetSpacing(const Spacing& spacing);

	//! 生成された伝搬時刻のデータを取得する。
	const TimeMapType& GetTimeMap() const;

	//! Fast Marching処理を実行する。
	void Execute();

	//! 時刻計算に2nd orderの近似も使う。
	void EnableSecondOrder();

	//! 時刻計算に2nd orderの近似は使わない。
	void DisableSecondOrder();

private:
	//! 2分ヒープで使う比較関数オブジェクト
	struct BinaryHeapEvaluator
	{
		Size size; //!< 到達時刻データのピクセルサイズ

		const TimeMapType* timeMap; //!< 到達時刻データのポインタ

		//! 
		bool operator()(const Point& pt0, const Point& pt1) const;

		//! 与えられた画像上の位置から、到達時刻データ配列の要素番号を取得する。
		size_t GetLinearIndex(const Point& pt) const;
	};

	//! 初期化
	void Initialize();

	//! 終了処理
	void Finalize();

	//! 指定した位置とその周辺の状態を更新する。
	void Update(const Point& pt);

	//! 指定した位置の周辺の状態を更新する。
	void UpdateNeighbors(const Point& pt);

	//! 指定した位置の状態を更新する。
	void UpdateSingleNeighbor(const Point& neighbor);

	//! 指定した位置の到達時刻を算出する。
	DataType ComputeArrivalTime(const Point& pt);

	//! 指定した位置の状態を変更する。
	void SetLabelAt(const Point& pt, PixelState state);

	//! 指定した位置の状態を取得する。
	PixelState GetLabelAt(const Point& pt) const;

	//! 指定した位置に到達時刻を与える。
	void SetArrivalTimeAt(const Point& pt, DataType arrivalTime);

	//! 指定した位置の到達時刻を取得する。
	DataType GetArrivalTimeAt(const Point& pt) const;

	//! 指定した位置の速度を取得する。
	DataType GetSpeedAt(const Point& pt) const;

	//! 1次近似の係数
	void ComputeQuadraticCoefficients1stOrder(
		DirectionType dirType,
		const DataType& value,
		DataType& a, DataType& b, DataType& c);

	//! 2次近似の係数
	void ComputeQuadraticCoefficients2ndOrder(
		DirectionType dirType,
		const DataType& value0,
		const DataType& value1,
		DataType& a, DataType& b, DataType& c);

	//! 指定した位置から内部配列の要素番号を計算する。
	int GetLinearIndex(const Point& pt) const;

	//! 指定した位置が画像の範囲内なら真を返す。
	bool IsPointInRange(const Point& pt) const;

	Size m_size; //!< 画像サイズ

	const DataType* m_speed; //!< 速度画像

	DataType m_constantSpeed; //!< 伝搬速度

	DataType m_stopTime; //!< Fast Marching処理を終了する時刻

	Point m_stopPoint; //!< Fast Marching処理を終了する位置

	TimeMapType m_timeMap; //!< 到達時刻マップ

	PixelStateImageType m_stateImage; //!< ピクセル状態配列

	SeedPoints m_seeds; //!< シード点群

	Spacing m_spacing; //!< 画像のピクセルサイズ(ミリメートル)

	Spacing m_invSquaredSpacing; //!< 画像のピクセルサイズの逆2乗

	BinaryHeap<Point, BinaryHeapEvaluator> m_heap; //!< 2分ヒープ。配列先頭のデータから処理される。

	bool m_hasStopPoint; //!< 終了位置が設定されているか示すフラグ。

	bool m_secondOrderEnabled; //!< 2次近似まで行うか示すフラグ。
};

} // namespace segmentation
} // namespace lssplib

#include "FastMarching.inl"
