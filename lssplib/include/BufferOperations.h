#pragma once

#include "BaseContainers.h"
#include "lssplib.h"
#include <vector>

namespace lssplib
{
namespace polygonization
{

//! @brief イメージバッファに対する処理をまとめた静的関数を持つクラス。モデル生成用。
class BufferOperations
{
private:
	//! @brief 標準コンストラクタ
	BufferOperations();

	//! @brief デストラクタ
	~BufferOperations();

public:
	LSSPLIB_API static void ClassifyMassesIntoExposedAndNonExposed(const unsigned char* baseBuffer, const Size3i& bufferDim, const std::vector<unsigned char*>& testedBuffers, std::vector<unsigned char*>& exposedBuffer, std::vector<unsigned char*>& nonExposedBuffer);

	LSSPLIB_API static int CopyConnectingVol(
		const int seed, 
		const unsigned char* input, 
		const Size3i& bufferDim, 
		unsigned char*& copy);

	LSSPLIB_API static void ExpandBuffer(
		const unsigned char* input, 
		const Size3i& bufferDim, 
		int expansionN,
		unsigned char*& expanded);

	LSSPLIB_API static bool IsOneBufferIncludedInOtherBuffer(const unsigned char* bufOne, const unsigned char* bufOther, const Size3i& bufferDim);

	LSSPLIB_API static void DivideBufferIntoMasses(const unsigned char* inputBuffer, const Size3i& bufferDim, std::vector<unsigned char*>& massOfBuffers);

	LSSPLIB_API static void FillBufferHoles(unsigned char* buffer, const Size3i& bufferDim);

	LSSPLIB_API static bool ObtainBoundingBoxOfObject(const unsigned char* buffer, const Size3i& bufferDim, Point3i& objectBoundMin, Point3i& objectBoundMax);

	LSSPLIB_API static void SubtractOneFromAnother(
		const Size3i& bufferDim, 
		const unsigned char* one, 
		unsigned char* another);

	LSSPLIB_API static void SubtractOneBufferFromOtherBuffer(const unsigned char* bufferOne, unsigned char* bufferOther, const Size3i& bufferDim);

private:
	static void ResetToInputBuffer(unsigned char*& buffer, const Size3i& bufferDim);

	static void ComputeIndicesOf26Neighbours(const int voxelIndex, std::vector<int>& twentySixNeighbours, const Size3i& bufferDim);

	static void ComputeIndicesOf4NeighboursInXY(const int voxelIndex, std::vector<int>& fourNeighbours, const Size3i& bufferDim);

	static void ComputeIndicesOf4NeighboursInYZ(const int voxelIndex, std::vector<int>& fourNeighbours, const Size3i& bufferDim);

	static void ComputeIndicesOf4NeighboursInZX(const int voxelIndex, std::vector<int>& fourNeighbours, const Size3i& bufferDim);

	static void FillHoleInXYPlane(const int sliceIndex_Z, unsigned char* buffer, const Size3i& bufferDim, const Point3i& objectBoundMin, const Point3i& objectBoundMax);

	static void FillHoleInYZPlane(const int sliceIndex_X, unsigned char* buffer, const Size3i& bufferDim, const Point3i& objectBoundMin, const Point3i& objectBoundMax);

	static void FillHoleInZXPlane(const int sliceIndex_Y, unsigned char* buffer, const Size3i& bufferDim, const Point3i& objectBoundMin, const Point3i& objectBoundMax);

	static void FillOutsideBufferInXYPlane(const int sliceIndex, unsigned char* buffer, const Size3i& bufferDim);

	static void FillOutsideBufferInYZPlane(const int sliceIndex_X, unsigned char* buffer, const Size3i& bufferDim);

	static void FillOutsideBufferInZXPlane(const int sliceIndex_X, unsigned char* buffer, const Size3i& bufferDim);

	static const bool FindStartIndex_XY(const int sliceIndex_Z, unsigned char* buffer, const Size3i& bufferDim, int& startIndex);

	static const bool FindStartIndex_YZ(const int sliceIndex_X, unsigned char* buffer, const Size3i& bufferDim, int& startIndex);

	static const bool FindStartIndex_ZX(const int sliceIndex_Y, unsigned char* buffer, const Size3i& bufferDim, int& startIndex);

	static void CreateAMassOfBuffer(const int voxelIndex, const unsigned char* inputBuffer, const Size3i& bufferDim, std::vector<unsigned char*>& massOfBuffers);

	static  bool IsThereAnyBufferThatTakesTrueAtTheIndex(const int targetIndex, const std::vector<unsigned char*>& buffersToBeTested);
};

} // namespace polygonization
} // namespace lssplib
