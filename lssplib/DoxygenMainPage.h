﻿//! @mainpage
//! @version 1.0.1
//! @author 株式会社レキシー
//! @date 2015/1/16
//! @copyright (C) 2012-2015 LEXI Co, Ltd. All rights reserved.
//!
//! @section overview 概要
//! LiverSim Segementation and Polygonization (LSSP) Libaryは、
//! 医用画像データから肝臓及び膵臓の抽出処理及び3Dモデル生成を行うためのC++画像処理ライブラリである。
//!
//! @section 使用する外部ライブラリ
//! 以下の外部ライブラリ(EIGEN及びITK)はLGPL2.1に基づいて使用される。
//! ライセンスの内容は下記URLを参照。\n
//! http://www.gnu.org/licenses/lgpl-2.1.html
//!
//! @subsection eigen EIGEN
//! @arg 汎用行列処理ライブラリ
//! @arg http://eigen.tuxfamily.org/
//! @arg Eigenのフォルダ構成
//! @dot
//! digraph eigen {
//!   graph [size=5.0, rankdir = "LR", margin="0.1, 0.1"];
//!   root [label="Eigen\nroot", shape=folder, fontname=Helvetica, fontsize=10];
//!   include [shape=folder, fontname=Helvetica, fontsize=10];
//!   Eigen [shape=folder, fontname=Helvetica, fontsize=10];
//!   EigenU [label="Eigen", shape=folder, fontname=Helvetica, fontsize=10];
//!   Unsupported [shape=folder, fontname=Helvetica, fontsize=10];
//!   header [label="*.h", shape=box, fontname=Helvetica, fontsize=10];
//!   headerU [label="*.h", shape=box, fontname=Helvetica, fontsize=10];
//!   root -> include;
//!   include -> Eigen -> header;
//!   include -> Unsupported -> EigenU -> headerU;
//! }
//! @enddot
//!
//!
//! @subsection itk Insight Toolkit (ITK)
//! @arg 医用画像の処理ライブラリ (ITK4.4を使用)
//! @arg http://itk.org
//! @arg ITKのフォルダ構成(Relase/Debug及びx86/x64毎に用意する)。
//! @dot
//! digraph itk {
//! graph [size=5.0, rankdir = "LR", margin="0.1, 0.1"];
//! root [label="ITK\nroot", shape=folder, fontname=Helvetica, fontsize=10];
//! include [shape=folder, fontname=Helvetica, fontsize=10];
//! include4d4 [label="ITK-4.4", shape=folder, fontname=Helvetica, fontsize=10];
//! lib [shape=folder, fontname=Helvetica, fontsize=10];
//! bin [shape=folder, fontname=Helvetica, fontsize=10];
//! libfiles [label="*.lib", shape=box, fontname=Helvetica, fontsize=10];
//! dllfiles [label="*.dll", shape=box, fontname=Helvetica, fontsize=10];
//! header [label="*.h", shape=box, fontname=Helvetica, fontsize=10];
//! root -> include -> include4d4 -> header;
//! root -> lib;
//! root -> bin;
//! lib -> libfiles;
//! bin -> dllfiles;
//! }
//! @enddot
//!
//!
//! @section 変更履歴
//!
//! @subsection version1d1 バージョン1.0.1
//! @arg 2015/1/16 static library生成及び各種バグ修正
//!
//! @subsection version1d0 バージョン1.0
//! @arg 2014/3/31 最初のバージョン
//!

//! @cond
#define DOXYGEN_MAIN_PAGE_INCLUDED
//! @endcond
